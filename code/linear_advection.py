"""Linear advection problem.
"""

import os
import numpy as np
from numpy import sin, cos, pi
import matplotlib.pyplot as plt

from compyle.api import declare
from pysph.base.nnps import DomainManager
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.sph.scheme import add_bool_argument
from pysph.solver.application import Application
from pysph.solver.utils import iter_output

from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio
from edac import AdaptiveEDACScheme
from corrections import GradientCorrection


layers = 3
L = 10


def linf_error(approx, exact):
    approx_max = np.max(approx)
    exact_max = np.max(exact)
    return abs(approx_max - exact_max) / exact_max


def l1_error(approx, exact):
    l1_err = np.average(np.abs(approx - exact))
    exact_max = np.max(exact)
    return l1_err / exact_max


def func_exact(x, y):
    tmp = 2*pi/L
    return sin(tmp*x)*cos(tmp*y)


def grad_exact(x, y):
    tmp = 2*pi/L
    return [tmp * cos(tmp*x) * cos(tmp*y), -tmp * sin(tmp*x) * sin(tmp*y)]


class FuncApproximationStd(Equation):
    def initialize(self, d_f_approx, d_idx):
        d_f_approx[d_idx] = 0

    def loop(self, d_idx, s_idx, d_f_approx, s_f, s_m, s_rho, WI, d_Linv, DWI):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]

        Linv = declare('matrix(4)')
        i = declare('int')
        for i in range(4):
            Linv[i] = d_Linv[d_idx*16 + i]
        WI = Linv[0]*WI + Linv[1]*DWI[0] + Linv[2]*DWI[1] + Linv[3]*DWI[2]

        d_f_approx[d_idx] += mj/rhoj * s_f[s_idx] * WI


class GradApproximationStd(Equation):
    def initialize(self, d_gradx, d_grady, d_idx):
        d_gradx[d_idx] = 0
        d_grady[d_idx] = 0

    def loop(self, d_idx, s_idx, d_gradx, d_grady, s_f, s_m, s_rho, d_f, DWI):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        fac = mj/rhoj * (s_f[s_idx] - d_f[d_idx])

        d_gradx[d_idx] += fac * DWI[0]
        d_grady[d_idx] += fac * DWI[1]


class Advection(Application):
    def initialize(self):
        self.rho0 = 1.0
        self.L = L
        self.u = u = 1.0
        self.v = v = 1.0
        self.U = np.sqrt(u*u + v*v)
        self.c0 = 10 * (self.U + 1)
        self.layers = layers
        self.dim = 2
        self._nnps = None

    def add_user_options(self, group):
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        group.add_argument(
            "--perturb", action="store", type=float, dest="perturb", default=0,
            help="Random perturbation of initial particles as a fraction "
            "of dx (setting it to zero disables it, the default)."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=50,
            help="Number of points along x direction. (default 50)"
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.0,
            help="Ratio h/dx."
        )
        group.add_argument(
            '--adapt-freq', action='store', type=int, dest='freq', default=1,
            help='No of times to run adaptive resolution.'
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting"
        )
        group.add_argument(
            '--shift', action='store', dest='shift', type=int,
            help='No of times to Shift the particles.'
        )

    def create_domain(self):
        return DomainManager(
            xmin=-self.L, xmax=self.L, ymin=-self.L, ymax=self.L,
            periodic_in_x=True, periodic_in_y=True, n_layers=3.0
        )

    def consume_user_options(self):
        nx = self.options.nx
        self.dx = dx = self.L / nx
        self.dx_min = self.dx/np.sqrt(2)**self.layers
        self.volume = dx * dx
        self.hdx = self.options.hdx

        self.h0 = self.hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        self.dt = cfl * self.h0 / (self.U+self.c0)

        self.tf = 2.0
        self.kernel_correction = False
        self.vacondio = self.options.vacondio

    def create_nnps(self):
        if self._nnps is None:
            domain = self.create_domain()
            from pysph.base.nnps import OctreeNNPS as NNPS
            self._nnps = NNPS(dim=2.0, particles=self.particles,
                              radius_scale=3.0, cache=True, domain=domain)
        return self._nnps

    def create_particles(self):
        # create the particles
        dx = self.dx
        _x = np.arange(-self.L+dx/2, self.L, dx)
        x, y = np.meshgrid(_x, _x)

        if self.options.perturb > 0:
            np.random.seed(1)
            factor = dx * self.options.perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor

        m = self.volume * self.rho0
        h = self.hdx * dx
        p = 0.0

        fluid = get_particle_array(
            name='fluid', x=x, y=y, m=m, h=h, u=self.u, v=self.v,
            rho=self.rho0, p=p
        )
        self.scheme.setup_properties([fluid])

        props = [
            'dhdx', 'f', 'gradx', 'grady', 'f_approx', 'gradx_exact', 'grady_exact'
        ]

        for pa in [fluid]:
            for prop in props:
                pa.add_property(prop)
            pa.add_output_arrays(props)

        fluid.f[:] = func_exact(fluid.x, fluid.y)
        fluid.gradx_exact[:], fluid.grady_exact[:] = grad_exact(fluid.x, fluid.y)
        return [fluid]

    def configure_scheme(self):
        scheme = self.scheme
        pfreq = 10
        scheme.configure(h=self.h0, nu=0.0, cfl=self.cfl)
        scheme.configure_solver(
            tf=self.tf, dt=self.dt, pfreq=pfreq,
            output_at_times=[0.2, 0.4, 0.8, 1.0]
        )

    def create_scheme(self):
        domain = self.create_domain()
        edac = AdaptiveEDACScheme(
            ['fluid'], [], dim=2, rho0=self.rho0, c0=self.c0,
            nu=None, h=None, cfl=None, has_ghosts=True,
            domain=domain
        )
        return edac

    def create_equations(self):
        equations = self.scheme.get_equations()
        eqns = []
        eqns.append(Group(
            equations=[
                GradientCorrection('fluid', ['fluid']),
                FuncApproximationStd('fluid', ['fluid'])
            ]
        ))
        eqns.append(Group(
            equations=[
                GradientCorrection('fluid', ['fluid']),
                GradApproximationStd('fluid', ['fluid'])
            ]
        ))
        # equations.extend(eqns)
        return equations

    def pre_step(self, solver):
        fluid = self.particles[0]
        dx = self.dx
        m0 = self.rho0*dx*dx
        x = fluid.x
        y = fluid.y
        fluid.m_max[:] = m0*1.05
        fluid.m_min[:] = m0*0.4
        n = self.layers
        for i in range(n):
            region = (np.abs(x) < L - i*L/n) & (np.abs(y) < L - i*L/n)
            fluid.m_max[region] = m0*0.6**i
            fluid.m_min[region] = m0*0.0

    def create_tools(self):
        arrays = self.particles
        if self.vacondio:
            t = AdaptiveResolutionVacondio(
                self.particles[0], dim=2, has_ghost=True,
                do_mass_exchange=False, hybrid=True, rho0=self.rho0
            )
        else:
            t = AdaptiveResolution(
                self.particles[0], dim=2, has_ghost=True,
                do_mass_exchange=False, rho0=self.rho0
            )
        t._orig_nnps = self.create_nnps()

        smoothing_sph_eval = t.setup_smoothing(
            dest='fluid', arrays=arrays, nnps=self.nnps,
            dim=self.dim, rho0=self.rho0
        )

        for i in range(6):
            print("t.pre_step", i, end='\r')
            self.pre_step(None)
            t.pre_step(solver=self.solver)
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)

        for i in range(50):
            print("Smoothing", i, end='\r')
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)
        print()
        return [t]

    def post_process(self, info_filename):
        files = self.output_files[1:]
        t, nbr_max, rho_linf, rho_l1 = [], [], [], []
        func_linf, func_l1 = [], []
        grad_linf, grad_l1 = [], []
        for sd, array in iter_output(files, 'fluid'):
            _t = sd['t']
            t.append(_t)

            linf = linf_error(array.rho, self.rho0)
            rho_linf = np.append(rho_linf, [linf])
            l1 = l1_error(array.rho, self.rho0)
            rho_l1 = np.append(rho_l1, [l1])

            nbr_max = np.append(nbr_max, [array.n_nbrs.max()])

            f_exact = array.f
            linf = linf_error(array.f_approx, f_exact)
            func_linf = np.append(func_linf, [linf])
            l1 = l1_error(array.f_approx, f_exact)
            func_l1 = np.append(func_l1, [l1])

            gradx_exact, _ = grad_exact(array.x, array.y)
            linf = linf_error(array.gradx, gradx_exact)
            grad_linf = np.append(grad_linf, [linf])
            l1 = l1_error(array.gradx, gradx_exact)
            grad_l1 = np.append(grad_l1, [l1])

        fig, ax = plt.subplots(1, 3, figsize=(12, 4))

        ax[0].set_xlabel('time (s)')
        ax[0].set_ylabel(r'rho $L_\infty$')
        ax[0].plot(t, rho_linf)
        ax[0].grid()
        ax[0].tick_params(axis='y')

        ax[1].set_xlabel('time (s)')
        ax[1].set_ylabel(r'Func $L_\infty$')
        ax[1].plot(t, func_linf)
        ax[1].grid()
        ax[1].tick_params(axis='y')

        ax[2].set_xlabel('time (s)')
        ax[2].set_ylabel(r'Grad $L_\infty$')
        ax[2].plot(t, grad_linf)
        ax[2].grid()
        ax[2].tick_params(axis='y')
        fig.suptitle(r'$L_\infty$ plots')

        fig.tight_layout()
        fname = os.path.join(self.output_dir, "linf.png")
        fig.savefig(fname)

        fig1, ax1 = plt.subplots(1, 3, figsize=(12, 4))

        ax1[0].set_xlabel('time (s)')
        ax1[0].set_ylabel(r'rho $L_1$')
        ax1[0].plot(t, rho_l1)
        ax1[0].grid()
        ax1[0].tick_params(axis='y')

        ax1[1].set_xlabel('time (s)')
        ax1[1].set_ylabel(r'Func $L_1$')
        ax1[1].plot(t, func_l1)
        ax1[1].grid()
        ax1[1].tick_params(axis='y')

        ax1[2].set_xlabel('time (s)')
        ax1[2].set_ylabel(r'Grad $L_1$')
        ax1[2].plot(t, grad_l1)
        ax1[2].grid()
        ax1[2].tick_params(axis='y')
        fig1.suptitle(r'$L_1$ plots')

        fig1.tight_layout()
        fname = os.path.join(self.output_dir, "l1.png")
        fig1.savefig(fname)
        plt.show()

    def customize_output(self):
        self._mayavi_config('''
        for name in ['fluid']:
            b = particle_arrays[name]
            b.scalar = 'vor'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
            b.point_size = 2.0
        ''')


if __name__ == '__main__':
    app = Advection()
    app.run()
    app.post_process(app.info_filename)
