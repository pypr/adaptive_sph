import numpy
from matplotlib import pyplot as plt
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator
import check_bg_mesh as CBG


class UpdateDensity(Equation):
    def __init__(self, dest, sources, dim, rho0, tolerance):
        super(UpdateDensity, self).__init__(dest, sources)
        self.dim = dim
        self.tolerance = tolerance
        self.rho0 = rho0
        self.conv = -1
        self.iters = 0

    def initialize(self, d_tmp_rho, d_idx, d_alpha):
        d_tmp_rho[d_idx] = 0.0
        d_alpha[d_idx] = 0.0

    def loop(self, d_idx, d_rho, s_m, s_idx, d_dRes, d_h, d_alpha,
             d_tmp_rho, XIJ, RIJ, SPH_KERNEL, HIJ):
        hi = d_h[d_idx]
        dwdq = SPH_KERNEL.dwdq(RIJ, hi)
        wij = SPH_KERNEL.kernel(XIJ, RIJ, hi)

        d_alpha[d_idx] -= s_m[s_idx] * RIJ * dwdq / hi
        d_tmp_rho[d_idx] += s_m[s_idx]*wij

    def post_loop(self, d_idx, d_rho, d_tmp_rho, d_alpha,
                  d_rhodiff, d_h, d_h0):
        Res = d_rho[d_idx] - d_tmp_rho[d_idx]
        fac = Res*self.dim

        # d_rho[d_idx] = d_rho[d_idx] * (1.0 - fac/(fac + d_alpha[d_idx]))
        d_rho[d_idx] = d_tmp_rho[d_idx]
        rhodiff = self.rho0 - d_rho[d_idx]
        d_rhodiff[d_idx] = abs(rhodiff)

        h_old = d_h[d_idx]
        # d_h[d_idx] = d_h0[d_idx] * (self.rho0/d_rho[d_idx])**(1.0/self.dim)
        d_h[d_idx] = h_old * (1.0 - rhodiff/(d_rho[d_idx] * self.dim))
        d_h[d_idx] = min(max(d_h[d_idx], 0.05), 0.4)

    def reduce(self, dst, t, dt):
        err = dst.rhodiff.mean()
        dst.iters[0] = self.iters
        hdiff = numpy.mean(numpy.abs(dst.h0 - dst.h)/dst.h0)
        hmin = numpy.min(dst.h)
        print(self.iters, err, hdiff, hmin)
        if err < self.tolerance:
            self.iters = 0
            self.conv = 1
        else:
            self.iters += 1
            self.conv = -1

    def converged(self):
        return self.conv


if __name__ == '__main__':
    from pysph.base.kernels import QuinticSpline
    from pysph.solver.utils import dump
    cr = pow(2.0, 1/4.)
    f, s, bg, t = CBG.main(cr=cr)
    t.set_initial_bg()
    t.initialize_fluid()

    print("No of particles ", f.get_number_of_particles())

    for p in ['alpha', 'tmp_rho', 'dRes', 'rhodiff', 'h0']:
        f.add_property(p)
    rho0 = 1.0
    dim = 2
    f.h[:] = bg.ds_min[0]
    if 'iters' not in f.constants.keys():
        f.add_constant('iters', [0])

    eqs = UpdateDensity(
        dest='fluid', sources=['fluid'], dim=2, tolerance=1e-2, rho0=1
    )
    grp = Group(
        [eqs], iterate=True, max_iterations=200, min_iterations=2,
        update_nnps=True
    )
    sph_eval = SPHEvaluator(arrays=[f, s], dim=2, equations=[grp],
                            kernel=QuinticSpline(2))
    sph_eval.evaluate()
    dump('test_density_0.hdf5', [f, s], dict(t=0.0, dt=0.0, count=1))
    fig, ax = plt.subplots(2, 2, figsize=(8, 6))
    ax = ax.ravel()
    plt.jet()
    im0 = ax[0].scatter(f.x, f.y, c=f.rho, s=f.m*50)
    ax[0].set_title('rho')
    fig.colorbar(im0, ax=ax[0])
    ax[1].hist(f.h, bins=40)
    ax[1].set_title('h')
    s2 = ax[2].scatter(f.x, f.y, c=f.h, s=f.m*25)#, vmax=0.4)
    ax[2].set_title('h')
    fig.colorbar(s2, ax=ax[2])
    plt.savefig('rho.png')
    plt.show()
