---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
%matplotlib notebook
from matplotlib import pyplot as plt
import numpy as np
```

```python
from compyle.api import get_config
get_config().use_openmp = True
```

```python
import check_bg_mesh as CBG
```

### Testing

```python
f, s, bg, t = CBG.main(cr=1.2)
```

```python
for i in range(15): t.update_bg()
```

```python
t.set_initial_bg()
```

```python
ds_min = bg.ds_min[0]
cr = bg.cr[0]
plt.figure()
plt.jet()
plt.scatter(bg.x, bg.y, c=np.log(bg.ds/ds_min)/np.log(cr), s=5)
#plt.scatter(bg.x, bg.y, c=bg.h, s=5)
plt.colorbar();
plt.scatter(s.x, s.y, s=5);
```

```python
ds_min = bg.ds_min[0]
cr = bg.cr[0]
n = np.log(bg.ds_max[0]/ds_min)/np.log(cr)
print("number of layers", n)
print("total length", 3*(bg.ds_max[0]*cr - ds_min)/(cr - 1))
```

```python

```

### Simple case

```python
f, s, bg, t = CBG.main(cr=1.2)
```

```python
# Same as above.
#f = CBG.create_fluid(0.4)
#s = CBG.create_solid(0.05)
#t = CBG.UpdateBackground(
#    f, dim=2, boundary=[s], ds_min=0.05, ds_max=0.4, cr=1.2,
#    rho_ref=1.0
#)
# bg = t.bg_pa
```

```python
# The initial bg mesh.
plt.scatter(bg.x, bg.y, c=bg.fixed, s=5); plt.colorbar();
plt.scatter(s.x, s.y, s=2.5);
```

```python
t.set_initial_bg()
```

```python
plt.figure()
plt.scatter(bg.x, bg.y, c=bg.ds, s=5)
plt.colorbar();
plt.scatter(s.x, s.y, s=5);
```

```python
# The configuration is stable, as if we iterate further, things are fairly stable
for i in range(20): 
    t.update_bg()
```

```python
plt.figure()
plt.scatter(bg.x, bg.y, c=bg.ds, s=5)
plt.colorbar();
```

## Moving the solid body and updating should update the mesh

```python
for i in range(50):
    s.x[:] = s.x - 0.05
    t.update_bg()
```

```python
plt.figure()
plt.scatter(bg.x, bg.y, c=bg.ds, s=5)
plt.colorbar();
```

```python
for i in range(50):
    s.x[:] = s.x + 0.05
    t.update_bg()
```

```python
plt.figure()
plt.scatter(bg.x, bg.y, c=bg.ds, s=5)
plt.colorbar();
```

## Changing the cr

Using `cr = 1.4`

```python
f = CBG.create_fluid(0.4)
s = CBG.create_solid(0.05)
t = CBG.UpdateBackground(
    f, dim=2, boundary=[s], ds_min=0.05, ds_max=0.4, cr=1.4,
    rho_ref=1.0
)
bg = t.bg_pa
t.set_initial_bg()
```

```python
for i in range(5):
    t.update_bg()
```

```python
plt.figure()
plt.scatter(bg.x, bg.y, c=bg.ds, s=5)
plt.colorbar();
```

## Case with two bodies

```python
f = CBG.create_fluid(0.4)
s = CBG.create_solid(0.05)
s.x[:] = s.x - 2.0
s.y[:] = s.y - 1.0

s1 = CBG.create_solid(0.05)
s1.set_name('solid1')
s1.x[:] = s1.x + 2.0
s1.y[:] = s1.y + 1.0

t = CBG.UpdateBackground(
    f, dim=2, boundary=[s, s1], ds_min=0.05, ds_max=0.4, cr=1.2,
    rho_ref=1.0
)
bg = t.bg_pa
t.set_initial_bg()
```

```python
for i in range(15): t.update_bg()
```

```python
plt.figure()
plt.scatter(bg.x, bg.y, c=bg.ds, s=10)
plt.colorbar();
plt.scatter(s1.x, s1.y, s=5);
plt.scatter(s.x, s.y, s=5);
```

## Setting up the initial fluid particles

```python
f, s, bg, t = CBG.main(cr=1.2)
```

```python
t.set_initial_bg()
```

```python
plt.figure()
plt.jet()
plt.scatter(bg.x, bg.y, c=bg.ds, s=10)
plt.colorbar();
plt.scatter(s.x, s.y, s=10);

```

```python
t.initialize_fluid()
```

```python
plt.figure()
plt.jet()
plt.axis('equal')
plt.scatter(f.x, f.y, c=f.ds, s=5)
plt.colorbar();
#plt.scatter(bg.x, bg.y, c=bg.ds, s=20, alpha=0.5);
#plt.colorbar();
```

Try with a much lower resolution

```python
f, s, bg, t = CBG.main(cr=1.2, dx_min=0.0125)
```

```python
t.set_initial_bg()
```

```python
plt.figure()
plt.jet()
plt.scatter(bg.x, bg.y, c=bg.ds, s=5)
plt.colorbar();
#plt.scatter(s.x, s.y, s=5);
```

```python

```
