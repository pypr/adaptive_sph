"""This is the split and merge algorithm.

- Find all split and merge candidates.
- Merge all particles that need to be merged.
- Add enough new particles for the splits and delete merged ones.
- Assign the indices of the particles to split to.
- Actually split the particles.

In this work we do not use m_ref but instead use m_min and m_max to determine
if we must split or merge. This nicely allows us to accommodate both Yang and
Kong's approach as well as the work of Vacondio et al.

See the manuscript for more details.

split = 0: do nothing
split = 1: split the particle
split = 2: merge particle (retain this after merging)
split = -1: Delete the particle since it is merged with another.
split = 3: Possibly exchange mass with a suitable neighbor.

TODO:

- Test and optimize for GPU.

"""

import logging
from time import time
from math import sqrt, log
import numpy
import numpy as np
from compyle.api import annotate, declare, wrap, profile, profile_ctx
from compyle.api import get_profile_info
from compyle.parallel import Elementwise
from compyle.low_level import cast
from pysph.base.kernels import QuinticSpline
from pysph.base.particle_array import ParticleArray
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.tools.interpolator import get_bounding_box
from pysph.solver.tools import Tool
from pysph.tools import geometry as G

logger = logging.getLogger(__name__)


class UpdateSmoothingLength(Equation):
    def __init__(self, dest, sources, Nr):
        self.Nr = Nr
        super(UpdateSmoothingLength, self).__init__(dest, sources)

    def initialize(self, d_idx, d_n_nbrs, d_htmp):
        d_n_nbrs[d_idx] = 0
        d_htmp[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_h, d_n_nbrs, d_htmp):
        d_n_nbrs[d_idx] += 1
        d_htmp[d_idx] += s_h[s_idx]

    def post_loop(self, d_idx, d_h, d_htmp, d_n_nbrs):
        n_nbrs = declare('int')
        n_nbrs = d_n_nbrs[d_idx]
        if n_nbrs > 0:
            Ni_inv = 1.0/(n_nbrs)
            Nr_Ni = sqrt(self.Nr * Ni_inv)
            d_h[d_idx] = (0.25 * d_h[d_idx] * (1 + Nr_Ni)
                          + 0.5*Ni_inv*d_htmp[d_idx])


class SmoothPositionBGShift(Equation):
    """This is meant to be used together with update smoothing length for the
    background particles.

    """
    def __init__(self, dest, sources, dim=2):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_h, d_h, d_fixed, d_au, d_av,
             d_aw, DWIJ, WIJ, SPH_KERNEL, HIJ):
        if d_fixed[d_idx] & 2 == 0:
            hi = d_h[d_idx]
            hj = s_h[s_idx]
            Vj = hj**self.dim

            dp = SPH_KERNEL.get_deltap() * HIJ
            WDX = SPH_KERNEL.kernel([0.0, 0.0, 0.0], dp, HIJ)
            fac = 0.5 * hi * hi
            tmp = fac * Vj * (1 + 0.24 * (WIJ/WDX)**4)
            d_au[d_idx] -= tmp * DWIJ[0]
            d_av[d_idx] -= tmp * DWIJ[1]
            d_aw[d_idx] -= tmp * DWIJ[2]

    def post_loop(self, d_idx, d_au, d_av, d_aw, d_x, d_y, d_z, d_fixed):
        if d_fixed[d_idx] & 2 == 0:
            d_x[d_idx] += d_au[d_idx]
            d_y[d_idx] += d_av[d_idx]
            d_z[d_idx] += d_aw[d_idx]


class InitializeShift(Equation):
    def initialize(self, d_idx, d_x, d_y, d_z, d_x0, d_y0, d_z0):
        d_x0[d_idx] = d_x[d_idx]
        d_y0[d_idx] = d_y[d_idx]
        d_z0[d_idx] = d_z[d_idx]


class SmoothPositionShift(Equation):
    """This is meant to be used together with update smoothing length for the
    background particles.

    """
    def __init__(self, dest, sources, rho0, dim=2):
        self.dim = dim
        self.rho0 = rho0
        super().__init__(dest, sources)


    def initialize(self, d_idx, d_shift_x, d_shift_y, d_shift_z):
        d_shift_x[d_idx] = 0.0
        d_shift_y[d_idx] = 0.0
        d_shift_z[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_shift_x, d_shift_y, d_shift_z,
             d_h, DWIJ, WIJ, SPH_KERNEL, HIJ):
        mj = s_m[s_idx]
        Vj = mj/self.rho0
        hi = d_h[d_idx]

        dp = SPH_KERNEL.get_deltap() * HIJ
        WDX = SPH_KERNEL.kernel([0.0, 0.0, 0.0], dp, HIJ)
        fac = 0.5 * hi * hi
        tmp = fac * Vj * (1 + 0.24*(WIJ/WDX)**4)
        d_shift_x[d_idx] -= tmp * DWIJ[0]
        d_shift_y[d_idx] -= tmp * DWIJ[1]
        d_shift_z[d_idx] -= tmp * DWIJ[2]

    def post_loop(self, d_idx, d_shift_x, d_shift_y, d_shift_z, d_x, d_y, d_z,
                  d_h):
        dr = sqrt(d_shift_x[d_idx] * d_shift_x[d_idx] +
                  d_shift_y[d_idx] * d_shift_y[d_idx] +
                  d_shift_z[d_idx] * d_shift_z[d_idx])

        percent = 0.25
        r_max = 0.025*d_h[d_idx]
        r_allowed = percent * r_max

        if dr > r_allowed:
            fac = r_allowed / dr
        else:
            fac = 1.0

        d_shift_x[d_idx] *= fac
        d_shift_y[d_idx] *= fac
        d_shift_z[d_idx] *= fac

        d_x[d_idx] += d_shift_x[d_idx]
        d_y[d_idx] += d_shift_y[d_idx]
        d_z[d_idx] += d_shift_z[d_idx]


class CorrectProperties(Equation):
    def initialize(self, d_idx, d_gradv, d_gradp):
        i, j = declare('int', 2)
        for i in range(3):
            d_gradp[d_idx*3 + i] = 0.0
            for j in range(3):
                d_gradv[9*d_idx + 3*i + j] = 0.0

    def loop(self, d_idx, s_idx, s_m, s_rho, d_gradv, d_p, s_p, d_gradp, DWIJ,
             VIJ):
        i, j = declare('int', 2)

        Vj = s_m[s_idx] / s_rho[s_idx]
        pij = d_p[d_idx] - s_p[s_idx]

        for i in range(3):
            d_gradp[d_idx*3 + i] += -Vj * pij * DWIJ[i]
            for j in range(3):
                d_gradv[d_idx*9 + 3*i + j] += -Vj * VIJ[i] * DWIJ[j]

    def post_loop(self, d_idx, d_u, d_v, d_w, d_gradv, d_gradp, d_x0, d_y0,
                  d_z0, d_p, d_x, d_y, d_z):
        res, shift = declare('matrix(3)', 2)
        i, j = declare('int', 2)

        shift[0] = d_x[d_idx] - d_x0[d_idx]
        shift[1] = d_y[d_idx] - d_y0[d_idx]
        shift[2] = d_z[d_idx] - d_z0[d_idx]

        deltap = 0.0
        for i in range(3):
            tmp = 0.0
            deltap += d_gradp[d_idx*3 + i] * shift[i]
            for j in range(3):
                tmp += d_gradv[d_idx*9 + 3*i + j] * shift[j]
            res[i] = tmp

        d_p[d_idx] += deltap
        d_u[d_idx] += res[0]
        d_v[d_idx] += res[1]
        d_w[d_idx] += res[2]


class FindSplitMergeBase(Equation):
    def initialize(self, d_idx, d_split, d_closest_idx):
        d_split[d_idx] = 0
        d_closest_idx[d_idx] = -1

    def loop_all(self, d_idx, d_h, d_m, d_m_min, d_m_max, d_split,
                 d_closest_idx, d_x, s_x, d_y, s_y, d_z, s_z, s_m,
                 s_m_max, s_m_min, s_h, N_NBRS, NBRS):
        k, s_idx, idx = declare('int', 3)
        r2ij, rc, r2max, mi, m_merge, m_max, m_min = declare('double', 7)
        min_max = declare('double')
        idx = -1
        rc = 1e12
        mi = d_m[d_idx]
        m_max = d_m_max[d_idx]
        m_min = d_m_min[d_idx]
        # Split
        if mi > m_max:
            d_split[d_idx] = 1
            for k in range(N_NBRS):
                s_idx = NBRS[k]
                if s_idx == d_idx:
                    continue
                r2ij = ((d_x[d_idx] - s_x[s_idx])**2 +
                        (d_y[d_idx] - s_y[s_idx])**2 +
                        (d_z[d_idx] - s_z[s_idx])**2)
                if r2ij < rc:
                    rc = r2ij
                    idx = s_idx
            d_closest_idx[d_idx] = idx
        # Merge
        # elif mi < m_min:
        else:
            d_split[d_idx] = 2
            for k in range(N_NBRS):
                s_idx = NBRS[k]
                if s_idx == d_idx:
                    continue
                r2ij = ((d_x[d_idx] - s_x[s_idx])**2 +
                        (d_y[d_idx] - s_y[s_idx])**2 +
                        (d_z[d_idx] - s_z[s_idx])**2)

                m_merge = mi + s_m[s_idx]
                min_max = min(m_max, s_m_max[s_idx])
                r2max = (d_h[d_idx] + s_h[s_idx])*0.5
                r2max *= r2max
                if ((r2ij < rc) and (m_merge < min_max) and
                    r2ij < r2max):
                    rc = r2ij
                    idx = s_idx
            d_closest_idx[d_idx] = idx
            if idx == -1:
                # No nearest particle, so cannot merge.
                d_split[d_idx] = 0

    def post_loop(self, d_idx, d_closest_idx, d_split, d_m,
                  d_x, d_y, d_z, d_fixed):
        idx = declare('int')
        m1, m2, mt = declare('double', 3)
        idx = d_closest_idx[d_idx]
        # Perform any merging.
        if d_split[d_idx] == 2 and d_closest_idx[idx] == d_idx:
            if d_idx < idx:
                d_split[d_idx] = 0
                # Merge.
                m1 = d_m[d_idx]
                m2 = d_m[idx]
                d_m[d_idx] += m2
                mt = 1.0/(m1 + m2)
                m1 *= mt
                m2 *= mt
                d_x[d_idx] = d_x[d_idx]*m1 + d_x[idx]*m2
                d_y[d_idx] = d_y[d_idx]*m1 + d_y[idx]*m2
                d_z[d_idx] = d_z[d_idx]*m1 + d_z[idx]*m2
            else:
                # Mark for deletion.
                d_split[d_idx] = -1
        elif d_split[d_idx] == 2:
            # No matching merge pair.
            d_split[d_idx] = 0
        elif d_split[d_idx] == 1 and d_fixed[d_idx] & 2 == 2:
            # Don't split the periphery background particles
            d_split[d_idx] = 0


class FindSplitMerge(FindSplitMergeBase):
    def post_loop(self, d_idx, d_closest_idx, d_split, d_m,
                  d_x, d_y, d_z, d_u, d_v, d_w, d_uhat, d_vhat, d_what,
                  d_p, d_h, d_rho, d_au, d_av, d_aw, d_ap,
                  d_auhat, d_avhat, d_awhat):
        idx = declare('int')
        m1, m2, mt = declare('double', 3)
        idx = d_closest_idx[d_idx]
        # Perform any merging.
        if d_split[d_idx] == 2 and d_closest_idx[idx] == d_idx:
            if d_idx < idx:
                d_split[d_idx] = 0
                # Merge.
                m1 = d_m[d_idx]
                m2 = d_m[idx]
                d_m[d_idx] += m2
                # Find h based on the mass of the merged particle, tmp is
                # hdx^2. Here hdx is a fixed value. First we find the previous
                # hdx.
                tmp = (d_rho[d_idx] * d_h[d_idx]**2)/m1
                d_h[d_idx] = sqrt(tmp * d_m[d_idx]/d_rho[d_idx])
                mt = 1.0/(m1 + m2)
                m1 *= mt
                m2 *= mt
                d_x[d_idx] = d_x[d_idx]*m1 + d_x[idx]*m2
                d_y[d_idx] = d_y[d_idx]*m1 + d_y[idx]*m2
                d_z[d_idx] = d_z[d_idx]*m1 + d_z[idx]*m2
                d_u[d_idx] = d_u[d_idx]*m1 + d_u[idx]*m2
                d_v[d_idx] = d_v[d_idx]*m1 + d_v[idx]*m2
                d_w[d_idx] = d_w[d_idx]*m1 + d_w[idx]*m2
                d_uhat[d_idx] = d_uhat[d_idx]*m1 + d_uhat[idx]*m2
                d_vhat[d_idx] = d_vhat[d_idx]*m1 + d_vhat[idx]*m2
                d_what[d_idx] = d_what[d_idx]*m1 + d_what[idx]*m2
                d_p[d_idx] = d_p[d_idx]*m1 + d_p[idx]*m2
                d_au[d_idx] = d_au[d_idx]*m1 + d_au[idx]*m2
                d_av[d_idx] = d_av[d_idx]*m1 + d_av[idx]*m2
                d_aw[d_idx] = d_aw[d_idx]*m1 + d_aw[idx]*m2
                d_ap[d_idx] = d_ap[d_idx]*m1 + d_ap[idx]*m2
                d_auhat[d_idx] = d_auhat[d_idx]*m1 + d_auhat[idx]*m2
                d_avhat[d_idx] = d_avhat[d_idx]*m1 + d_avhat[idx]*m2
                d_awhat[d_idx] = d_awhat[d_idx]*m1 + d_awhat[idx]*m2
            else:
                # Mark for deletion.
                d_split[d_idx] = -1
        elif d_split[d_idx] == 2:
            # No matching merge pair.
            d_split[d_idx] = 0


class MassExchange(Equation):
    """This is to be used in a group after the FindSplitMerge is completed.
    This equation allows the masses to be merged.
    """
    def converged(self):
        return -1

    def loop_all(self, d_idx, d_m, d_m_max, d_split, d_closest_idx,
                 d_x, d_y, d_z, d_h, s_x, s_y, s_z, s_m, s_h, s_split, s_m_max,
                 N_NBRS, NBRS):
        k, s_idx, idx = declare('int', 3)
        r2ij, rc, mr, mi, m_merge, m_min, m_max = declare('double', 7)
        idx = -1
        rc = 1e12
        mi = d_m[d_idx]
        m_max = d_m_max[d_idx]
        mdiff = m_max - mi
        if d_split[d_idx] != -1 and (mi < m_max*0.95):
            for k in range(N_NBRS):
                s_idx = NBRS[k]
                h = (d_h[d_idx] + s_h[s_idx])*0.5
                r2ij = ((d_x[d_idx] - s_x[s_idx])**2 +
                        (d_y[d_idx] - s_y[s_idx])**2 +
                        (d_z[d_idx] - s_z[s_idx])**2)
                if (s_idx == d_idx or s_split[s_idx] == -1 or r2ij > h or
                    s_m[s_idx] > m_max*0.95 or s_m[s_idx] <= mdiff or
                    abs(s_m_max[s_idx] - m_max) > 1e-12):
                    continue
                if r2ij < rc:
                    rc = r2ij
                    idx = s_idx
            d_closest_idx[d_idx] = idx
            if idx != -1:
                d_split[d_idx] = 3

    def post_loop(self, d_idx, d_closest_idx, d_split, d_m, d_m_max,
                  d_x, d_y, d_z, d_u, d_v, d_w, d_uhat, d_vhat, d_what,
                  d_p, d_au, d_av, d_aw, d_auhat, d_avhat, d_awhat, d_ap):
        idx, do_merge = declare('int', 2)
        mi, mdiff, mj, morig = declare('double', 4)
        idx = d_closest_idx[d_idx]
        mdiff = d_m_max[d_idx] - d_m[d_idx]
        do_merge = 0
        # Perform any mass exchange.
        if ((idx > -1) and (d_split[d_idx] == 3) and
            (d_split[idx] == 3) and d_closest_idx[idx] == d_idx):
            if d_m[d_idx] > d_m[idx]:
                do_merge = 1
            elif d_m[d_idx] < d_m[idx]:
                do_merge = -1
            elif abs(d_m[d_idx] - d_m[idx]) < 1e-12:
                if d_idx < idx:
                    do_merge = 1
                else:
                    do_merge = -1
            if abs(d_m[idx] - mdiff) < 1e-12:
                # Here the mass absorbed is equal to mass of neighbor
                # We do not want it to have zero mass so do not exchange.
                do_merge = 0
        else:
            do_merge = 0

        if do_merge == 1:
            d_split[d_idx] = 0
            morig = d_m[d_idx]
            mi = morig + mdiff
            d_m[d_idx] = mi
            d_m[idx] -= mdiff
            d_split[d_idx] = 0
            d_split[idx] = 0

            # Only big one moves and changes props.
            mj = mdiff/d_m[d_idx]
            mi = morig/d_m[d_idx]
            d_x[d_idx] = mi*d_x[d_idx] + mj*d_x[idx]
            d_y[d_idx] = mi*d_y[d_idx] + mj*d_y[idx]
            d_z[d_idx] = mi*d_z[d_idx] + mj*d_z[idx]
            d_u[d_idx] = mi*d_u[d_idx] + mj*d_u[idx]
            d_v[d_idx] = mi*d_v[d_idx] + mj*d_v[idx]
            d_w[d_idx] = mi*d_w[d_idx] + mj*d_w[idx]
            d_uhat[d_idx] = mi*d_uhat[d_idx] + mj*d_uhat[idx]
            d_vhat[d_idx] = mi*d_vhat[d_idx] + mj*d_vhat[idx]
            d_what[d_idx] = mi*d_what[d_idx] + mj*d_what[idx]
            d_p[d_idx] = mi*d_p[d_idx] + mj*d_p[idx]

            d_au[d_idx] = mi*d_au[d_idx] + mj*d_au[idx]
            d_av[d_idx] = mi*d_av[d_idx] + mj*d_av[idx]
            d_aw[d_idx] = mi*d_aw[d_idx] + mj*d_aw[idx]
            d_auhat[d_idx] = mi*d_auhat[d_idx] + mj*d_auhat[idx]
            d_avhat[d_idx] = mi*d_avhat[d_idx] + mj*d_avhat[idx]
            d_awhat[d_idx] = mi*d_awhat[d_idx] + mj*d_awhat[idx]
            d_ap[d_idx] = mi*d_ap[d_idx] + mj*d_ap[idx]


class SkipNearBoundary(Equation):
    def loop_all(self, d_idx, d_h, d_split, d_m, d_rho, d_x,
                 s_x, d_y, s_y, d_z, s_z, N_NBRS, NBRS):
        k, s_idx = declare('int', 2)
        r2ij, min_r, ds = declare('double', 3)
        min_r = 1e12
        ds = 0.0
        if d_split[d_idx] == 1 and (N_NBRS > 0):
            for k in range(N_NBRS):
                s_idx = NBRS[k]
                r2ij = ((d_x[d_idx] - s_x[s_idx])**2 +
                        (d_y[d_idx] - s_y[s_idx])**2 +
                        (d_z[d_idx] - s_z[s_idx])**2)
                min_r = min(min_r, r2ij)
            # XXX: change for 3D
            ds = 0.6*sqrt(d_m[d_idx]/d_rho[d_idx])
            min_r = sqrt(min_r)
            if min_r < ds:
                # Don't split this particle.
                d_split[d_idx] = 0


# Note that fixed can be 0, 1 or 2.  We use bitmasks to identify the state.
# fixed = 0, normal particle.
# fixed = 1, boundary particle
# fixed = 2, fixed particle (i.e. on the periphery)

class MarkBoundary(Equation):
    def initialize(self, d_idx, d_fixed):
        d_fixed[d_idx] -= (d_fixed[d_idx] & 1)

    def loop_all(self, d_idx, d_ds, d_m_max, d_m_min, d_fixed,
                 d_rho, s_ds_min, N_NBRS):
        ds, mref = declare('double', 2)
        if N_NBRS > 0:
            if (d_fixed[d_idx] & 1) == 1:
                ds = min(s_ds_min[0], d_ds[d_idx])
            else:
                ds = s_ds_min[0]
            d_fixed[d_idx] |= 1
            d_ds[d_idx] = ds
            # XXX: Fix for 3D
            mref = d_rho[d_idx]*ds*ds
            d_m_max[d_idx] = 1.05*mref
            d_m_min[d_idx] = 0.5*mref


class UpdateSpacingOld(Equation):
    def initialize(self, d_idx, d_htmp):
        d_htmp[d_idx] = -1.0

    def loop_all(self, d_idx, d_ds, d_fixed, d_ds_min, d_h, d_ds_max, d_cr,
                 d_htmp, d_x, s_x, d_y, s_y, d_z, s_z, N_NBRS, NBRS):
        k, s_idx = declare('int', 2)
        ds, ds_max, r2ij = declare('double', 3)
        ds_max = d_ds_max[0]
        ds = ds_max
        if (d_fixed[d_idx] & 1) == 0:
            for k in range(N_NBRS):
                s_idx = NBRS[k]
                r2ij = ((d_x[d_idx] - s_x[s_idx])**2 +
                        (d_y[d_idx] - s_y[s_idx])**2 +
                        (d_z[d_idx] - s_z[s_idx])**2)
                if r2ij <= d_h[d_idx]*d_h[d_idx]:
                    ds = min(ds, d_ds[s_idx])
            if ds < ds_max:
                ds *= d_cr[0]
            if ds > ds_max:
                ds = ds_max
            d_htmp[d_idx] = ds

    def post_loop(self, d_idx, d_htmp, d_ds, d_rho, d_m_min, d_m_max):
        ds = d_htmp[d_idx]
        if ds > 0.0:
            d_ds[d_idx] = ds
            # XXX: Fix for 3D
            mref = d_rho[d_idx]*ds*ds
            d_m_max[d_idx] = 1.05*mref
            d_m_min[d_idx] = 0.5*mref

    def converged(self):
        return -1


class UpdateSpacing(Equation):
    def initialize(self, d_idx, d_htmp):
        d_htmp[d_idx] = -1.0

    def loop_all(self, d_idx, d_ds, d_fixed, d_ds_min, d_h, d_ds_max, d_cr,
                 d_htmp, N_NBRS, NBRS):
        k, s_idx, n = declare('int', 3)
        ds_min, ds_avg, cr, smax, smin, ds = declare('double', 6)
        ds_min = d_ds_min[0]
        cr = d_cr[0]
        ds_avg = 1.0
        smax = ds_min
        smin = d_ds_max[0]
        if (d_fixed[d_idx] & 1) == 0 and N_NBRS > 0:
            for k in range(N_NBRS):
                s_idx = NBRS[k]
                ds = d_ds[s_idx]
                ds_avg *= ds
                smax = max(smax, ds)
                smin = min(smin, ds)
            ds_avg = pow(ds_avg, 1.0/N_NBRS)
            if smax/smin < cr*cr*cr:
                d_htmp[d_idx] = min(d_ds_max[0], cr*smin)
            else:
                d_htmp[d_idx] = ds_avg

    def post_loop(self, d_idx, d_htmp, d_ds, d_rho, d_m_min, d_m_max):
        ds = d_htmp[d_idx]
        if ds > 0.0:
            d_ds[d_idx] = ds
            # XXX: Fix for 3D
            mref = d_rho[d_idx]*ds*ds
            d_m_max[d_idx] = 1.05*mref
            d_m_min[d_idx] = 0.5*mref

    def converged(self):
        return -1


class SetMRef(Equation):
    def loop_all(self, d_idx, d_m_min, d_m_max, d_rho, d_ds, d_h, s_ds,
                 s_rho_ref, s_ds_max, N_NBRS, NBRS):
        k, s_idx = declare('int', 2)
        ds, ds_max, r2ij, mref = declare('double', 4)
        ds_max = s_ds_max[0]
        ds = ds_max
        for k in range(N_NBRS):
            s_idx = NBRS[k]
            ds = min(ds, s_ds[s_idx])

        d_ds[d_idx] = ds
        # FIXME in 3D.
        mref = s_rho_ref[0]*ds*ds
        d_m_max[d_idx] = 1.05*mref
        d_m_min[d_idx] = 0.5*mref


class SmoothedVorticity(Equation):
    def initialize(self, d_idx, d_vor):
        d_vor[d_idx] = 0.0

    def loop(self, s_idx, d_idx, d_vor, s_vor):
        d_vor[d_idx] = max(d_vor[d_idx], abs(s_vor[s_idx]))

    def reduce(self, dst, t, dt):
        dst.vor_max[0] = numpy.max(dst.vor)


class SmoothedVorticityBG(Equation):
    def loop(self, s_idx, d_idx, d_vor, s_vor):
        d_vor[d_idx] = max(d_vor[d_idx], abs(s_vor[s_idx]))

    def reduce(self, dst, t, dt):
        dst.vor_max[0] = numpy.max(dst.vor)


class SolutionAdaptive(Equation):
    def __init__(self, dest, sources, cutoff, xmax, abs_cutoff):
        self.cutoff = cutoff
        self.xmax = xmax
        self.abs_cutoff = abs_cutoff
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_fixed, d_vor, d_ds, d_rho, d_m_min,
                  d_m_max, d_vor_max, d_ds_min):
        vm = d_vor_max[0]
        w = abs(d_vor[d_idx])
        ds_ref = d_ds[d_idx]
        cutoff = max(self.cutoff*vm, self.abs_cutoff)
        if (d_fixed[d_idx] & 1) == 0 and w > cutoff and d_x[d_idx] < self.xmax:
            s = d_ds_min[0]
            ds = min(s, ds_ref)
            d_ds[d_idx] = ds
            d_fixed[d_idx] |= 1
            # XXX: Fix for 3D
            mref = d_rho[d_idx]*ds*ds
            d_m_max[d_idx] = 1.05*mref
            d_m_min[d_idx] = 0.5*mref


class SolutionAdaptiveBin(Equation):
    def __init__(self, dest, sources, bins, xmax, abs_cutoff):
        self.bins = bins
        self.xmax = xmax
        self.abs_cutoff = abs_cutoff
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_fixed, d_vor, d_ds, d_rho, d_m_min,
                  d_m_max, d_vor_max, d_ds_min, d_ds_max, d_cr):
        level, nbins, max_lev = declare('int', 3)
        vm = d_vor_max[0]
        w = abs(d_vor[d_idx])
        ds_ref = d_ds[d_idx]
        cr = d_cr[0]
        max_lev = cast(log(d_ds_max[0]/d_ds_min[0])/log(cr) + 0.5, "int")
        nbins = self.bins
        cutoff = max(0.1*vm, self.abs_cutoff)
        if (d_fixed[d_idx] & 1) == 0 and w > cutoff and d_x[d_idx] < self.xmax:
            if nbins == 1:
                s = d_ds_min[0]
            else:
                tmp = cast(w/vm*nbins, "int")
                level = cast((tmp + 1.0)*max_lev/nbins, "int")
                s = d_ds_max[0]/pow(cr, min(max_lev, level))
            ds = min(s, ds_ref)
            d_ds[d_idx] = ds
            d_fixed[d_idx] |= 1
            # XXX: Fix for 3D
            mref = d_rho[d_idx]*ds*ds
            d_m_max[d_idx] = 1.05*mref
            d_m_min[d_idx] = 0.5*mref


@annotate
def copy_props(i, indices, copy_indices, d_x, d_y, d_z, d_u, d_v, d_w, d_vmag,
               d_uhat, d_vhat, d_what, d_p, d_m, d_rho, d_h, d_closest_idx,
               d_m_min, d_m_max,
               d_au, d_av, d_aw, d_ap, d_auhat, d_avhat, d_awhat):
    # The original algorithm of Yang and Kong but this only works in 2D.
    closest_idx, c_idx, d_idx = declare('int', 3)
    xij, yij, zij, rij, tmp, ds = declare('double', 5)
    d_idx = indices[i]
    c_idx = copy_indices[i]
    closest_idx = d_closest_idx[d_idx]
    if closest_idx > -1:
        xij = d_x[d_idx] - d_x[closest_idx]
        yij = d_y[d_idx] - d_y[closest_idx]
        zij = d_z[d_idx] - d_z[closest_idx]
        rij = sqrt(xij*xij + yij*yij + zij*zij)
        xij /= rij
        yij /= rij
        zij /= rij
        # FIXME: this only works in 2D
        tmp = xij
        xij = -yij
        yij = tmp
    else:
        xij = 1.0
        yij = 0.0
        zij = 0.0

    ds = 0.5*0.6*sqrt(d_m[d_idx]/d_rho[d_idx])

    d_m[d_idx] *= 0.5
    d_x[c_idx] = d_x[d_idx] + ds*xij
    d_y[c_idx] = d_y[d_idx] + ds*yij
    d_z[c_idx] = d_z[d_idx] + ds*zij
    d_x[d_idx] -= ds*xij
    d_y[d_idx] -= ds*yij
    d_z[d_idx] -= ds*zij
    d_m[c_idx] = d_m[d_idx]
    d_u[c_idx] = d_u[d_idx]
    d_v[c_idx] = d_v[d_idx]
    d_w[c_idx] = d_w[d_idx]
    d_vmag[c_idx] = d_vmag[d_idx]
    d_uhat[c_idx] = d_uhat[d_idx]
    d_vhat[c_idx] = d_vhat[d_idx]
    d_what[c_idx] = d_what[d_idx]
    d_p[c_idx] = d_p[d_idx]
    d_rho[c_idx] = d_rho[d_idx]
    d_h[c_idx] = d_h[d_idx]
    d_m_min[c_idx] = d_m_min[d_idx]
    d_m_max[c_idx] = d_m_max[d_idx]
    d_au[c_idx] = d_au[d_idx]
    d_av[c_idx] = d_av[d_idx]
    d_aw[c_idx] = d_aw[d_idx]
    d_ap[c_idx] = d_ap[d_idx]
    d_auhat[c_idx] = d_auhat[d_idx]
    d_avhat[c_idx] = d_avhat[d_idx]
    d_awhat[c_idx] = d_awhat[d_idx]


@annotate
def copy_props2d_bg(i, indices, copy_indices, d_x, d_y, d_z,
                    d_m, d_rho, d_h, d_ds, d_m_min, d_m_max, d_fixed,
                    d_closest_idx):
    closest_idx, c_idx, d_idx = declare('int', 3)
    xij, yij, zij, tmp, ds = declare('double', 5)
    d_idx = indices[i]
    c_idx = copy_indices[i]
    closest_idx = d_closest_idx[d_idx]
    if closest_idx > -1:
        xij = abs(d_x[d_idx] - d_x[closest_idx])
        yij = abs(d_y[d_idx] - d_y[closest_idx])
        zij = 0.0
        # Only works in 2D.
        tmp = min(xij, yij)
        if tmp == xij:
            xij = 1.0
            yij = 0.0
        else:
            xij = 0.0
            yij = 1.0
    else:
        xij = 1.0
        yij = 0.0
        zij = 0.0

    # This is some approximate distance.
    ds = 0.7*sqrt(d_m[d_idx]/d_rho[d_idx])

    d_m[d_idx] *= 0.5
    d_x[c_idx] = d_x[d_idx] + ds*xij
    d_y[c_idx] = d_y[d_idx] + ds*yij
    d_z[c_idx] = d_z[d_idx] + ds*zij
    d_x[d_idx] -= ds*xij
    d_y[d_idx] -= ds*yij
    d_z[d_idx] -= ds*zij
    d_m[c_idx] = d_m[d_idx]
    d_rho[c_idx] = d_rho[d_idx]
    d_h[c_idx] = d_h[d_idx]
    d_ds[c_idx] = d_ds[d_idx]
    d_m_min[c_idx] = d_m_min[d_idx]
    d_m_max[c_idx] = d_m_max[d_idx]
    if (d_fixed[d_idx] & 2) == 2:
        d_fixed[c_idx] = 2


@annotate
def copy_props2d(i, indices, copy_indices, d_x, d_y, d_z, d_u, d_v, d_w,
                 d_vmag, d_uhat, d_vhat, d_what, d_p, d_m, d_rho, d_h,
                 d_closest_idx, d_m_min, d_m_max,
                 d_au, d_av, d_aw, d_ap, d_auhat, d_avhat, d_awhat):
    closest_idx, c_idx, d_idx = declare('int', 3)
    xij, yij, zij, tmp, ds = declare('double', 5)
    d_idx = indices[i]
    c_idx = copy_indices[i]
    closest_idx = d_closest_idx[d_idx]
    if closest_idx > -1:
        xij = abs(d_x[d_idx] - d_x[closest_idx])
        yij = abs(d_y[d_idx] - d_y[closest_idx])
        zij = 0.0
        # Only works in 2D.
        tmp = min(xij, yij)
        if tmp == xij:
            xij = 1.0
            yij = 0.0
        else:
            xij = 0.0
            yij = 1.0
    else:
        xij = 1.0
        yij = 0.0
        zij = 0.0

    ds = 0.5*0.6*sqrt(d_m[d_idx]/d_rho[d_idx])

    d_m[d_idx] *= 0.5
    d_x[c_idx] = d_x[d_idx] + ds*xij
    d_y[c_idx] = d_y[d_idx] + ds*yij
    d_z[c_idx] = d_z[d_idx] + ds*zij
    d_x[d_idx] -= ds*xij
    d_y[d_idx] -= ds*yij
    d_z[d_idx] -= ds*zij
    d_m[c_idx] = d_m[d_idx]
    d_u[c_idx] = d_u[d_idx]
    d_v[c_idx] = d_v[d_idx]
    d_w[c_idx] = d_w[d_idx]
    d_vmag[c_idx] = d_vmag[d_idx]
    d_uhat[c_idx] = d_uhat[d_idx]
    d_vhat[c_idx] = d_vhat[d_idx]
    d_what[c_idx] = d_what[d_idx]
    d_p[c_idx] = d_p[d_idx]
    d_rho[c_idx] = d_rho[d_idx]
    d_h[c_idx] = d_h[d_idx]
    d_m_min[c_idx] = d_m_min[d_idx]
    d_m_max[c_idx] = d_m_max[d_idx]
    d_au[c_idx] = d_au[d_idx]
    d_av[c_idx] = d_av[d_idx]
    d_aw[c_idx] = d_aw[d_idx]
    d_ap[c_idx] = d_ap[d_idx]
    d_auhat[c_idx] = d_auhat[d_idx]
    d_avhat[c_idx] = d_avhat[d_idx]
    d_awhat[c_idx] = d_awhat[d_idx]


@annotate
def copy_props3d(i, indices, copy_indices, d_x, d_y, d_z, d_u, d_v, d_w,
                 d_vmag, d_uhat, d_vhat, d_what, d_p, d_m, d_rho, d_h,
                 d_closest_idx, d_m_min, d_m_max):
    closest_idx, c_idx, d_idx = declare('int', 3)
    xij, yij, zij, tmp, ds = declare('double', 5)
    d_idx = indices[i]
    c_idx = copy_indices[i]
    closest_idx = d_closest_idx[d_idx]
    if closest_idx > -1:
        xij = abs(d_x[d_idx] - d_x[closest_idx])
        yij = abs(d_y[d_idx] - d_y[closest_idx])
        zij = abs(d_z[d_idx] - d_z[closest_idx])
        tmp = min(min(xij, yij), zij)
        if tmp == xij:
            xij = 1.0
            yij = 0.0
            zij = 0.0
        elif tmp == yij:
            xij = 0.0
            yij = 1.0
            zij = 0.0
        else:
            xij = 0.0
            yij = 0.0
            zij = 1.0
    else:
        xij = 1.0
        yij = 0.0
        zij = 0.0

    ds = 0.5*0.6*pow(d_m[d_idx]/d_rho[d_idx], 1./3.)

    d_m[d_idx] *= 0.5
    d_x[c_idx] = d_x[d_idx] + ds*xij
    d_y[c_idx] = d_y[d_idx] + ds*yij
    d_z[c_idx] = d_z[d_idx] + ds*zij
    d_x[d_idx] -= ds*xij
    d_y[d_idx] -= ds*yij
    d_z[d_idx] -= ds*zij
    d_m[c_idx] = d_m[d_idx]
    d_u[c_idx] = d_u[d_idx]
    d_v[c_idx] = d_v[d_idx]
    d_w[c_idx] = d_w[d_idx]
    d_vmag[c_idx] = d_vmag[d_idx]
    d_uhat[c_idx] = d_uhat[d_idx]
    d_vhat[c_idx] = d_vhat[d_idx]
    d_what[c_idx] = d_what[d_idx]
    d_p[c_idx] = d_p[d_idx]
    d_rho[c_idx] = d_rho[d_idx]
    d_h[c_idx] = d_h[d_idx]
    d_m_min[c_idx] = d_m_min[d_idx]
    d_m_max[c_idx] = d_m_max[d_idx]


class AdaptiveResolution(Tool):
    def __init__(self, pa, dim, rho0, boundary=None, bg=False,
                 do_mass_exchange=False, freq=1, has_ghost=False,
                 kernel=None, nnps=None):
        self.pa = pa
        self.name = pa.name
        self.rho0 = rho0
        self.dim = dim
        self.boundary = boundary if boundary is not None else []
        self.backend = pa.backend
        self.bg = bg
        self._count = 0
        self._orig_nnps = nnps
        self.smoother = None
        self.freq = freq
        self.has_ghost = has_ghost
        self.N_SPLIT = 2
        if kernel is None:
            self.kernel = QuinticSpline(dim=dim)
        else:
            self.kernel = kernel

        # Turns on/off mass exchange of fluid particles.
        self.do_mass_exchange = do_mass_exchange
        if 'm_min' not in pa.properties:
            pa.add_property('m_min')
            pa.add_property('m_max')
            pa.add_property('closest_idx', type='int')
            pa.add_property('split', type='int')

        self.sph_eval = self._setup_evaluator()
        if bg:
            assert dim == 2, "Only dim=2 supported now."
            if dim == 2:
                self.copy_props = Elementwise(copy_props2d_bg,
                                              backend=self.backend)
        else:
            if dim == 2:
                self.copy_props = Elementwise(copy_props2d,
                                              backend=self.backend)
            elif dim == 3:
                self.copy_props = Elementwise(copy_props3d,
                                              backend=self.backend)
            else:
                raise NotImplementedError(
                    'Adaptive resolution not supported in 1D.'
                )

    def _create_nnps(self, **kw):
        from pysph.base.nnps import OctreeNNPS as NNPS
        # from pysph.base.nnps import LinkedListNNPS as NNPS
        kw['cache'] = True
        return NNPS(**kw)

    def _setup_evaluator(self):
        arrays = [self.pa]
        groups = []
        eqs = []
        if self.bg:
            eqs.append(
                FindSplitMergeBase(dest=self.name, sources=[self.name])
            )
        else:
            eqs.append(FindSplitMerge(dest=self.name, sources=[self.name]))
        if self.boundary:
            names = [x.name for x in self.boundary]
            eqs.append(SkipNearBoundary(dest=self.name, sources=names))
            arrays.extend(self.boundary)
        groups.append(Group(equations=eqs))
        if not self.bg and self.do_mass_exchange:
            groups.append(Group(
                equations=[MassExchange(dest=self.name, sources=[self.name])]
            ))

        sph_eval = SPHEvaluator(
            arrays=arrays, equations=groups, dim=self.dim,
            nnps_factory=self._create_nnps, kernel=self.kernel
        )
        return sph_eval

    @profile(name='AdaptiveResolution._add_remove_split_particles')
    def _add_remove_split_particles(self):
        pa = self.pa
        if self.has_ghost:
            pa.remove_tagged_particles(2)
        n_orig = pa.get_number_of_particles()
        if self.backend == 'cython':
            split = pa.split
            if self.do_mass_exchange:
                # All the merged split values should be zero.
                n_mass_merge = np.sum(split == 2)
                msg = "Nonzero merged masses " + str(n_mass_merge)
                assert n_mass_merge == 0, msg
            # FIXME: This should all be done in parallel.
            # Note: only the particles that must be removed are marked
            # as split = -1.
            merged = split == -1
            split_cond = split == 1
            n_merge = np.sum(merged)
            n_split = np.sum(split_cond)*(self.N_SPLIT - 1)
            n_add = n_split - n_merge
            merged_idx = np.where(merged)[0]
            if n_add > 0:
                extra_idx = np.arange(n_orig, n_orig + n_add)
                new_indices = np.hstack((extra_idx, merged_idx))
                pa.extend(n_add)
                pa.set_num_real_particles(n_orig + n_add)
            elif n_add == 0:
                new_indices = merged_idx
            elif n_add < 0:
                new_indices = merged_idx[:n_split]

            if len(new_indices) > 0:
                split_ids = np.where(split_cond)[0]
                if self.bg:
                    args = wrap(
                        split_ids, new_indices, pa.x, pa.y, pa.z, pa.m,
                        pa.rho, pa.h, pa.ds, pa.m_min, pa.m_max, pa.fixed,
                        pa.closest_idx, backend=self.backend
                    )
                else:
                    args = wrap(
                        split_ids, new_indices, pa.x, pa.y, pa.z, pa.u, pa.v,
                        pa.w, pa.vmag, pa.uhat, pa.vhat, pa.what, pa.p, pa.m,
                        pa.rho, pa.h, pa.closest_idx, pa.m_min, pa.m_max,
                        pa.au, pa.av, pa.aw, pa.ap,
                        pa.auhat, pa.avhat, pa.awhat,
                        backend=self.backend
                    )
                self.copy_props(*args)
            if n_add < 0:
                pa.remove_particles(merged_idx[n_split:], align=True)
            if self.has_ghost:
                n = pa.get_number_of_particles()
                pa.gid[:] = np.arange(n)
        else:
            NotImplementedError('GPU not yet supported')

    def setup_smoothing(self, dest, arrays, nnps, dim, rho0, iters=5):
        def _create_nnps(**kw):
            return nnps

        sources = [x.name for x in arrays]
        shift = [
            Group(equations=[
                InitializeShift(dest=dest, sources=None)
            ]),
            Group(equations=[
                SmoothPositionShift(dest=dest, sources=sources, rho0=rho0,
                                    dim=dim)
            ], iterate=True, min_iterations=iters, max_iterations=iters),
            Group(equations=[
                CorrectProperties(dest=dest, sources=sources)
            ])
        ]

        return SPHEvaluator(
            arrays=arrays, equations=shift, dim=self.dim,
            nnps_factory=_create_nnps, kernel=self.kernel
        )

    @profile(name='AdaptiveResolution.run')
    def run(self):
        # FIXME: mass conservation check is temporary and just for debugging.
        mi = np.sum(self.pa.m)
        self.sph_eval.update()
        self.sph_eval.evaluate()
        self._add_remove_split_particles()
        if not self.bg and self._orig_nnps is not None:
            if self.smoother is None:
                self.smoother = self.setup_smoothing(
                    self.name, [self.pa], self._orig_nnps, self.dim,
                    rho0=self.rho0, iters=3
                )
            with profile_ctx("AdaptiveResolution.smoother.update"):
                self.smoother.update()
                self.smoother.evaluate()
        mf = np.sum(self.pa.m)
        diff = mi - mf
        msg = "Mass not conserved, diff, %s problem in algorithm!" % diff
        assert abs(diff) < 1e-8, msg

    def pre_step(self, solver=None):
        self._count += 1
        if self._count % self.freq == 0:
            self.run()
            if self.has_ghost:
                solver.nnps.update_domain()


def setup_properties(particle_arrays):
    for pa in particle_arrays:
        pa.add_property('htmp')
        pa.add_property('ds')
        pa.add_property('n_nbrs', type='int')
        pa.add_output_arrays(['n_nbrs', 'h'])
        pa.add_property('m_min')
        pa.add_property('m_max')
        pa.add_property('closest_idx', type='int')
        pa.add_property('split', type='int')
        pa.add_property('gradv', stride=9)
        pa.add_property('gradp', stride=3)
        pa.add_property('shift_x')
        pa.add_property('shift_y')
        pa.add_property('shift_z')


def setup_background_props(pa, ds_min, ds_max, cr, rho_ref):
    for prop in ('x', 'y', 'z', 'au', 'av', 'aw',
                 'h', 'rho', 'ds', 'm', 'vor'):
        pa.add_property(prop)
    setup_properties([pa])
    pa.add_property('fixed', type='int')
    pa.add_constant('vor_max', 0.0)
    pa.add_constant('ds_min', float(ds_min))
    pa.add_constant('ds_max', float(ds_max))
    pa.add_constant('cr', float(cr))
    pa.add_constant('rho_ref', float(rho_ref))
    pa.set_output_arrays(['x', 'y', 'z', 'h', 'ds', 'm_min', 'm_max',
                          'fixed', 'n_nbrs'])


class UpdateBackground(Tool):
    def __init__(self, fluid_pa, dim, boundary, ds_min, ds_max, cr, rho_ref,
                 radius_scale=3, freq=5, bg_freq=10, sol_adapt=0,
                 kernel=None, abs_cutoff=0.01):
        self.pa = fluid_pa
        self.name = fluid_pa.name
        self.dim = dim
        self._count = 0
        self.freq = freq
        self.bg_freq = bg_freq
        self.sol_adapt = sol_adapt
        self.abs_cutoff = abs_cutoff
        self.rho_ref = rho_ref
        self.boundary = boundary if boundary is not None else []
        if kernel is None:
            self.kernel = QuinticSpline(dim=dim)
        else:
            self.kernel = kernel
        self.bg_pa = ParticleArray(name='bg')
        setup_background_props(
            self.bg_pa, ds_min=ds_min, ds_max=ds_max, cr=cr,
            rho_ref=rho_ref
        )
        self.radius_scale = radius_scale
        self._bounds = None
        self._setup_background()
        self._adapt_bg = AdaptiveResolution(self.bg_pa, dim=dim, bg=True,
                                            rho0=rho_ref)
        self.adapt_fluid = None
        self.sph_eval = self._setup_bg_evaluator()
        self.mref_eval = self._setup_mref_evaluator()
        # self.set_initial_bg()

    def _setup_background(self):
        bg_pa = self.bg_pa
        pa = self.pa
        dim = self.dim
        bounds = get_bounding_box([pa], tight=True)
        self._bounds = bounds
        k = 1  # self.radius_scale*0.5
        self.k = k
        # Notes:
        # - the bg pa spacing should be kernel.radius_scale*h = (k*h)
        # - the bg_pa's h should be same as fluid.
        # - the bg_pa rho should be k**dim size of fluid.
        # - during splitting the n_neighbors will be less, so change that.
        ds_max = bg_pa.ds_max[0]
        ds_min = bg_pa.ds_min[0]
        dx = ds_max*k
        bnds = bounds + 10.0*np.asarray([-dx, dx, -dx, dx, -dx, dx])

        if dim == 2:
            dx5 = dx*5
            x, y = np.mgrid[bnds[0]:bnds[1] + dx:dx,
                            bnds[2]:bnds[3] + dx:dx]
            z = np.zeros_like(x)
            region = ~(
                (x > (bnds[0] + dx5)) & (x < (bnds[1] - dx5)) &
                (y > (bnds[2] + dx5)) & (y < (bnds[3] - dx5))
            )
        elif dim == 3:
            x, y, z = np.mgrid[bnds[0]:bnds[1] + dx:dx,
                               bnds[2]:bnds[3] + dx:dx,
                               bnds[4]:bnds[5] + dx:dx]

            region = ~(
                (x > (bounds[0] + dx)) & (x < (bounds[1] - dx)) &
                (y > (bounds[2] + dx)) & (y < (bounds[3] - dx))
                (z > (bounds[4] + dx)) & (z < (bounds[5] - dx))
            )
        fixed = np.zeros(x.shape, dtype=np.int32)
        fixed[region] = 2
        fixed = fixed.ravel()

        one = np.ones(x.size)
        h = one*ds_max
        volume = ds_max**dim
        rho = bg_pa.rho_ref[0]*one*k**dim
        ds = ds_max*one
        m_ref = rho*volume
        x, y, z = (np.ravel(_) for _ in (x, y, z))
        bg_pa.add_particles(
            x=x, y=y, z=z, h=h, m=volume*rho, rho=rho, ds=ds,
            m_max=m_ref*1.05, m_min=m_ref*0.5, fixed=fixed
        )

    def _create_nnps(self, **kw):
        from pysph.base.nnps import OctreeNNPS as NNPS
        # from pysph.base.nnps import LinkedListNNPS as NNPS
        kw['radius_scale'] = self.radius_scale
        kw['cache'] = True
        return NNPS(**kw)

    def _create_nnps_mref(self, **kw):
        from pysph.base.nnps import OctreeNNPS as NNPS
        # This is evaluated once and each time things will change
        # no point caching.
        kw['cache'] = False
        return NNPS(**kw)

    def solution_adaptive(self, t=0.0, dt=0.0):
        return self.sol_adapt > 0

    def _split_bg(self):
        self._adapt_bg.run()
        self.sph_eval.update()

    def _setup_bg_evaluator(self):
        bg_pa = self.bg_pa
        f_name = self.pa.name
        eqs = []
        boundary_names = [x.name for x in self.boundary]
        ds_min = bg_pa.ds_min[0]
        ds_max = bg_pa.ds_max[0]
        cr = bg_pa.cr[0]
        xmax = self._bounds[1]
        self.n_iters = int(round(np.log(ds_max/ds_min)/np.log(cr) + 0.5))
        if self.solution_adaptive() and 'vor' not in self.pa.properties:
            self.pa.add_property('vor')
            self.pa.add_output_arrays(['vor'])
        name = bg_pa.name
        eqs.append(Group(
            equations=[MarkBoundary(dest=name, sources=boundary_names)]
        ))
        eqs.append(Group(
            equations=[SmoothedVorticity(dest=name, sources=[f_name])],
            condition=self.solution_adaptive
        ))
        eqs.append(Group(
            equations=[
                SolutionAdaptive(dest=name, sources=None,
                                 cutoff=self.sol_adapt, xmax=0.9*xmax,
                                 abs_cutoff=self.abs_cutoff)
            ],
            condition=self.solution_adaptive,
        ))
        eqs.append(Group(
            equations=[UpdateSpacing(dest=name, sources=[name])],
            post=self._split_bg
        ))
        # FIXME in 3D.
        Nr = round(48*(1/self.k)**self.dim + 0.5)
        repeat = 5
        # XXX: Remove the pseudo boundaries Assumes that pseudo particle array
        # is called wake.
        shift_names = [x for x in boundary_names if x != 'wake']
        all_sources = [name] + shift_names
        eqs.append(Group(
            equations=[
                UpdateSmoothingLength(dest=name, sources=all_sources, Nr=Nr),
                SmoothPositionBGShift(dest=name, sources=all_sources,
                                      dim=self.dim)
            ], iterate=True, min_iterations=repeat, max_iterations=repeat,
            update_nnps=False
        ))

        arrays = [self.bg_pa, self.pa] + self.boundary
        sph_eval = SPHEvaluator(
            arrays=arrays, equations=eqs, dim=self.dim,
            nnps_factory=self._create_nnps, kernel=self.kernel
        )
        return sph_eval

    def _setup_mref_evaluator(self):
        bg_pa = self.bg_pa
        arrays = [self.pa, bg_pa]
        # TODO:
        # - Test an actual problem.
        eqs = []
        eqs.append(Group(
            equations=[SetMRef(dest=self.name, sources=[bg_pa.name])]
        ))
        sph_eval = SPHEvaluator(
            arrays=arrays, equations=eqs, dim=self.dim,
            nnps_factory=self._create_nnps_mref, kernel=self.kernel
        )
        return sph_eval

    def _create_fluid_outer_box(self):
        pa = self.pa
        dim = self.dim
        dx = self.bg_pa.ds_max[0]
        bounds = get_bounding_box([pa], tight=True)
        bnds = bounds + 3.0*np.asarray([-dx, dx, -dx, dx, -dx, dx])
        dx2 = dx*0.5
        if dim == 2:
            x, y = np.mgrid[bnds[0]:bnds[1] + dx:dx,
                            bnds[2]:bnds[3] + dx:dx]
            region = ~(
                (x > (bounds[0] - dx2)) & (x < (bounds[1] + dx2)) &
                (y > (bounds[2] - dx2)) & (y < (bounds[3] + dx2))
            )
            z = np.zeros_like(x)
        else:
            x, y, z = np.mgrid[bnds[0]:bnds[1] + dx:dx,
                               bnds[2]:bnds[3] + dx:dx,
                               bnds[4]:bnds[5] + dx:dx]
            region = ~(
                (x > (bounds[0] - dx2)) & (x < (bounds[1] + dx2)) &
                (y > (bounds[2] - dx2)) & (y < (bounds[3] + dx2)) &
                (z > (bounds[4] - dx2)) & (z < (bounds[5] + dx2))
            )

        x, y, z = (np.ravel(t[region]) for t in (x, y, z))
        m = pa.m[0]
        rho = pa.rho[0]
        h = pa.h[0]
        box = ParticleArray(name='box', x=x, y=y, z=z, m=m, rho=rho, h=h)
        return box

    def initialize_fluid(self, solids=None, vacondio=True, hybrid=False):
        from adaptv import AdaptiveResolutionVacondio
        pa = self.pa
        dim = self.dim
        box = self._create_fluid_outer_box()

        if vacondio:
            adapt = AdaptiveResolutionVacondio(
                pa, dim, rho0=self.rho_ref, boundary=[box],
                freq=self.freq, hybrid=hybrid
            )
        else:
            adapt = AdaptiveResolution(
                pa, dim, rho0=self.rho_ref, boundary=[box], freq=self.freq
            )

        self.box = box
        self.adapt_fluid = adapt
        ds_min = self.bg_pa.ds_min[0]

        # FIXME in 3D.
        Nr = 28
        repeat = 3
        eqs = []
        name = pa.name
        sources = [name, 'box']
        eqs.append(Group(
            equations=[
                UpdateSmoothingLength(dest=name, sources=sources, Nr=Nr),
                SmoothPositionShift(dest=name, sources=sources,
                                    rho0=self.rho_ref, dim=self.dim)
            ], iterate=True, min_iterations=repeat, max_iterations=repeat,
            update_nnps=False
        ))
        arrays = [pa, box]
        sph_eval = SPHEvaluator(
            arrays=arrays, equations=eqs, dim=dim,
            nnps_factory=self._create_nnps, kernel=self.kernel
        )

        sep = '-'*70
        msg = 'Initialzing fluid particles:'
        print(sep, '\n', msg)

        start = time()
        n = int(self.n_iters * 3)
        for i in range(n):
            self.set_mref()
            adapt.run()
            sph_eval.update()
            sph_eval.evaluate()
        # call set_mref after the iteration is done to make sure ds is set
        # correctly, otherwise, ds of few particles will be zero.
        self.set_mref()
        solids = solids if solids is not None else []
        if len(solids) > 0:
            for solid in solids:
                sources.append(solid.name)

        repeat = 5
        shift_eqs = [Group(
            equations=[
                SmoothPositionShift(dest=name, sources=sources,
                                    rho0=self.rho_ref, dim=self.dim)
            ], iterate=True, min_iterations=repeat, max_iterations=repeat,
            update_nnps=False
        )]

        if len(solids) > 0:
            arrays.extend(solids)
        shift_eval = SPHEvaluator(
            arrays=arrays, equations=shift_eqs, dim=dim,
            nnps_factory=self._create_nnps, kernel=self.kernel
        )
        for solid in solids:
            G.remove_overlap_particles(pa, solid, ds_min, dim=2)
        for i in range(self.n_iters):
            shift_eval.update()
            shift_eval.evaluate()
        msg = f'time took to initialize the fluid, {time()-start:.4f}s'
        print(msg, '\n', sep)
        logger.info('Initialize fluid:\n%s \n  %s\n%s', sep, msg, sep)

        # Add boundary to adaptive.

        if vacondio:
            self.adapt_fluid = AdaptiveResolutionVacondio(
                pa, dim, rho0=self.rho_ref,
                boundary=[box]+solids, freq=self.freq, hybrid=hybrid
            )
        else:
            self.adapt_fluid = AdaptiveResolution(
                pa, dim, rho0=self.rho_ref,
                boundary=[box]+solids, freq=self.freq
            )
        get_profile_info().clear()

    def set_initial_bg(self):
        n = int(self.n_iters * 3)
        for i in range(n):
            self.update_bg()

    @profile(name='UpdateBackground.update_bg')
    def update_bg(self):
        self.sph_eval.update()
        self.sph_eval.evaluate()
        self.mref_eval.update()

    @profile(name='UpdateBackground.set_mref')
    def set_mref(self):
        self.mref_eval.evaluate()

    def run(self):
        self.update_bg()
        self.set_mref()

    def post_step(self, solver=None):
        self._count += 1
        if self._count % self.bg_freq == 0:
            self.update_bg()
        self.set_mref()
