"""Keplerian disk problem.
"""

import os
import numpy as np
from numpy import sin, cos, pi
import matplotlib.pyplot as plt
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.sph.scheme import add_bool_argument
from pysph.solver.application import Application
from pysph.solver.utils import iter_output

from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio
from edac import AdaptiveEDACScheme


L = 2.0


def linf_error(approx, exact):
    approx_max = np.max(approx)
    exact_max = np.max(exact)
    return abs(approx_max - exact_max) / exact_max


def l1_error(approx, exact):
    l1_err = np.average(np.abs(approx - exact))
    exact_max = np.max(exact)
    return l1_err / exact_max


def func_exact(x, y):
    tmp = 2*pi/L
    return sin(tmp*x)*cos(tmp*y)


def grad_exact(x, y):
    tmp = 2*pi/L
    return [tmp * cos(tmp*x) * cos(tmp*y), -tmp * sin(tmp*x) * sin(tmp*y)]


class KeplerianForce(Equation):
    def initialize(self, d_idx, d_au, d_av, d_aw, d_x, d_y, d_z):
        r2 = d_x[d_idx]**2 + d_y[d_idx]**2 + d_z[d_idx]**2
        fac = pow((r2 + 1e-16), -1.5)
        d_au[d_idx] += -fac * d_x[d_idx]
        d_av[d_idx] += -fac * d_y[d_idx]
        d_aw[d_idx] += -fac * d_z[d_idx]


class Keplerian(Application):
    def initialize(self):
        self.rho0 = 1.0
        self.L = L
        self.u = u = 0.0
        self.v = v = 0.0
        self.U = np.sqrt(u*u + v*v)
        self.c0 = 10 * (self.U + 1)
        self.dim = 2

    def add_user_options(self, group):
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        group.add_argument(
            "--perturb", action="store", type=float, dest="perturb", default=0,
            help="Random perturbation of initial particles as a fraction "
            "of dx (setting it to zero disables it, the default)."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=50,
            help="Number of points along x direction. (default 50)"
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.0,
            help="Ratio h/dx."
        )
        group.add_argument(
            '--adapt-freq', action='store', type=int, dest='freq', default=1,
            help='No of times to run adaptive resolution.'
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting"
        )
        group.add_argument(
            '--shift', action='store', dest='shift', type=int,
            help='No of times to Shift the particles.'
        )

    def consume_user_options(self):
        layers = 3
        nx = self.options.nx
        self.dx = dx = self.L / nx
        self.dx_min = self.dx/np.sqrt(2)**layers
        self.volume = dx * dx
        self.hdx = self.options.hdx

        self.h0 = self.hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        self.dt = cfl * self.h0 / (self.U+self.c0) * 5

        self.tf = 2.0
        self.kernel_correction = False
        self.vacondio = self.options.vacondio

    def create_nnps(self):
        from pysph.base.nnps import OctreeNNPS as NNPS
        return NNPS(dim=2.0, particles=self.particles, radius_scale=3.0,
                    cache=True)

    def create_particles(self):
        dx = self.dx

        r = np.arange(0.5*L, 1.5*L, dx)
        x, y = np.array([]), np.array([])
        for i in r:
            theta = np.linspace(0, 2*pi, int(2*pi*i/dx), endpoint=False)
            x = np.append(x,  i * cos(theta))
            y = np.append(y,  i * sin(theta))

        if self.options.perturb > 0:
            np.random.seed(1)
            factor = dx * self.options.perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor

        m = self.volume * self.rho0
        h = self.hdx * dx
        p = 0.0

        fluid = get_particle_array(
            name='fluid', x=x, y=y, m=m, h=h, u=self.u, v=self.v,
            rho=self.rho0, p=p
        )

        dxs = self.dx
        ms = dxs*dxs * self.rho0
        h = self.hdx * dxs

        r = np.arange(0.5*L - 5*dx, 0.5*L, dxs)
        x, y = np.array([]), np.array([])
        for i in r:
            theta = np.linspace(0, 2*pi, int(2*pi*i/dxs), endpoint=False)
            x = np.append(x,  i * cos(theta))
            y = np.append(y,  i * sin(theta))

        r = np.arange(1.5*L, 1.5*L + 5*dx, dxs)
        x1, y1 = np.array([]), np.array([])
        for i in r:
            theta = np.linspace(0, 2*pi, int(2*pi*i/dxs), endpoint=False)
            x1 = np.append(x1,  i * cos(theta))
            y1 = np.append(y1,  i * sin(theta))
        xs = np.append(x, x1)
        ys = np.append(y, y1)

        solid = get_particle_array(
            name='solid', x=xs, y=ys, m=ms, h=h, u=0.0, v=0.0,
            rho=self.rho0, p=0
        )
        self.scheme.setup_properties([fluid, solid])

        props = ['dhdx', 'f', 'gradx', 'grady', 'f_approx',
                 'gradx_exact', 'grady_exact']

        for pa in [fluid, solid]:
            for prop in props:
                pa.add_property(prop)
            pa.add_output_arrays(props)

        fluid.f[:] = func_exact(fluid.x, fluid.y)
        fluid.gradx_exact[:], fluid.grady_exact[:] = grad_exact(fluid.x, fluid.y)

        fluid.add_property('rij')
        fluid.rij[:] = np.sqrt(fluid.x**2 + fluid.y**2)
        fluid.add_property('angle')
        fluid.angle[:] = np.pi + np.arctan2(fluid.x/fluid.rij, -fluid.y/fluid.rij)
        vtheta = 1/np.sqrt(fluid.rij)
        fluid.u[:] = vtheta * cos(fluid.angle)
        fluid.v[:] = vtheta * sin(fluid.angle)
        return [fluid, solid]

    def configure_scheme(self):
        scheme = self.scheme
        pfreq = 10
        scheme.configure(h=self.h0, nu=0.0, cfl=self.cfl)
        scheme.configure_solver(
            tf=self.tf, dt=self.dt, pfreq=pfreq,
            output_at_times=[0.2, 0.4, 0.8, 1.0]
        )

    def create_scheme(self):
        edac = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=self.rho0, c0=self.c0,
            nu=None, h=None, cfl=None
        )
        return edac

    def create_equations(self):
        from edac import SlipVelocityExtrapolation
        equations = self.scheme.get_equations()
        # Remove SetWallvelocity and add Slipvelocityextrapolation
        equations[1].equations.pop()
        equations[1].equations.append(
            SlipVelocityExtrapolation(dest='solid', sources=['fluid']),
        )
        # Add solid as force to ShiftForce.
        equations[-1].equations[0].sources = ['fluid', 'solid']
        eqns = []
        eqns.append(Group(
            equations=[KeplerianForce('fluid', ['fluid'])]
        ))
        equations.extend(eqns)
        return equations

    def pre_step(self, solver):
        fluid = self.particles[0]
        dx = self.dx
        m0 = self.rho0*dx*dx
        rij = np.sqrt(fluid.x**2 + fluid.y**2)
        angle = np.pi + np.arctan2(fluid.x/rij, -fluid.y/rij)
        fluid.m_max[:] = m0*1.1
        fluid.m_min[:] = m0*0.4
        region = ((angle > 0/4 * np.pi) & (angle < 1/4 * np.pi) & (rij > 1.2) &
                  (rij < 2.8))
        fluid.m_max[region] = m0*0.6
        fluid.m_min[region] = m0*0.0
        region = ((angle > 2/4 * np.pi) & (angle < 3/4 * np.pi) & (rij > 1.2) &
                  (rij < 2.8))
        fluid.m_max[region] = m0*0.6
        fluid.m_min[region] = m0*0.0
        region = ((angle > 4/4 * np.pi) & (angle < 5/4 * np.pi) & (rij > 1.2) &
                  (rij < 2.8))
        fluid.m_max[region] = m0*0.6
        fluid.m_min[region] = m0*0.0
        region = ((angle > 6/4 * np.pi) & (angle < 7/4 * np.pi) & (rij > 1.2) &
                  (rij < 2.8))
        fluid.m_max[region] = m0*0.6
        fluid.m_min[region] = m0*0.0

    def create_tools(self):
        arrays = self.particles
        if self.vacondio:
            t = AdaptiveResolutionVacondio(
                self.particles[0], dim=2, has_ghost=True,
                do_mass_exchange=False, hybrid=True, rho0=self.rho0
            )
        else:
            t = AdaptiveResolution(
                self.particles[0], dim=2, has_ghost=True,
                do_mass_exchange=False, rho0=self.rho0
            )

        smoothing_sph_eval = t.setup_smoothing(
            dest='fluid', arrays=arrays, nnps=self.nnps,
            dim=self.dim, rho0=self.rho0
        )

        for i in range(6):
            print("t.pre_step", i, end='\r')
            self.pre_step(None)
            t.pre_step(solver=self.solver)
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)
        print()

        for i in range(50):
            print("Smoothing", i, end='\r')
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)
        print()
        return [t]


if __name__ == '__main__':
    app = Keplerian()
    app.run()
    app.post_process(app.info_filename)
