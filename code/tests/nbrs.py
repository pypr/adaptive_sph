'''Compute optimal mass for a given smoothing length factor, alpha, and daughter
placement factor, epsilon.

'''
import numpy as np
from compyle.utils import ArgumentParser
import matplotlib.pyplot as plt

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.base.kernels import (CubicSpline, QuinticSpline, WendlandQuintic)
from edac import (SummationDensityGather, SummationDensityScatter,
                  SummationDensity, ShiftPositionLind)


class Neighbors(Equation):
    def initialize(self, d_idx, d_n_nbrs):
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, d_n_nbrs):
        d_n_nbrs[d_idx] += 1


def create_particles(dx, dim, perturb=0.0):
    _x = np.arange(0, 20*dx, dx)
    _y = np.arange(-20*dx, 20*dx, dx)
    x, y = np.meshgrid(_x, _y)

    dx_min = dx/2
    _x = np.arange(-20*dx, 0, dx_min)
    _y = np.arange(-20*dx, 20*dx, dx_min)
    x1, y1 = np.meshgrid(_x, _y)
    x = np.append(x, x1)
    y = np.append(y, y1)

    if dim == 2:
        m = np.ones_like(x) * (dx)**dim
        m[x<0.0] = (dx_min)**dim

        h = np.ones_like(x) * dx
        h[x<0.0] = dx_min

        cond = ((abs(x) < 11*dx) & (abs(y) < 11*dx))
        xs, ys, hs, ms = x[~cond], y[~cond], h[~cond], m[~cond]
        x, y, h, m = x[cond], y[cond], h[cond], m[cond]

        if perturb > 0:
            np.random.seed(1)
            factor = dx * perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor
        dummy = get_particle_array(name='dummy', x=x, y=y, h=h, m=m)
        solid = get_particle_array(name='solid', x=xs, y=ys, h=hs, m=ms)
    else:
        raise NotImplementedError("1/3-D is not implemented.")

    props = ['awhat', 'ravg', 'auhat', 'mT', 'avhat', 'n_nbrs']
    for prop in props:
        dummy.add_property(prop)
        solid.add_property(prop)

    xd, yd, zd = [0, 0, 0]
    h = dx
    m = h**dim
    fluid = get_particle_array(name='fluid', x=xd, y=yd, z=zd, h=h, m=m,
                               n_nbrs=0)
    plt.scatter(dummy.x, dummy.y, c=dummy.h, s=dummy.m*5000, cmap='RdBu')
    plt.scatter(solid.x, solid.y, c=solid.h, s=solid.m*50000, marker='x', cmap='RdBu')
    plt.scatter(fluid.x, fluid.y, marker='x', s=100)
    plt.show()
    return dummy, fluid, solid


def shift_equations(dummy, solid, dim, kernel):
    eq = []
    eqns = []
    eq.append(ShiftPositionLind(dummy.name, [solid.name, dummy.name], dim=dim))
    eqns.append(Group(equations=eq))
    shift_eval = SPHEvaluator(
        arrays=[dummy, solid], equations=eqns, dim=dim,
        kernel=kernel
    )
    return shift_eval


def create_equations(dummy, fluid, dim, kernel):
    eq, eqns = [], []
    eq.append(
        # SummationDensityGather(fluid.name, [dummy.name, fluid.name])
        # SummationDensityScatter(fluid.name, [dummy.name, fluid.name])
        SummationDensity(fluid.name, [dummy.name])
    )
    eq.append(
        Neighbors(fluid.name, [dummy.name])
    )
    eqns.append(Group(equations=eq))

    sph_eval = SPHEvaluator(
        arrays=[dummy, fluid], equations=eqns, dim=dim,
        kernel=kernel
    )
    return sph_eval


def compute_density(dx=0.05, dim=2, perturb=0.0, plot=False, shift=0):
    kernels = [
        CubicSpline, QuinticSpline, WendlandQuintic,
    ]
    kernels = [kernel(dim=dim) for kernel in kernels]
    dummy, fluid, solid = create_particles(dx, dim, perturb)

    if shift > 0:
        shift_eval = shift_equations(dummy, solid, dim, kernels[0])
        for i in range(shift):
            print('Shifting', i)
            shift_eval.update()
            shift_eval.evaluate(dt=0.04)
        plt.scatter(dummy.x, dummy.y, c=dummy.h, s=dummy.m*5000)
        plt.scatter(solid.x, solid.y, c='r', s=1)
        plt.scatter(fluid.x, fluid.y, marker='x', s=100)
        plt.show()

    hdxs = np.arange(0.5, 5.0, 0.02)

    density = np.zeros((len(kernels), len(hdxs)))
    nbrs = np.zeros((len(kernels), len(hdxs)))
    for i, kernel in enumerate(kernels):
        sph_eval = create_equations(dummy, fluid, dim, kernel)
        for j, hdx in enumerate(hdxs):
            fluid.h[:] = hdx * dx
            sph_eval.update()
            sph_eval.evaluate()
            density[i, j] = fluid.rho[0]
            nbrs[i, j] = fluid.n_nbrs[0]
    if plot:
        plt.figure(figsize=(8, 6))
        plt.plot(hdxs, np.ones_like(hdxs), 'k--', label='Exact')
        for i, kernel in enumerate(kernels):
            plt.plot(hdxs, density[i], label=kernel.__class__.__name__)
            # plt.plot(hdxs, nbrs[i], label=kernel.__class__.__name__)
            plt.xlabel('hdx, Smoothing length factor')
            plt.ylabel(r'Density, $\rho$')
            plt.legend()
            plt.grid()
        plt.show()
    return density


if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('--dx', action='store', type=float, dest='dx',
                   default=0.05, help='Particle discretization.')
    p.add_argument('--dim', action='store', type=int, dest='dim',
                   default=2, help='Dimension of the simulation.')
    p.add_argument('--shift', action='store', type=int, dest='shift',
                   default=2, help='No of times to Shift the particles.')
    p.add_argument(
        '--plot', action='store_true', dest='plot',
        default=False, help='Show plots at the end of simulation'
    )
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    p.add_argument(
        "--perturb", action="store", type=float, dest="perturb", default=0,
        help="Random perturbation of initial particles as a fraction "
        "of dx (setting it to zero disables it, the default)."
    )
    o = p.parse_args()
    compute_density(dx=o.dx, dim=o.dim, perturb=o.perturb, plot=o.plot,
                    shift=o.shift)
