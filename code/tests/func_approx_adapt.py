'''Compute optimal mass for a given smoothing length factor, alpha, and daughter
placement factor, epsilon.

'''
import numpy as np
from numpy import cos, pi, sin, exp
from compyle.api import declare, annotate, Reduction
from compyle.utils import ArgumentParser
from compyle.array import wrap
import matplotlib.pyplot as plt
from matplotlib import ticker

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator

from edac import update_rho
from optimal_mass import KernelMomentsDaughter, KernelMomentsMother


class FuncApproximationStd(Equation):
    def initialize(self, d_f_approx, d_idx):
        d_f_approx[d_idx] = 0

    def loop(self, d_idx, s_idx, d_f_approx, s_f, s_m, s_rho, WJ):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        d_f_approx[d_idx] += mj/rhoj * s_f[s_idx] * WJ


def global_error(a, b):
    diff = a-b
    return np.sqrt(np.sum(diff*diff)/len(diff))


def daughter_coords(dist, dim, no_of_daughters):
    if dim == 1:
        x = np.linspace(-dist*0.5, dist*0.5, no_of_daughters)
        y = np.zeros(no_of_daughters)
        z = np.zeros(no_of_daughters)
    elif dim == 2:
        n = no_of_daughters - 1
        theta = np.linspace(np.pi/n, 2*np.pi+np.pi/n, n, endpoint=False)
        ctheta, stheta = np.cos(theta), np.sin(theta)
        x, y, z = dist * ctheta, dist * stheta, np.zeros_like(theta)
        x = np.append([0], x)
        y = np.append([0], y)
        z = np.append([0], z)
    elif dim == 3:
        assert no_of_daughters == 13, "Others config not implemented."
        ang = np.arctan2(0.5*(1 + np.sqrt(5)), 1)
        x1, y1 = dist * np.cos(ang), dist * np.sin(ang)
        verts = np.array([
            [0, 0, 0],
            [0, x1, y1], [0, -x1, y1], [0, x1, -y1], [0, -x1, -y1],
            [x1, 0, y1], [-x1, 0, y1], [x1, 0, -y1], [-x1, 0, -y1],
            [x1, y1, 0], [-x1, y1, 0], [x1, -y1, 0], [-x1, -y1, 0]
        ])
        x, y, z = np.transpose(verts)
    return x, y, z


def mother_particles(dx, dim):
    _x = np.arange(-1, 1, dx)
    n = len(_x)
    y = np.zeros_like(n**dim)
    z = np.zeros_like(n**dim)
    if dim == 1:
        x = np.meshgrid(_x)[0]
    elif dim == 2:
        x, y = np.meshgrid(_x, _x)
    elif dim == 3:
        x, y, z = np.meshgrid(_x, _x, _x)
    return x, y, z


def daughter_particles(dx, dim, no_of_daughters, dist):
    _x = np.arange(-1, 1, dx)
    n = len(_x)
    y = np.zeros([n]*dim)
    z = np.zeros([n]*dim)
    xd, yd, zd = daughter_coords(dist, dim, no_of_daughters)
    if dim == 1:
        x = np.meshgrid(_x)[0]
    elif dim == 2:
        x, y = np.meshgrid(_x, _x)
    elif dim == 3:
        x, y, z = np.meshgrid(_x, _x, _x)
    cond = (abs(x) < 1e-12) & (abs(y) < 1e-12) & (abs(z) < 1e-12)
    x = np.append(x[~cond], xd)
    y = np.append(y[~cond], yd)
    z = np.append(z[~cond], zd)
    return x, y, z


def create_particles(dx, h, dim, no_of_daughters, plot):
    # Mother particle points
    xm, ym, zm = mother_particles(dx, dim)
    f = cosx(xm)
    mother = get_particle_array(
        name='mother', x=xm, y=ym, z=zm, h=h, wij=0, m=dx**dim,
        f=f
    )

    # The interpolation points.
    xs, ys, zs = mother_particles(dx/4, dim)
    sample = get_particle_array(name='sample', x=xs, y=ys, z=zs, h=h, wij=0,
                                f_approx=0, m=(dx*0.5)**dim)

    # Daughter particle points
    xd, yd, zd = daughter_particles(dx, dim, no_of_daughters,
                                            dist=dx/2)
    f = cosx(xd)

    m = dx**dim
    hd = np.ones_like(xd) * h
    hd[-no_of_daughters:] = h
    md = np.ones_like(xd) * m
    md[-no_of_daughters:] = m
    daughter = get_particle_array(
        name='daughter', x=xd, y=yd, z=zd, h=hd, m=md, wij=0.0, f=f
    )
    if plot:
        if dim < 3:
            plt.scatter(sample.x, sample.y, marker='.',
                        s=1)
            plt.scatter(daughter.x, daughter.y, marker='x',
                        s=daughter.m/max(daughter.m)*20)
            plt.scatter(mother.x, mother.y, marker='.',
                        s=mother.m/max(mother.m)*20)
            plt.show()
    return mother, daughter, sample


def cosx(x):
    return cos(2*pi*x) + sin(3*pi*x)


def polynomial1d(x, deg):
    return x**deg


def create_equations(method):
    mother_eqns = []
    eq = []
    eq.extend(
        update_rho(['mother'], ['mother'], method)
    )
    eq.append(Group(
        equations=[FuncApproximationStd('sample', ['mother'])]
    ))
    mother_eqns.extend(eq)

    daughter_eqns = []
    eq = []
    eq.extend(
        update_rho(['daughter'], ['daughter'], method)
    )
    eq.append(Group(
        equations=[FuncApproximationStd('sample', ['daughter'])]
    ))
    daughter_eqns.extend(eq)
    return mother_eqns, daughter_eqns


def mass_h(dim, no_of_daughters, m0, beta, rho):
    m2 = m0/(1 + beta)
    m1 = beta * m2
    h1 = pow(m1/rho, 1.0/dim)
    h2 = pow(m2/rho, 1.0/dim)
    n = no_of_daughters
    if no_of_daughters%2 == 0:
        n = no_of_daughters - 1
    return [m1] + [m2]*n, [h1] + [h2]*n


def compute_error(N=100, dim=2, kernel=None, plot=False, backend='cython',
                  print_info=False, vacondio=False, no_of_daughters=4):
    if kernel is None:
        from pysph.base.kernels import QuinticSpline as Kernel
        kernel = Kernel(dim=dim)
    dx = 1.0/N
    h = 1.2*dx

    mother, daughter, sample = create_particles(dx, h, dim, no_of_daughters, plot)
    arrays = [mother, daughter, sample]
    mother_eqns, daughter_eqns = create_equations(method='standard')
    mother_eval = SPHEvaluator(
        arrays=arrays, equations=mother_eqns, dim=dim, kernel=kernel
    )

    # Evaluate Mother
    mother_eval.update()
    mother_eval.evaluate()
    mother_f = sample.f_approx.copy()

    # Index of the (0, 0, 0) particle.
    cond = ((abs(mother.x) < 1e-12) &
            (abs(mother.y) < 1e-12) &
            (abs(mother.z) < 1e-12))
    idx = np.where(cond)[0]

    daughter_eval = SPHEvaluator(
        arrays=arrays, equations=daughter_eqns, dim=dim, kernel=kernel
    )
    # Evaluate Daughter
    daughter_eval.update()
    daughter_eval.evaluate()
    daughter_f = sample.f_approx.copy()

    betas = np.linspace(0.1, 1, 20)
    separation = np.linspace(0.1, 1., 40)

    rho = 1.0
    m0 = rho * dx * dx
    error = np.zeros([len(betas), len(separation)])
    xij = np.zeros(3)
    for i, beta in enumerate(betas):
        md, hd = mass_h(dim, no_of_daughters, m0, beta, rho)
        daughter.h[-no_of_daughters:] = hd
        daughter.m[-no_of_daughters:] = md
        mt = md[0] + md[1]
        for j, sep in enumerate(separation):
            size = dx/2
            xd, yd, zd = daughter_coords(sep*size, dim, no_of_daughters)
            daughter.x[-no_of_daughters:] = xd
            xm = (xd[0]*md[0] + xd[1]*md[1]) * mt

            # Update mother
            mother.x[idx] = xm
            if vacondio:
                xij[0] = xd[0] - xm
                rij = np.abs(xij[0])
                w1 = md[0] * kernel.kernel(xij, rij, hd[0])
                xij[0] = xd[1] - xm
                rij = np.abs(xij[0])
                w2 = md[1] * kernel.kernel(xij, rij, hd[1])
                xij[0] = 0.0
                rij = 0.0
                w3 = kernel.kernel(xij, rij, 1.0)
                mother.h[idx] = np.sqrt(w3/(w1 + w2))
            # Evaluate Mother
            mother_eval.update()
            mother_eval.evaluate()
            mother_f = sample.f_approx.copy()

            # Evaluate Daughter
            daughter_eval.update()
            daughter_eval.evaluate()
            daughter_f = sample.f_approx.copy()

            e = global_error(mother_f, daughter_f)

            if print_info:
                print('-----')
                print(f"{beta/h:.2f}, {sep/h:.2f}, {e:.2E}")
            error[i, j] = e
            # if dim < 3:
            #     plt.scatter(daughter.x, daughter.y, marker='.',
            #                 s=daughter.m/max(daughter.m)*50)
            #     plt.scatter(mother.x, mother.y, marker='x',
            #                 s=mother.m/max(mother.m)*50)
            #     plt.xlim(-2*dx, 2*dx)
            #     plt.show()
            #     plt.clf()
            # plt.plot(sample.x, cosx(sample.x)-daughter_f, label=f'sep={sep}')
        #     plt.plot(sample.x, cosx(sample.x))
        #     plt.plot(sample.x, daughter_f, label=f'sep={sep}')
        #     xs, rhos = zip(*sorted(zip(daughter.x, daughter.rho)))
        #     # plt.plot(xs, rhos, label=f'sep={sep}')
        # plt.title(f"Mass ratio = {beta}")
        # plt.legend()
        # plt.show()
        # plt.clf()
    idx = np.unravel_index(np.argmin(error, axis=None), error.shape)
    print("beta\t separation\t error\t")
    print(f"{betas[idx[0]]/h:.2f}\t {separation[idx[1]]/h:.2f}\t {error[idx]:.2E}")
    if plot:
        c = plt.contourf(betas, separation, error.T, levels=10)
        plt.colorbar(c)
        plt.xlabel(r'$\beta=\frac{l_1}{l_2}$, Mass ratio.')
        plt.ylabel('Separation b/w daughter particles.')
        name = '_vacondio' if vacondio else ''
        plt.title(r'L_2($f_{mother} - f_{daughter})$'+f'{name}.')
        plt.grid()
        plt.savefig(f"func_error{name}.png")
        plt.show()

if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('-d', action='store', type=int, dest='no_of_daughters',
                   default=7, help='Number of daughter particles.')
    p.add_argument('--np', action='store', type=float, dest='np',
                   default=50, help='Total no of particles in the domain.')
    p.add_argument('--dim', action='store', type=int, dest='dim',
                   default=2, help='Dimension of the simulation.')
    p.add_argument(
        '--plot', action='store_true', dest='plot',
        default=False, help='Show plots at the end of simulation.'
    )
    p.add_argument(
        '--vacondio', action='store_true', dest='vacondio',
        default=False, help='Use vacondio merging.'
    )
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    o = p.parse_args()

    compute_error(N=o.np, dim=o.dim, backend=o.backend, plot=o.plot,
                  print_info=o.print_info, vacondio=o.vacondio,
                  no_of_daughters=o.no_of_daughters)
