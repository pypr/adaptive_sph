'''Compute optimal mass for a given smoothing length factor, alpha, and daughter
placement factor, epsilon.

'''
import numpy as np
from compyle.utils import ArgumentParser
import matplotlib.pyplot as plt

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.base.kernels import QuinticSpline


class Neighbors(Equation):
    def initialize(self, d_idx, d_n_nbrs):
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, d_n_nbrs):
        d_n_nbrs[d_idx] += 1


def create_particles(dx, perturb=0.0):
    N = int(1/dx)
    x = np.linspace(-1, 1, N)

    m = np.ones_like(x) * dx
    h = np.ones_like(x) * dx

    if perturb > 0:
        np.random.seed(1)
        factor = dx * perturb
        x += np.random.random(x.shape) * factor
    fluid = get_particle_array(name='fluid', x=x, h=h, m=m, rho=1.0)

    props = ['converged', 'omega', 'grhox', 'grhoz', 'wij', 'f', 'f_approx',
             'gradx', 'grady']
    for prop in props:
        fluid.add_property(prop)
    fluid.add_property('n_nbrs', type='int')
    fluid.add_property('iters')
    return fluid


class FuncApproximationStd(Equation):
    def initialize(self, d_f_approx, d_idx):
        d_f_approx[d_idx] = 0

    def loop(self, d_idx, s_idx, d_f_approx, s_f, s_m, s_rho, WJ):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        d_f_approx[d_idx] += mj/rhoj * s_f[s_idx] * WJ


class GradApproximationStd(Equation):
    def initialize(self, d_gradx, d_grady, d_idx):
        d_gradx[d_idx] = 0
        d_grady[d_idx] = 0

    def loop(self, d_idx, s_idx, d_gradx, d_grady, s_p, s_m, s_rho, d_p, DWI):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        fac = mj/rhoj * (s_p[s_idx] - d_p[d_idx])

        d_gradx[d_idx] += fac * DWI[0]
        d_grady[d_idx] += fac * DWI[1]


class SummationDensity(Equation):
    def initialize(self, d_idx, d_rho):
        d_rho[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_m, WIJ):
        d_rho[d_idx] += s_m[s_idx]*WIJ


def create_equations(fluid, kernel):
    eqns = []
    eqns.append(Group(
        equations=[Neighbors(fluid.name, [fluid.name])]
    ))
    eqns.append(Group(
        equations=[SummationDensity(fluid.name, [fluid.name])]
    ))
    eqns.append(Group(
        equations=[FuncApproximationStd(fluid.name, [fluid.name])]
    ))
    eqns.append(Group(
        equations=[GradApproximationStd(fluid.name, [fluid.name])]
    ))

    sph_eval = SPHEvaluator(
        arrays=[fluid], equations=eqns, dim=1,
        kernel=kernel
    )
    return sph_eval


def compute_density(dx=0.05, perturb=0.0):
    kernel = QuinticSpline(dim=1)
    fluid = create_particles(dx, perturb)

    fluid.f[:] = 10*np.cos(2*np.pi*fluid.x)
    fluid.f[:] = fluid.x**0
    sph_eval = create_equations(fluid, kernel)
    sph_eval.update()
    sph_eval.evaluate()
    plt.plot(fluid.x, fluid.f_approx)
    plt.plot(fluid.x, fluid.f, 'r--*')
    plt.plot(fluid.x, fluid.rho, 'k.-')
    plt.plot(fluid.x, fluid.m, 'b.-')
    plt.title("Function approximation")
    plt.show()
    return fluid.rho


if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('--dx', action='store', type=float, dest='dx',
                   default=0.05, help='Particle discretization.')
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    p.add_argument(
        "--perturb", action="store", type=float, dest="perturb", default=0,
        help="Random perturbation of initial particles as a fraction "
        "of dx (setting it to zero disables it, the default)."
    )
    o = p.parse_args()
    compute_density(dx=o.dx, perturb=o.perturb)
