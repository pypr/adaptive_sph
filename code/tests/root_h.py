'''Compute optimal mass for a given smoothing length factor, alpha, and daughter
placement factor, epsilon.

'''
import numpy as np
from compyle.utils import ArgumentParser
import matplotlib.pyplot as plt

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.base.kernels import (CubicSpline, QuinticSpline, WendlandQuintic)
from edac import (SummationDensityGather, SummationDensityScatter,
                  SummationDensity, update_rho, update_smoothing_length)


class Neighbors(Equation):
    def initialize(self, d_idx, d_n_nbrs):
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, d_n_nbrs):
        d_n_nbrs[d_idx] += 1


def create_particles(dx, dim, layers, perturb=0.0):
    if dim != 2:
        raise NotImplementedError("1/3-D is not implemented.")
    size = 40*dx
    sizey = size/2
    start = 0.0
    dx_min = dx
    x, y, m, h = [], [], [], []
    for _ in range(layers):
        _x = np.arange(start, start+size, dx_min) + dx_min/2
        _y = np.arange(-size/2, size/2 + dx_min, dx_min)
        x1, y1 = np.meshgrid(_x, _y)
        x = np.append(x, x1)
        y = np.append(y, y1)

        _m = np.ones_like(x1) * (dx_min)**dim
        _h = np.ones_like(x1) * dx_min

        m = np.append(m, _m)
        h = np.append(h, _h)

        dx_min *= 0.5
        start += size

    total_size = size*layers
    bndry = 5*dx
    cond = ((x > bndry) &
            (x < total_size - bndry) &
            (abs(y) < sizey-3*dx))
    xs, ys, hs, ms = x[~cond], y[~cond], h[~cond], m[~cond]
    x, y, h, m = x[cond], y[cond], h[cond], m[cond]

    if perturb > 0:
        np.random.seed(1)
        factor = dx * perturb
        cond = (abs(y) > 1e-6)
        x[cond] += np.random.random(x[cond].shape) * factor
        y[cond] += np.random.random(x[cond].shape) * factor
    fluid = get_particle_array(name='fluid', x=x, y=y, h=h, m=m)
    solid = get_particle_array(name='solid', x=xs, y=ys, h=hs, m=ms)

    props = ['awhat', 'ravg', 'auhat', 'mT', 'avhat', 'htmp', 'beta', 'gamma',
             'h0', 'rho_k', 'psi', 'arho', 'div', 'ah', 'grhoy', 'dwdh',
             'converged', 'omega', 'grhox', 'grhoz', 'wij']
    for prop in props:
        fluid.add_property(prop)
        solid.add_property(prop)
    fluid.add_property('n_nbrs', type='int')
    fluid.add_property('iters')
    return fluid, solid


def create_equations(fluid, solid, dim, kernel, rho0, method, rho_method):
    eqns = []
    eq = update_smoothing_length(
        [fluid.name], [solid.name, fluid.name], dim, rho0, method, hdx=1.0
    )
    eqns.extend(eq)
    eq = []
    if method in ['yangkong', 'donothing']:
        eq.extend(
            update_rho([fluid.name], [fluid.name, solid.name], rho_method)
        )
    eq.append(Group(
        equations=[Neighbors(fluid.name, [fluid.name])]
    ))
    eqns.extend(eq)
    print(eqns)

    sph_eval = SPHEvaluator(
        arrays=[fluid, solid], equations=eqns, dim=dim,
        kernel=kernel
    )
    return sph_eval


def compute_density(dx=0.05, layers=2, dim=2, perturb=0.0, plot=False,
                    rho0=1.0, method='donothing', rho_method='standard'):
    kernels = [
        CubicSpline,
        QuinticSpline,
        WendlandQuintic,
    ]
    kernels = [kernel(dim=dim) for kernel in kernels]
    fluid, solid = create_particles(dx, dim, layers, perturb)

    dx_min = dx/layers
    indices = np.where((abs(fluid.y) < dx_min/2) & (abs(fluid.x - 2) < 2*dx))
    ns = indices[0]
    for n in ns:
        plt.scatter(fluid.x[n], fluid.y[n], c=fluid.h[n], marker='x')
    plt.scatter(fluid.x, fluid.y, c=fluid.h, s=fluid.m*5000, cmap='RdBu')
    plt.scatter(solid.x, solid.y, s=solid.m*10000, marker='x', cmap='grey')
    hs = np.arange(0.4, 3, 0.1)
    plt.title("Initial particle configuration. (x)-boundary,\
    (o)-fluid.")
    plt.show()

    hdxs = np.arange(0.6, 2.2, 0.2)

    _, ax = plt.subplots(1, 3, figsize=(12, 8))
    for i, kernel in enumerate(kernels):
        sph_eval = create_equations(fluid, solid, dim, kernel, rho0, method,
                                    rho_method)
        hs = np.arange(0.4, 5, 0.1)
        for j, n in enumerate(ns):
            fluid.h[:] = 2.0*fluid.m**(1.0/dim)
            h0 = fluid.h[n]
            print(h0)
            rho = []
            for fac in hs:
                fluid.h[n] = h0 * fac
                sph_eval.update()
                sph_eval.evaluate()
                rho.append(fluid.rho[n])
            ax[i].plot(hs, rho, label=f'{n}')
        ax[i].plot(hs, np.ones_like(hs), '--')
        ax[i].plot(hs, np.ones_like(hs), '--')
        ax[i].set_title(kernel.__class__.__name__)
        ax[i].legend()
        ax[i].set_ylim(1 - 1e-5, 1 + 1e-5)
    plt.show()

    _, ax = plt.subplots(3, 2, figsize=(12, 8))
    for i, kernel in enumerate(kernels):
        sph_eval = create_equations(fluid, solid, dim, kernel, rho0, method,
                                    rho_method)
        for j, hdx in enumerate(hdxs):
            fluid.h[:] = hdx*fluid.m**(1.0/dim)
            solid.h[:] = hdx*solid.m**(1.0/dim)
            idx = np.where(abs(fluid.y) < 1e-6)[0]

            if abs(hdx - 1.2) < 1e-6:
                # Fat
                fluid.h[398] *= 0.991
                fluid.h[399] *= 0.861
                # Thin
                fluid.h[2275] *= 1.378
                fluid.h[2276] *= 1.084

            sph_eval.update()
            sph_eval.evaluate()
            idx = np.where(abs(fluid.y) < 1e-6)[0]
            x, rho, h = fluid.x[idx], fluid.rho[idx], fluid.h[idx]
            xs, rhos, hs = zip(*sorted(zip(x, rho, h)))
            ax[i, 0].plot(xs, rhos, '.-', label=f'{hdx:.1f}')
            ax[i, 1].plot(xs, hs, '.-', label=f'{hdx:.1f}')
            ax[i, 0].set_title(kernel.__class__.__name__)
            ax[i, 1].set_title(kernel.__class__.__name__)
        ax[i, 0].plot(xs, np.ones_like(xs), 'k--')
        ax[i, 0].set_xlim(min(xs)+3*dx, max(xs)-10*dx/2**(layers))
        # ax[i, 0].set_ylim(0.95, 1.05)
        # ax[i, 0].set_xlim(1.42, 1.56)
        ax[i, 0].legend(bbox_to_anchor=(1, 1), loc='upper left', borderaxespad=0.0,
                     fontsize='xx-small')
    plt.show()
    return fluid.rho


if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('--dx', action='store', type=float, dest='dx',
                   default=0.05, help='Particle discretization.')
    p.add_argument('--dim', action='store', type=int, dest='dim',
                   default=2, help='Dimension of the simulation.')
    p.add_argument('--layers', action='store', type=int, dest='layers',
                   default=2, help='No of adaptive layers.')
    p.add_argument(
        "--update-h", action="store", type=str, dest='update_h',
        default='donothing',
        choices=['scatter', 'gather', 'yangkong', 'donothing', 'continuity'],
        help="Choice of method to compute h."
    )
    p.add_argument(
        "--rho-method", action="store", type=str, dest='rho_method',
        default='standard',
        choices=['scatter', 'gather', 'standard'],
        help="Choice of method to compute h."
    )
    p.add_argument(
        '--plot', action='store_true', dest='plot',
        default=False, help='Show plots at the end of simulation'
    )
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    p.add_argument(
        "--perturb", action="store", type=float, dest="perturb", default=0,
        help="Random perturbation of initial particles as a fraction "
        "of dx (setting it to zero disables it, the default)."
    )
    o = p.parse_args()
    compute_density(dx=o.dx, layers=o.layers, dim=o.dim, perturb=o.perturb, plot=o.plot,
                    method=o.update_h, rho_method=o.rho_method)
