"""Simple advection problem.
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from pysph.solver.utils import iter_output, load



from pysph.base.kernels import CubicSpline as Kernel
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.solver.solver import Solver
import pysph.tools.geometry as geom
from pysph.sph.equation import Equation, Group
from pysph.sph.integrator import EulerIntegrator
from pysph.sph.integrator_step import IntegratorStep
from pysph.sph.scheme import add_bool_argument

from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio
from edac import update_rho, update_smoothing_length, ShiftPositionLind


class ComputeAccelerations(Equation):
    def __init__(self, dest, sources, ax, ay, az):
        self.ax = ax
        self.ay = ay
        self.az = az
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_ax, d_ay, d_az):
        d_ax[d_idx] = self.ax
        d_ay[d_idx] = self.ay
        d_az[d_idx] = self.az


class Neighbors(Equation):
    def initialize(self, d_idx, d_n_nbrs):
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, d_n_nbrs):
        d_n_nbrs[d_idx] += 1


class AdvectionStepper(IntegratorStep):
    def initialize(self):
        pass

    def stage1(self, d_idx, d_x, d_y, d_z, d_ax, d_ay, d_az, dt):
        d_x[d_idx] = d_x[d_idx] + dt * d_ax[d_idx]
        d_y[d_idx] = d_y[d_idx] + dt * d_ay[d_idx]
        d_z[d_idx] = d_z[d_idx] + dt * d_az[d_idx]


class Advection(Application):
    def initialize(self):
        self.dim = 2
        self.u = 0.0
        self.v = 0.0
        self.L = 10
        self.U = 1.0
        self.rho0 = 1.0
        self.c0 = 10 * self.U
        self.p0 = self.rho0*self.c0**2

    def add_user_options(self, group):
        group.add_argument(
            "--perturb", action="store", type=float, dest="perturb", default=0,
            help="Random perturbation of initial particles as a fraction "
            "of dx (setting it to zero disables it, the default)."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=30,
            help="Number of points along x direction. (default 30)"
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.0,
            help="Ratio h/dx."
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=False,
            help="Use Vacondio et al. approach for splitting"
        )
        add_bool_argument(
            group, 'shift', dest='shift', default=False,
            help="Shift the particles."
        )
        group.add_argument(
            "--update-h", action="store", type=str, dest='update_h',
            default='yangkong',
            choices=['scatter', 'gather', 'yangkong', 'donothing',
                     'continuity'],
            help="Choice of method to compute h."
        )
        group.add_argument(
            "--sd", action="store", type=str, dest='summation_density',
            default='standard', choices=['gather', 'standard', 'scatter'],
            help="Choice of method to compute density."
        )
        group.add_argument(
            "--layers", action="store", type=int, dest="layers", default=2,
            help="No of adaptive refinement zones in the simulation area."
        )

    def consume_user_options(self):
        nx = self.options.nx
        self.layers = self.options.layers

        self.dx = dx = self.L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        self.h0 = self.hdx * self.dx
        self.dt = 1.0 * self.h0 / (self.U + self.c0)
        self.tf = 2.0
        self.vacondio = self.options.vacondio
        self.update_h = self.options.update_h
        self.shift = self.options.shift
        self.summation_density = self.options.summation_density

    def create_particles(self):
        # create the particles
        dx = self.dx
        x, y = geom.get_2d_block(dx, self.L, self.L, [0, 0])

        if self.options.perturb > 0:
            np.random.seed(1)
            factor = dx * self.options.perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor

        m = self.volume * self.rho0
        h = self.hdx * dx
        p = np.sin(2*np.pi*x/self.L)*np.cos(2*np.pi*y/self.L)

        fluid = get_particle_array(
            name='fluid', x=x, y=y, m=m, h=h, u=self.u, v=self.v,
            rho=self.rho0, p=p
        )

        xs, ys = geom.get_2d_tank(dx, [0, -self.L/2-dx], self.L+2*dx,
                                  self.L+2*dx, 4, top=True)

        ms = self.volume * self.rho0
        hs = self.hdx * dx

        solid = get_particle_array(
            name='solid', x=xs, y=ys, m=ms, h=hs, rho=self.rho0
        )

        props = [
            'V', 'ax', 'ay', 'az', 'auhat', 'avhat', 'p0', 'awhat', 'beta'
        ]

        for p in props:
            fluid.add_property(p)

        fluid.add_property('htmp')
        fluid.add_property('n_nbrs', type='int')
        fluid.add_output_arrays(['h', 'n_nbrs', 'beta'])
        props = ['m_min', 'm_max', 'uhat', 'vhat', 'what', 'vmag', 'rho_k',
                 'psi', 'beta', 'gamma', 'ravg', 'mT', 'h0', 'ah', 'omega',
                 'grhox', 'grhoy', 'dwdh', 'converged', 'div', 'arho', 'grhoz']

        for pa in [fluid, solid]:
            pa.add_property('closest_idx', type='int')
            pa.add_property('split', type='int')
            pa.add_constant('l2norm', [0.0])
            pa.add_constant('iters', [0.0])
            pa.add_constant('vmax', [0.0])
            for prop in props:
                pa.add_property(prop)
            pa.add_output_arrays(['m_min', 'm_max', 'closest_idx', 'split'])
        return [fluid, solid]

    def create_solver(self):
        kernel = Kernel(dim=self.dim)
        integrator = EulerIntegrator(fluid=AdvectionStepper())

        solver = Solver(
            dim=self.dim, integrator=integrator, kernel=kernel
        )
        return solver

    def create_equations(self):
        eqns = []
        fluid = 'fluid'
        all_pa = ['fluid', 'solid']
        eqns.extend(
            update_smoothing_length(
                [fluid], all_pa, self.dim, self.rho0, self.update_h, self.hdx
            )
        )
        eqns.extend(
            update_rho([fluid], all_pa, self.summation_density)
        )

        eqns.append(Group(
            equations=[Neighbors(fluid, all_pa)]
        ))
        if self.shift:
            eqns.append(Group(
                equations=[ShiftPositionLind(fluid, all_pa, dim=self.dim)],
                iterate=True, max_iterations=10
            ))
        eqns.append(Group(
            equations=[ComputeAccelerations(fluid, None, self.u, self.v, 0.0)]
        ))
        return eqns

    def pre_step(self, solver):
        fluid = self.particles[0]
        dx = self.dx
        m0 = self.rho0*dx*dx
        x = fluid.x
        y = fluid.y
        k = 0.5
        fluid.m_max[:] = m0*1.5
        fluid.m_min[:] = m0*0.7
        m = m0
        for i in np.linspace(self.L/2-0.1, 0, self.layers, endpoint=False):
            m *= k
            region = (y > -i) & (y < i) & (x > -i) & (x < i)
            fluid.m_max[region] = m*1.5
            fluid.m_min[region] = m*0.7
        fluid.m_min[region] = m

    def create_tools(self):
        if self.vacondio:
            t = AdaptiveResolutionVacondio(
                self.particles[0], dim=2, has_ghost=False,
                do_mass_exchange=False, freq=1, hybrid=1
            )
        else:
            t = AdaptiveResolution(
                self.particles[0], dim=2, has_ghost=False,
                do_mass_exchange=False, freq=1
            )

        smoothing_sph_eval = t.setup_smoothing(
            'fluid', self.particles, self.nnps, dim=self.dim, rho0=self.rho0
        )

        for i in range(10):
            print("t.pre_step", i)
            self.pre_step(None)
            t.pre_step()
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)

        for i in range(50):
            print("Smoothing", i, end='\r')
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)
        print()

        self.particles[0].h[:] = (self.particles[0].m / self.rho0)**(1/2.0)
        return [t]

    def post_process(self, info_filename):
        files = self.output_files
        t, rho_max, nbr_max = [], [], []
        for sd, array in iter_output(files, 'fluid'):
            _t = sd['t']
            t.append(_t)
            rho_max = np.append(rho_max, [array.rho.max()])
            nbr_max = np.append(nbr_max, [array.n_nbrs.max()])
        rho_max = (rho_max - 1) * 100

        f = load(files[-1])['arrays']['fluid']
        fig, ax = plt.subplots(2, 2, figsize=(8, 6))
        ax = ax.ravel()
        plt.jet()
        im0 = ax[0].scatter(f.x, f.y, c=f.rho, s=1)
        ax[0].set_title('rho')
        fig.colorbar(im0, ax=ax[0])
        hdx = f.h / pow(f.m/self.rho0, 1.0/self.dim)
        ax[1].hist(hdx, bins=10)
        ax[1].set_title('h')
        s2 = ax[2].scatter(f.x, f.y, c=hdx, s=1)
        fig.colorbar(s2, ax=ax[2])
        ax[2].set_title('h')

        color = 'tab:red'
        ax[3].set_xlabel('time (s)')
        ax[3].set_ylabel('rho error', color=color)
        ax[3].plot(range(len(t)), rho_max, color=color)
        ax[3].tick_params(axis='y', labelcolor=color)

        ax2 = ax[3].twinx()

        color = 'tab:blue'
        ax2.set_ylabel('NBRS', color=color)
        ax2.plot(range(len(t)), nbr_max, color=color)
        ax2.tick_params(axis='y', labelcolor=color)

        fig.tight_layout()
        plt.savefig(os.path.join(self.output_dir, 'rho.png'))
        plt.show()



if __name__ == '__main__':
    app = Advection()
    app.run()
    app.post_process(app.info_filename)
