'''Compute the global error in when split, using a fixed mass ratio of center
to outer particle i.e., beta = m_center / m_outer is held a constant.

Run this by:

    $ python split_error.py --openmp --h 1.0 --print --n 7 --plot --beta 1 --center

The option --center will put a one daughter particle at the center, in the above
command, there are 7 daughter particles of which one is placed at the
center. The beta value is fixed at 1.0, --print will print error values for a
corresponding smoothing length, alpha, and separation distance, epsilon.

'''
from math import sqrt
import numpy as np
from compyle.api import declare, annotate, Reduction
from compyle.utils import ArgumentParser
from compyle.array import wrap
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator


class KernelMomentsMother(Equation):
    def __init__(self, dest, sources, dx):
        self.dx = dx
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_y, d_z, d_h_M, d_wij_M,
                  SPH_KERNEL):
        wij_M = declare('double')
        XI = declare('matrix(3)')
        XI[0] = d_x[d_idx]
        XI[1] = d_y[d_idx]
        XI[2] = d_z[d_idx]
        RI = sqrt(XI[0]*XI[0] + XI[1]*XI[1] + XI[2]*XI[2])
        wij_M = SPH_KERNEL.kernel(XI, RI, d_h_M[0])
        d_wij_M[d_idx] = wij_M


class KernelMomentsDaughter(Equation):
    def __init__(self, dest, sources, dx, no_of_daughters):
        self.dx = dx
        self.nod = no_of_daughters
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_y, d_z, d_h_D, d_wij_D, d_daughter_x,
                  d_daughter_y, d_daughter_z, SPH_KERNEL):
        XIJ = declare('matrix(3)')
        RIJ = declare('double')
        i = declare('int')
        for i in range(self.nod):
            XIJ[0] = d_x[d_idx] - d_daughter_x[i]
            XIJ[1] = d_y[d_idx] - d_daughter_y[i]
            XIJ[2] = d_z[d_idx] - d_daughter_z[i]
            RIJ = sqrt(XIJ[0]*XIJ[0] + XIJ[1]*XIJ[1] + XIJ[2]*XIJ[2])

            d_wij_D[d_idx*self.nod + i] = SPH_KERNEL.kernel(XIJ, RIJ, d_h_D[i])


def global_error(mass, c, bk, Qlk):
    return c - 2*np.dot(mass, bk) + np.dot(mass, np.dot(Qlk, mass))


def create_particles(dx, h, dim, no_of_daughters):
    _x = np.arange(-5*h, 5*h, dx)
    xd, yd, zd = np.zeros([3, no_of_daughters])

    alpha = 1.0
    epsilon = 1.0
    if dim == 2:
        x, y = np.meshgrid(_x, _x)
        rad = epsilon*h
        xd, yd, zd = daughter_coords(rad, dim, no_of_daughters)
        dummy = get_particle_array(name='dummy', x=x, y=y, h=h)
    elif dim == 3:
        x, y, z = np.meshgrid(_x, _x, _x)
        rad = epsilon*h
        xd, yd, zd = daughter_coords(rad, dim, no_of_daughters)
        dummy = get_particle_array(name='dummy', x=x, y=y, z=z, h=h)
    else:
        raise NotImplementedError("1-D is not implemented.")

    dummy.add_property('wij_M')
    dummy.add_property('wij_D', stride=no_of_daughters)
    dummy.add_constant('MM', [0.0])
    dummy.add_constant('MD', [0.0]*no_of_daughters)
    dummy.add_constant('DD', [0.0]*no_of_daughters**2)
    dummy.add_constant('h_M', [h])
    dummy.add_constant('h_D', [alpha*h]*no_of_daughters)
    dummy.add_constant('daughter_x', xd)
    dummy.add_constant('daughter_y', yd)
    dummy.add_constant('daughter_z', zd)
    return dummy


def create_equations(dx, dummy, dim, no_of_daughters, kernel):
    eq = []
    eq.append(
        KernelMomentsMother(dummy.name, None, dx=dx)
    )
    mother_eqns = [Group(equations=eq)]

    eq = []
    eq.append(
        KernelMomentsDaughter(dummy.name, None, dx=dx,
                              no_of_daughters=no_of_daughters)
    )
    daughter_eqns = [Group(equations=eq)]

    mother_eval = SPHEvaluator(
        arrays=[dummy], equations=mother_eqns, dim=dim,
        kernel=kernel
    )
    daughter_eval = SPHEvaluator(
        arrays=[dummy], equations=daughter_eqns, dim=dim,
        kernel=kernel
    )
    return mother_eval, daughter_eval


@annotate
def square(i, x, y):
    return x[i] * y[i]


def compute_coeffs(pa, dx, dim, no_of_daughters, backend='cython'):
    n = no_of_daughters
    dxdim = dx**dim

    wij_M = wrap(pa.wij_M, backend=backend)
    r = Reduction('a+b', map_func=square, backend=backend)

    wij_D = []
    for i in range(n):
        wij_D.append(wrap(pa.wij_D[i::n].copy(), backend=backend))

    c = r(wij_M, wij_M) * dxdim

    bk, Qlk = np.zeros(n), np.zeros([n, n])
    for i in range(n):
        bk[i] = r(wij_M, wij_D[i]) * dxdim
        for j in range(n):
            Qlk[i, j] = r(wij_D[i], wij_D[j]) * dxdim
    return c, bk, Qlk


def daughter_coords(radius, dim, no_of_daughters, plot=False, center=False):
    if dim == 2:
        n = no_of_daughters
        if center:
            n = no_of_daughters - 1
        theta = np.linspace(np.pi/n, 2*np.pi+np.pi/n, n, endpoint=False)
        ctheta, stheta = np.cos(theta), np.sin(theta)
        x, y, z = radius * ctheta, radius * stheta, np.zeros_like(theta)
        if center:
            x = np.append([0], x)
            y = np.append([0], y)
            z = np.append([0], z)
    elif dim == 3:
        assert no_of_daughters == 13, "Others config not implemented."
        ang = np.arctan2(0.5*(1 + np.sqrt(5)), 1)
        x1, y1 = radius * np.cos(ang), radius * np.sin(ang)
        verts = np.array([
            [0, 0, 0],
            [0, x1, y1], [0, -x1, y1], [0, x1, -y1], [0, -x1, -y1],
            [x1, 0, y1], [-x1, 0, y1], [x1, 0, -y1], [-x1, 0, -y1],
            [x1, y1, 0], [-x1, y1, 0], [x1, -y1, 0], [-x1, -y1, 0]
        ])
        x, y, z = np.transpose(verts)
    if plot and dim == 2:
        plt.scatter(x, y)
        plt.title('Initial Child particle placement.')
        plt.show()
    return x, y, z


def compute_factors(h=1, dim=2, no_of_daughters=7, kernel=None, center=True,
                    plot=False, backend='cython', print_info=False,
                    beta=1.0):
    if kernel is None:
        from pysph.base.kernels import QuinticSpline as Kernel
        kernel = Kernel(dim=dim)
    m0 = 1.0
    dx = 0.025*h

    dummy = create_particles(dx, h, dim, no_of_daughters)
    xd, yd, zd = daughter_coords(1.0, dim, no_of_daughters, plot, center=center)

    mother_eval, daughter_eval = create_equations(
        dx, dummy, dim, no_of_daughters, kernel
    )
    mother_eval.evaluate()

    alphas = np.linspace(0.4, 1.0, 30) * h
    radius = np.linspace(0.1, 1.0, 20) * h

    if center:
        n = no_of_daughters - 1
        m2 = m0/(n + beta)
        m1 = beta * m2
        mass = [m1] + [m2]*(n)
        mass = np.array(mass)
    else:
        n = no_of_daughters
        mass = np.ones(n) * m0/n
        m2 = m0/(n + beta)
        m1 = beta * m2
    error = np.zeros([len(radius), len(alphas), ])
    for j, alpha in enumerate(alphas):
        for i, rad in enumerate(radius):
            dummy.h_D[0] = alpha
            dummy.h_D[1:] = alpha
            xd, yd, zd = daughter_coords(rad, dim, no_of_daughters, plot=False,
                                         center=center)
            dummy.daughter_x[:] = xd
            dummy.daughter_y[:] = yd
            dummy.daughter_z[:] = zd
            daughter_eval.update()
            daughter_eval.evaluate()
            mass = np.array(mass)
            c, bk, Qlk = compute_coeffs(dummy, dx, dim, no_of_daughters, backend)
            e = global_error(mass, c, bk, Qlk)
            if print_info:
                print('-----')
                print(f"{alpha/h:.2f}, {rad/h:.2f}, {e:.2E}")
            error[i, j] = e
    idx = np.unravel_index(np.argmin(error, axis=None), error.shape)
    print("alpha\t radius\t error\t")
    print(f"{alphas[idx[1]]/h:.2f}\t {radius[idx[0]]/h:.2f}\t {error[idx]:.2E}")
    if plot:
        c = plt.contourf(alphas, radius, error, levels=20, norm=LogNorm())
        plt.colorbar()
        # plt.title('Global error when particles split')
        plt.xlabel(r'$\alpha$, Smoothing length factor.')
        plt.ylabel(r'$\epsilon$, Distance from the parent.')
        plt.grid('on')
        plt.savefig(f"split_error_beta_{beta}.png")
        plt.show()


if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('--n', action='store', type=int, dest='no_of_daughters',
                   default=7, help='Number of daughter particles.')
    p.add_argument('--h', action='store', type=float, dest='h',
                   default=1.2, help='Mother particle smoothing length.')
    p.add_argument('--beta', action='store', type=float, dest='beta',
                   default=1.0, help='Center to outer particle mass ratio.')
    p.add_argument('--dim', action='store', type=int, dest='dim',
                   default=2, help='Dimension of the simulation.')
    p.add_argument(
        '--plot', action='store_true', dest='plot',
        default=False, help='Show plots at the end of simulation.'
    )
    p.add_argument(
        '--center', action='store_true', dest='center',
        default=False, help='Put a particle in the center after splitting.'
    )
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    o = p.parse_args()

    compute_factors(no_of_daughters=o.no_of_daughters, h=o.h, dim=o.dim,
                    center=o.center, backend=o.backend, plot=o.plot,
                    print_info=o.print_info, beta=o.beta)
