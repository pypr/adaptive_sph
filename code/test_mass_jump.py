'''Compute rho, mass average, function and h. Here the particles are setup with
dx resolution on one side and dx/2 resolution with mass, m/2 on the other side.
Plot are generated in the end show the values of the quantities at y = 0 line.
Interpolation is not used here, the y=0 line of particles are held fixed even
when perturbing or shifting.

Run this by:
    $ python test_mass_jump.py --openmp --plot --perturb 0.0 --shift 0 -n 100
'''
import numpy as np
from numpy import cos, pi, sin, exp
from compyle.utils import ArgumentParser
import matplotlib.pyplot as plt

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.base.kernels import (CubicSpline, QuinticSpline, WendlandQuintic)
from edac import (SummationDensityGather, SummationDensityScatter,
                  SummationDensity, ShiftPositionLind)


class FuncApproximationStd(Equation):
    def initialize(self, d_f_approx, d_idx):
        d_f_approx[d_idx] = 0

    def loop(self, d_idx, s_idx, d_f_approx, s_f, s_m, s_rho, WIJ):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        d_f_approx[d_idx] += mj/rhoj * s_f[s_idx] * WIJ


class OptimizeH(Equation):
    def __init__(self, dest, sources, dim, rho0, hdx):
        self.dim = dim
        self.rho0 = rho0
        self.hdx = hdx
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_n_nbrs, d_m_avg):
        d_n_nbrs[d_idx] = 0
        d_m_avg[d_idx] = 0.0

    def loop(self, d_idx, d_n_nbrs, s_m, s_idx, d_m_avg):
        d_n_nbrs[d_idx] += 1
        d_m_avg[d_idx] += s_m[s_idx]

    def post_loop(self, d_idx, d_h, d_m_avg, d_n_nbrs):
        d_m_avg[d_idx] = d_m_avg[d_idx] / d_n_nbrs[d_idx]
        mavg = d_m_avg[d_idx]
        d_h[d_idx] = self.hdx * pow(mavg/self.rho0, 1.0/self.dim)


def create_particles(dx, dim, perturb=0.0):
    _x = np.arange(0, 50*dx, dx)
    _y = np.arange(-50*dx, 50*dx, dx)
    x, y = np.meshgrid(_x, _y)

    dx_min = dx/np.sqrt(2)
    _x = np.arange(-50*dx, -dx_min/2, dx_min)
    fac = int(50*np.sqrt(2))
    _y = np.arange(-fac*dx_min, fac*dx_min, dx_min)
    x1, y1 = np.meshgrid(_x, _y)
    x = np.append(x, x1)
    y = np.append(y, y1)

    if dim == 2:
        m = np.ones_like(x) * (dx)**dim
        m[x<0.0] = (dx_min)**dim

        h = np.ones_like(x) * dx
        h[x<0.0] = dx_min

        cond = ((abs(x) < 35*dx) & (abs(y) < 35*dx))
        xs, ys, hs, ms = x[~cond], y[~cond], h[~cond], m[~cond]
        x, y, h, m = x[cond], y[cond], h[cond], m[cond]

        if perturb > 0:
            np.random.seed(1)
            factor = dx * perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor
        fluid = get_particle_array(name='fluid', x=x, y=y, h=h, m=m, rho=1.0)
        solid = get_particle_array(name='solid', x=xs, y=ys, h=hs, m=ms, rho=1.0)
    else:
        raise NotImplementedError("1/3-D is not implemented.")

    props = ['awhat', 'ravg', 'auhat', 'mT', 'avhat',
             'n_nbrs', 'wij', 'm_avg', 'f', 'h_tmp', 'f_approx']
    for prop in props:
        fluid.add_property(prop)
        solid.add_property(prop)

    fluid.f[:] = cos(20*pi*fluid.x) + sin(2*pi*fluid.x)
    solid.f[:] = cos(20*pi*solid.x) + sin(2*pi*solid.x)

    plt.scatter(solid.x, solid.y, c=solid.h, s=solid.m*50000, marker='x',
                cmap='RdBu')
    plt.scatter(fluid.x, fluid.y, c=fluid.h, s=fluid.m/max(fluid.m)*10,
                cmap='RdBu')
    plt.show()
    return fluid, solid


def shift_equations(fluid, solid, dim, kernel):
    eq = []
    eqns = []
    eq.append(ShiftPositionLind(
        fluid.name, [solid.name, fluid.name], dim=dim
    ))
    eqns.append(Group(equations=eq))
    shift_eval = SPHEvaluator(
        arrays=[fluid, solid], equations=eqns, dim=dim,
        kernel=kernel
    )
    return shift_eval


def create_equations(fluid, solid, dim, rho0, hdx, kernel, iters=5):
    fname = fluid.name
    sname = solid.name
    eq, eqns = [], []
    eq.append(Group(
        equations=[SummationDensity(fname, [fname, sname])]
    ))
    eqns.extend(eq)
    eq = []
    eq.append(
        OptimizeH(fname, [fname, sname], dim, rho0, hdx)
    )
    eqns.append(
        Group(equations=eq, iterate=True, min_iterations=iters,
              max_iterations=iters)
    )
    eq = []
    eq.append(
        SummationDensity(fname, [fname, sname])
    )
    eqns.append(Group(equations=eq))

    eqns.append(Group(
        equations=[FuncApproximationStd(fname, [fname, sname])]
    ))
    print(eqns)

    sph_eval = SPHEvaluator(
        arrays=[fluid, solid], equations=eqns, dim=dim,
        kernel=kernel
    )
    return sph_eval


def compute_density(n=50, dim=2, perturb=0.0, plot=False, shift=0):
    dx = 1.0/n
    kernels = [
        CubicSpline, QuinticSpline, WendlandQuintic,
    ]
    kernels = [kernel(dim=dim) for kernel in kernels]
    fluid, solid = create_particles(dx, dim, perturb)

    if shift > 0:
        shift_eval = shift_equations(fluid, solid, dim, kernels[1])
        for i in range(shift):
            print('Shifting', i)
            shift_eval.update()
            shift_eval.evaluate(dt=0.04)
        plt.scatter(solid.x, solid.y, c='r', s=1)
        plt.scatter(fluid.x, fluid.y, c=fluid.h, s=fluid.m*5000)
        plt.show()

    rho0 = 1.0
    hdx = 1.0
    density = np.zeros(len(kernels))
    sph_eval = create_equations(fluid, solid, dim, rho0, hdx, kernels[1],
                                iters=100)
    sph_eval.update()
    sph_eval.evaluate()
    if plot:
        cond = (abs(fluid.y) < 1e-12)
        idx = np.where(cond)[0]
        xs, rhos, mavg, hs, f_approx, f_exact = zip(*sorted(zip(
            fluid.x[idx], fluid.rho[idx],
            fluid.m_avg[idx], fluid.h[idx],
            fluid.f_approx[idx], fluid.f[idx]
        )))
        plt.figure(figsize=(10, 6))
        plt.subplot(221)
        plt.plot(xs, rhos)
        plt.ylabel('rho')
        plt.subplot(222)
        plt.plot(xs, mavg)
        plt.ylabel('m_avg')
        plt.subplot(223)
        plt.plot(xs, hs)
        plt.ylabel('h')
        plt.subplot(224)
        plt.plot(xs, f_approx)
        plt.plot(xs, f_exact)
        plt.ylabel('f')
        plt.xlim(-0.1, 0.1)
        plt.show()
    return density


if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('-n', action='store', type=float, dest='n',
                   default=50, help='Particle discretization.')
    p.add_argument('--dim', action='store', type=int, dest='dim',
                   default=2, help='Dimension of the simulation.')
    p.add_argument('--shift', action='store', type=int, dest='shift',
                   default=2, help='No of times to Shift the particles.')
    p.add_argument(
        '--plot', action='store_true', dest='plot',
        default=False, help='Show plots at the end of simulation'
    )
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    p.add_argument(
        "--perturb", action="store", type=float, dest="perturb", default=0,
        help="Random perturbation of initial particles as a fraction "
        "of dx (setting it to zero disables it, the default)."
    )
    o = p.parse_args()
    compute_density(n=o.n, dim=o.dim, perturb=o.perturb, plot=o.plot,
                    shift=o.shift)
