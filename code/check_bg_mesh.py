import numpy as np

from pysph.base.utils import get_particle_array

from adapt import UpdateBackground, setup_properties


def create_fluid(dx):
    x, y = np.mgrid[-10:10:dx, -10:10:dx]
    m = dx*dx
    fluid = get_particle_array(name='fluid', x=x, y=y, rho=1.0, m=m, h=dx)
    for prop in ('m_ref', 'uhat', 'vhat', 'what', 'au', 'au',
                 'au', 'vmag'):
        fluid.add_property(prop)
    fluid.add_property('fixed', type='int')
    setup_properties([fluid])
    return fluid


def create_solid(dx):
    x, y = np.mgrid[-0.5:0.5:dx, -0.5:0.5:dx]
    solid = get_particle_array(name='solid', x=x, y=y)
    solid.add_constant('ds_min', dx)
    return solid


def main(dx_max=0.4, dx_min=0.05, cr=1.2):
    f = create_fluid(dx_max)
    s = create_solid(dx_min)
    t = UpdateBackground(
        f, dim=2, boundary=[s], ds_min=dx_min, ds_max=dx_max, cr=cr,
        rho_ref=1.0
    )
    return f, s, t.bg_pa, t
