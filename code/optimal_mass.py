'''Compute optimal mass for a given smoothing length factor, alpha, and
daughter placement factor, epsilon.

'''
from math import sqrt
import numpy as np
from scipy.optimize import minimize
from compyle.api import declare, annotate, Reduction
from compyle.utils import ArgumentParser
from compyle.array import wrap
import matplotlib.pyplot as plt

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator


class KernelMomentsMother(Equation):
    def __init__(self, dest, sources, dx):
        self.dx = dx
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_y, d_z, d_h_M, d_wij_M,
                  SPH_KERNEL):
        wij_M = declare('double')
        XI = declare('matrix(3)')
        XI[0] = d_x[d_idx]
        XI[1] = d_y[d_idx]
        XI[2] = d_z[d_idx]
        RI = sqrt(XI[0]*XI[0] + XI[1]*XI[1] + XI[2]*XI[2])
        wij_M = SPH_KERNEL.kernel(XI, RI, d_h_M[0])
        d_wij_M[d_idx] = wij_M


class KernelMomentsDaughter(Equation):
    def __init__(self, dest, sources, dx, no_of_daughters):
        self.dx = dx
        self.nod = no_of_daughters
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_y, d_z, d_h_D, d_wij_D, d_daughter_x,
                  d_daughter_y, d_daughter_z, SPH_KERNEL):
        XIJ = declare('matrix(3)')
        RIJ = declare('double')
        i = declare('int')
        for i in range(self.nod):
            XIJ[0] = d_x[d_idx] - d_daughter_x[i]
            XIJ[1] = d_y[d_idx] - d_daughter_y[i]
            XIJ[2] = d_z[d_idx] - d_daughter_z[i]
            RIJ = sqrt(XIJ[0]*XIJ[0] + XIJ[1]*XIJ[1] + XIJ[2]*XIJ[2])

            d_wij_D[d_idx*self.nod + i] = SPH_KERNEL.kernel(XIJ, RIJ, d_h_D[i])


def global_error(x, c, bk, Qlk, no_of_daughters):
    n = no_of_daughters - 1
    mass = np.append([x[0]], [x[1]/n]*n)
    return c - 2*np.dot(mass, bk) + np.dot(mass, np.dot(Qlk, mass))


def mass_conserve(x, no_of_daughters):
    n = no_of_daughters - 1
    mass = np.append([x[0]], [x[1]/n]*n)
    return np.sum(mass) - 1


def density_conserve(x, no_of_daughters, dim, h):
    n = no_of_daughters - 1
    mass = np.append([x[0]], [x[1]/n]*n)
    rho = mass/h**dim
    return np.sum(rho) - 1.0


def create_particles(dx, h, dim, no_of_daughters):
    _x = np.arange(-5*h, 5*h, dx)
    xd, yd, zd = np.zeros([3, no_of_daughters])

    alpha = 1.0
    epsilon = 1.0
    if dim == 2:
        x, y = np.meshgrid(_x, _x)
        rad = epsilon*h
        xd, yd, zd = daughter_coords(rad, dim, no_of_daughters)
        dummy = get_particle_array(name='dummy', x=x, y=y, h=h)
    elif dim == 3:
        x, y, z = np.meshgrid(_x, _x, _x)
        rad = epsilon*h
        xd, yd, zd = daughter_coords(rad, dim, no_of_daughters)
        dummy = get_particle_array(name='dummy', x=x, y=y, z=z, h=h)
    else:
        raise NotImplementedError("1-D is not implemented.")

    dummy.add_property('wij_M')
    dummy.add_property('wij_D', stride=no_of_daughters)
    dummy.add_constant('MM', [0.0])
    dummy.add_constant('MD', [0.0]*no_of_daughters)
    dummy.add_constant('DD', [0.0]*no_of_daughters**2)
    dummy.add_constant('h_M', [h])
    dummy.add_constant('h_D', [alpha*h]*no_of_daughters)
    dummy.add_constant('daughter_x', xd)
    dummy.add_constant('daughter_y', yd)
    dummy.add_constant('daughter_z', zd)
    return dummy


def create_equations(dx, dummy, dim, no_of_daughters, kernel):
    eq = []
    eq.append(
        KernelMomentsMother(dummy.name, None, dx=dx)
    )
    mother_eqns = [Group(equations=eq)]

    eq = []
    eq.append(
        KernelMomentsDaughter(dummy.name, None, dx=dx,
                              no_of_daughters=no_of_daughters)
    )
    daughter_eqns = [Group(equations=eq)]

    mother_eval = SPHEvaluator(
        arrays=[dummy], equations=mother_eqns, dim=dim,
        kernel=kernel
    )
    daughter_eval = SPHEvaluator(
        arrays=[dummy], equations=daughter_eqns, dim=dim,
        kernel=kernel
    )
    return mother_eval, daughter_eval


@annotate
def square(i, x, y):
    return x[i] * y[i]


def compute_coeffs(pa, dx, dim, no_of_daughters, backend='cython'):
    n = no_of_daughters
    dxdim = dx**dim

    wij_M = wrap(pa.wij_M, backend=backend)
    r = Reduction('a+b', map_func=square, backend=backend)

    wij_D = []
    for i in range(n):
        wij_D.append(wrap(pa.wij_D[i::n].copy(), backend=backend))

    c = r(wij_M, wij_M) * dxdim

    bk, Qlk = np.zeros(n), np.zeros([n, n])
    for i in range(n):
        bk[i] = r(wij_M, wij_D[i]) * dxdim
        for j in range(n):
            Qlk[i, j] = r(wij_D[i], wij_D[j]) * dxdim
    return c, bk, Qlk


def daughter_coords(radius, dim, no_of_daughters, plot=False):
    if dim == 2:
        n = no_of_daughters - 1
        theta = np.linspace(np.pi/n, 2*np.pi+np.pi/n, n, endpoint=False)
        ctheta, stheta = np.cos(theta), np.sin(theta)
        x, y, z = radius * ctheta, radius * stheta, np.zeros_like(theta)
        x = np.append([0], x)
        y = np.append([0], y)
        z = np.append([0], z)
    elif dim == 3:
        assert no_of_daughters == 13, "Others config not implemented."
        ang = np.arctan2(0.5*(1 + np.sqrt(5)), 1)
        x1, y1 = radius * np.cos(ang), radius * np.sin(ang)
        verts = np.array([
            [0, 0, 0],
            [0, x1, y1], [0, -x1, y1], [0, x1, -y1], [0, -x1, -y1],
            [x1, 0, y1], [-x1, 0, y1], [x1, 0, -y1], [-x1, 0, -y1],
            [x1, y1, 0], [-x1, y1, 0], [x1, -y1, 0], [-x1, -y1, 0]
        ])
        x, y, z = np.transpose(verts)
    if plot and dim == 2:
        plt.scatter([0], [0], s=3500, facecolors='none', edgecolors='k')
        plt.scatter(x, y, s=500, c='tab:red')
        plt.axis('equal')
        plt.axis('off')
        n = no_of_daughters
        plt.savefig(f'particle_config_{n}', transparent=True)
        plt.show()
        plt.clf()
    return x, y, z


def compute_factors(h=1, dim=2, no_of_daughters=7, kernel=None, plot=False,
                    backend='cython', print_info=False):
    if kernel is None:
        from pysph.base.kernels import QuinticSpline as Kernel
        kernel = Kernel(dim=dim)
    dx = 0.05*h

    dummy = create_particles(dx, h, dim, no_of_daughters)
    xd, yd, zd = daughter_coords(1.0, dim, no_of_daughters, plot)

    mother_eval, daughter_eval = create_equations(
        dx, dummy, dim, no_of_daughters, kernel
    )
    mother_eval.evaluate()

    alphas = np.arange(0.3, 1.0, 0.02) * h
    radius = np.arange(0.3, 1.0, 0.02) * h

    error = np.zeros([len(alphas), len(radius)])
    mass = np.zeros([len(alphas), len(radius), 2])
    mass_ratio = np.zeros([len(alphas), len(radius)])

    n = no_of_daughters - 1
    x0 = [1/n]*2
    bounds = ((0, 1), (0, 1))
    for i, alpha in enumerate(alphas):
        for j, rad in enumerate(radius):
            dummy.h_D[:] = alpha
            xd, yd, zd = daughter_coords(rad, dim, no_of_daughters, plot=False)
            dummy.daughter_x[:] = xd
            dummy.daughter_y[:] = yd
            dummy.daughter_z[:] = zd
            daughter_eval.update()
            daughter_eval.evaluate()
            c, bk, Qlk = compute_coeffs(dummy, dx, dim, no_of_daughters,
                                        backend)
            cons = (
                {'type': 'eq', 'fun': mass_conserve,
                 'args': (no_of_daughters,)},
            )
            e = minimize(global_error, x0, args=(c, bk, Qlk, no_of_daughters),
                         bounds=bounds, constraints=cons, method='SLSQP')
            if print_info:
                print('-----')
                print(f"{alpha/h:.2f}, {rad/h:.2f}, {e.fun:.2E}, "
                      f"{e.x[0]:.4f} {e.x[1]/n:.4f}")
            error[i, j] = e.fun
            mass[i, j, :] = e.x
            mass_ratio[i, j] = e.x[0]/e.x[1]
    idx = np.unravel_index(np.argmin(error, axis=None), error.shape)
    print("alpha\t radius\t error\t\t l1\t l2")
    print(f"{alphas[idx[0]]/h:.2f}\t {radius[idx[1]]/h:.2f}\t {error[idx]:.2E}"
          f"\t {mass[idx][0]:.4f}\t {mass[idx][1]/n:.4f}")
    if plot:
        c = plt.contourf(radius, alphas, np.log10(error),
                         np.linspace(-8, 0, 21))
        plt.colorbar(c)
        plt.xlabel(r'$\epsilon$, Distance from the parent.')
        plt.ylabel(r'$\alpha$, Smoothing length factor.')
        plt.grid()
        plt.savefig("global_error.png", transparent=True)
        plt.show()
        c = plt.contour(radius, alphas, mass_ratio, np.linspace(0.01, 1.0, 21))
        plt.clabel(c, inline=True, fontsize=6)
        plt.xlabel(r'$\epsilon$, Distance from the parent.')
        plt.ylabel(r'$\alpha$, Smoothing length factor')
        plt.grid()
        plt.savefig("optimal_mass.png", transparent=True)
        plt.show()
    return mass[idx, 0], mass[idx, 1]/n


def daughter_plots(radius=1.0, dim=2.0):
    fig, ax = plt.subplots(1, 3)
    i = 0
    for n in [4, 5, 7]:
        x, y, _ = daughter_coords(radius, dim, n, plot=False)
        ax[i].plot(x, y, '.')
        ax[i].tick_params(
            top=False, bottom=False, left=False, right=False,
            labelbottom=False, labelleft=False
        )
        ax[i].set_aspect('equal')
        i += 1
    plt.show()
    fig.savefig("daughter_positions.pdf")


if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('-n', action='store', type=int, dest='no_of_daughters',
                   default=7, help='Number of daughter particles.')
    p.add_argument('--h', action='store', type=float, dest='h',
                   default=1.2, help='Mother particle smoothing length.')
    p.add_argument('--dim', action='store', type=int, dest='dim',
                   default=2, help='Dimension of the simulation.')
    p.add_argument(
        '--plot', action='store_true', dest='plot',
        default=False, help='Show plots at the end of simulation'
    )
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    o = p.parse_args()

    compute_factors(no_of_daughters=o.no_of_daughters, h=o.h, dim=o.dim,
                    backend=o.backend, plot=o.plot, print_info=o.print_info)
