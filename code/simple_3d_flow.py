import numpy as np

from pysph.examples.trivial_inlet_outlet import InletOutletApp
from pysph.base.utils import get_particle_array
from pysph.base.kernels import QuinticSpline
from pysph.solver.solver import Solver
from pysph.sph.integrator import PECIntegrator
from pysph.sph.bc.inlet_outlet_manager import (
    OutletStep, InletStep
)

from adapt import AdaptiveResolution


class SimpleFlow3D(InletOutletApp):

    def create_particles(self):
        # Initially fluid has no particles -- these are generated by the inlet.
        fluid = get_particle_array(name='fluid')

        # Setup the inlet particle array with just the particles we need at the
        dx = 0.1
        x, y, z = np.mgrid[-1+dx/2: 0: dx, 0:1:dx, 0:1:dx]
        self.m0 = dx*dx*dx
        m = self.m0
        h = dx*1.5
        rho = 1.0

        u = self.options.speed

        inlet = get_particle_array(name='inlet', x=x, y=y, z=z,
                                   m=m, h=h, u=u, rho=rho)
        x += 2.0
        outlet = get_particle_array(name='outlet', x=x, y=y, z=z,
                                    m=m, h=h, u=u, rho=rho)

        particles = [inlet, fluid, outlet]

        props = ['ioid', 'disp', 'x0']
        for p in props:
            for pa in particles:
                pa.add_property(p)

        for pa in particles:
            for prop in ('uhat', 'vhat', 'what', 'm_ref'):
                pa.add_property(prop)
            pa.add_property('closest_idx', type='int')
            pa.add_property('split', type='int')
            pa.add_output_arrays(['m_ref', 'closest_idx', 'split'])
        return particles

    def create_solver(self):
        self.iom = self._create_inlet_outlet_manager()
        kernel = QuinticSpline(dim=3)
        integrator = PECIntegrator(
            fluid=InletStep(), inlet=InletStep(),
            outlet=OutletStep()
        )
        self.iom.active_stages = [2]
        self.iom.setup_iom(dim=3, kernel=kernel)
        self.iom.update_dx(dx=0.1)
        dt = 1e-2
        tf = 12

        solver = Solver(
            kernel=kernel, dim=3, integrator=integrator, dt=dt, tf=tf,
            adaptive_timestep=False, pfreq=20
        )
        return solver

    def pre_step(self, solver):
        f = self.particles[1]
        m0 = self.m0
        x = f.x
        y = f.y
        z = f.z
        f.m_ref[:] = m0
        region = ((x > 0.25) & (x < 0.75) & (y > 0.25) &
                  (y < 0.75) & (z > 0.25) & (z < 0.75))
        f.m_ref[region] = m0*0.5
        region = ((x > 0.375) & (x < 0.625) & (y > 0.375) & (y < 0.625) &
                  (z > 0.375) & (z < 0.625))
        f.m_ref[region] = m0*0.25

    def create_tools(self):
        t = AdaptiveResolution(self.particles[1], dim=3, rho0=1.0)
        return [t]


if __name__ == '__main__':
    app = SimpleFlow3D()
    app.run()
