'''
Vacondio formulation.
'''
from compyle.api import Reduction
from math import sqrt, exp
import numpy
from compyle.api import declare, annotate
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group, MultiStageEquations
from pysph.sph.scheme import Scheme
from pysph.sph.integrator import Integrator
from pysph.solver.solver import Solver
from pysph.base.reduce_array import serial_reduce_array


class PECIntegrator(Integrator):
    def one_timestep(self, t, dt):
        self.initialize()

        # Predict
        self.stage1()
        self.update_domain()

        # Call any post-stage functions.
        self.do_post_stage(0.5*dt, 1)

        self.compute_accelerations(0)

        # Correct
        self.stage2()

        self.compute_accelerations(1)

        self.update_domain()

        self.do_post_stage(dt, 2)


class ComputeGamma(Equation):
    def initialize(self, d_idx, d_gamma):
        d_gamma[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_rho, d_gamma, d_beta, d_h, s_h, SPH_KERNEL,
             RIJ, VIJ, XIJ):
        fac =  s_m[s_idx]/(d_rho[d_idx] * d_beta[d_idx])
        dwdq = SPH_KERNEL.dwdq(RIJ, s_h[s_idx])
        dwdr = 1.0/d_h[d_idx] * dwdq
        vijdotxij = VIJ[0] * XIJ[0] + VIJ[1] * XIJ[1] + VIJ[2] * XIJ[2]
        d_gamma[d_idx] += fac * dwdr * vijdotxij


@annotate
def square(i, x):
    return x[i] * x[i]


class ComputeResidual(Equation):
    def __init__(self, dest, sources, tolerance, max_iterations):
        self.tol = tolerance
        self.count = 0
        self.conv = 0
        self.max_iterations = max_iterations
        self.l2norm = 0.0
        self.reduction_linf = Reduction('max(a, b)', backend='cython')
        self.reduction_l2norm = Reduction('a+b', map_func=square, backend='cython')
        super().__init__(dest, sources)

    def reduce(self, dst, t, dt):
        from compyle.api import wrap

        self.count += 1
        dst.iters[0] = self.count

        psi = declare('object')
        psi = wrap(dst.psi, backend='cython')
        self.l2norm = sqrt(self.reduction_l2norm(psi)/psi.length)
        self.conv = 1 if self.l2norm < self.tol else -1

    def converged(self):
        # print(self.l2norm, self.count)
        if self.conv == 1 and self.count < self.max_iterations:
            # print(self.l2norm, self.count)
            self.count = 0
        if self.count >= self.max_iterations:
            self.count = 0
            print("Max iterations exceeded")
        return self.conv


class ContinuityEquationH(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_ah):
        d_ah[d_idx] = 0.0

    def loop(self, d_idx, d_ah, d_h, DWI, DWJ, VIJ):
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWJ[0] + DWI[0])
        dwij[1] = 0.5 * (DWJ[1] + DWI[1])
        dwij[2] = 0.5 * (DWJ[2] + DWI[2])
        vijdotdwij = dwij[0]*VIJ[0] + dwij[1]*VIJ[1] + dwij[2]*VIJ[2]
        d_ah[d_idx] += d_h[d_idx]/self.dim * vijdotdwij

    def post_loop(self, d_idx, d_ah, d_h, dt):
        d_h[d_idx] = d_h[d_idx] + dt * dt * d_ah[d_idx]


class ComputeBeta(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_beta):
        # FIXME: Should the value be initialized to 1.0 or 0.0.
        d_beta[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_beta, d_h, SPH_KERNEL, RIJ):
        mj = s_m[s_idx]
        dim_1 = 1.0/self.dim
        fac = mj * RIJ * dim_1
        dwdq = SPH_KERNEL.dwdq(RIJ, d_h[d_idx])
        dwdr = 1.0/d_h[d_idx] * dwdq
        d_beta[d_idx] += -fac * dwdr

    def post_loop(self, d_idx, d_rho, d_beta):
        # Division by rho in post_loop helps in putting this together with
        # summation density in the groups.
        if d_rho[d_idx] > 1e-16:
            d_beta[d_idx] /= d_rho[d_idx]
        if abs(d_beta[d_idx]) < 1e-16:
            d_beta[d_idx] = 1.0


class ContinuityEquationVacondio(Equation):
    def __init__(self, dest, sources, c0, xi):
        self.c0 = c0
        self.xi = xi
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_arho):
        d_arho[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_rho, d_arho, s_m, d_h, XIJ, R2IJ, VIJ,
             EPS, DWJ):
        mj = s_m[s_idx]
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]

        fac = rhoi * mj / rhoj

        vijdotdwj = VIJ[0]*DWJ[0] + VIJ[1]*DWJ[1] + VIJ[2]*DWJ[2]
        d_arho[d_idx] += fac * vijdotdwj

        tmp = (rhoj/rhoi - 1.0)/(R2IJ + EPS)
        psiij = tmp * (DWJ[0]*XIJ[0] + DWJ[1]*XIJ[1] + DWJ[2]*XIJ[2])
        d_arho[d_idx] += self.xi * d_h[d_idx] * self.c0 * mj / rhoj * psiij


class ContinuityEquationVacondioAdaptH(Equation):
    def post_loop(self, d_idx, d_arho, d_beta):
        d_arho[d_idx] = d_arho[d_idx]/d_beta[d_idx]


class MomentumEquationVacondio(Equation):
    def __init__(self, dest, sources, c0, nu, gx=0., gy=0., gz=0.0):
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.c0 = c0
        self.nu = nu
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_rho, d_au, d_av, d_aw, d_p, s_p, s_m,
             XIJ, R2IJ, VIJ, EPS, DWI, DWJ):
        mj = s_m[s_idx]
        rhoij = d_rho[d_idx] * s_rho[s_idx]
        pi = d_p[d_idx]
        pj = s_p[s_idx]

        fac = mj / rhoij

        d_au[d_idx] -= fac * (pi * DWJ[0] + pj * DWI[0])
        d_av[d_idx] -= fac * (pi * DWJ[1] + pj * DWI[1])
        d_aw[d_idx] -= fac * (pi * DWJ[2] + pj * DWI[2])

        rhoij = (d_rho[d_idx] + s_rho[s_idx])
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWJ[0] + DWI[0])
        dwij[1] = 0.5 * (DWJ[1] + DWI[1])
        dwij[2] = 0.5 * (DWJ[2] + DWI[2])
        Fij = dwij[0]*XIJ[0] + dwij[1]*XIJ[1] + dwij[2]*XIJ[2]

        tmp = mj * 4 * self.nu * Fij/(rhoij * (R2IJ + EPS))

        d_au[d_idx] += tmp * VIJ[0]
        d_av[d_idx] += tmp * VIJ[1]
        d_aw[d_idx] += tmp * VIJ[2]

    def post_loop(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] += self.gx
        d_av[d_idx] += self.gy
        d_aw[d_idx] += self.gz


class ShiftForceVacondio(Equation):
    def __init__(self, dest, sources, cs, cfl):
        # FIXME: This shift factor is not throughly tested.
        self.shift_factor = cfl/cs/cs
        self.reduction = Reduction('max(a, b)', backend='cython')
        super().__init__(dest, sources)

    def py_initialize(self, dst, t, dt):
        from compyle.api import wrap
        vmag = wrap(dst.vmag, backend='cython')
        dst.vmax[0] = self.reduction(vmag)

    def initialize(self, d_idx, d_auhat, d_avhat, d_awhat, d_ravg, d_mT,
                   d_n_nbrs):
        d_auhat[d_idx] = 0.0
        d_avhat[d_idx] = 0.0
        d_awhat[d_idx] = 0.0
        d_ravg[d_idx] = 0.0
        d_mT[d_idx] = 0.0
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, s_idx, s_m, d_auhat, d_avhat, d_awhat, d_n_nbrs,
             d_ravg, d_mT, RIJ, XIJ):
        mj = s_m[s_idx]
        r3ij = RIJ * RIJ * RIJ

        d_ravg[d_idx] += RIJ
        d_mT[d_idx] += mj
        d_n_nbrs[d_idx] += 1

        # Don't use EPS here instead of 1e-8, since eps = 0.01 * HIJ**2,
        # when the value of HIJ is very small the value of tmp increases
        # and spoils the solution.
        if r3ij > 1e-9:
            tmp = mj / r3ij
            d_auhat[d_idx] += tmp * XIJ[0]
            d_avhat[d_idx] += tmp * XIJ[1]
            d_awhat[d_idx] += tmp * XIJ[2]

    def post_loop(self, d_idx, d_mT, d_ravg, d_auhat, d_avhat, d_awhat,
                  d_n_nbrs, d_vmax, dt):
        d_mT[d_idx] /= d_n_nbrs[d_idx]
        d_ravg[d_idx] /= d_n_nbrs[d_idx]
        mT = d_mT[d_idx]
        ravg = d_ravg[d_idx]
        ravg2 = ravg*ravg
        # The fac has a dt factor which is omitted here to update the velocites
        # instead of the positions.
        fac = self.shift_factor / mT * ravg2 * d_vmax[0] / dt
        d_auhat[d_idx] = fac * d_auhat[d_idx]
        d_avhat[d_idx] = fac * d_avhat[d_idx]
        d_awhat[d_idx] = fac * d_awhat[d_idx]


class ShiftPositionVacondio(Equation):
    def __init__(self, dest, sources, shift_factor):
        self.beta = shift_factor
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_auhat, d_avhat, d_awhat, d_ravg, d_mT,
                   d_n_nbrs):
        d_auhat[d_idx] = 0.0
        d_avhat[d_idx] = 0.0
        d_awhat[d_idx] = 0.0
        d_ravg[d_idx] = 0.0
        d_mT[d_idx] = 0.0
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, s_idx, s_m, d_auhat, d_avhat, d_awhat, d_n_nbrs,
             d_ravg, d_mT, RIJ, XIJ):
        mj = s_m[s_idx]
        r3ij = RIJ * RIJ * RIJ

        d_ravg[d_idx] += RIJ
        d_mT[d_idx] += mj
        d_n_nbrs[d_idx] += 1

        # Don't use EPS here instead of 1e-8, since eps = 0.01 * HIJ**2,
        # when the value of HIJ is very small the value of tmp increases
        # and spoils the solution.
        if r3ij > 1e-9:
            tmp = mj / r3ij
            d_auhat[d_idx] += tmp * XIJ[0]
            d_avhat[d_idx] += tmp * XIJ[1]
            d_awhat[d_idx] += tmp * XIJ[2]

    def post_loop(self, d_idx, d_mT, d_ravg, d_auhat, d_avhat, d_awhat, d_x,
                  d_y, d_z, d_n_nbrs, dt):
        d_mT[d_idx] /= d_n_nbrs[d_idx]
        d_ravg[d_idx] /= d_n_nbrs[d_idx]
        mT = d_mT[d_idx]
        ravg = d_ravg[d_idx]
        ravg2 = ravg*ravg
        # The fac has a dt factor which is omitted here to update the velocites
        # instead of the positions.
        fac = self.beta / mT * ravg2 * dt
        d_x[d_idx] += fac * d_auhat[d_idx]
        d_y[d_idx] += fac * d_avhat[d_idx]
        d_z[d_idx] += fac * d_awhat[d_idx]


class TaitEOS(Equation):
    def __init__(self, dest, sources, rho0, c0, gamma, p0=0.0):
        self.rho0 = rho0
        self.rho01 = 1.0/rho0
        self.c0 = c0
        self.gamma = gamma
        self.gamma1 = 0.5*(gamma - 1.0)
        self.B = rho0*c0*c0/gamma
        self.p0 = p0
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_rho, d_p):
        ratio = d_rho[d_idx] * self.rho01
        tmp = pow(ratio, self.gamma)

        d_p[d_idx] = self.p0 + self.B * (tmp - 1.0)


class Vacondio(Scheme):
    def __init__(self, fluids, solids, dim, c0, nu, rho0, gx=0.0, gy=0.0,
                 gz=0.0, eps=0.0, h=0.0, xi=0.0, shift_factor=0.0):
        self.c0 = c0
        self.nu = nu
        self.rho0 = rho0
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.dim = dim
        self.eps = eps
        self.xi = xi
        self.fluids = fluids
        self.solids = solids
        self.h = h
        self.shift_factor = shift_factor
        self.solver = None

    def configure_solver(self, kernel=None, integrator_cls=None,
                         extra_steppers=None, **kw):
        from pysph.base.kernels import CubicSpline as Kernel
        from pysph.sph.integrator_step import WCSPHStep
        if kernel is None:
            kernel = Kernel(dim=self.dim)
        steppers = {}
        if extra_steppers is not None:
            steppers.update(extra_steppers)

        step_cls = WCSPHStep
        cls = integrator_cls if integrator_cls is not None else PECIntegrator

        for fluid in self.fluids:
            if fluid not in steppers:
                steppers[fluid] = step_cls()

        integrator = cls(**steppers)

        self.solver = Solver(
            dim=self.dim, integrator=integrator, kernel=kernel, **kw
        )

    def get_equations(self):
        from pysph.sph.basic_equations import XSPHCorrection
        eqns = []
        for fluid in self.fluids:
            eqns.append(Group(
                equations=[
                    TaitEOS(fluid, None, rho0=self.rho0, c0=self.c0, gamma=7.0)
                ]
            ))
        for fluid in self.fluids:
            eqns.append(Group(
                equations=[
                    ContinuityEquationVacondio(
                        fluid, self.fluids, self.c0, self.eps
                    ),
                    MomentumEquationVacondio(
                        fluid, self.fluids, c0=self.c0, nu=self.nu
                    ),
                    XSPHCorrection(fluid, self.fluids)
                ]
            ))
        shift = []
        for fluid in self.fluids:
            shift.append(Group(
                equations=[
                    ShiftPositionVacondio(
                        fluid, self.fluids, shift_factor=self.shift_factor
                    )
                ]
            ))
        return MultiStageEquations([eqns, shift])

    def setup_properties(self, particles, clean=True):
        particle_arrays = {p.name: p for p in particles}
        dummy = get_particle_array(
            name='junk', gid=particle_arrays['fluid'].gid
        )
        props = []
        for name, array in dummy.properties.items():
            d = dict(name=name, type=array.get_c_type())
            if name in dummy.stride:
                d.update({'stride': dummy.stride[name]})
            props.append(d)

        constants = []
        for name, array in dummy.constants.items():
            d = dict(name=name, data=array)
            constants.append(d)

        output_props = dummy.output_property_arrays
        for fluid in self.fluids:
            pa = particle_arrays[fluid]
            self._ensure_properties(pa, props, clean)
            pa.set_output_arrays(output_props)
            for const in constants:
                pa.add_constant(**const)

        props = 'ax ay az arho rho0 u0 v0 w0 x0 y0 z0 uhat vhat what vmag'
        props += ' cs dt_cfl dt_force'
        for fluid in self.fluids:
            pa = particle_arrays[fluid]
            for prop in props.split():
                pa.add_property(prop)
            pa.add_property('dpos', stride=3)
