"""Simple advection problem.
"""
import os
from numpy import pi, cos, sin
import numpy as np
import matplotlib.pyplot as plt

from pysph.base.kernels import QuinticSpline as Kernel
from pysph.base.nnps import DomainManager
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.solver.solver import Solver
from pysph.sph.equation import Equation, Group
from pysph.sph.integrator import EulerIntegrator
from pysph.sph.integrator_step import IntegratorStep
from pysph.sph.scheme import add_bool_argument
from pysph.solver.utils import iter_output

from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio
from edac import update_rho, update_smoothing_length, ShiftPositionLind
from vacondio import ComputeBeta
from optimize_h import (FuncApproximationStd,
                                 GradApproximationStdCorrected, GradH)


L = 1.0
U = 1.0
rho0 = 1.0
c0 = 10 * U
p0 = rho0*c0*c0


class ComputeAccelerations(Equation):
    def __init__(self, dest, sources, ax, ay, az):
        self.ax = ax
        self.ay = ay
        self.az = az
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_ax, d_ay, d_az, d_u, d_v, d_w):
        d_ax[d_idx] = d_u[d_idx]
        d_ay[d_idx] = d_v[d_idx]
        d_az[d_idx] = d_w[d_idx]


class MaxNeighbors(Equation):
    def loop_all(self, d_idx, d_n_nbrs, N_NBRS):
        d_n_nbrs[d_idx] = N_NBRS

    def loop(self, d_idx, s_idx, s_n_nbrs, d_n_nbrs, d_max_nbrs):
        d_max_nbrs[d_idx] = max(d_n_nbrs[d_idx], s_n_nbrs[s_idx])


class AdvectionStepper(IntegratorStep):
    def initialize(self):
        pass

    def stage1(self, d_idx, d_x, d_y, d_z, d_ax, d_ay, d_az, dt):
        d_x[d_idx] = d_x[d_idx] + dt * d_ax[d_idx]
        d_y[d_idx] = d_y[d_idx] + dt * d_ay[d_idx]
        d_z[d_idx] = d_z[d_idx] + dt * d_az[d_idx]


def linf_error(approx, exact):
    approx_max = np.max(approx)
    exact_max = np.max(exact)
    return abs(approx_max - exact_max) / exact_max


def l1_error(approx, exact):
    l1_err = np.average(np.abs(approx - exact))
    exact_max = np.max(exact)
    return l1_err / exact_max


def func_exact(x, y):
    tmp = 2*pi/L
    return sin(tmp*x)*cos(tmp*y)


def grad_exact(x, y):
    tmp = 2*pi/L
    return [tmp * cos(tmp*x) * cos(tmp*y), -tmp * sin(tmp*x) * sin(tmp*y)]


class Advection(Application):
    def initialize(self):
        self.dim = 2
        self.u = 1.0
        self.v = 1.0

    def add_user_options(self, group):
        group.add_argument(
            "--perturb", action="store", type=float, dest="perturb", default=0,
            help="Random perturbation of initial particles as a fraction "
            "of dx (setting it to zero disables it, the default)."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=50,
            help="Number of points along x direction. (default 50)"
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.0,
            help="Ratio h/dx."
        )
        group.add_argument(
            '--adapt-freq', action='store', type=int, dest='freq', default=1,
            help='No of times to run adaptive resolution.'
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting"
        )
        group.add_argument(
            "--update-h", action="store", type=str, dest='update_h',
            default='yangkong',
            choices=['optimize', 'yangkong', 'donothing'],
            help="Choice of method to compute h."
        )
        group.add_argument(
            '--shift', action='store', dest='shift', type=int,
            help='No of times to Shift the particles.'
        )
        group.add_argument(
            "--sd", action="store", type=str, dest='summation_density',
            default='gather', choices=['gather', 'standard', 'scatter'],
            help="Choice of method to compute density."
        )

    def create_domain(self):
        return DomainManager(
            xmin=-L, xmax=L, ymin=-L, ymax=L, periodic_in_x=True,
            periodic_in_y=True
        )

    def create_nnps(self):
        from pysph.base.nnps import OctreeNNPS as NNPS
        return NNPS(dim=2.0, particles=self.particles, radius_scale=3.0,
                    cache=True, domain=self.domain)

    def consume_user_options(self):
        nx = self.options.nx

        self.dx = dx = L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        self.h0 = self.hdx * self.dx
        self.dt = 1.0 * self.h0 / (U + c0)
        self.tf = 2.0
        self.vacondio = self.options.vacondio
        self.rho0 = rho0
        self.update_h = self.options.update_h
        self.shift = self.options.shift
        self.freq = self.options.freq
        self.summation_density = self.options.summation_density

    def create_particles(self):
        # create the particles
        dx = self.dx
        _x = np.arange(-L+dx/2, L, dx)
        x, y = np.meshgrid(_x, _x)

        if self.options.perturb > 0:
            np.random.seed(1)
            factor = dx * self.options.perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor

        m = self.volume * rho0
        h = self.hdx * dx

        p = func_exact(x, y)

        fluid = get_particle_array(
            name='fluid', x=x, y=y, m=m, h=h, u=self.u, v=self.v,
            rho=rho0, p=p
        )

        props = [
            'V', 'ax', 'ay', 'az', 'auhat', 'avhat', 'p0', 'awhat', 'beta',
            'max_nbrs'
        ]

        for p in props:
            fluid.add_property(p)

        fluid.add_property('htmp')
        fluid.add_property('n_nbrs', type='int')

        props = ['m_min', 'm_max', 'uhat', 'vhat', 'what', 'vmag',
                 'psi', 'beta', 'gamma', 'ravg', 'mT', 'h0', 'ah', 'm_avg',
                 'm_nbr_max', 'dhdx', 'func', 'gradx', 'grady', 'm_ref']

        for pa in [fluid]:
            pa.add_property('Linv', stride=16)
            pa.add_property('closest_idx', type='int')
            pa.add_property('split', type='int')
            pa.add_constant('iters', [0.0])
            pa.add_constant('vmax', [0.0])
            for prop in props:
                pa.add_property(prop)
            pa.add_output_arrays(props)
        return [fluid]

    def create_solver(self):
        kernel = Kernel(dim=self.dim)
        integrator = EulerIntegrator(fluid=AdvectionStepper())

        solver = Solver(
            dim=self.dim, integrator=integrator, kernel=kernel
        )
        return solver

    def create_equations(self):
        eqns = []
        eqns.extend(
            update_smoothing_length(
                ['fluid'], ['fluid'], self.dim, rho0, self.update_h,
                hdx=self.hdx
            )
        )
        eqns.append(Group(
            equations=[MaxNeighbors('fluid', ['fluid'])]
        ))
        eqns.extend(
            update_rho(['fluid'], ['fluid'], self.summation_density)
        )
        eqns.append(Group(
            equations=[ComputeBeta('fluid', ['fluid'], self.dim)]
        ))
        # Should we do the accelerations here?
        if self.shift > 0:
            eq = []
            eq.extend(
                update_rho(['fluid'], ['fluid'], self.summation_density)
            )
            eq.append(Group(
                equations=[ShiftPositionLind(
                    'fluid', ['fluid'], dim=self.dim
                )],
                update_nnps=True
            ))
            eqns.append(Group(
                equations=eq,
                iterate=True, min_iterations=self.shift,
                max_iterations=self.shift, update_nnps=True
            ))

        eqns.append(Group(
            equations=[ComputeBeta('fluid', ['fluid'], dim=self.dim)]
        ))
        eqns.append(Group(
            equations=[GradH('fluid', ['fluid'], dim=self.dim)]
        ))
        eqns.append(Group(
            equations=[
                FuncApproximationStd('fluid', ['fluid'],),
                GradApproximationStdCorrected('fluid', ['fluid'], dim=self.dim)
            ]
        ))
        eqns.append(Group(
            equations=[ComputeAccelerations('fluid', None, self.u, self.v, 0.0)]
        ))
        return eqns

    def pre_step(self, solver):
        fluid = self.particles[0]
        dx = self.dx
        mref = m0 = rho0*dx*dx
        x, y = fluid.x, fluid.y
        fluid.m_ref[:] = mref
        fluid.m_max[:] = mref*1.251
        fluid.m_min[:] = mref*0.8
        region = (np.abs(x) < L*0.75) & (np.abs(y) < L*0.75)
        mref = m0*0.5
        fluid.m_ref[region] = mref
        fluid.m_max[region] = m0*0.69
        fluid.m_min[region] = m0*0.3
        region = (np.abs(x) < L/4) & (np.abs(y) < L/4)
        mref = m0*0.5*0.5
        fluid.m_ref[region] = mref
        fluid.m_max[region] = m0*0.35
        fluid.m_min[region] = mref*0.1

    def create_tools(self):
        if self.vacondio:
            t = AdaptiveResolutionVacondio(
                self.particles[0], dim=2, has_ghost=True,
                do_mass_exchange=False, freq=1, hybrid=True,
            )
        else:
            t = AdaptiveResolution(
                self.particles[0], dim=2, has_ghost=True,
                do_mass_exchange=False, freq=1
            )
        arrays = self.particles

        for i in range(4):
            print("t.pre_step", i, end='\r')
            self.pre_step(None)
            t.pre_step(solver=self.solver)
        print()

        t.setup_smoothing(
            dest='fluid', arrays=arrays, nnps=self.nnps,
            dt=self.dt, dim=self.dim
        )

        for i in range(50):
            print("Smoothing", i, end='\r')
            t.smoothing_sph_eval.update()
            t.smoothing_sph_eval.evaluate(dt=self.dt)
        print()
        t.smoothing_sph_eval = None
        return [t]

    def post_process(self, info_filename):
        files = self.output_files[1:]
        t, nbr_max, rho_linf, rho_l1 = [], [], [], []
        func_linf, func_l1 = [], []
        grad_linf, grad_l1 = [], []
        for sd, array in iter_output(files, 'fluid'):
            _t = sd['t']
            t.append(_t)

            linf = linf_error(array.rho, rho0)
            rho_linf = np.append(rho_linf, [linf])
            l1 = l1_error(array.rho, rho0)
            rho_l1 = np.append(rho_l1, [l1])

            nbr_max = np.append(nbr_max, [array.n_nbrs.max()])

            p_exact = array.p
            linf = linf_error(array.func, p_exact)
            func_linf = np.append(func_linf, [linf])
            l1 = l1_error(array.func, p_exact)
            func_l1 = np.append(func_l1, [l1])

            gradx_exact, _ = grad_exact(array.x, array.y)
            linf = linf_error(array.gradx, gradx_exact)
            grad_linf = np.append(grad_linf, [linf])
            l1 = l1_error(array.gradx, gradx_exact)
            grad_l1 = np.append(grad_l1, [l1])

        fig, ax = plt.subplots(1, 3, figsize=(12, 4))

        ax[0].set_xlabel('time (s)')
        ax[0].set_ylabel(r'rho $L_\infty$')
        ax[0].plot(t, rho_linf)
        ax[0].grid()
        ax[0].tick_params(axis='y')

        ax[1].set_xlabel('time (s)')
        ax[1].set_ylabel(r'Func $L_\infty$')
        ax[1].plot(t, func_linf)
        ax[1].grid()
        ax[1].tick_params(axis='y')

        ax[2].set_xlabel('time (s)')
        ax[2].set_ylabel(r'Grad $L_\infty$')
        ax[2].plot(t, grad_linf)
        ax[2].grid()
        ax[2].tick_params(axis='y')
        fig.suptitle(r'$L_\infty$ plots')

        fig.tight_layout()
        fname = os.path.join(self.output_dir, "linf.png")
        fig.savefig(fname)

        fig1, ax1 = plt.subplots(1, 3, figsize=(12, 4))

        ax1[0].set_xlabel('time (s)')
        ax1[0].set_ylabel(r'rho $L_1$')
        ax1[0].plot(t, rho_l1)
        ax1[0].grid()
        ax1[0].tick_params(axis='y')

        ax1[1].set_xlabel('time (s)')
        ax1[1].set_ylabel(r'Func $L_1$')
        ax1[1].plot(t, func_l1)
        ax1[1].grid()
        ax1[1].tick_params(axis='y')

        ax1[2].set_xlabel('time (s)')
        ax1[2].set_ylabel(r'Grad $L_1$')
        ax1[2].plot(t, grad_l1)
        ax1[2].grid()
        ax1[2].tick_params(axis='y')
        fig1.suptitle(r'$L_1$ plots')

        fig1.tight_layout()
        fname = os.path.join(self.output_dir, "l1.png")
        fig1.savefig(fname)
        plt.show()


if __name__ == '__main__':
    app = Advection()
    app.run()
    app.post_process(app.info_filename)
