'''Compute global error when two particles merge in 2D. There are two different
 options:
 1. Vacondio merging, where,

      h_M = sqrt(m_M * W_MM/(m_a * W_Ma + m_b * W_Mb))

  where subscripts a and b are the particles which are merging to give M, with
  mass m_M = m_a + m_b. W_Ma = W(x_M - x_a, h_a), W_Mb = W(x_M - x_b, h_b), and
  W_MM = W(x_M - x_M, 1) = W(0, 1) = coefficient of the Kernel.

 2.Non Vacondio merging, the h_M = sqrt(m_M/rho), where rho is constant
 set to 1.0.

Run this code using,

    $ python merge_error.py --openmp --plot --vacondio
    (or)
    $ python merge_error.py --openmp --plot

'''
from math import sqrt
import numpy as np
from scipy.optimize import minimize
from compyle.api import declare, annotate, Reduction
from compyle.utils import ArgumentParser
from compyle.array import wrap
import matplotlib.pyplot as plt
from matplotlib import ticker

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator

from optimal_mass import KernelMomentsDaughter, KernelMomentsMother


def global_error(mass, c, bk, Qlk):
    return c - 2*np.dot(mass, bk) + np.dot(mass, np.dot(Qlk, mass))


def daughter_coords(separation, plot=False):
    x = np.array([-separation*0.5, separation*0.5])
    if plot:
        plt.scatter(x, [0, 0])
        plt.show()
    return x


def create_particles(dx, h, dim):
    no_of_daughters = 2
    _x = np.arange(-5*h, 5*h, dx)
    xd, yd, zd = np.zeros([3, no_of_daughters])

    xd = daughter_coords(separation=1.0)
    if dim == 2:
        x, y = np.meshgrid(_x, _x)
        dummy = get_particle_array(name='dummy', x=x, y=y, h=h)
    elif dim == 3:
        x, y, z = np.meshgrid(_x, _x, _x)
        dummy = get_particle_array(name='dummy', x=x, y=y, z=z, h=h)
    else:
        raise NotImplementedError("1-D is not implemented.")

    dummy.add_property('wij_M')
    dummy.add_property('wij_D', stride=no_of_daughters)
    dummy.add_constant('MM', [0.0])
    dummy.add_constant('MD', [0.0]*no_of_daughters)
    dummy.add_constant('DD', [0.0]*no_of_daughters**2)
    dummy.add_constant('h_M', [h])
    dummy.add_constant('h_D', [h]*no_of_daughters)
    dummy.add_constant('daughter_x', xd)
    dummy.add_constant('daughter_y', [0.0]*no_of_daughters)
    dummy.add_constant('daughter_z', [0.0]*no_of_daughters)

    dummy.add_constant('mother_x', [0.0])
    return dummy


def create_equations(dx, dummy, dim, kernel):
    no_of_daughters = 2
    eq = []
    eq.append(
        KernelMomentsMother(dummy.name, None, dx=dx)
    )
    mother_eqns = [Group(equations=eq)]

    eq = []
    eq.append(
        KernelMomentsDaughter(dummy.name, None, dx=dx,
                              no_of_daughters=no_of_daughters)
    )
    daughter_eqns = [Group(equations=eq)]

    mother_eval = SPHEvaluator(
        arrays=[dummy], equations=mother_eqns, dim=dim,
        kernel=kernel
    )
    daughter_eval = SPHEvaluator(
        arrays=[dummy], equations=daughter_eqns, dim=dim,
        kernel=kernel
    )
    return mother_eval, daughter_eval


@annotate
def square(i, x, y):
    return x[i] * y[i]


def compute_coeffs(pa, dx, dim, backend='cython'):
    n = 2
    dxdim = dx**dim

    wij_M = wrap(pa.wij_M, backend=backend)
    r = Reduction('a+b', map_func=square, backend=backend)

    wij_D = []
    for i in range(n):
        wij_D.append(wrap(pa.wij_D[i::n].copy(), backend=backend))

    c = r(wij_M, wij_M) * dxdim

    bk, Qlk = np.zeros(n), np.zeros([n, n])
    for i in range(n):
        bk[i] = r(wij_M, wij_D[i]) * dxdim
        for j in range(n):
            Qlk[i, j] = r(wij_D[i], wij_D[j]) * dxdim
    return c, bk, Qlk


def compute_error(h=1, dim=2, kernel=None, plot=False, backend='cython',
                  print_info=False, vacondio=False):
    if kernel is None:
        from pysph.base.kernels import QuinticSpline as Kernel
        kernel = Kernel(dim=dim)
    dx = 0.025*h

    dummy = create_particles(dx, h, dim)
    mother_eval, daughter_eval = create_equations(dx, dummy, dim, kernel)

    betas = np.linspace(0.1, 1, 30)
    separation = np.linspace(0.1, 1., 40) * h

    m0 = 1.0
    rho = m0/(h*h)
    error = np.zeros([len(betas), len(separation)])
    xij = np.zeros(3)
    for i, beta in enumerate(betas):
        m2 = m0/(1 + beta)
        m1 = beta * m2
        mt = 1.0/(m1 + m2)
        dummy.h_D[0] = pow(m1/rho, 1.0/dim)
        dummy.h_D[1] = pow(m2/rho, 1.0/dim)
        for j, sep in enumerate(separation):
            xd = daughter_coords(sep, plot=False)
            dummy.daughter_x[:] = xd
            xm = (xd[0]*m1 + xd[1]*m2) * mt
            dummy.mother_x[:] = xm
            if vacondio:
                xij[0] = xd[0] - xm
                rij = np.abs(xij[0])
                w1 = m1 * kernel.kernel(xij, rij, dummy.h_D[0])
                xij[0] = xd[1] - xm
                rij = np.abs(xij[0])
                w2 = m2 * kernel.kernel(xij, rij, dummy.h_D[1])
                xij[0] = 0.0
                rij = 0.0
                w3 = kernel.kernel(xij, rij, 1.0)
                dummy.h_M[0] = np.sqrt(w3/(w1 + w2))
            else:
                dummy.h_M[0] = np.sqrt(m0/rho)


            # Evaluate Mother
            mother_eval.update()
            mother_eval.evaluate()

            # Evaluate Daughter
            daughter_eval.update()
            daughter_eval.evaluate()

            c, bk, Qlk = compute_coeffs(dummy, dx, dim, backend)
            mass = [m1, m2]
            e = global_error(mass, c, bk, Qlk)

            if print_info:
                print('-----')
                print(f"{beta/h:.2f}, {sep/h:.2f}, {e:.2E}")
            error[i, j] = e
    idx = np.unravel_index(np.argmin(error, axis=None), error.shape)
    print("beta\t separation\t error\t")
    print(f"{betas[idx[0]]/h:.2f}\t {separation[idx[1]]/h:.2f}\t {error[idx]:.2E}")
    if plot:
        if vacondio:
            c = plt.contourf(betas, separation, error.T,
                             locator=ticker.LogLocator(numticks=10), levels=10)
            title = ' with vacondio'
            fname = '_vacondio'
        else:
            c = plt.contourf(betas, separation, error.T, levels=10)
            title = ' w/o vacondio'
            fname = '_w_o_vacondio'
        plt.colorbar(c)
        plt.xlabel(r'$\beta=\frac{l_1}{l_2}$, Mass ratio.')
        plt.ylabel('Separation b/w daughter particles.')
        # plt.title('Global density error' + title)
        plt.grid()
        plt.savefig(f"merge_error{fname}.png")
        plt.show()

if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('--h', action='store', type=float, dest='h',
                   default=1.2, help='Mother particle smoothing length.')
    p.add_argument('--dim', action='store', type=int, dest='dim',
                   default=2, help='Dimension of the simulation.')
    p.add_argument(
        '--plot', action='store_true', dest='plot',
        default=False, help='Show plots at the end of simulation.'
    )
    p.add_argument(
        '--vacondio', action='store_true', dest='vacondio',
        default=False, help='Use vacondio merging.'
    )
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    o = p.parse_args()

    compute_error(h=o.h, dim=o.dim, backend=o.backend, plot=o.plot,
                  print_info=o.print_info, vacondio=o.vacondio)
