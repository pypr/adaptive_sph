"""Taylor Green vortex flow Problem.
np.abs(u + np.exp(-8.0*np.pi*np.pi / 1000 * 2.5) * np.cos(2*np.pi*x) * np.sin(2*np.pi*y))

np.abs(p - np.exp(-8.0*np.pi*np.pi / 1000 * 2.5) * 0.25 * (np.cos(4*np.pi*x) + np.cos(4*np.pi*y)))
"""

import os
import numpy as np

from pysph.examples import taylor_green as TG
from pysph.base.nnps import DomainManager
from pysph.sph.scheme import add_bool_argument

from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio
from edac import AdaptiveEDACScheme

U, L = TG.U, TG.L


class TaylorGreen(TG.TaylorGreen):
    def add_user_options(self, group):
        super().add_user_options(group)
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting"
        )
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        group.add_argument(
            "--patches", action="store", type=int, dest="patches",
            default=7, choices=list(range(1, 9)),
            help="Number of patches with adaptive resolution."
        )
        add_bool_argument(
            group, 'adaptive', dest='adaptive', default=True,
            help="Use adaptive particle resolution."
        )

    def create_domain(self):
        return DomainManager(
            xmin=0, xmax=L, ymin=0, ymax=L, periodic_in_x=True,
            periodic_in_y=True, n_layers=3.0
        )

    def create_nnps(self):
        from pysph.base.nnps import OctreeNNPS as NNPS
        return NNPS(dim=2.0, particles=self.particles, radius_scale=3.0,
                    cache=True, domain=self.domain)

    def consume_user_options(self):
        nx = self.options.nx
        re = self.options.re
        self.nu = nu = U * L / re
        self.dx = dx = L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx
        self.dim = 2
        self.adaptive = self.options.adaptive

        self.patches = self.options.patches

        layers = 2
        if not self.adaptive:
            layers = 0
        self.dx_min = self.dx/(2**(layers/self.dim))
        self.h0 = self.hdx * self.dx
        self.hmin = self.hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        # FIXME: Use the h of lowest resolution.
        dt_cfl = cfl * self.hmin / (U+TG.c0)
        dt_viscous = 0.125 * self.h0**2 / nu
        dt_force = 0.25 * 1.0

        self.dt = min(dt_cfl, dt_viscous, dt_force)
        self.tf = 2.0
        self.kernel_correction = False
        # Trick parent class into working correctly when creating particles.
        self.options.scheme = 'edac'
        self.vacondio = self.options.vacondio

    def create_particles(self):
        [fluid] = super().create_particles()
        self.scheme.setup_properties([fluid])
        fluid.add_property('gid')
        no_of_particles = fluid.get_number_of_particles()
        fluid.gid[:] = np.arange(no_of_particles)
        return [fluid]

    def configure_scheme(self):
        scheme = self.scheme
        pfreq = 10
        scheme.configure(h=self.h0, nu=self.nu, cfl=self.cfl)
        scheme.configure_solver(
            tf=self.tf, dt=self.dt, pfreq=pfreq,
            output_at_times=[0.2, 0.4, 0.8, 1.0]
        )

    def create_scheme(self):
        domain = self.create_domain()
        edac = AdaptiveEDACScheme(
            ['fluid'], [], dim=2, rho0=TG.rho0, c0=TG.c0,
            nu=None, h=None, has_ghosts=True, cfl=None,
            domain=domain
        )
        return edac

    def pre_step(self, solver):
        fluid = self.particles[0]
        dx = self.dx
        m0 = TG.rho0*dx*dx
        x = fluid.x
        y = fluid.y
        k = 0.5
        fluid.m_max[:] = m0*1.05
        fluid.m_min[:] = m0*0.6

        m0 *= k
        for i in range(self.patches):
            if i < 4:
                xmin, xmax = 0.15, 0.35
                ymin, ymax = 0.15, 0.35
                xinc, yinc = 0.0, 0.0
                if self.patches == 1:
                    xmin, xmax = 0.1, 0.5
                    ymin, ymax = 0.1, 0.5
                    m0 *= k
                if i == 1:
                    xinc, yinc = 0.5, 0.5
                elif i == 2:
                    xinc, yinc = 0.5, 0
                elif i == 3:
                    xinc, yinc = 0, 0.5
            else:
                xmin, xmax = 0.15, 0.35
                ymin, ymax = 0.4, 0.6
                xinc, yinc = 0.0, 0.0
                if i == 5:
                    xinc, yinc = 0.5, 0.0
                elif i == 6:
                    xinc, yinc = 0.25, 0.25
                elif i == 7:
                    xinc, yinc = 0.25, -0.25
            ymin += yinc
            ymax += yinc
            xmin += xinc
            xmax += xinc
            region = (y > ymin) & (y < ymax) & (x > xmin) & (x < xmax)
            fluid.m_max[region] = m0*1.0
            fluid.m_min[region] = m0*0.3

    def create_tools(self):
        if not self.adaptive:
            return []
        arrays = self.particles
        if self.vacondio:
            t = AdaptiveResolutionVacondio(
                self.particles[0], dim=2, has_ghost=True, rho0=TG.rho0,
                do_mass_exchange=False, hybrid=True, freq=5, nnps=self.nnps
            )
        else:
            t = AdaptiveResolution(
                self.particles[0], dim=2, has_ghost=True, rho0=TG.rho0,
                do_mass_exchange=False
            )

        smoothing_sph_eval = t.setup_smoothing(
            dest='fluid', arrays=arrays, nnps=self.nnps,
            dim=self.dim, rho0=TG.rho0
        )

        for i in range(8):
            print("t.pre_step", i, end='\r')
            self.pre_step(None)
            t.pre_step(self.solver)
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)

        for i in range(50):
            print("Smoothing", i, end='\r')
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)
        print()
        fluid = arrays[0]
        # import matplotlib.pyplot as plt
        # plt.scatter(fluid.x, fluid.y, c=fluid.m_min, s=1)
        # plt.show()
        # plt.violinplot(1/fluid.m**0.5)
        # plt.show()
        re = self.options.re
        b = -8.0*np.pi*np.pi / re
        u, v, p = TG.exact_solution(U=TG.U, b=b, t=0, x=fluid.x, y=fluid.y)
        fluid.u[:] = u
        fluid.v[:] = v
        fluid.au[:] = 0.0
        fluid.av[:] = 0.0
        fluid.p[:] = p
        fluid.vmag[:] = np.sqrt(u**2 + v**2)
        return [t]

    def post_process(self, info_fname):
        info = self.read_info(info_fname)
        if len(self.output_files) == 0:
            return

        from pysph.solver.utils import iter_output
        decay_rate = -8.0 * np.pi**2 / self.options.re

        files = self.output_files
        t, ke, ke_ex, decay, linf, l1, p_l1 = [], [], [], [], [], [], []
        vol_err = []
        for sd, array in iter_output(files, 'fluid'):
            _t = sd['t']
            t.append(_t)
            x, y, m, u, v, p = self._get_post_process_props(array)
            u_e, v_e, p_e = TG.exact_solution(U, decay_rate, _t, x, y)
            vmag2 = u**2 + v**2
            vmag = np.sqrt(vmag2)
            ke.append(0.5 * np.sum(m * vmag2))
            vmag2_e = u_e**2 + v_e**2
            vmag_e = np.sqrt(vmag2_e)
            ke_ex.append(0.5 * np.sum(m * vmag2_e))

            vmag_max = vmag.max()
            decay.append(vmag_max)
            theoretical_max = U * np.exp(decay_rate * _t)
            linf.append(np.max(np.abs(vmag - vmag_e) / theoretical_max))

            l1_err = np.average(np.abs(vmag - vmag_e))
            avg_vmag_e = np.average(np.abs(vmag_e))
            # scale the error by the maximum velocity.
            l1.append(l1_err / avg_vmag_e)

            p_e_max = np.abs(p_e).max()
            p_error = np.average(np.abs(p - p_e)) / p_e_max
            p_l1.append(p_error)

            vol_tot = np.sum(array.m / array.rho)
            vol_err.append(np.abs(vol_tot/L - 1.0) * 100)

        t, ke, ke_ex, decay, l1, linf, p_l1, vol_err = list(map(
            np.asarray, (t, ke, ke_ex, decay, l1, linf, p_l1, vol_err))
        )
        decay_ex = U * np.exp(decay_rate * t)
        fname = os.path.join(self.output_dir, 'results.npz')
        np.savez(
            fname, t=t, ke=ke, ke_ex=ke_ex, decay=decay, linf=linf, l1=l1,
            p_l1=p_l1, decay_ex=decay_ex, vol_err=vol_err
        )

        import matplotlib
        matplotlib.use('Agg')

        from matplotlib import pyplot as plt
        plt.clf()
        plt.semilogy(t, decay_ex, label="exact")
        plt.semilogy(t, decay, label="computed")
        plt.xlabel('t')
        plt.ylabel('max velocity')
        plt.legend()
        fig = os.path.join(self.output_dir, "decay.png")
        plt.savefig(fig, dpi=300)

        plt.clf()
        plt.plot(t, linf)
        plt.grid()
        plt.xlabel('t')
        plt.ylabel(r'$L_\infty$ error')
        fig = os.path.join(self.output_dir, "linf_error.png")
        plt.savefig(fig, dpi=300)

        plt.clf()
        plt.plot(t, l1, label="error")
        plt.grid()
        plt.xlabel('t')
        plt.ylabel(r'$L_1$ error')
        fig = os.path.join(self.output_dir, "l1_error.png")
        plt.savefig(fig, dpi=300)

        plt.clf()
        plt.plot(t, p_l1, label="error")
        plt.grid()
        plt.xlabel('t')
        plt.ylabel(r'$L_1$ error for $p$')
        fig = os.path.join(self.output_dir, "p_l1_error.png")
        plt.savefig(fig, dpi=300)

        plt.clf()
        plt.semilogy(t, vol_err)
        plt.xlabel('t')
        plt.ylabel('% error in volume')
        fig = os.path.join(self.output_dir, "vol_err.png")
        plt.savefig(fig, dpi=300)

        from pysph.solver.utils import iter_output, load

        files = self.output_files
        t, rho_max, ke, ke_exact = [], [], [], []
        for sd, array in iter_output(files, 'fluid'):
            _t = sd['t']
            t.append(_t)
            rho_max = np.append(rho_max, [array.rho.max()])
            vmag2 = array.u**2 + array.v**2
            m = array.m
            ke_total = np.sum(0.5*m*vmag2)
            ke = np.append(ke, [ke_total])

            re = self.options.re
            b = -8.0*np.pi*np.pi / re
            ue, ve, _ = TG.exact_solution(U=TG.U, b=b, t=_t, x=array.x,
                                          y=array.y)
            vmage2 = ue**2 + ve**2
            ke_exact_total = np.sum(0.5 * m * vmage2)
            ke_exact = np.append(ke_exact, [ke_exact_total])
        rho_max = (rho_max - 1) * 100
        plt.clf()
        plt.plot(t, rho_max)
        plt.grid()
        plt.xlabel('time (s)')
        plt.ylabel('Density error in %')
        fig_name = os.path.join(self.output_dir, "density_error.png")
        plt.savefig(fig_name, dpi=300)
        plt.clf()

        plt.semilogy(t, ke_exact, label='exact')
        plt.semilogy(t, ke, label='computed')
        plt.xlabel('time (s)')
        plt.ylabel('ke')
        plt.legend()
        fig_name = os.path.join(self.output_dir, "ke_decay.png")
        plt.savefig(fig_name, dpi=300)
        plt.clf()

        fig, ax = plt.subplots(1, 1, figsize=(4, 3))
        fig.subplots_adjust(wspace=0.2)
        data = load(files[-1])
        f = data['arrays']['fluid']
        im = ax.scatter(
            f.x, f.y, marker='.', s=20, c=f.vmag, edgecolors='none'
        )
        plt.colorbar(im, ax=ax)
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        plt.axis('equal')
        ax.tick_params(left=False, bottom=False, labelleft=False,
                       labelbottom=False)
        ax.set_xlabel("Particle velocity magnitude plots")
        plt.tight_layout(pad=0)
        fig_name = os.path.join(self.output_dir, "pplot.png")
        fig.savefig(fig_name, dpi=300)
        plt.close()
        plt.clf()

        fig, ax = plt.subplots(1, 1, figsize=(4, 3))
        fig.subplots_adjust(wspace=0.2)
        data = load(files[-1])
        f = data['arrays']['fluid']
        im = ax.scatter(
            f.x, f.y, marker='.', s=20, c=f.m, edgecolors='none'
        )
        ax.tick_params(left=False, bottom=False, labelleft=False,
                       labelbottom=False)
        ax.set_xlabel("Particle mass distribution")
        plt.colorbar(im, ax=ax, format='%.0e')
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        plt.tight_layout(pad=0)
        fig_name = os.path.join(self.output_dir, "pplot_mass.png")
        plt.axis('equal')
        fig.savefig(fig_name, dpi=300)
        plt.close()
        plt.clf()

        fig, ax = plt.subplots(1, 1, figsize=(4, 3))
        fig.subplots_adjust(wspace=0.2)
        data = load(files[-1])
        f = data['arrays']['fluid']
        im = ax.scatter(
            f.x, f.y, marker='.', s=20, c=f.rho, edgecolors='none'
        )
        plt.colorbar(im, ax=ax)
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        plt.axis('equal')
        ax.tick_params(left=False, bottom=False, labelleft=False,
                       labelbottom=False)
        ax.set_xlabel("Particle density distribution")
        plt.tight_layout(pad=0)
        fig_name = os.path.join(self.output_dir, "pplot_rho.png")
        fig.savefig(fig_name, dpi=300)
        plt.close()
        plt.clf()

    def customize_output(self):
        self._mayavi_config('''
        b = particle_arrays['fluid']
        b.scalar = 'vmag'
        b.plot.module_manager.scalar_lut_manager.lut_mode = 'blue-red'
        b.plot.module_manager.scalar_lut_manager.number_of_colors = 16
        for name in ['fluid']:
            b = particle_arrays[name]
            b.point_size = 4.0
        ''')


if __name__ == '__main__':
    app = TaylorGreen()
    app.run()
    app.post_process(app.info_filename)
