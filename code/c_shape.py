"""
Flow past c-shaped object.

Sun: ((1.2*1.8 - 0.5*pi*(0.5**2 - 0.25**2))*200*200
      + (2.5*1.6 - 1.2*1.8)*100*100 + (3*2 - 2.5*1.6)*50*50)
cond = ((f.x - 4) > -1.25) & ((f.x - 4) < 1.75) & (f.y > -0.8) & (f.y < 1.2)
f.x[cond].shape
"""
import numpy as np
from numpy import pi, sin, cos

from pysph.base.utils import get_particle_array
from fpc_auto import WindTunnel


# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
u_freestream = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


def semicircle(ri, ro, dx, shift=True, ends=True, up=True):
    if shift:
        r = np.arange(ri+dx/2, ro, dx)
    else:
        r = np.arange(ri, ro, dx)
    x, y = np.array([]), np.array([])
    for i in r:
        spacing = dx
        theta = np.linspace(0, pi, int(pi*i/spacing))
        if not ends:
            theta = theta[1:-1]
        x = np.append(x, i * cos(theta))
        sign = 1 if up else -1
        y = np.append(y, sign * i * sin(theta))
    return x, y


def c_shape(d, dx, shift=True):
    r = d/2
    x, y = semicircle(r/2, r, dx, ends=False)
    x1, y1 = semicircle(0, r/4, dx, shift=shift, up=False)
    x = np.hstack((x, x1-0.75*r, x1+0.75*r))
    y = np.hstack((y, y1, y1))
    return x, y


class CShape(WindTunnel):
    def initialize(self):
        super().initialize()
        self.remove_inner_layers = False

    def add_user_options(self, group):
        super().add_user_options(group)
        group.set_defaults(
            dc=1.0, re=2000, tf=30, potential=False, dx_min=0.005,
            Lt=20, Wt=5, dx_max=0.04, cr=1.08
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.cxy = self.Lt / 5, -0.5

    def _create_solid(self):
        dx = self.dx_min
        h0 = self.hdx*dx
        dia = self.dc
        x, y = c_shape(dia, dx, shift=True)

        x += self.cxy[0]
        y += self.cxy[1]
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, m=volume*rho, rho=rho, h=h0
        )
        solid.add_constant('ds_min', dx)
        return solid


if __name__ == '__main__':
    app = CShape()
    app.run()
    app.post_process(app.info_filename)
