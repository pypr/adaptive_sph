"""This is the split and merge algorithm from

- Vacondio, Rogers, Stansby, Accurate particle splitting for smoothed particle
  hydrodynamics in shallow water with shock capturing, IJNMF, 2012.

- Vacondio, Rogers, Stansby, Mignosa, Feldman, Variable resolution for SPH: A
  dynamic particle coalescing and splitting scheme, CMAME, 2013

- Vacondio, Rogers, Stansby, Mignosa, Variable resolution for SPH in three
  dimensions: Towards optimal splitting and coalescing for dynamic adaptivity,
  CMAME 2016"

- Feldman and Bonet, Dynamic refinement and boundary contact forces in SPH with
  applications in fluid flow problems, IJNME, 2007

"""

from math import sqrt, pi, cos, sin
import numpy as np
from compyle.api import annotate, declare, wrap
from compyle.low_level import cast
from compyle.parallel import Elementwise
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator

from adapt import (AdaptiveResolution, FindSplitMergeBase, FindSplitMerge,
                   SkipNearBoundary, MassExchange)

M_PI = pi


class FindSplitMergeV(FindSplitMerge):
    def loop_all(self, d_idx, d_h, d_m, d_m_min, d_m_max, d_split,
                 d_closest_idx, d_x, s_x, d_y, s_y, d_z, s_z, s_m,
                 s_m_max, s_m_min, s_h, N_NBRS, NBRS):
        k, s_idx, idx = declare('int', 3)
        r2ij, rc, r2max, mi, m_merge, m_max, m_min = declare('double', 7)
        min_max = declare('double')
        idx = -1
        rc = 1e12
        mi = d_m[d_idx]
        m_max = d_m_max[d_idx]
        m_min = d_m_min[d_idx]
        # Split
        if mi > m_max:
            d_split[d_idx] = 1
        # Merge
        # elif mi < m_min:
        else:
            d_split[d_idx] = 2
            for k in range(N_NBRS):
                s_idx = NBRS[k]
                if s_idx == d_idx:
                    continue
                r2ij = ((d_x[d_idx] - s_x[s_idx])**2 +
                        (d_y[d_idx] - s_y[s_idx])**2 +
                        (d_z[d_idx] - s_z[s_idx])**2)

                m_merge = mi + s_m[s_idx]
                min_max = min(m_max, s_m_max[s_idx])
                r2max = (d_h[d_idx] + s_h[s_idx])*0.5
                r2max *= r2max
                if ((r2ij < rc) and (m_merge < min_max) and
                    r2ij < r2max):
                    rc = r2ij
                    idx = s_idx
            d_closest_idx[d_idx] = idx
            if idx == -1:
                # No nearest particle, so cannot merge.
                d_split[d_idx] = 0

    def post_loop(self, d_idx, d_closest_idx, d_split, d_m,
                  d_x, d_y, d_z, d_u, d_v, d_w, d_uhat, d_vhat, d_what,
                  d_p, d_h, d_au, d_av, d_aw, d_ap,
                  d_auhat, d_avhat, d_awhat,
                  SPH_KERNEL):
        idx = declare('int')
        m1, m2, mt, w1, w2, w3, rij, xm, ym, zm = declare('double', 10)
        xij = declare('matrix(3)')
        idx = d_closest_idx[d_idx]
        # Perform any merging.
        if d_split[d_idx] == 2 and d_closest_idx[idx] == d_idx:
            if d_idx < idx:
                d_split[d_idx] = 0
                # Merge.
                m1 = d_m[d_idx]
                m2 = d_m[idx]
                d_m[d_idx] += m2
                mt = 1.0/(m1 + m2)
                m1 *= mt
                m2 *= mt
                xm = d_x[d_idx]*m1 + d_x[idx]*m2
                ym = d_y[d_idx]*m1 + d_y[idx]*m2
                zm = d_z[d_idx]*m1 + d_z[idx]*m2
                xij[0] = d_x[d_idx] - xm
                xij[1] = d_y[d_idx] - ym
                xij[2] = d_z[d_idx] - zm
                rij = sqrt(xij[0]**2 + xij[1]**2 + xij[2]**2)
                w1 = m1*SPH_KERNEL.kernel(xij, rij, d_h[d_idx])
                xij[0] = d_x[idx] - xm
                xij[1] = d_y[idx] - ym
                xij[2] = d_z[idx] - zm
                rij = sqrt(xij[0]**2 + xij[1]**2 + xij[2]**2)
                w2 = m2*SPH_KERNEL.kernel(xij, rij, d_h[idx])
                xij[0] = 0.0
                xij[1] = 0.0
                xij[2] = 0.0
                rij = 0.0
                w3 = SPH_KERNEL.kernel(xij, rij, 1.0)
                # FIXME: This is only accurate in 2D.
                d_h[d_idx] = sqrt(w3/(w1 + w2))
                d_x[d_idx] = xm
                d_y[d_idx] = ym
                d_z[d_idx] = zm
                d_u[d_idx] = d_u[d_idx]*m1 + d_u[idx]*m2
                d_v[d_idx] = d_v[d_idx]*m1 + d_v[idx]*m2
                d_w[d_idx] = d_w[d_idx]*m1 + d_w[idx]*m2
                d_uhat[d_idx] = d_uhat[d_idx]*m1 + d_uhat[idx]*m2
                d_vhat[d_idx] = d_vhat[d_idx]*m1 + d_vhat[idx]*m2
                d_what[d_idx] = d_what[d_idx]*m1 + d_what[idx]*m2
                d_p[d_idx] = d_p[d_idx]*m1 + d_p[idx]*m2
                d_au[d_idx] = d_au[d_idx]*m1 + d_au[idx]*m2
                d_av[d_idx] = d_av[d_idx]*m1 + d_av[idx]*m2
                d_aw[d_idx] = d_aw[d_idx]*m1 + d_aw[idx]*m2
                d_ap[d_idx] = d_ap[d_idx]*m1 + d_ap[idx]*m2

                d_auhat[d_idx] = d_auhat[d_idx]*m1 + d_auhat[idx]*m2
                d_avhat[d_idx] = d_avhat[d_idx]*m1 + d_avhat[idx]*m2
                d_awhat[d_idx] = d_awhat[d_idx]*m1 + d_awhat[idx]*m2
            else:
                # Mark for deletion.
                d_split[d_idx]= -1
        elif d_split[d_idx] == 2:
            # No matching merge pair.
            d_split[d_idx] = 0


class PreMergeSetup(Equation):
    def initialize(self, d_idx, d_split, d_closest_idx):
        d_split[d_idx] = 0
        d_closest_idx[d_idx] = -1


class MergeIterative(FindSplitMerge):

    def initialize(self, d_idx, d_closest_idx):
        # Note that d_split should NOT be set here as we mark
        # the deleted items there.
        # We must overload this method since we are overriding FindSplitMerge
        # which sets split to zero.
        d_closest_idx[d_idx] = -1

    def converged(self):
        return -1

    def loop_all(self, d_idx, d_h, d_m, d_m_min, d_m_max, d_split,
                 d_closest_idx, d_x, s_x, d_y, s_y, d_z, s_z, s_m,
                 s_split, s_m_max, s_m_min, s_h, N_NBRS, NBRS):
        k, s_idx, idx = declare('int', 3)
        r2ij, rc, r2max, mi, m_merge, m_max, m_min = declare('double', 7)
        min_max = declare('double')
        idx = -1
        rc = 1e12
        mi = d_m[d_idx]
        m_max = d_m_max[d_idx]
        m_min = d_m_min[d_idx]
        if mi < m_min and d_split[d_idx] != -1:
            d_split[d_idx] = 2
            for k in range(N_NBRS):
                s_idx = NBRS[k]
                if s_idx == d_idx or s_split[s_idx] == -1:
                    continue
                r2ij = ((d_x[d_idx] - s_x[s_idx])**2 +
                        (d_y[d_idx] - s_y[s_idx])**2 +
                        (d_z[d_idx] - s_z[s_idx])**2)

                m_merge = mi + s_m[s_idx]
                min_max = min(m_max, s_m_max[s_idx])
                r2max = (d_h[d_idx] + s_h[s_idx])*0.5
                r2max *= r2max
                if ((r2ij < rc) and (m_merge < min_max) and
                    s_m[s_idx] < s_m_min[s_idx] and r2ij < r2max):
                    rc = r2ij
                    idx = s_idx
            d_closest_idx[d_idx] = idx
            if idx == -1:
                # No nearest particle, so cannot merge.
                d_split[d_idx] = 0


@annotate
def lcg(xn):
    # A Linear congruential generator. Values of a, n, and c are taken from
    # Pierre  L'Ecuyer, https://doi.org/10.1090%2FS0025-5718-99-00996-5
    a, m, c = declare('int', 3)
    a = 20501397
    m = 2147483648
    c = 3
    r = cast((a*xn + c) % m, 'double')
    return r/m


@annotate
def copy_props2d_vacondio_6_beta_1(i, indices, copy_indices, d_x, d_y, d_z, d_u,
                                   d_v, d_w, d_vmag, d_uhat, d_vhat, d_what, d_p, d_m,
                                   d_rho, d_h, d_closest_idx, d_m_min, d_m_max,
                                   d_au, d_av, d_aw, d_ap,
                                   d_auhat, d_avhat, d_awhat):
    # Parameters for mass ratio of centre to outer child particles, \(beta =
    # 1\), with (6+1) configuration for splitting. Run split_error.py by passing
    # --n 7, --beta 1, and --centre as command-line arguments.
    #alpha = 0.9; eps # = 0.4; l1 # ... l7 = 1/7
    idx, d_idx, j = declare('int', 3)
    theta, ds, hj, mj, l1, l2, alpha, eps = declare('double', 8)
    alpha = 0.9
    eps = 0.4
    l1 = 1.0/7.0
    l2 = l1
    d_idx = indices[i]

    ds = eps*d_h[d_idx]
    hj = alpha*d_h[d_idx]
    mj = d_m[d_idx]*l2
    d_m[d_idx] *= l1
    d_h[d_idx] = hj

    # Note that Feldman and Bonet suggest using the same velocity
    # for the children.
    for j in range(6):
        idx = copy_indices[6*i + j]
        theta = M_PI/3*j
        d_m[idx] = mj
        d_h[idx] = hj
        d_x[idx] = d_x[d_idx] + ds*cos(theta)
        d_y[idx] = d_y[d_idx] + ds*sin(theta)
        d_z[idx] = d_z[d_idx]
        d_u[idx] = d_u[d_idx]
        d_v[idx] = d_v[d_idx]
        d_w[idx] = d_w[d_idx]
        d_vmag[idx] = d_vmag[d_idx]
        d_uhat[idx] = d_uhat[d_idx]
        d_vhat[idx] = d_vhat[d_idx]
        d_what[idx] = d_what[d_idx]
        d_p[idx] = d_p[d_idx]
        d_rho[idx] = d_rho[d_idx]
        d_m_min[idx] = d_m_min[d_idx]
        d_m_max[idx] = d_m_max[d_idx]
        d_au[idx] = d_au[d_idx]
        d_av[idx] = d_av[d_idx]
        d_aw[idx] = d_aw[d_idx]
        d_ap[idx] = d_ap[d_idx]
        d_auhat[idx] = d_auhat[d_idx]
        d_avhat[idx] = d_avhat[d_idx]
        d_awhat[idx] = d_awhat[d_idx]


@annotate
def copy_props2d_vacondio_6_beta_2(i, indices, copy_indices, d_x, d_y, d_z, d_u,
                                   d_v, d_w, d_vmag, d_uhat, d_vhat, d_what, d_p, d_m,
                                   d_rho, d_h, d_closest_idx, d_m_min, d_m_max,
                                   d_au, d_av, d_aw, d_ap,
                                   d_auhat, d_avhat, d_awhat):
    # Parameters from the Vacondio et al 2012 paper:
    # alpha = 0.9; eps = 0.4; l1 = 0.1787; l2 ... l7 = 0.136883333
    idx, d_idx, j = declare('int', 3)
    theta, ds, hj, mj, l1, l2, alpha, eps = declare('double', 8)
    alpha = 0.9
    eps = 0.4
    l1 = 0.25
    l2 = (1.0 - l1)/6.0
    d_idx = indices[i]

    ds = eps*d_h[d_idx]
    hj = alpha*d_h[d_idx]
    mj = d_m[d_idx]*l2
    d_m[d_idx] *= l1
    d_h[d_idx] = hj

    # Note that Feldman and Bonet suggest using the same velocity
    # for the children.
    for j in range(6):
        idx = copy_indices[6*i + j]
        theta = M_PI/3*j
        d_m[idx] = mj
        d_h[idx] = hj
        d_x[idx] = d_x[d_idx] + ds*cos(theta)
        d_y[idx] = d_y[d_idx] + ds*sin(theta)
        d_z[idx] = d_z[d_idx]
        d_u[idx] = d_u[d_idx]
        d_v[idx] = d_v[d_idx]
        d_w[idx] = d_w[d_idx]
        d_vmag[idx] = d_vmag[d_idx]
        d_uhat[idx] = d_uhat[d_idx]
        d_vhat[idx] = d_vhat[d_idx]
        d_what[idx] = d_what[d_idx]
        d_p[idx] = d_p[d_idx]
        d_rho[idx] = d_rho[d_idx]
        d_m_min[idx] = d_m_min[d_idx]
        d_m_max[idx] = d_m_max[d_idx]
        d_au[idx] = d_au[d_idx]
        d_av[idx] = d_av[d_idx]
        d_aw[idx] = d_aw[d_idx]
        d_ap[idx] = d_ap[d_idx]
        d_auhat[idx] = d_auhat[d_idx]
        d_avhat[idx] = d_avhat[d_idx]
        d_awhat[idx] = d_awhat[d_idx]


@annotate
def copy_props2d_vacondio_4_beta_1(i, indices, copy_indices, d_x, d_y, d_z, d_u,
                                   d_v, d_w, d_vmag, d_uhat, d_vhat, d_what, d_p, d_m,
                                   d_rho, d_h, d_closest_idx, d_m_min, d_m_max,
                                   d_au, d_av, d_aw, d_ap,
                                   d_auhat, d_avhat, d_awhat):
    # Parameters from the Vacondio et al 2012 paper for 4 configuration.
    # alpha = 0.9; eps = 0.4; l1 = 0.25 ; l2 ... l4 = 0.25
    idx, d_idx, j = declare('int', 3)
    theta, ds, hj, mj, l1, l2, alpha, eps = declare('double', 8)
    alpha = 0.92
    eps = 0.4
    l1 = 0.25
    l2 = (1.0 - l1)/3.0
    d_idx = indices[i]

    ds = eps*d_h[d_idx]
    hj = alpha*d_h[d_idx]
    mj = d_m[d_idx]*l2
    d_m[d_idx] *= l1
    d_h[d_idx] = hj

    theta0 = 0.0
    # theta0 = lcg(d_idx) * M_PI

    # Note that Feldman and Bonet suggest using the same velocity
    # for the children.
    for j in range(3):
        idx = copy_indices[3*i + j]
        # theta = M_PI*j + (M_PI / 12 * (d_idx % 13)) % M_PI
        theta = M_PI/2*(j+1/2) + theta0
        d_m[idx] = mj
        d_h[idx] = hj
        d_x[idx] = d_x[d_idx] + ds*cos(theta)
        d_y[idx] = d_y[d_idx] + ds*sin(theta)
        d_z[idx] = d_z[d_idx]
        d_u[idx] = d_u[d_idx]
        d_v[idx] = d_v[d_idx]
        d_w[idx] = d_w[d_idx]
        d_vmag[idx] = d_vmag[d_idx]
        d_uhat[idx] = d_uhat[d_idx]
        d_vhat[idx] = d_vhat[d_idx]
        d_what[idx] = d_what[d_idx]
        d_p[idx] = d_p[d_idx]
        d_rho[idx] = d_rho[d_idx]
        d_m_min[idx] = d_m_min[d_idx]
        d_m_max[idx] = d_m_max[d_idx]
        d_au[idx] = d_au[d_idx]
        d_av[idx] = d_av[d_idx]
        d_aw[idx] = d_aw[d_idx]
        d_ap[idx] = d_ap[d_idx]
        d_auhat[idx] = d_auhat[d_idx]
        d_avhat[idx] = d_avhat[d_idx]
        d_awhat[idx] = d_awhat[d_idx]

    theta = M_PI/2 * (3 + 1/2)
    d_x[d_idx] = d_x[d_idx] + ds*cos(theta)
    d_y[d_idx] = d_y[d_idx] + ds*sin(theta)


@annotate
def copy_props2d(i, indices, copy_indices, d_x, d_y, d_z, d_u, d_v, d_w,
                 d_vmag, d_uhat, d_vhat, d_what, d_p, d_m, d_rho, d_h,
                 d_closest_idx, d_m_min, d_m_max,
                 d_au, d_av, d_aw, d_ap,
                 d_auhat, d_avhat, d_awhat):
    # Parameters for hybrid which are using Yang and Kong values, where alpha =
    # 1/sqrt(2). Run the optimal_mass.py to get the values of l1 and l2.
    # alpha = 0.707; eps = 0.66; l1 = 0.2797; l2 ... l7 = 0.1201
    idx, d_idx, j = declare('int', 3)
    theta, ds, hj, mj, l1, l2, alpha, eps = declare('double', 8)
    alpha = 0.707
    eps = 0.66
    l1 = 0.2796983003071741
    l2 = (1.0 - l1)/6.0
    d_idx = indices[i]

    ds = eps*d_h[d_idx]
    hj = alpha*d_h[d_idx]
    mj = d_m[d_idx]*l2
    d_m[d_idx] *= l1
    d_h[d_idx] = hj

    # Note that Feldman and Bonet suggest using the same velocity
    # for the children.
    for j in range(6):
        idx = copy_indices[6*i + j]
        theta = M_PI/3*j
        d_m[idx] = mj
        d_h[idx] = hj
        d_x[idx] = d_x[d_idx] + ds*cos(theta)
        d_y[idx] = d_y[d_idx] + ds*sin(theta)
        d_z[idx] = d_z[d_idx]
        d_u[idx] = d_u[d_idx]
        d_v[idx] = d_v[d_idx]
        d_w[idx] = d_w[d_idx]
        d_vmag[idx] = d_vmag[d_idx]
        d_uhat[idx] = d_uhat[d_idx]
        d_vhat[idx] = d_vhat[d_idx]
        d_what[idx] = d_what[d_idx]
        d_p[idx] = d_p[d_idx]
        d_rho[idx] = d_rho[d_idx]
        d_m_min[idx] = d_m_min[d_idx]
        d_m_max[idx] = d_m_max[d_idx]
        d_au[idx] = d_au[d_idx]
        d_av[idx] = d_av[d_idx]
        d_aw[idx] = d_aw[d_idx]
        d_ap[idx] = d_ap[d_idx]
        d_auhat[idx] = d_auhat[d_idx]
        d_avhat[idx] = d_avhat[d_idx]
        d_awhat[idx] = d_awhat[d_idx]



@annotate
def copy_props2d_vacondio(i, indices, copy_indices, d_x, d_y, d_z, d_u, d_v,
                          d_w, d_vmag, d_uhat, d_vhat, d_what, d_p, d_m, d_rho, d_h,
                          d_closest_idx, d_m_min, d_m_max,
                          d_au, d_av, d_aw, d_ap,
                          d_auhat, d_avhat, d_awhat):
    # Parameters from the Vacondio et al 2012 paper:
    # alpha = 0.9; eps = 0.4; l1 = 0.1787; l2 ... l7 = 0.136883333
    idx, d_idx, j = declare('int', 3)
    theta, ds, hj, mj, l1, l2, alpha, eps = declare('double', 8)
    alpha = 0.9
    eps = 0.4
    l1 = 0.178705766141917
    l2 = (1.0 - l1)/6.0
    d_idx = indices[i]

    ds = eps*d_h[d_idx]
    hj = alpha*d_h[d_idx]
    mj = d_m[d_idx]*l2
    d_m[d_idx] *= l1
    d_h[d_idx] = hj

    # Note that Feldman and Bonet suggest using the same velocity
    # for the children.
    for j in range(6):
        idx = copy_indices[6*i + j]
        theta = M_PI/3*j
        d_m[idx] = mj
        d_h[idx] = hj
        d_x[idx] = d_x[d_idx] + ds*cos(theta)
        d_y[idx] = d_y[d_idx] + ds*sin(theta)
        d_z[idx] = d_z[d_idx]
        d_u[idx] = d_u[d_idx]
        d_v[idx] = d_v[d_idx]
        d_w[idx] = d_w[d_idx]
        d_vmag[idx] = d_vmag[d_idx]
        d_uhat[idx] = d_uhat[d_idx]
        d_vhat[idx] = d_vhat[d_idx]
        d_what[idx] = d_what[d_idx]
        d_p[idx] = d_p[d_idx]
        d_rho[idx] = d_rho[d_idx]
        d_m_min[idx] = d_m_min[d_idx]
        d_m_max[idx] = d_m_max[d_idx]
        d_au[idx] = d_au[d_idx]
        d_av[idx] = d_av[d_idx]
        d_aw[idx] = d_aw[d_idx]
        d_ap[idx] = d_ap[d_idx]
        d_auhat[idx] = d_auhat[d_idx]
        d_avhat[idx] = d_avhat[d_idx]
        d_awhat[idx] = d_awhat[d_idx]


@annotate
def copy_props3d(i, indices, copy_indices, d_x, d_y, d_z, d_u, d_v, d_w,
                 d_vmag, d_uhat, d_vhat, d_what, d_p, d_m, d_rho, d_h,
                 d_closest_idx, d_m_min, d_m_max):
    # FIXME: this is not implemented yet and will not work now.
    closest_idx, c_idx, d_idx = declare('int', 3)
    xij, yij, zij, tmp, ds = declare('double', 5)
    d_idx = indices[i]
    c_idx = copy_indices[i]
    closest_idx = d_closest_idx[d_idx]
    if closest_idx > -1:
        xij = abs(d_x[d_idx] - d_x[closest_idx])
        yij = abs(d_y[d_idx] - d_y[closest_idx])
        zij = abs(d_z[d_idx] - d_z[closest_idx])
        tmp = min(min(xij, yij), zij)
        if tmp == xij:
            xij = 1.0
            yij = 0.0
            zij = 0.0
        elif tmp == yij:
            xij = 0.0
            yij = 1.0
            zij = 0.0
        else:
            xij = 0.0
            yij = 0.0
            zij = 1.0
    else:
        xij = 1.0
        yij = 0.0
        zij = 0.0

    ds = 0.5*0.6*pow(d_m[d_idx]/d_rho[d_idx], 1./3.)

    d_m[d_idx] *= 0.5
    d_x[c_idx] = d_x[d_idx] + ds*xij
    d_y[c_idx] = d_y[d_idx] + ds*yij
    d_z[c_idx] = d_z[d_idx] + ds*zij
    d_x[d_idx] -= ds*xij
    d_y[d_idx] -= ds*yij
    d_z[d_idx] -= ds*zij
    d_m[c_idx] = d_m[d_idx]
    d_u[c_idx] = d_u[d_idx]
    d_v[c_idx] = d_v[d_idx]
    d_w[c_idx] = d_w[d_idx]
    d_vmag[c_idx] = d_vmag[d_idx]
    d_uhat[c_idx] = d_uhat[d_idx]
    d_vhat[c_idx] = d_vhat[d_idx]
    d_what[c_idx] = d_what[d_idx]
    d_p[c_idx] = d_p[d_idx]
    d_rho[c_idx] = d_rho[d_idx]
    d_h[c_idx] = d_h[d_idx]
    d_m_min[c_idx] = d_m_min[d_idx]
    d_m_max[c_idx] = d_m_max[d_idx]


class AdaptiveResolutionVacondio(AdaptiveResolution):
    def __init__(self, pa, dim, rho0, boundary=None, bg=False,
                 do_mass_exchange=False, freq=5,
                 has_ghost=False, hybrid=True, nnps=None):
        self.hybrid = hybrid
        super().__init__(pa, dim, rho0, boundary, bg, do_mass_exchange, freq,
                         has_ghost, nnps=nnps)
        # Each particle is split into these many particles
        if dim == 2:
            self.N_SPLIT = 7
        else:
            raise NotImplementedError(
                'Vacondio scheme not supported in 3D yet.'
            )
        if not bg:
            if dim == 2:
                if hybrid:
                    self.copy_props = Elementwise(copy_props2d_vacondio_6_beta_1,
                                                backend=self.backend)
                else:
                    # If not hybrid use Vacondio's defaults.
                    self.copy_props = Elementwise(copy_props2d_vacondio,
                                                  backend=self.backend)
            elif dim == 3:
                self.copy_props = Elementwise(copy_props3d,
                                              backend=self.backend)
            else:
                raise NotImplementedError(
                    'Adaptive resolution not supported in 1D.'
                )

    def _post_split_update(self):
        if self.hybrid:
            self._add_remove_split_particles()
            self.sph_eval.update(update_domain=False)

    def _setup_evaluator(self):
        arrays = [self.pa]
        groups = []
        eqs = []
        name = self.name
        if self.bg:
            eqs.append(
                FindSplitMergeBase(dest=self.name, sources=[self.name])
            )
        elif self.hybrid:
            # Make hybrid work with Yang and Kong parameters.
            eqs.append(
                FindSplitMergeV(dest=self.name, sources=[self.name])
            )
        else:
            eqs.append(FindSplitMergeV(dest=self.name, sources=[self.name]))
        if self.boundary:
            names = [x.name for x in self.boundary]
            eqs.append(SkipNearBoundary(dest=self.name, sources=names))
            arrays.extend(self.boundary)
        if not self.bg:
            groups.append(Group(equations=eqs, post=self._post_split_update))
        else:
            groups.append(Group(equations=eqs))

        if not self.bg and self.do_mass_exchange:
            groups.append(Group(
                equations=[
                    PreMergeSetup(dest=name, sources=None),
                    MassExchange(dest=name, sources=[name])
                ]
            ))

        if not self.bg and self.hybrid:
            groups.append(
                Group(equations=[PreMergeSetup(dest=name, sources=[name])])
            )
            equations = [MergeIterative(dest=name, sources=[name])]
            groups.append(
                Group(equations=equations, iterate=True, max_iterations=3)
            )

        sph_eval = SPHEvaluator(
            arrays=arrays, equations=groups, dim=self.dim,
            nnps_factory=self._create_nnps
        )
        return sph_eval
