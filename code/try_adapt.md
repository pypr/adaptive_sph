---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
%matplotlib notebook
from matplotlib import pyplot as plt
import numpy as np
```

```python
import adapt
import check_adapt as CA
from pysph.base.kernels import QuinticSpline
```

## A simple example

```python
k1 = QuinticSpline(dim=1)
k2 = QuinticSpline(dim=2)
```

```python
dx = 0.05
f1 = CA.create_fluid_1d(dx=dx)
se1 = CA.get_rho_eval(f1, kernel=k1, dim=1)
x = np.linspace(-1, 1, 100)
intp1 = CA.get_interpolator(f1, x=x, kernel=k1)
```

The original distribution of $\rho$ and the given property, $u$.

```python
se1.update()
se1.evaluate()
```

```python
plt.figure()
plt.plot(f1.x, f1.rho, '-o')
plt.plot(f1.x[4:-4], f1.rho[4:-4], '-o')
```

```python
u = intp1.interpolate('u')
```

```python
plt.figure()
plt.plot(f1.x, f1.u, '-o')
plt.plot(x, u)
```

```python
CA.split1d(f1, [25], ds=0.05*0.2, h_fac=1.0)
```

```python
se1.update()
se1.evaluate()
plt.figure()
plt.plot(f1.x, f1.rho, 'o')
plt.plot(f1.x[4:-4], f1.rho[4:-4], 'o')
```

```python
intp1.update()
u = intp1.interpolate('u')
plt.figure()
plt.plot(f1.x, f1.u, 'o')
plt.plot(x, u)
```

## Experimenting with splitting particles in 1D

```python
k1 = QuinticSpline(dim=1)
dx = 0.05
f1 = CA.create_fluid_1d(dx=dx, hdx=1.0)
se1 = CA.get_rho_eval(f1, kernel=k1, dim=1)
x = np.linspace(-1, 1, 100)
intp1 = CA.get_interpolator(f1, x=x, kernel=k1)
```

```python
plt.figure()
ax1 = plt.subplot(1, 2, 1)
ax2 = plt.subplot(1, 2, 2)
ax2.plot(f1.x, f1.u, 'o')

for fac in (0.0, 0.1, 0.2, 0.3, 0.4, 0.5):
    f1 = CA.create_fluid_1d(dx=dx)
    CA.split1d(f1, [25], ds=dx*fac, h_fac=1.0)
    #CA.split1d(f1, [23, 24, 25, 26, 27], ds=dx*fac, h_fac=1.0)
    se1.update_particle_arrays([f1])
    se1.evaluate()
    ax1.plot(f1.x, f1.rho, 'o', label='fac=%.1f'%fac)
    intp1.update_particle_arrays([f1])
    u = intp1.interpolate('u')
    ax2.plot(x, u, label='fac=%.1f'%fac)
ax1.legend()
ax2.legend()

```

```python
ax1.set_ylim(0, 1.2)
```

```python
from pysph.base.kernels import Gaussian
k1 = Gaussian(dim=1)
dx = 0.05
f1 = CA.create_fluid_1d(dx=dx)
se1 = CA.get_rho_eval(f1, kernel=k1, dim=1)
x = np.linspace(-1, 1, 100)
intp1 = CA.get_interpolator(f1, x=x, kernel=k1)
```

```python
plt.figure()
ax1 = plt.subplot(1, 2, 1)
ax2 = plt.subplot(1, 2, 2)
ax2.plot(f1.x, f1.u, 'o')

for fac in (0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7):
    f1 = CA.create_fluid_1d(dx=dx)
    CA.split1d(f1, [25], ds=dx*fac, h_fac=1.0)
    se1.update_particle_arrays([f1])
    se1.evaluate()
    ax1.plot(f1.x, f1.rho, 'o', label='fac=%.1f'%fac)
    intp1.update_particle_arrays([f1])
    u = intp1.interpolate('u')
    ax2.plot(x, u, label='fac=%.1f'%fac)
ax1.legend()
ax2.legend()


```

```python

```

## 2D

```python

```

```python
ar = AdaptiveResolution(f, dim=2, rho0=1.0)
```
