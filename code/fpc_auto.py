"""
Flow past cylinder
"""
import logging
from time import time
import os
import numpy
import numpy as np
from numpy import pi, cos, sin, exp

from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme, SolidWallNoSlipBCReverse


logger = logging.getLogger()

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
u_freestream = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


def potential_flow(x, y, u_infty=1.0, center=(0, 0), diameter=1.0):
    x = x - center[0]
    y = y - center[1]
    z = x + 1j*y
    a2 = (diameter * 0.5)**2
    vel = (u_infty - a2/(z*z)).conjugate()
    u = vel.real
    v = vel.imag
    p = 500 - 0.5 * rho * np.abs(vel)**2
    return u, v, p


def compute_forces(fname, sname, nu):
    from edac import (
        MomentumEquationPressureGradient2, SummationDensityGather,
        SetWallVelocity, EvaluateNumberDensity, ComputeBeta
    )

    all_sources = [fname, sname]
    equations = []
    g1 = []
    g1.extend([
        SummationDensityGather(dest=sname, sources=all_sources),
        ComputeBeta(dest=sname, sources=all_sources, dim=2),
        EvaluateNumberDensity(dest=sname, sources=[fname]),
        SetWallVelocity(dest=sname, sources=[fname]),
    ])
    equations.append(Group(equations=g1, real=False))

    g2 = []
    g2.extend([
        MomentumEquationPressureGradient2(dest=sname, sources=[fname]),
        SolidWallNoSlipBCReverse(dest=sname, sources=[fname], nu=nu),
    ])
    equations.append(Group(equations=g2, real=True))
    return equations


class ShepardInterpolateCharacteristics(Equation):
    def initialize(self, d_idx, d_J1, d_J2u, d_J3u, d_J2v, d_J3v, d_v):
        d_J1[d_idx] = 0.0
        d_J2u[d_idx] = 0.0
        d_J3u[d_idx] = 0.0

        d_J2v[d_idx] = 0.0
        d_J3v[d_idx] = 0.0
        d_v[d_idx] = 0.0

    def loop(self, d_idx, d_J1, d_J2u, s_J1, s_J2u, d_J3u, s_J3u, s_idx, s_J2v,
             s_J3v, d_J2v, d_J3v, WIJ, s_v, d_v):
        d_J1[d_idx] += s_J1[s_idx] * WIJ
        d_J2u[d_idx] += s_J2u[s_idx] * WIJ
        d_J3u[d_idx] += s_J3u[s_idx] * WIJ

        d_J2v[d_idx] += s_J2v[s_idx] * WIJ
        d_J3v[d_idx] += s_J3v[s_idx] * WIJ

        d_v[d_idx] += s_v[s_idx] * WIJ

    def post_loop(self, d_idx, d_J1, d_J2u, d_wij, d_avg_j2u, d_avg_j1, d_J3u,
                  d_avg_j3u, d_J2v, d_J3v, d_avg_j2v, d_avg_j3v, d_v):
        if d_wij[d_idx] > 1e-14:
            d_J1[d_idx] /= d_wij[d_idx]
            d_J2u[d_idx] /= d_wij[d_idx]
            d_J3u[d_idx] /= d_wij[d_idx]

            d_J2v[d_idx] /= d_wij[d_idx]
            d_J3v[d_idx] /= d_wij[d_idx]

            d_v[d_idx] /= d_wij[d_idx]
        else:
            d_J1[d_idx] = d_avg_j1[0]
            d_J2u[d_idx] = d_avg_j2u[0]
            d_J3u[d_idx] = d_avg_j3u[0]

            d_J2v[d_idx] = d_avg_j2v[0]
            d_J3v[d_idx] = d_avg_j3v[0]

    def reduce(self, dst, t, dt):
        dst.avg_j1[0] = numpy.average(dst.J1[dst.wij > 0.0001])
        dst.avg_j2u[0] = numpy.average(dst.J2u[dst.wij > 0.0001])
        dst.avg_j3u[0] = numpy.average(dst.J3u[dst.wij > 0.0001])

        dst.avg_j2v[0] = numpy.average(dst.J2v[dst.wij > 0.0001])
        dst.avg_j3v[0] = numpy.average(dst.J3v[dst.wij > 0.0001])


class EvaluateCharacterisctics(Equation):
    def __init__(self, dest, sources, c_ref, rho_ref, u_ref, p_ref, v_ref):
        self.c_ref = c_ref
        self.rho_ref = rho_ref
        self.p_ref = p_ref
        self.u_ref = u_ref
        self.v_ref = v_ref
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_u, d_v, d_p, d_rho, d_J1, d_J2u, d_J3u, d_J2v,
                   d_J3v):
        a = self.c_ref
        rho_ref = self.rho_ref

        rho = d_rho[d_idx]
        pdiff = d_p[d_idx] - self.p_ref
        udiff = d_u[d_idx] - self.u_ref
        vdiff = d_v[d_idx] - self.v_ref

        d_J1[d_idx] = -a * a * (rho - rho_ref) + pdiff
        d_J2u[d_idx] =  rho * a * udiff + pdiff
        d_J3u[d_idx] = -rho * a * udiff + pdiff

        d_J2v[d_idx] =  rho * a * vdiff + pdiff
        d_J3v[d_idx] = -rho * a * vdiff + pdiff


class EvaluatePropertyfromCharacteristics(Equation):
    def __init__(self, dest, sources, c_ref, rho_ref, u_ref, v_ref, p_ref):
        self.c_ref = c_ref
        self.rho_ref = rho_ref
        self.p_ref = p_ref
        self.u_ref = u_ref
        self.v_ref = v_ref
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_p, d_J1, d_J2u, d_J2v, d_J3u, d_J3v, d_rho,
                   d_u, d_v, d_xn, d_yn):
        a = self.c_ref
        a2_1 = 1.0/(a*a)
        rho = d_rho[d_idx]
        xn = d_xn[d_idx]
        yn = d_yn[d_idx]

        # Characteristic in the downstream direction.
        if xn > 0.5 or yn > 0.5:
            J1 = d_J1[d_idx]
            J3 = 0.0
            if xn > 0.5:
                J2 = d_J2u[d_idx]
                d_u[d_idx] = self.u_ref + (J2 - J3) / (2 * rho * a)
            else:
                J2 = d_J2v[d_idx]
                d_v[d_idx] = self.v_ref + (J2 - J3) / (2 * rho * a)
        # Characteristic in the upstream direction.
        else:
            J1 = 0.0
            J2 = 0.0
            if xn < -0.5:
                J3 = d_J3u[d_idx]
                d_u[d_idx] = self.u_ref + (J2 - J3) / (2 * rho * a)
            else:
                J3 = d_J3v[d_idx]
                d_v[d_idx] = self.v_ref + (J2 - J3) / (2 * rho * a)

        d_rho[d_idx] = self.rho_ref + a2_1 * (-J1 + 0.5 * (J2 + J3))
        d_p[d_idx] = self.p_ref + 0.5 * (J2 + J3)


class ResetInletVelocity(Equation):
    def __init__(self, dest, sources, U, V, W):
        self.U = U
        self.V = V
        self.W = W

        super().__init__(dest, sources)

    def loop(self, d_idx, d_u, d_v, d_w, d_uref):
        if d_idx == 0:
            d_uref[0] = self.U
        d_u[d_idx] = self.U
        d_v[d_idx] = self.V
        d_w[d_idx] = self.W


class WindTunnel(Application):
    def initialize(self):
        # Geometric parameters
        self.Lt = 50.0  # length of tunnel
        self.Wt = 15.0  # half width of tunnel
        self.dc = 1.2  # diameter of cylinder
        self.nl = 10  # Number of layers for wall/inlet/outlet
        self.sol_adapt = 0.0
        self._nnps = None
        self.remove_inner_layers = False

    def add_user_options(self, group):
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=200,
            help="Reynolds number."
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--dx-min", action="store", type=float, dest="dx_min",
            default=0.025,
            help="Minimum resolution."
        )
        group.add_argument(
            "--dx-max", action="store", type=float, dest="dx_max",
            default=0.5,
            help="Maximum resolution."
        )
        cr = pow(2.0, 1.0/4)
        group.add_argument(
            "--cr", action="store", type=float, dest="cr",
            default=cr,
            help="Resolution step ratio."
        )
        group.add_argument(
            "--lt", action="store", type=float, dest="Lt", default=50,
            help="Length of the WindTunnel."
        )
        group.add_argument(
            "--wt", action="store", type=float, dest="Wt", default=25,
            help="Half width of the WindTunnel."
        )
        group.add_argument(
            "--dc", action="store", type=float, dest="dc", default=2.0,
            help="Diameter of the cylinder."
        )
        add_bool_argument(
            group, 'potential', dest='potential', default=True,
            help='Initialize with potential flow.'
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting."
        )
        add_bool_argument(
            group, 'hybrid', dest='hybrid', default=True,
            help="Use iterative merging."
        )
        group.add_argument(
            '--solution-adapt', action="store", type=float,
            dest='sol_adapt', default=self.sol_adapt,
            help=("Automatically adapt to the solution. "
                  "Argument is the cutoff fraction.")
        )
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        add_bool_argument(
            group, 'wake', dest='wake', default=False,
            help="Add wake region."
        )

    def consume_user_options(self):
        if self.options.n_damp is None:
            self.options.n_damp = 20
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.dc = self.options.dc
        self.sol_adapt = self.options.sol_adapt
        dx_min = self.options.dx_min
        dx_max = self.options.dx_max
        cr = self.options.cr
        self.dx_min = dx_min
        self.dx_max = dx_max
        self.cr = cr
        re = self.options.re

        self.nu = nu = umax * self.dc / re
        self.cxy = self.Lt / 2.5, 0.0

        self.hdx = hdx = self.options.hdx

        self.h = h = hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        dt_cfl = cfl * h / (c0 + umax)
        dt_viscous = 0.125 * h**2 / nu

        self.dt = min(dt_cfl, dt_viscous)
        self.tf = 6.0
        self.vacondio = self.options.vacondio
        self.hybrid = self.options.hybrid
        self.wake = self.options.wake

    def _create_solid(self):
        dx = self.dx_min
        h0 = self.hdx*dx

        r = np.arange(dx/2, self.dc/2, dx)
        x, y = np.array([]), np.array([])
        for i in r:
            spacing = dx
            theta = np.linspace(0, 2*pi, int(2*pi*i/spacing), endpoint=False)
            x = np.append(x,  i * cos(theta))
            y = np.append(y,  i * sin(theta))

        x += self.cxy[0]
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, m=volume*rho, rho=rho, h=h0
        )
        solid.add_constant('ds_min', dx)
        # Remove inner layers of the cylinder. This will save a lot of space if
        # dx_min is very low.
        self.remove_inner_layers = True
        return solid

    def _create_pseudo_wake1(self):
        dx = self.dx_min
        h0 = self.hdx*dx
        dc = 1.5*self.dc

        r = np.arange(dx/2, dc/2, dx)
        x, y = np.array([]), np.array([])
        for i in r:
            spacing = dx
            theta = np.linspace(0, 2*pi, int(2*pi*i/spacing), endpoint=False)
            x = np.append(x,  i * cos(theta))
            y = np.append(y,  i * sin(theta))

        x += self.cxy[0]
        m = dx*dx * rho
        wake = get_particle_array(name='wake', x=x, y=y, m=m, rho=rho, h=h0)
        wake.add_constant('ds_min', dx)
        wake.set_output_arrays(['x'])
        return wake

    def _create_pseudo_wake(self):
        dx = self.dx_min
        dc = self.dc
        x0, y0 = self.cxy
        l = 2.025*dc
        w = 1.25*dc
        y0 -= 0.5*w
        x0 -= 1.05
        h0 = self.hdx*dx
        x, y = np.mgrid[x0:x0+l:dx, y0:y0+w:dx]
        volume = dx*dx
        wake = get_particle_array(
            name='wake', x=x, y=y, m=volume*rho, rho=rho, h=h0
        )
        wake.add_constant('ds_min', dx)
        wake.set_output_arrays(['x'])
        return wake

    def _create_box(self):
        dx = self.dx_max
        m = rho * dx * dx
        h0 = self.hdx*dx
        layers = self.nl * dx
        w = self.Wt
        l = self.Lt
        x, y = np.mgrid[-layers:l+layers:dx, -w-layers:w+layers:dx]

        # First form walls, then inlet and outlet, and finally fluid.
        wall_cond = (y > w - dx/2) | (y < -w + dx/2)
        xw, yw = x[wall_cond], y[wall_cond]
        x, y = x[~wall_cond], y[~wall_cond]
        wall = get_particle_array(
            name='wall', x=xw, y=yw, m=m, h=h0, rho=rho
        )

        # Used in the Non-reflection boundary conditions. See create_equations
        # below.
        props = [
            'xn', 'yn', 'zn', 'J2v', 'J3v', 'J2u', 'J3u', 'J1', 'wij2', 'disp',
            'ioid'
        ]
        for prop in props:
            wall.add_property(prop)
        consts = [
            'avg_j2u', 'avg_j3u', 'avg_j2v', 'avg_j3v', 'avg_j1', 'uref'
        ]
        for const in consts:
            wall.add_constant(const, 0.0)
        wall.yn[wall.y > 0.0] = 1.0
        wall.yn[wall.y <= 0.0] = -1.0

        # Create Inlet.
        inlet_cond = (x < dx/2)
        xi, yi = x[inlet_cond], y[inlet_cond]
        x, y = x[~inlet_cond], y[~inlet_cond]
        inlet = get_particle_array(
            name='inlet', x=xi, y=yi, m=m, h=h0, u=u_freestream, rho=rho,
            p=0.0, uhat=u_freestream, xn=-1.0, yn=0.0
        )

        # Create Outlet.
        outlet_cond = (x > l - dx/2)
        xo, yo = x[outlet_cond], y[outlet_cond]
        # Use uhat=umax. So that the particles are moving out, if 0.0 is used
        # instead, the outlet particles will not move.
        outlet = get_particle_array(
            name='outlet', x=xo, y=yo, m=m, h=h0, u=u_freestream, rho=rho,
            p=0.0, uhat=u_freestream, vhat=0.0, xn=1.0, yn=0.0
        )

        # Create Fluid.
        xf, yf = x[~outlet_cond], y[~outlet_cond]
        fluid = get_particle_array(
            name='fluid', x=xf, y=yf, m=m, h=h0, u=u_freestream, rho=rho,
            p=0.0, vmag=0.0
        )
        setup_properties([fluid, inlet, outlet])
        return fluid, wall, inlet, outlet

    def create_nnps(self):
        if self._nnps is None:
            from pysph.base.nnps import OctreeNNPS as NNPS
            self._nnps = NNPS(dim=2.0, particles=self.particles,
                              radius_scale=3.0, cache=True)
        return self._nnps

    def _create_bg(self, dest, sources):
        bg_freq = 10 if self.sol_adapt > 0 else 500

        self._update_bg = UpdateBackground(
            dest, dim=2, boundary=sources, ds_min=self.dx_min,
            ds_max=self.dx_max, cr=self.cr, rho_ref=rho, freq=5,
            bg_freq=bg_freq, sol_adapt=self.sol_adapt
        )
        sep = '-'*70
        msg = 'Generating background (BG) particles:'
        print(sep, '\n', msg)

        start = time()
        self._update_bg.set_initial_bg()

        msg = f'time took to generate the BG particles, {time()-start:.4f}s'
        print(msg, '\n', sep)
        logger.info('BG initialize:\n%s\n  %s\n%s', sep, msg, sep)

        sources_wo_wake = [x for x in sources if x.name != 'wake']
        self._update_bg.initialize_fluid(
            solids=sources_wo_wake, vacondio=self.vacondio, hybrid=self.hybrid
        )
        bg_pa = self._update_bg.bg_pa
        return bg_pa

    def create_particles(self):
        fluid, wall, inlet, outlet = self._create_box()
        solid = self._create_solid()

        particles = [fluid, inlet, outlet, solid, wall]
        bg_sources = [solid]

        # Do not add wake, if present, to shift_sources.
        self.shift_sources = list(particles)

        if not self.wake or self.sol_adapt:
            wake = None
        else:
            wake = self._create_pseudo_wake()
            particles.append(wake)
            bg_sources.append(wake)
        self.wake = wake

        # Do not use clean=True here. The properties not used in EDAC equations
        # but used in the create_equations below will be erased.
        self.scheme.setup_properties(particles, clean=False)

        bg_pa = self._create_bg(fluid, bg_sources)

        if self.remove_inner_layers:
            # Not needed if you have multiple bodies.
            for source in bg_sources:
                if source.name == 'wake':
                    # Don't remove from the wake particles.
                    continue
                G.remove_overlap_particles(
                    bg_pa, source, self.dx_min, dim=2
                )
                retain_layers = 10
                cond = (
                    (source.x - self.cxy[0])**2
                    + (source.y - self.cxy[1])**2
                    < (self.dc/2 - retain_layers*self.dx_min)**2
                )
                indices = np.where(cond)[0]
                source.remove_particles(indices)

        particles.append(bg_pa)

        if self.options.potential:
            u, v, p = potential_flow(inlet.x, inlet.y, u_freestream, self.cxy, self.dc)
            inlet.u[:] = u
            inlet.v[:] = v
            inlet.p[:] = p
            u, v, p = potential_flow(fluid.x, fluid.y, u_freestream, self.cxy, self.dc)
            fluid.u[:] = u
            fluid.v[:] = v
            fluid.p[:] = p
            fluid.uhat[:] = u
            fluid.vhat[:] = v
            fluid.vmag[:] = np.sqrt(u**2 + v**2)
        return particles

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], cfl=None
        )
        return s

    def create_equations(self):
        from pysph.sph.equation import Group
        from edac import EvaluateNumberDensity
        from pysph.sph.bc.inlet_outlet_manager import (
            UpdateNormalsAndDisplacements
        )

        equations = self.scheme.get_equations()
        eq = []
        eq.append(
            Group(equations=[
                EvaluateCharacterisctics(
                    dest='fluid', sources=None, c_ref=c0, rho_ref=rho,
                    u_ref=u_freestream, v_ref=0.0, p_ref=0.0
                )
            ])
        )
        eq.append(
            Group(equations=[
                UpdateNormalsAndDisplacements(
                    'inlet', None, xn=-1, yn=0, zn=0, xo=0, yo=0, zo=0
                ),
                UpdateNormalsAndDisplacements(
                    'outlet', None, xn=1, yn=0, zn=0, xo=0, yo=0, zo=0
                ),
                EvaluateNumberDensity(dest='inlet', sources=['fluid']),
                ShepardInterpolateCharacteristics(dest='inlet', sources=['fluid']),
                EvaluateNumberDensity(dest='wall', sources=['fluid']),
                ShepardInterpolateCharacteristics(dest='wall', sources=['fluid']),
                EvaluateNumberDensity(dest='outlet', sources=['fluid']),
                ShepardInterpolateCharacteristics(dest='outlet', sources=['fluid']),
            ])
        )
        eq.append(Group(equations=[
            EvaluatePropertyfromCharacteristics(
                dest='wall', sources=None, c_ref=c0, rho_ref=rho,
                u_ref=u_freestream, v_ref=0.0, p_ref=0.0
            ),
            EvaluatePropertyfromCharacteristics(
                dest='inlet', sources=None, c_ref=c0, rho_ref=rho,
                u_ref=u_freestream, v_ref=0.0, p_ref=0.0
            ),
            EvaluatePropertyfromCharacteristics(
                dest='outlet', sources=None, c_ref=c0, rho_ref=rho,
                u_ref=u_freestream, v_ref=0.0, p_ref=0.0
            )])
        )
        # Remove solid bc as pressure is set by the no-reflection bc.
        wall_solid_bc = equations[2].equations.pop()
        equations = eq + equations
        return equations

    def create_tools(self):
        af = self._update_bg.adapt_fluid
        af._orig_nnps = self.create_nnps()
        # FIXME: Awful Hack!
        af.smoother = af.setup_smoothing(
            af.name, self.shift_sources, af._orig_nnps, af.dim,
            rho0=af.rho0, iters=3
        )
        arrays = list(self.particles)
        if self.wake is not None:
            arrays.remove(self.wake)
        return [self._update_bg, self._update_bg.adapt_fluid]

    def configure_scheme(self):
        scheme = self.scheme
        self.iom = self._create_inlet_outlet_manager()
        scheme.inlet_outlet_manager = self.iom
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        self.iom.update_dx(self.dx_max)
        scheme.configure(h=self.h, nu=self.nu, cfl=self.cfl)

        scheme.configure_solver(
            kernel=kernel, tf=self.tf, dt=self.dt, pfreq=pfreq, n_damp=0,
            output_at_times=list(range(1, 7))
        )

    def _get_io_info(self):
        from pysph.sph.bc.hybrid.outlet import Outlet
        from hybrid_simple_inlet_outlet import Inlet, SimpleInletOutlet

        i_has_ghost = False
        o_has_ghost = False
        i_update_cls = Inlet
        o_update_cls = Outlet
        manager = SimpleInletOutlet

        props_to_copy = [
            'x0', 'y0', 'z0', 'uhat', 'vhat', 'what', 'x', 'y', 'z',
            'u', 'v', 'w', 'm', 'h', 'rho', 'p', 'ioid'
        ]
        props_to_copy += ['u0', 'v0', 'w0', 'p0']

        inlet_info = InletInfo(
            pa_name='inlet', normal=[-1.0, 0.0, 0.0],
            refpoint=[self.dx_max/2, 0.0, 0.0], equations=None,
            has_ghost=i_has_ghost, update_cls=i_update_cls,
            umax=u_freestream
        )

        outlet_info = OutletInfo(
            pa_name='outlet', normal=[1.0, 0.0, 0.0],
            refpoint=[self.Lt - self.dx_max/2, 0.0, 0.0], equations=None,
            has_ghost=o_has_ghost, update_cls=o_update_cls,
            props_to_copy=props_to_copy
        )

        return inlet_info, outlet_info, manager

    def _create_inlet_outlet_manager(self):
        inlet_info, outlet_info, manager = self._get_io_info()
        iom = manager(
            fluid_arrays=['fluid'], inletinfo=[inlet_info],
            outletinfo=[outlet_info]
        )
        return iom

    def create_inlet_outlet(self, particle_arrays):
        iom = self.iom
        io = iom.get_inlet_outlet(particle_arrays)
        return io

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')

        t, cd, cl, cd_sf, cl_sf = self._compute_force_vs_t('fluid', 'solid')
        self._plot_coeffs(t, cd, cl, cd_sf, cl_sf)
        self.plot_centreline_velocity()
        return t, cd, cl, cd_sf, cl_sf

    def _compute_force_vs_t(self, fluid_name, solid_name):
        import gc
        from pysph.solver.utils import iter_output, load
        from pysph.tools.sph_evaluator import SPHEvaluator

        data = load(self.output_files[0])
        parrays = []
        for name in [fluid_name, solid_name]:
            parrays.append(data['arrays'][name])
        prop = ['awhat', 'auhat', 'avhat', 'wg', 'vg', 'ug', 'V', 'uf', 'vf',
                'wf', 'wij', 'vmag', 'pavg', 'nnbr', 'auf', 'avf', 'awf']
        for p in prop:
            for parray in parrays:
                if p not in parray.properties:
                    parray.add_property(p)

        # We find the force of the solid on the fluid and the opposite of that
        # is the force on the solid. Note that the assumption is that the solid
        # is far from the inlet and outlet so those are ignored.

        equations = compute_forces(fluid_name, solid_name, nu=self.nu)
        sph_eval = SPHEvaluator(
            arrays=parrays, equations=equations, dim=2,
            kernel=QuinticSpline(dim=2)
        )
        t, cd, cl, cl_sf, cd_sf = [], [], [], [], []
        msg = f"diameter: {self.dc}, dx_min: {self.dx_min}, "
        msg += f"dx_max: {self.dx_max}, Cr: {self.cr:.4f}, nu: {self.nu:.4f}"
        print(msg)

        # Don't use the zeroth file as au is zero which results in nan's.
        at_file = 1
        if os.path.exists(self.res):
            results = np.load(self.res)
            t, cl, cd = map(list, [results['t'], results['cl'], results['cd']])
            cl_sf, cd_sf = map(list, [results['cl_sf'], results['cd_sf']])
            latest_time = t[-1]
            latest_file = len(t)
            guess_file = self.output_files[latest_file]
            guess_time = load(guess_file)['solver_data']['t']
            tdiff = guess_time - latest_time
            if tdiff > 0:
                step = -1
            else:
                step = 1
            for i, file in enumerate(self.output_files[latest_file::step]):
                data = load(file)
                sd  = data['solver_data']
                total_files = len(self.output_files[latest_file::])
                print(f"Searching for latest file, {i}/{total_files}", end='\r')
                if abs(t[-1] - sd['t']) < 1e-6:
                    at_file = self.output_files.index(file)
                    break

        files_total = len(self.output_files[at_file:])
        if files_total == 1:
            data = np.load(self.res)
            return data['t'], data['cd'], data['cl'], data['cd_sf'], data['cl_sf']

        i = 0
        for sd, arrays in iter_output(self.output_files[at_file:]):
            fluid = arrays[fluid_name]
            solid = arrays[solid_name]
            for p in prop:
                solid.add_property(p)
                fluid.add_property(p)
            solid.add_property('beta')
            solid.beta[:] = 1.0
            t.append(sd['t'])
            fluid.rho[:] = 1000
            solid.rho[:] = 1000
            sph_eval.update_particle_arrays([fluid, solid])
            sph_eval.evaluate()
            fx = solid.m*solid.au
            fy = solid.m*solid.av
            auf = solid.m*solid.auf
            avf = solid.m*solid.avf
            cd.append(np.sum(fx)/(0.5 * rho * umax**2 * self.dc))
            cl.append(np.sum(fy)/(0.5 * rho * umax**2 * self.dc))
            cd_sf.append(np.sum(auf)/(0.5 * rho * umax**2 * self.dc))
            cl_sf.append(np.sum(avf)/(0.5 * rho * umax**2 * self.dc))
            msg = f"iters: {i}/{files_total}, "
            msg += f"t: {t[-1]:.4f}, Cd: {cd[-1]:.4f}, Cl: {cl[-1]:.4f}"
            msg += f" Clf: {cl_sf[-1]:.4f}, Cdf: {cd_sf[-1]:.4f}"
            print(msg)
            gc.collect()
            i += 1
        t, cd, cl, cd_sf, cl_sf = list(map(np.asarray, (t, cd, cl, cd_sf, cl_sf)))
        np.savez(self.res, t=t, cd=cd, cl=cl, cl_sf=cl_sf, cd_sf=cd_sf)
        return t, cd, cl, cd_sf, cl_sf

    def _plot_coeffs(self, t, cd, cl, cd_sf, cl_sf):
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, cd, label=r'$C_d$')
        plt.plot(t, cl, label=r'$C_l$')
        plt.plot(t, cd_sf, label=r'$C_df$')
        plt.plot(t, cl_sf, label=r'$C_lf$')
        plt.xlabel(r'$t$')
        plt.ylabel('cd/cl')
        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()

    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')

    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()

    def plot_centreline_velocity(self):
        import matplotlib.pyplot as plt
        from cyarray.carray import UIntArray
        from pysph.base import nnps
        from pysph.solver.utils import iter_output

        # interpolation points.
        n = 100
        w = 2*self.dc
        x = np.linspace(0, w, n)
        x += self.cxy[0]
        dummy = get_particle_array(name='dummy', x=x, h=1/n)

        nbrs = UIntArray()
        times = list(range(1, 6))
        t_all, xpos_all, urear_all = [], [], []
        for sd, arrays in iter_output(self.output_files):
            t = sd['t']
            if t not in times:
                continue
            fluid = arrays['fluid']
            nps = nnps.LinkedListNNPS(2, [fluid, dummy])
            xpos, urear = [], []
            for i in range(n):
                nps.get_nearest_particles(0, 1, i, nbrs)
                if len(nbrs) > 0:
                    dist = np.sqrt(
                        (fluid.x[nbrs] - dummy.x[i])**2 +
                        (fluid.y[nbrs] - dummy.y[i])**2
                    )
                    min_idx = nbrs[dist.argmin()]
                    xpos.append(fluid.x[min_idx] - self.cxy[0])
                    urear.append(fluid.u[min_idx])
            plt.plot(xpos, urear, label=f'$t$ = {t}')
            t_all.append(t)
            xpos_all.append(xpos)
            urear_all.append(urear)
        res = os.path.join(self.output_dir, 'u_rear.npz')
        np.savez(res, t=t_all, xpos=xpos_all, urear=urear_all)
        plt.legend()
        plt.savefig(os.path.join(self.output_dir, 'u_rear.png'))
        plt.show()
        plt.close()


if __name__ == '__main__':
    app = WindTunnel()
    app.run()
    app.post_process(app.info_filename)
