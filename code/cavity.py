"""Cavity with adaptive sph
"""
import os
import numpy as np
from pysph.sph.scheme import add_bool_argument
from pysph.examples.cavity import LidDrivenCavity as Cavity
from pysph.tools import geometry as G
from edac import AdaptiveEDACScheme
from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio


# domain and reference values
L = 1.0
Umax = 1.0
c0 = 10 * Umax
rho0 = 1.0
p0 = c0 * c0 * rho0


class CavityAdaptive(Cavity):
    def add_user_options(self, group):
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=100,
            help="Number of points along x direction."
        )
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=100,
            help="Reynolds number (defaults to 100)."
        )
        self.n_avg = 5
        group.add_argument(
            "--n-vel-avg", action="store", type=int, dest="n_avg",
            default=None,
            help="Average velocities over these many saved timesteps."
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting"
        )
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        add_bool_argument(
            group, 'adaptive', dest='adaptive', default=True,
            help="Use adaptive particle resolution."
        )

    def create_nnps(self):
        from pysph.base.nnps import OctreeNNPS as NNPS
        return NNPS(dim=2.0, particles=self.particles, radius_scale=3.0,
                    cache=True)

    def consume_user_options(self):
        self.dim = 2
        self.hdx = 1.0
        super().consume_user_options()
        self.vacondio = self.options.vacondio
        self.dx_min = self.dx

        h0 = self.hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        # Use the minimum of h to update dt.
        dt_cfl = cfl * h0 / (c0 + Umax)
        dt_viscous = 0.125 * h0**2 / self.nu
        dt_force = 1.0
        self.tf = 10.0
        self.dt = min(dt_cfl, dt_viscous, dt_force)
        self.options.scheme = 'edac'
        self.adaptive = self.options.adaptive

    def configure_scheme(self):
        h0 = self.hdx * self.dx
        self.scheme.configure(h=h0, nu=self.nu, cfl=self.cfl)
        self.scheme.configure_solver(tf=self.tf, dt=self.dt, pfreq=100)

    def create_scheme(self):
        s = AdaptiveEDACScheme(
            fluids=['fluid'], solids=['solid'], dim=2, c0=c0, rho0=rho0,
            nu=0.0, eps=0.0, h=None, cfl=None
        )
        return s

    def create_particles(self):
        [fluid, solid] = super().create_particles()
        # Create solid particles at the highest resolution.
        n = solid.get_number_of_particles()
        solid.remove_particles(np.arange(n))
        dx = self.dx_min
        ghost = 5*dx
        x, y = np.mgrid[-ghost - dx/2:1 + ghost + dx/2:dx,
                        -ghost - dx/2:1 + ghost + dx/2:dx]
        region = (x < 0) | (x > 1) | (y < 0) | (y > 1)
        solid.add_particles(x=np.ravel(x[region]), y=np.ravel(y[region]))
        volume = dx*dx
        solid.m[:] = volume
        solid.rho[:] = fluid.rho[0]
        solid.h[:] = dx
        y = solid.y
        solid.u[y > 1.0] = 1.0

        self.scheme.setup_properties([fluid, solid])
        fluid.x[:] += np.random.random(fluid.x.shape) * 0.1 * self.dx_min
        return [fluid, solid]

    def pre_step(self, solver):
        f = self.particles[0]
        dx = self.dx
        m0 = dx*dx
        y = f.y
        x = f.x
        f.m_max[:] = m0*1.05
        f.m_min[:] = m0*0.4
        region = (x < 0.7) & (x > 0.3) & (y > 0.3) & (y < 0.85)
        m0 *= 2
        f.m_max[region] = m0*1.05
        f.m_min[region] = m0*0.4
        region = (x < 0.6) & (x > 0.4) & (y > 0.4) & (y < 0.6)
        m0 *= 2
        f.m_max[region] = m0*1.05
        f.m_min[region] = m0*0.4

    def create_tools(self):
        if not self.adaptive:
            return []
        fluid, solid = self.particles
        if self.vacondio:
            t = AdaptiveResolutionVacondio(
                fluid, boundary=[solid], dim=2, freq=1,
                do_mass_exchange=False, hybrid=True, rho0=rho0
            )
        else:
            t = AdaptiveResolution(
                fluid, dim=2, boundary=[solid], freq=1,
                do_mass_exchange=False, rho0=rho0
            )
        smoothing_sph_eval = t.setup_smoothing(
            'fluid', [fluid, solid], self.nnps, dim=self.dim, rho0=rho0
        )

        for i in range(10):
            print("t.pre_step", i, end='\r')
            self.pre_step(None)
            t.pre_step()
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)

        G.remove_overlap_particles(fluid, solid, self.dx_min, self.dim)

        for i in range(200):
            print("Smoothing", i, end='\r')
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)
        return [t]

    def customize_output(self):
        self._mayavi_config('''
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.scalar = 'vmag'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'jet'
            b.point_size = 4.0
        ''')

    def post_process(self, info_filename):
        super().post_process(info_filename)
        import matplotlib.pyplot as plt
        from pysph.solver.utils import iter_output, load

        files = self.output_files
        t, rho_max = [], []
        for sd, array in iter_output(files, 'fluid'):
            _t = sd['t']
            t.append(_t)
            rho_max = np.append(rho_max, [array.rho.max()])
        rho_max = (rho_max - 1) * 100
        plt.clf()
        plt.plot(t, rho_max)
        plt.xlabel('time (s)')
        plt.ylabel('Density error in %')
        fig_name = os.path.join(self.output_dir, "density_error.png")
        plt.savefig(fig_name, dpi=300)

        plt.clf()

        fig, ax = plt.subplots(1, 1, figsize=(4, 3))
        fig.subplots_adjust(wspace=0.2)
        data = load(files[-1])
        f = data['arrays']['fluid']
        im = ax.scatter(
            f.x, f.y, marker='.', s=20, c=f.vmag, edgecolors='none'
        )
        plt.colorbar(im, ax=ax)
        ax.tick_params(left=False, bottom=False, labelleft=False,
                       labelbottom=False)
        ax.set_xlabel("Particle velocity magnitude distribution")
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        fig_name = os.path.join(self.output_dir, "pplot.png")
        fig.savefig(fig_name, dpi=300)
        plt.close()
        plt.clf()

        fig, ax = plt.subplots(1, 1, figsize=(4, 3))
        fig.subplots_adjust(wspace=0.2)
        data = load(files[-1])
        f = data['arrays']['fluid']
        im = ax.scatter(
            f.x, f.y, marker='.', s=20, c=f.m, edgecolors='none'
        )
        ax.tick_params(left=False, bottom=False, labelleft=False,
                       labelbottom=False)
        ax.set_xlabel("Particle mass distribution")
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        plt.colorbar(im, ax=ax, format='%.0e')
        fig_name = os.path.join(self.output_dir, "pplot_mass.png")
        fig.savefig(fig_name, dpi=300)
        plt.close()
        plt.clf()

        fig, ax = plt.subplots(1, 1, figsize=(4, 3))
        fig.subplots_adjust(wspace=0.2)
        data = load(files[-1])
        f = data['arrays']['fluid']
        im = ax.scatter(
            f.x, f.y, marker='.', s=20, c=f.rho, edgecolors='none'
        )
        plt.colorbar(im, ax=ax)
        ax.tick_params(left=False, bottom=False, labelleft=False,
                       labelbottom=False)
        ax.set_xlabel("Particle density distribution")
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        fig_name = os.path.join(self.output_dir, "pplot_rho.png")
        fig.savefig(fig_name, dpi=300)
        plt.close()
        plt.clf()


if __name__ == '__main__':
    app = CavityAdaptive()
    app.run()
    app.post_process(app.info_filename)
