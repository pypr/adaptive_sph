'''Comparison of Vacondio's original formulation with ours. L1, Linf error norm
for summation density, function and gradient approximation are printed on the
screen, various hist and scatter plots are saved in the current directory.

Sample command

    $ python optimize_h.py --openmp --shift 50 --freq 6 --pert 0.2
      --show --gradh
'''
import os
import numpy as np
from numpy import cos, pi, sin
from compyle.api import declare
from compyle.utils import ArgumentParser
import matplotlib.pyplot as plt
from matplotlib import rc

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.tools.interpolator import Interpolator
from pysph.base.kernels import QuinticSpline as Kernel
from pysph.base.nnps import DomainManager

from edac import ShiftPositionLind
from adaptv import AdaptiveResolutionVacondio
from corrections import (GradientCorrectionPreStep, GradientCorrection)


rc('font', **{'family': 'Helvetica', 'size': 8})
rc('legend', fontsize='small')
rc('axes.grid', which='both', axis='both')
# rc('axes.formatter', limits=(3, 6), use_mathtext=True)
rc('grid', linewidth=0.5, linestyle='--')
rc('xtick', direction='in', top=True)
rc('ytick', direction='in', right=True)
rc('savefig', dpi=300, format='png', pad_inches=0.1)
rc('lines', linewidth=1.5)


sizex = 1
sizey = 1


def create_domain(lx, ly):
    return DomainManager(xmin=-lx, xmax=lx, ymin=-ly, ymax=ly,
                          periodic_in_x=True, periodic_in_y=True)


class ComputeBeta1(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_beta):
        d_beta[d_idx] = 1.0

    def loop(self, d_idx, s_idx, s_m, d_rho, d_beta, d_h, SPH_KERNEL, RIJ):
        mj = s_m[s_idx]
        rhoi = d_rho[d_idx]
        hi = d_h[d_idx]

        dh_drho = -hi/(rhoi * self.dim)

        dwdq = SPH_KERNEL.dwdq(RIJ, d_h[d_idx])
        dqdh = -RIJ/(hi*hi)
        dwdh = dwdq * dqdh
        d_beta[d_idx] += -dh_drho * mj * dwdh


class ComputeBeta(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_beta):
        d_beta[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_beta, d_h, SPH_KERNEL, RIJ):
        mj = s_m[s_idx]
        dim_1 = 1.0/self.dim
        fac = mj * RIJ * dim_1
        dwdq = SPH_KERNEL.dwdq(RIJ, d_h[d_idx])
        dwdr = 1.0/d_h[d_idx] * dwdq
        d_beta[d_idx] += -fac * dwdr

    def post_loop(self, d_idx, d_rho, d_beta):
        # Division by rho in post_loop helps in putting this together with
        # summation density in the groups.
        if d_rho[d_idx] > 1e-16:
            d_beta[d_idx] /= d_rho[d_idx]
        if abs(d_beta[d_idx]) < 1e-16:
            d_beta[d_idx] = 1.0
        d_beta[d_idx] = 20


class GradH(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_dhdx, d_idx):
        d_dhdx[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_rho, d_h, DWI, d_dhdx, d_beta):
        mj = s_m[s_idx]
        rhoi = d_rho[d_idx]
        betai = d_beta[d_idx]
        hi = d_h[d_idx]
        drhodx = mj/betai * DWI[0]
        d_dhdx[d_idx] -= hi/(self.dim*rhoi) * drhodx


class SummationDensityGather(Equation):
    def initialize(self, d_idx, d_rho):
        d_rho[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_m, WI, d_Linv, DWI):

        Linv = declare('matrix(4)')
        i = declare('int')
        for i in range(4):
            Linv[i] = d_Linv[d_idx*16 + i]
        WI = Linv[0]*WI + Linv[1]*DWI[0] + Linv[2]*DWI[1] + Linv[3]*DWI[2]

        d_rho[d_idx] += s_m[s_idx]*WI


class FuncApproximationStd(Equation):
    def initialize(self, d_func, d_idx):
        d_func[d_idx] = 0

    def loop(self, d_idx, s_idx, d_func, s_p, s_m, s_rho, WI, d_Linv, DWI):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]

        Linv = declare('matrix(4)')
        i = declare('int')
        for i in range(4):
            Linv[i] = d_Linv[d_idx*16 + i]
        WI = Linv[0]*WI + Linv[1]*DWI[0] + Linv[2]*DWI[1] + Linv[3]*DWI[2]

        d_func[d_idx] += mj/rhoj * s_p[s_idx] * WI


class GradApproximationStdCorrected(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_gradx, d_grady, d_idx):
        d_gradx[d_idx] = 0
        d_grady[d_idx] = 0

    def loop(self, d_idx, s_idx, d_gradx, s_p, s_m, s_rho, d_p, d_dhdx, d_h,
             SPH_KERNEL, RIJ, DWI):
        rhoj = s_rho[s_idx]
        mj = s_m[s_idx]
        hi = d_h[d_idx]
        fac = mj/rhoj * (s_p[s_idx] - d_p[d_idx])

        dwdq = SPH_KERNEL.dwdq(RIJ, d_h[d_idx])
        dwdh = -RIJ * dwdq/(hi*hi)
        d_gradx[d_idx] += fac * (DWI[0] + dwdh*d_dhdx[d_idx])


class GradApproximationStd(Equation):
    def initialize(self, d_gradx, d_grady, d_idx):
        d_gradx[d_idx] = 0
        d_grady[d_idx] = 0

    def loop(self, d_idx, s_idx, d_gradx, d_grady, s_p, s_m, s_rho, d_p, DWI):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        fac = mj/rhoj * (s_p[s_idx] - d_p[d_idx])

        d_gradx[d_idx] += fac * DWI[0]
        d_grady[d_idx] += fac * DWI[1]


class AverageMass(Equation):
    def initialize(self, d_idx, d_n_nbrs, d_m_avg):
        d_n_nbrs[d_idx] = 0
        d_m_avg[d_idx] = 0.0

    def loop(self, d_idx, d_n_nbrs, s_m, s_idx, d_m_avg):
        d_n_nbrs[d_idx] += 1
        d_m_avg[d_idx] += s_m[s_idx]

    def post_loop(self, d_idx, d_m_avg, d_n_nbrs):
        d_m_avg[d_idx] = d_m_avg[d_idx] / d_n_nbrs[d_idx]


class OptimizeH1(Equation):
    def __init__(self, dest, sources, dim, rho0, hdx):
        self.dim = dim
        self.rho0 = rho0
        self.hdx = hdx
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_m_nbr_max):
        d_m_nbr_max[d_idx] = 0.0

    def loop(self, d_idx, s_m, s_idx, d_m_nbr_max):
        d_m_nbr_max[d_idx] = max(s_m[s_idx], d_m_nbr_max[d_idx])

    def post_loop(self, d_idx, d_h, d_m_nbr_max):
        mmax = d_m_nbr_max[d_idx]
        mass = mmax
        d_h[d_idx] = self.hdx * pow(mass/self.rho0, 1.0/self.dim)


class OptimizeH(Equation):
    def __init__(self, dest, sources, dim, rho0, hdx):
        self.dim = dim
        self.rho0 = rho0
        self.hdx = hdx
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_m_nbr_max, d_n_nbrs):
        d_m_nbr_max[d_idx] = 0.0
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, s_m, s_idx, d_m_nbr_max, d_n_nbrs):
        d_m_nbr_max[d_idx] += s_m[s_idx]
        d_n_nbrs[d_idx] += 1

    def post_loop(self, d_idx, d_h, d_m_nbr_max, d_n_nbrs):
        mass = d_m_nbr_max[d_idx]/d_n_nbrs[d_idx]
        d_h[d_idx] = self.hdx * pow(mass/self.rho0, 1.0/self.dim)


def funcxy(x, y):
    return cos(2*pi*x) + sin(2*pi*x)


def gradxy(x, y):
    return -2*pi*sin(2*pi*x) + 2*pi*cos(2*pi*x)


def create_particles(dx, dim, perturb=0.1):
    _x = np.arange(-sizex+dx/2, sizex, dx)
    _y = np.arange(-sizey+dx/2, sizey, dx)
    x, y = np.meshgrid(_x, _y)

    if dim == 2:
        m = np.ones_like(x) * (dx)**dim
        h = np.ones_like(x) * dx

        if perturb > 0.0:
            np.random.seed(1234)
            x += np.random.random(x.shape) * perturb * dx
            y += np.random.random(y.shape) * perturb * dx

        fluid = get_particle_array(name='fluid', x=x, y=y, h=h, m=m, rho=1.0)
    else:
        raise NotImplementedError("1/3-D is not implemented.")

    props = ['awhat', 'ravg', 'auhat', 'mT', 'avhat', 'n_nbrs', 'wij', 'm_avg', 'ap',
             'func', 'h_tmp', 'm_min', 'm_max', 'm_ref', 'split', 'uhat', 'vhat',
             'what', 'vmag', 'm_nbr_max', 'beta', 'dhdx', 'dhdy', 'gradx', 'grady']
    for prop in props:
        fluid.add_property(prop)
    fluid.add_property('closest_idx', type='int')

    fluid.p[:] = funcxy(fluid.x, fluid.y)
    fluid.add_property('L', stride=16)
    fluid.add_property('Linv', stride=16)
    return fluid


def shift_equations(fluid, dim, kernel):
    dx_min = min((fluid.m/fluid.rho)**(1/dim))
    fname = fluid.name
    sources = [fname]
    eq = []
    eqns = []
    eq.append(ShiftPositionLind(
        fname, sources, dim=dim
    ))
    eqns.append(Group(equations=eq))

    arrays = [fluid]
    domain_manager = create_domain(lx=sizex, ly=sizey)
    shift_eval = SPHEvaluator(
        arrays=arrays, equations=eqns, dim=dim,
        kernel=kernel, domain_manager=domain_manager
    )
    return shift_eval


def approx_equations(fluid, dim, kernel, gradh):
    fname = fluid.name
    sources = [fname]
    eqns = []
    if gradh:
        eqns.append(Group(
            equations=[
                ComputeBeta(fname, sources, dim=dim)
            ]
        ))

        eqns.append(Group(
            equations=[
                GradH(fname, sources, dim=dim)
            ]
        ))

        eqns.append(Group(
            equations=[
                GradientCorrection(fname, sources),
                FuncApproximationStd(fname, sources),
                GradApproximationStdCorrected(fname, sources, dim=dim)
            ]
        ))
    else:
        eqns.append(Group(
            equations=[
                GradientCorrection(fname, sources),
                FuncApproximationStd(fname, sources),
                GradApproximationStd(fname, sources)
            ]
        ))

    arrays = [fluid]
    domain_manager = create_domain(lx=sizex, ly=sizey)
    sph_eval = SPHEvaluator(
        arrays=arrays, equations=eqns, dim=dim,
        kernel=kernel, domain_manager=domain_manager
    )
    return sph_eval


def create_equations(fluid, dim, rho0, hdx, kernel, iters, optimize):
    fname = fluid.name
    sources = [fname]
    eq, eqns = [], []
    eq.append(Group(
        equations=[AverageMass(fname, sources)]
    ))
    if optimize:
        eq.append(Group(
            equations=[OptimizeH(fname, sources, dim, rho0, hdx)]
        ))
        iterate = True
    else:
        iterate = False
        iters = 1
    eqns.append(Group(
        equations=eq, iterate=iterate, min_iterations=iters,
        max_iterations=iters
    ))
    eqns.append(Group(
        equations=[
            GradientCorrectionPreStep(fname, sources, dim=2),
        ]
    ))
    eq = []
    eq.append(
        SummationDensityGather(fname, sources)
    )
    eqns.append(Group(equations=eq))

    arrays = [fluid]
    domain_manager = create_domain(lx=sizex, ly=sizey)
    sph_eval = SPHEvaluator(
        arrays=arrays, equations=eqns, dim=dim,
        kernel=kernel, domain_manager=domain_manager
    )
    return sph_eval


def interpolater(fluid, kernel, props):
    arrays = [fluid]
    xmin, xmax = min(fluid.x), max(fluid.x)
    x = np.linspace(xmin, xmax, 1000)
    y = np.zeros_like(x)
    interp = Interpolator(arrays, x=x, y=y, kernel=kernel,
                          method='sph')
    approx = []
    for prop in props:
        approx.append(interp.interpolate(prop))
    return x, y, approx


def density_plots(ax, fluid, m0, kernel, label):
    props = ['rho', 'h', 'm_avg', 'm']
    fluid.h[:] = np.sqrt(m0)
    x, _, (rho, h, m_avg, m) = interpolater(fluid, kernel, props)

    ax[0].plot(x, rho, label=label)
    ax[0].set_ylim(0.95, 1.05)
    ax[0].set_ylabel('rho')
    ax[0].legend()

    ax[1].plot(x, m/max(m), label='m '+label)
    ax[1].plot(x, m_avg/max(m_avg), label='m_avg ' + label)
    ax[1].set_ylabel('m_avg/max(m)')
    ax[1].legend()

    ax[2].plot(x, h/max(h), label=label)
    ax[2].set_ylabel('h')
    ax[2].legend()


def approx_plots(fig, ax, fluid, kernel, label):
    props = ['p', 'u']
    x, y, (f_approx, f_gradx) = interpolater(fluid, kernel, props)
    f_exact = funcxy(x, y)
    f_error = abs(f_approx - f_exact)/max(f_exact)
    ax[0].plot(x, f_error, label=label)
    ax[0].set_ylabel('f')
    ax[0].set_ylim(-0.01, 0.05)
    ax[0].set_xlim(-2.5, 2.5)

    grad_exact = gradxy(x, y)
    grad_error = abs(f_gradx - grad_exact)/max(grad_exact)
    ax[1].plot(x, grad_error, label=label)
    ax[1].set_ylim(-0.01, 0.05)
    ax[1].set_xlim(-2.5, 2.5)
    ax[1].set_ylabel(r'$grad_x$ f')
    handles, labels = ax[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper center')


def mass_hist(i, fig, ax, fluid, label):
    ax[i].violinplot(fluid.m/max(fluid.m))
    ax[i].set_title(f"{label} mass distribution.")


def nbrs_hist(i, fig, ax, fluid, label):
    ax[i].violinplot(fluid.n_nbrs)
    ax[i].set_title(f"{label} neighbor distribution.")


def hist_plots(i, fig, ax, fluid, label, rho0):
    ax[i].set_title(f"{label}")
    ax[i].set_ylabel("h")
    ax[i].hist(fluid.h/fluid.h.max(), density=True)

    grad_exact = gradxy(fluid.x, fluid.y)
    grad_error = abs(fluid.gradx - grad_exact)/max(grad_exact)
    ax[i+1].set_ylabel("Error in gradient")
    ax[i+1].hist(grad_error, density=True)

    ax[i+2].set_ylabel("rho")
    rho_error = abs(fluid.rho - rho0)/rho0
    ax[i+2].hist(rho_error, density=True)

    f_exact = funcxy(fluid.x, fluid.y)
    f_error = abs(fluid.func - f_exact)/max(f_exact)
    ax[i+3].set_ylabel("error in function")
    ax[i+3].hist(f_error, density=True)


def scatter_h_plots(i, fig, ax, fluid, label):
    nbrs = np.average(fluid.n_nbrs)
    print("Neighbors:", nbrs)
    x, y = fluid.x.copy(), fluid.y.copy()
    h = fluid.h.copy()
    f = ax[i].scatter(x, y, c=h/np.max(h), s=1)
    fig.colorbar(f, ax=ax[i], shrink=0.7)
    ax[i].set_axis_off()
    ax[i].set_aspect('equal', 'box')
    ax[i].margins(x=0)
    ax[i].set_title(f"{label} h.")


def scatter_rho_plots(i, fig, ax, rho0, fluid, label):
    x, y = fluid.x, fluid.y
    rho_error = abs(fluid.rho - rho0)/rho0
    f = ax[i].scatter(x, y, c=rho_error, s=1)
    fig.colorbar(f, ax=ax[i], shrink=0.7)
    ax[i].set_axis_off()
    ax[i].set_aspect('equal', 'box')
    ax[i].margins(x=0)
    ax[i].set_title(f"{label} density.")


def scatter_func_plots(i, fig, ax, fluid, label):
    x, y = fluid.x, fluid.y
    f_approx = fluid.func
    f_exact = funcxy(x, y)
    f_error = abs(f_approx - f_exact)/max(f_exact)
    f = ax[i].scatter(x, y, c=f_error, s=0.5, vmin=0, vmax=0.0075,
                      rasterized=True)
    fig.colorbar(f, ax=ax[i], shrink=0.7)
    ax[i].set_xlim(-1, 1)
    ax[i].set_ylim(-1, 1)
    ax[i].set_aspect('equal', 'box')
    ax[i].margins(x=0)
    ax[i].set_xlabel(f"{label}")


def scatter_grad_plots(i, fig, ax, fluid, label):
    x, y = fluid.x, fluid.y
    f_gradx = fluid.gradx
    grad_exact = gradxy(x, y)
    grad_error = abs(f_gradx - grad_exact)/max(grad_exact)
    f = ax[i].scatter(x, y, c=grad_error, s=0.5, vmin=0.0, vmax=0.075,
                      rasterized=True)
    fig.colorbar(f, ax=ax[i], shrink=0.7)
    ax[i].set_aspect('equal')
    ax[i].set_xlim(-1, 1)
    ax[i].set_ylim(-1, 1)
    ax[i].set_xlabel(f"{label}")


def error(fluid, rho0, label):
    x, y = fluid.x, fluid.y

    exact_max = rho0
    linf = np.max(np.abs(fluid.rho - rho0)) / exact_max
    l1_err = np.average(np.abs(fluid.rho - rho0))
    avg_vmag_e = rho0
    l1 = l1_err / avg_vmag_e
    print(f"Rho error {label}\t Linf: {linf:.2e} \t L1 = {l1:.1e}")

    f_approx = fluid.func
    f_exact = funcxy(x, y)
    exact_max = f_exact.max()
    linf = np.max(np.abs(f_approx - f_exact)) / exact_max
    l1_err = np.average(np.abs(f_approx - f_exact))
    l1 = l1_err / exact_max
    print(f"Func error {label}\t Linf: {linf:.1e} \t L1 = {l1:.1e}")

    f_gradx = fluid.gradx
    grad_exact = gradxy(x, y)
    exact_max = grad_exact.max()
    linf = np.max(np.abs(f_gradx - grad_exact)) / exact_max
    l1_err = np.average(np.abs(f_gradx - grad_exact))
    l1 = l1_err / exact_max
    print(f"Grad error {label}\t Linf: {linf:.1e} \t L1 = {l1:.1e}")
    print('------')


def adaptive(fluid, dim, m0, sph_eval, freq, hybrid, rho0):
    fluid.m_ref[:] = m0
    fluid.m_max[:] = m0*1.7
    fluid.m_min[:] = m0*0.8
    region = (np.abs(fluid.x) < sizex*0.5) & (np.abs(fluid.y) < sizey*0.5)
    fluid.m_ref[region] = m0*0.5
    fluid.m_max[region] = m0*0.5 * 1.2
    fluid.m_min[region] = m0*0.5 * 0.1
    boundary = []
    t = AdaptiveResolutionVacondio(
        fluid, boundary=boundary, dim=dim, has_ghost=True,
        do_mass_exchange=False, hybrid=hybrid, rho0=rho0, freq=1
    )

    for i in range(freq):
        print("t.pre_step", i, end='\r')
        t.pre_step(solver=sph_eval)
    print()
    sph_eval.update()


def shift_particles(fluid, dim, kernel, shift_freq):
    shift_eval = shift_equations(fluid, dim, kernel)
    for i in range(shift_freq):
        print('Shifting', i, end='\r')
        shift_eval.update()
        shift_eval.evaluate(dt=0.04)
    print()


def compute_density(fluid, rho0, hdx, m0, dim, kernel, optimize, shift,
                    freq=2):
    sph_eval = create_equations(fluid, dim, rho0, hdx, kernel, iters=1,
                                optimize=optimize)

    hybrid = bool(optimize)
    adaptive(fluid, dim, m0, sph_eval, freq, hybrid, rho0)
    if shift > 0:
        shift_particles(fluid, dim, kernel, shift)

    sph_eval.update()
    sph_eval.evaluate()


def func_approximation(fluid, dim, kernel, gradh):
    sph_eval = approx_equations(fluid, dim, kernel, gradh)
    sph_eval.update()
    sph_eval.evaluate()


def main(n=50, dim=2, show=False, shift=0, gradh=False, freq=2, perturb=0.0,
         dirname=None):
    dx = 1.0/n
    kernel = Kernel(dim=dim)

    rho0 = 1.0
    hdx = 1.0
    m0 = rho0 * pow(dx, dim)

    fig2, ax2 = plt.subplots(1, 2, figsize=(8, 4))
    fig3, ax3 = plt.subplots(1, 2, figsize=(8, 4))
    fig4, ax4 = plt.subplots(1, 2, figsize=(8, 4))
    fig5, ax5 = plt.subplots(1, 2, figsize=(8, 4))
    fig6, ax6 = plt.subplots(4, 2, figsize=(12, 8))
    ax6 = ax6.T.ravel()
    fig7, ax7 = plt.subplots(1, 2, figsize=(8, 4))
    fig8, ax8 = plt.subplots(1, 2, figsize=(8, 4))
    i, j = 0, 0
    for optimize in [False, True]:
        if not optimize:
            f = 1
        else:
            f = freq
        fluid = create_particles(dx, dim, perturb=perturb)
        compute_density(fluid, rho0, hdx, m0, dim, kernel, optimize,
                        shift, freq=f)
        fig1, ax1 = plt.subplots(1, 1, figsize=(8, 4))
        f = ax1.scatter(fluid.x, fluid.y, c=fluid.m/max(fluid.m), s=1)
        ax1.axis('equal')
        fig1.colorbar(f)

        func_approximation(fluid, dim, kernel, gradh)
        label = 'OptimizeH' if optimize else 'Vacondio'
        error(fluid, rho0, label)
        label = '(b)' if optimize else '(a)'
        scatter_func_plots(i, fig2, ax2, fluid, label)
        scatter_grad_plots(i, fig3, ax3, fluid, label)
        scatter_rho_plots(i, fig4, ax4, rho0, fluid, label)
        mass_hist(i, fig5, ax5, fluid, label)
        nbrs_hist(i, fig8, ax8, fluid, label)
        hist_plots(j, fig6, ax6, fluid, label, rho0)
        j += 4
        scatter_h_plots(i, fig7, ax7, fluid, label)
        i += 1
    if dirname is None:
        dirname = 'optimize_h_output'
    os.makedirs(dirname, exist_ok=True)
    fig1.savefig(os.path.join(dirname, "comparison_mass.png"))
    fig2.savefig(os.path.join(dirname, "comparison_func.png"), dpi=150)
    fig3.savefig(os.path.join(dirname, "comparison_grad.png"), dpi=150)
    fig4.savefig(os.path.join(dirname, "comparison_rho.png"))
    fig5.savefig(os.path.join(dirname, "comparison_mass.png"))
    fig6.savefig(os.path.join(dirname, "comparison_h.png"))
    fig7.savefig(os.path.join(dirname, "comparison_scatter_h.png"))
    fig8.savefig(os.path.join(dirname, "comparison_nbrs.png"))
    if show:
        plt.show()


if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('-n', action='store', type=int, dest='n',
                   default=50, help='Particle discretization.')
    p.add_argument('--dim', action='store', type=int, dest='dim',
                   default=2, help='Dimension of the simulation.')
    p.add_argument('--perturb', action='store', type=float, dest='perturb',
                   default=0.0, help='Perturb initial positions.')
    p.add_argument('--freq', action='store', type=int, dest='freq',
                   default=2, help='Adaptive pre_step frequency.')
    p.add_argument('--shift', action='store', type=int, dest='shift',
                   default=2, help='No of times to Shift the particles.')
    p.add_argument('-d', action='store', type=str, dest='dirname',
                   default=None, help='Directory name.')
    p.add_argument(
        '--show', action='store_true', dest='show',
        default=False, help='Show plots at the end of simulation.'
    )
    p.add_argument(
        '--gradh', action='store_true', dest='grad_h',
        default=False, help='Use grad h corrections.'
    )
    p.add_argument(
        '--print-info', action='store_true', dest='print_info',
        default=False, help='Print values for each alpha and eps.'
    )
    o = p.parse_args()
    main(n=o.n, dim=o.dim, show=o.show, shift=o.shift, gradh=o.grad_h,
         freq=o.freq, perturb=o.perturb, dirname=o.dirname)
