"""Gresho Chan vortex problem.
"""

import os
import numpy as np
from numpy import sin, cos, pi, log
from pysph.base.utils import get_particle_array
from pysph.base.nnps import DomainManager
from pysph.sph.scheme import add_bool_argument
from pysph.solver.application import Application

from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio
from edac import AdaptiveEDACScheme


L = 1.0


def exact_solution(x, y):
    r = np.sqrt(x*x + y*y)
    u_theta, p = np.zeros((2,) + x.shape)
    u_theta[r >= 0.4] = 0.0
    u_theta[r < 0.4] = 2 - 5*r[r < 0.4]
    u_theta[r < 0.2] = 5*r[r < 0.2]
    p[r >= 0.4] = 3 + 4 * log(2)
    p[r < 0.4] = (
        9 - 4 * log(0.2) + 12.5 * r[r < 0.4]**2
        - 20 * r[r < 0.4] + 4 * log(r[r < 0.4])
    )
    p[r < 0.2] = 5 + 12.5 * r[r < 0.2]**2
    theta = np.arctan2(y, x)
    theta[theta < 0.0] += 2*pi
    u, v = -u_theta * sin(theta), u_theta * cos(theta)
    return u, v, p, theta


class GreshoChan(Application):
    def initialize(self):
        self.rho0 = 1.0
        self.L = L
        self.U = 1.0
        self.c0 = 10 * (self.U + 1)
        self.dim = 2

    def add_user_options(self, group):
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        group.add_argument(
            "--perturb", action="store", type=float, dest="perturb", default=0,
            help="Random perturbation of initial particles as a fraction "
            "of dx (setting it to zero disables it, the default)."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=50,
            help="Number of points along x direction. (default 50)"
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.0,
            help="Ratio h/dx."
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting."
        )
        add_bool_argument(
            group, 'adaptive', dest='adaptive', default=True,
            help="Use adaptive particle resolution."
        )

    def create_domain(self):
        return DomainManager(
            xmin=-self.L/2, xmax=self.L/2, ymin=-self.L/2, ymax=self.L/2,
            periodic_in_x=True, periodic_in_y=True, n_layers=3.0
        )

    def consume_user_options(self):
        if self.options.adaptive:
            layers = 1
        else:
            layers = 0
        nx = self.options.nx
        self.dx = dx = self.L / nx
        self.dx_min = self.dx/np.sqrt(2)**layers
        self.volume = dx * dx
        self.hdx = self.options.hdx

        self.h0 = self.hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        self.dt = cfl * self.h0 / (self.U+self.c0)

        self.tf = 3.0
        self.kernel_correction = False
        self.vacondio = self.options.vacondio
        self.adaptive = self.options.adaptive

    def create_particles(self):
        dx = self.dx

        _x = np.arange(-L/2 + dx/2, L/2, dx)
        x, y = np.meshgrid(_x, _x)
        if self.options.perturb > 0:
            np.random.seed(1)
            factor = dx * self.options.perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor

        m = self.volume * self.rho0
        h = self.hdx * dx
        u, v, p, theta = exact_solution(x, y)

        fluid = get_particle_array(
            name='fluid', x=x, y=y, m=m, h=h, u=u, v=v, rho=self.rho0, p=p
        )
        self.scheme.setup_properties([fluid])

        for pa in [fluid]:
            pa.add_property('theta')
            pa.theta[:] = theta.ravel()
            pa.add_output_arrays(['theta'])
        fluid.add_property('gid')
        no_of_particles = fluid.get_number_of_particles()
        fluid.gid[:] = np.arange(no_of_particles)
        return [fluid]

    def configure_scheme(self):
        scheme = self.scheme
        pfreq = 200
        scheme.configure(h=self.h0, nu=0.0, cfl=self.cfl)
        scheme.configure_solver(
            tf=self.tf, dt=self.dt, pfreq=pfreq,
        )

    def create_scheme(self):
        domain = self.create_domain()
        edac = AdaptiveEDACScheme(
            ['fluid'], [], dim=2, rho0=self.rho0, c0=self.c0,
            nu=None, h=None, cfl=None, alpha=0.0, has_ghosts=True,
            domain=domain
        )
        return edac

    def pre_step(self, solver):
        fluid = self.particles[0]
        dx = self.dx
        m0 = self.rho0*dx*dx
        rij = np.sqrt(fluid.x**2 + fluid.y**2)
        angle = np.pi + np.arctan2(fluid.x/rij, -fluid.y/rij)
        fluid.m_max[:] = m0*1.05
        fluid.m_min[:] = m0*0.4
        m0 *= 0.5

        region = ((angle > 0/1 * np.pi) & (angle < 1/1 * np.pi)
                  & (rij > 0.01*self.L) & (rij < 0.45*self.L))
        fluid.m_max[region] = m0*1.05
        fluid.m_min[region] = m0*0.0

    def create_tools(self):
        if not self.adaptive:
            return []
        arrays = self.particles
        if self.vacondio:
            t = AdaptiveResolutionVacondio(
                self.particles[0], dim=2, has_ghost=True,
                do_mass_exchange=False, hybrid=True, rho0=self.rho0
            )
        else:
            t = AdaptiveResolution(
                self.particles[0], dim=2, has_ghost=True,
                do_mass_exchange=False, rho0=self.rho0
            )

        # smoothing_sph_eval = t.setup_smoothing(
        #     dest='fluid', arrays=arrays, nnps=self.nnps,
        #     dim=self.dim, rho0=self.rho0
        # )

        # for i in range(8):
        #     print("t.pre_step", i, end='\r')
        #     self.pre_step(None)
        #     t.pre_step(solver=self.solver)
        #     smoothing_sph_eval.update()
        #     smoothing_sph_eval.evaluate(dt=self.dt)

        # for i in range(100):
        #     print("Smoothing", i, end='\r')
        #     smoothing_sph_eval.update()
        #     smoothing_sph_eval.evaluate(dt=self.dt)
        # print()
        # fluid = arrays[0]
        # u, v, p, theta = exact_solution(x=fluid.x, y=fluid.y)
        # fluid.u[:] = u
        # fluid.v[:] = v
        # fluid.au[:] = 0.0
        # fluid.av[:] = 0.0
        # fluid.p[:] = p
        # fluid.theta[:] = theta
        # fluid.vmag[:] = np.sqrt(u**2 + v**2)
        return [t]

    def post_process(self, info_filename):
        self.read_info(info_filename)
        import matplotlib.pyplot as plt
        from pysph.solver.utils import load

        files = self.output_files
        data = load(files[-1])
        f = data['arrays']['fluid']
        t = data['solver_data']['t']
        r1 = np.sqrt(f.x**2 + f.y**2)
        vmag = np.sqrt(f.u**2 + f.v**2)
        ue, ve, _, _ = exact_solution(f.x, f.y)
        vmag_l1 = np.abs(f.u-ue) + np.abs(f.v-ve)
        l1norm = np.mean(vmag_l1)
        plt.scatter(r1, vmag, s=1, color='k')
        r = np.linspace(0, 1, 1000)
        u_theta = np.zeros(r.shape)
        u_theta[r >= 0.4] = 0.0
        u_theta[r < 0.4] = 2 - 5*r[r < 0.4]
        u_theta[r < 0.2] = 5*r[r < 0.2]
        umage = np.abs(u_theta)
        plt.plot(r, umage, 'r-')
        plt.annotate(rf"$t={t:.2f}$s", (0.5, 1.0), fontsize='xx-large')
        plt.annotate(rf"$L_1$ = {l1norm:.2e}", (0.5, 0.9), fontsize='xx-large')
        plt.xlabel(r"$r$, Distance from the center of the vortex.")
        plt.ylabel(r"$|\mathbf{v}|$, velocity magnitude.")
        plt.xlim(0, self.L)
        plt.ylim(-0.1, 1.2)
        plt.tight_layout(pad=0)
        fig_name = os.path.join(self.output_dir, "r_vs_vmag.png")
        plt.savefig(fig_name, dpi=300)
        plt.clf()

        plt.scatter(f.x, f.y, c=vmag, s=1, cmap='jet')
        plt.xlabel(r"$x$")
        plt.ylabel(r"$y$")
        plt.colorbar()
        plt.xlim(-self.L/2, self.L/2)
        plt.ylim(-self.L/2, self.L/2)
        fig_name = os.path.join(self.output_dir, "pplot.png")
        plt.savefig(fig_name, dpi=300)
        plt.clf()

        t_total, mom_x, mom_y, angular_mom, decay, tot_mass = [], [], [], [], [], []
        for i, file in enumerate(files):
            data = load(file)
            f = data['arrays']['fluid']
            t = data['solver_data']['t']
            print(f"Processed {i}/{len(files)}. At time = {t:.2f}s", end='\r')
            t_total.append(t)
            mom_x.append(np.abs(np.sum(f.m * f.u)))
            mom_y.append(np.abs(np.sum(f.m * f.v)))
            tot_mass.append(np.sum(f.m))
            angular_mom.append(np.abs(np.sum(f.m*(f.x * f.v - f.y * f.u))))
            vmag = np.sqrt(f.u**2 + f.v**2)
            decay.append(np.max(vmag))

        fig, ax = plt.subplots(1, 3, figsize=(16, 4), constrained_layout=True)
        ax[0].semilogy(t_total, mom_x)
        ax[1].semilogy(t_total, mom_y)
        ax[2].semilogy(t_total, angular_mom)
        ax[0].set_ylabel(r"Total momentum in $x$-direction")
        ax[1].set_ylabel(r"Total momentum in $y$-direction")
        ax[2].set_ylabel(r"Angular momentum in $z$-direction")
        for i in range(3):
            ax[i].set_xlabel("time")
        fig_name = os.path.join(self.output_dir, "conservation.png")
        fig.savefig(fig_name, dpi=300)
        plt.clf()

        plt.plot(t_total, decay)
        plt.xlabel(r"$t$")
        plt.ylabel('max velocity')
        fig_name = os.path.join(self.output_dir, "decay.png")
        plt.savefig(fig_name, dpi=300)
        plt.clf()

        fname = os.path.join(self.output_dir, 'results.npz')
        np.savez(
            fname, t=t, r=r, umage=umage, l1norm=np.array([l1norm]),
            t_total=t_total, mom_x=mom_x, mom_y=mom_y, angular_mom=angular_mom,
            decay=decay, tot_mass=tot_mass
        )


if __name__ == '__main__':
    app = GreshoChan()
    app.run()
    app.post_process(app.info_filename)
