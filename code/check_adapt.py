from collections import defaultdict
import numpy as np

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Group
from pysph.sph.basic_equations import SummationDensity
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.tools.interpolator import Interpolator


def create_fluid_1d(dx, hdx=1.0):
    x = np.arange(-1, 1 + dx/2, dx)
    u = np.sin(x*np.pi*2)
    rho = 1.0
    m = rho*dx
    pa = get_particle_array(
        name='fluid', x=x, u=u, rho=rho, m=m, h=dx*hdx, m_ref=m
    )
    return pa


def create_fluid_2d(dx, hdx=1.0):
    x, y = np.mgrid[-1:1:dx, -1:1:dx]
    k = 2*np.pi
    u = np.sin(k*x)*np.cos(k*x)
    rho = 1.0
    m = rho*dx
    pa = get_particle_array(
        name='fluid', x=x, y=y, u=u, rho=rho, m=m, h=dx*hdx, m_ref=m
    )
    for prop in ('uhat', 'vhat', 'what', 'au', 'au',
                 'au', 'vmag'):
        pa.add_property(prop)

    return pa


def get_rho_eval(pa, kernel=None, dim=1):
    name = pa.name
    eqs = [SummationDensity(dest=name, sources=[name])]
    g = Group(equations=eqs)
    sph_eval = SPHEvaluator(arrays=[pa], equations=[g], dim=dim, kernel=kernel)
    return sph_eval


def get_interpolator(pa, x=None, y=None, z=None, kernel=None, method='sph'):
    intp = Interpolator([pa], x=x, y=y, z=z, kernel=kernel, method=method)
    return intp


def split1d(pa, indices, ds, h_fac):
    '''Split the particles given by the indices in the given array and move them
    apart by ds.

    '''
    props = defaultdict(list)
    for i in indices:
        pa.m[i] *= 0.5
        x = pa.x[i]
        pa.x[i] = x - ds*0.5
        pa.h[i] *= h_fac
        props['x'].append(x+ds*0.5)
        props['m'].append(pa.m[i])
        props['rho'].append(pa.rho[i])
        props['h'].append(pa.h[i])
        props['u'].append(pa.u[i])

    pa.add_particles(**props)
