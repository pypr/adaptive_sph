"""
Fourth-order Runge-Kutta integrator for EDAC scheme.
"""
from math import sqrt
from pysph.sph.integrator import Integrator
from pysph.sph.integrator_step import IntegratorStep


class RK3Integrator(Integrator):
    def one_timestep(self, t, dt):
        self.initialize()

        # stage 1
        self.compute_accelerations()
        self.stage1()
        self.update_domain()
        self.do_post_stage(1./6*dt, 1)

        # stage 2
        self.compute_accelerations(update_nnps=False)
        self.stage2()
        self.update_domain()
        self.do_post_stage(1./3*dt, 2)

        # stage 3
        self.compute_accelerations()
        self.stage3()
        self.update_domain()
        self.do_post_stage(1./3*dt, 3)


class EDACTVFRK3Step(IntegratorStep):
    def initialize(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z, d_u0, d_v0,
                   d_w0, d_u, d_v, d_w, d_p0, d_p, d_vmag):
        d_x0[d_idx] = d_x[d_idx]
        d_y0[d_idx] = d_y[d_idx]
        d_z0[d_idx] = d_z[d_idx]

        d_u0[d_idx] = d_u[d_idx]
        d_v0[d_idx] = d_v[d_idx]
        d_w0[d_idx] = d_w[d_idx]

        d_p0[d_idx] = d_p[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

    def stage1(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
               d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_au,
               d_av, d_auhat, d_avhat, d_awhat, d_uhat, d_vhat, d_what,
               d_aw, d_ap, dt, d_vmag):
        d_u[d_idx] = d_u0[d_idx] + dt * d_au[d_idx]
        d_v[d_idx] = d_v0[d_idx] + dt * d_av[d_idx]
        d_w[d_idx] = d_w0[d_idx] + dt * d_aw[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])
        d_uhat[d_idx] = d_u[d_idx] + dt * d_auhat[d_idx]
        d_vhat[d_idx] = d_v[d_idx] + dt * d_avhat[d_idx]
        d_what[d_idx] = d_w[d_idx] + dt * d_awhat[d_idx]

        d_x[d_idx] = d_x0[d_idx] + dt * d_uhat[d_idx]
        d_y[d_idx] = d_y0[d_idx] + dt * d_vhat[d_idx]
        d_z[d_idx] = d_z0[d_idx] + dt * d_what[d_idx]

        d_p[d_idx] = d_p0[d_idx] + dt * d_ap[d_idx]

    def stage2(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
               d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_au, d_av,
               d_aw, d_auhat, d_avhat, d_awhat, d_uhat, d_vhat, d_what,
               d_ap, dt, d_vmag):
        d_u[d_idx] = 0.25 * (3 * d_u0[d_idx] + d_u[d_idx] + dt * d_au[d_idx])
        d_v[d_idx] = 0.25 * (3 * d_v0[d_idx] + d_v[d_idx] + dt * d_av[d_idx])
        d_w[d_idx] = 0.25 * (3 * d_w0[d_idx] + d_w[d_idx] + dt * d_aw[d_idx])

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

        d_uhat[d_idx] = d_u[d_idx] + 0.25 * dt * d_auhat[d_idx]
        d_vhat[d_idx] = d_v[d_idx] + 0.25 * dt * d_avhat[d_idx]
        d_what[d_idx] = d_w[d_idx] + 0.25 * dt * d_awhat[d_idx]

        d_x[d_idx] = 0.25 * (3 * d_x0[d_idx] + d_x[d_idx] + dt * d_uhat[d_idx])
        d_y[d_idx] = 0.25 * (3 * d_y0[d_idx] + d_y[d_idx] + dt * d_vhat[d_idx])
        d_z[d_idx] = 0.25 * (3 * d_z0[d_idx] + d_z[d_idx] + dt * d_what[d_idx])

        d_p[d_idx] = 0.25 * (3 * d_p0[d_idx] + d_p[d_idx] + dt * d_ap[d_idx])

    def stage3(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
               d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_au, d_av,
               d_aw, d_auhat, d_avhat, d_awhat, d_uhat, d_vhat, d_what,
               d_ap, d_vmag, dt):

        d_u[d_idx] = 1./3 * (d_u0[d_idx] + 2. * (d_u[d_idx] + dt * d_au[d_idx]))
        d_v[d_idx] = 1./3 * (d_v0[d_idx] + 2. * (d_v[d_idx] + dt * d_av[d_idx]))
        d_w[d_idx] = 1./3 * (d_w0[d_idx] + 2. * (d_w[d_idx] + dt * d_aw[d_idx]))

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

        d_uhat[d_idx] = d_u[d_idx] + 2./3 * dt * d_auhat[d_idx]
        d_vhat[d_idx] = d_v[d_idx] + 2./3 * dt * d_avhat[d_idx]
        d_what[d_idx] = d_w[d_idx] + 2./3 * dt * d_awhat[d_idx]

        d_x[d_idx] = 1./3 * (d_x0[d_idx] + 2. * (d_x[d_idx] + dt*d_uhat[d_idx]))
        d_y[d_idx] = 1./3 * (d_y0[d_idx] + 2. * (d_y[d_idx] + dt*d_vhat[d_idx]))
        d_z[d_idx] = 1./3 * (d_z0[d_idx] + 2. * (d_z[d_idx] + dt*d_what[d_idx]))

        d_p[d_idx] = 1./3 * (d_p0[d_idx] + 2. * (d_p[d_idx] + dt*d_ap[d_idx]))
