"""
Flow past cylinder
"""
import os
import numpy as np
from numpy import cos, sin, pi, exp

from compyle.api import declare
from pysph.sph.equation import Equation
from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument
from pysph.solver.utils import load, iter_output
from pysph.tools.sph_evaluator import SPHEvaluator

from edac import AdaptiveEDACScheme
from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio


# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


def potential_flow(x, y, u_infty=1.0, center=(0, 0), diameter=1.0):
    x = x - center[0]
    y = y - center[1]
    z = x + 1j*y
    a2 = (diameter * 0.5)**2
    vel = (u_infty - a2/(z*z)).conjugate()
    u = vel.real
    v = vel.imag
    p = 500 - 0.5 * rho * np.abs(vel)**2
    return u, v, p


class SolidWallNoSlipBCReverse(Equation):
    def __init__(self, dest, sources, nu):
        self.nu = nu
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_auf, d_avf, d_awf):
        d_auf[d_idx] = 0.0
        d_avf[d_idx] = 0.0
        d_awf[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho,
             d_ug, d_vg, d_wg,
             d_auf, d_avf, d_awf,
             s_u, s_v, s_w,
             DWI, DWJ, R2IJ, EPS, XIJ):
        mj = s_m[s_idx]
        rhoij = (d_rho[d_idx] + s_rho[s_idx])
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWJ[0] + DWI[0])
        dwij[1] = 0.5 * (DWJ[1] + DWI[1])
        dwij[2] = 0.5 * (DWJ[2] + DWI[2])
        Fij = dwij[0]*XIJ[0] + dwij[1]*XIJ[1] + dwij[2]*XIJ[2]

        tmp = 4 * mj * self.nu * Fij / (rhoij * (R2IJ + EPS))

        d_auf[d_idx] += tmp * (d_ug[d_idx] - s_u[s_idx])
        d_avf[d_idx] += tmp * (d_vg[d_idx] - s_v[s_idx])
        d_awf[d_idx] += tmp * (d_wg[d_idx] - s_w[s_idx])


class ResetInletVelocity(Equation):
    def __init__(self, dest, sources, U, V, W):
        self.U = U
        self.V = V
        self.W = W

        super().__init__(dest, sources)

    def loop(self, d_idx, d_u, d_v, d_w, d_uref):
        if d_idx == 0:
            d_uref[0] = self.U
        d_u[d_idx] = self.U
        d_v[d_idx] = self.V
        d_w[d_idx] = self.W


class WindTunnel(Application):
    def initialize(self):
        # Geometric parameters
        self.Lt = 50.0  # length of tunnel
        self.Wt = 25.0  # half width of tunnel
        self.dc = 1.2  # diameter of cylinder
        self.cxy = 20., 0.0  # center of cylinder
        self.nl = 10  # Number of layers for wall/inlet/outlet
        self.dim = 2

    def add_user_options(self, group):
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=200,
            help="Reynolds number."
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=20,
            help="Number of points in 1D of the cylinder."
        )
        group.add_argument(
            "--lt", action="store", type=float, dest="Lt", default=50,
            help="Length of the WindTunnel."
        )
        group.add_argument(
            "--wt", action="store", type=float, dest="Wt", default=25,
            help="Half width of the WindTunnel."
        )
        group.add_argument(
            "--dc", action="store", type=float, dest="dc", default=2.0,
            help="Diameter of the cylinder."
        )
        add_bool_argument(
            group, 'potential', dest='potential', default=True,
            help='Initialize with potential flow.'
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting"
        )
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        add_bool_argument(
            group, 'hybrid', dest='hybrid', default=True,
            help="Use iterative merging."
        )

    def consume_user_options(self):
        if self.options.n_damp is None:
            self.options.n_damp = 20
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.dc = self.options.dc
        nx = self.options.nx
        re = self.options.re

        self.nu = nu = umax * self.dc / re
        # self.cxy = self.Lt / 5., 0.0

        self.dx = dx = self.dc / nx
        self.volume = dx * dx
        self.hdx = hdx = self.options.hdx
        hdx = self.options.hdx
        self.nl = (int)(10.0*hdx)

        layers = 3
        self.dx_min = self.dx / pow(7, layers/self.dim)
        self.h_min = hdx * self.dx_min

        self.h = h = hdx * self.dx
        self.cfl = cfl = self.options.cfl_factor
        dt_cfl = cfl * self.h_min / (c0 + umax)
        dt_viscous = 0.125 * h**2 / nu

        self.dt = min(dt_cfl, dt_viscous)
        print("dt is ", self.dt)
        self.tf = 6.0
        self.vacondio = self.options.vacondio
        self.hybrid = self.options.hybrid

    def _create_fluid(self):
        dx = self.dx
        h0 = self.hdx*dx
        x, y = np.mgrid[dx / 2:self.Lt:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        fluid = get_particle_array(
            name='fluid', m=m, x=x, y=y, h=h0, V=1.0 / volume, u=umax,
            p=0.0, rho=rho, vmag=0.0)
        xc, yc = self.cxy
        xnew = x - xc
        ynew = y - yc
        znew = xnew + 1j*ynew
        a2 = (self.dc * 0.5)**2
        vel = (umax - a2/(znew*znew)).conjugate()
        return fluid

    def _create_solid(self, dx):
        h0 = self.hdx*dx

        r = np.arange(dx/2, self.dc/2, dx)
        x, y = np.array([]), np.array([])
        for i in r:
            # spacing = 10*dx if i < self.dc/2 - 5*dx else dx
            spacing = dx
            theta = np.linspace(0, 2*pi, int(2*pi*i/spacing), endpoint=False)
            x = np.append(x,  i * cos(theta))
            y = np.append(y,  i * sin(theta))

        x += self.cxy[0]
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y,
            m=volume*rho, rho=rho, h=h0, V=1.0/volume)
        solid.add_constant('ds_min', dx)
        return solid

    def _create_wall(self):
        dx = self.dx
        h0 = self.h
        x0, y0 = np.mgrid[
            dx/2: self.Lt+self.nl*dx+self.nl*dx: dx, dx/2: self.nl*dx: dx]
        x0 -= self.nl*dx
        y0 -= self.nl*dx+self.Wt
        x0 = np.ravel(x0)
        y0 = np.ravel(y0)

        x1 = np.copy(x0)
        y1 = np.copy(y0)
        y1 += self.nl*dx+2*self.Wt
        x1 = np.ravel(x1)
        y1 = np.ravel(y1)

        x0 = np.concatenate((x0, x1))
        y0 = np.concatenate((y0, y1))
        volume = dx*dx
        wall = get_particle_array(
            name='wall', x=x0, y=y0, m=volume*rho, rho=rho, h=h0,
            V=1.0/volume)
        return wall

    def _set_wall_normal(self, pa):
        props = ['xn', 'yn', 'zn']
        for p in props:
            pa.add_property(p)

        y = pa.y
        cond = y > 0.0
        pa.yn[cond] = 1.0
        cond = y < 0.0
        pa.yn[cond] = -1.0

    def _create_outlet(self):
        dx = self.dx
        h0 = self.h
        nl = self.nl
        x, y = np.mgrid[dx/2:nl * dx:dx,  -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x += self.Lt
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        # Use uhat=umax. So that the particles are moving out, if 0.0 is used
        # instead, the outlet particles will not move.
        outlet = get_particle_array(
            name='outlet', x=x, y=y, m=m, h=h0, V=1.0/volume, u=umax,
            p=0.0, rho=one * rho, uhat=umax, vhat=0.0)
        return outlet

    def _create_inlet(self):
        dx = self.dx
        h0 = self.h
        nl = self.nl
        x, y = np.mgrid[dx / 2:nl*dx:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x = x - nl * dx
        one = np.ones_like(x)
        volume = one * dx * dx

        inlet = get_particle_array(
            name='inlet', x=x, y=y, m=volume * rho, h=h0, u=umax, rho=rho,
            V=1.0 / volume, p=0.0)
        return inlet

    def create_nnps(self):
        from pysph.base.nnps import OctreeNNPS as NNPS
        return NNPS(dim=2.0, particles=self.particles, radius_scale=3.0,
                    cache=True)

    def create_particles(self):
        dx = self.dx
        fluid = self._create_fluid()
        solid = self._create_solid(self.dx_min)
        outlet = self._create_outlet()
        inlet = self._create_inlet()
        wall = self._create_wall()

        ghost_inlet = self.iom.create_ghost(inlet, inlet=True)
        ghost_outlet = self.iom.create_ghost(outlet, inlet=False)

        particles = [fluid, inlet, outlet, solid, wall]
        if ghost_inlet:
            particles.append(ghost_inlet)
        if ghost_outlet:
            particles.append(ghost_outlet)

        self.scheme.setup_properties(particles)

        fluid.add_property('htmp')
        fluid.add_property('n_nbrs', type='int')
        fluid.add_output_arrays(['h'])
        for pa in [fluid]:
            pa.add_property('vmag')
            pa.add_property('m_ref')
            pa.add_property('closest_idx', type='int')
            pa.add_property('split', type='int')
            pa.add_output_arrays(['m_ref', 'closest_idx', 'split'])

        self._set_wall_normal(wall)

        fluid.uag[:] = 1.0
        fluid.uta[:] = 1.0
        outlet.uta[:] = 1.0
        if not self.hybrid:
            fluid.add_property('mT')
            fluid.add_property('ravg')
        return particles

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], cfl=None
        )
        return s

    def pre_step(self, solver):
        f = self.particles[0]
        dx = self.dx
        m0 = rho*dx*dx
        x = f.x
        y = f.y
        f.m_max[:] = m0*1.05
        f.m_min[:] = m0*0.6

        region = (np.abs(y) < 6) & (x > 12) & (x < 28)
        f.m_max[region] = m0*1.05/7**1
        f.m_min[region] = m0*0.0
        region = (np.abs(y) < 4) & (x > 14) & (x < 26)
        f.m_max[region] = m0*1.05/7**2
        f.m_min[region] = m0*0.0
        region = (np.abs(y) < 2.5) & (x > 16) & (x < 24)
        f.m_max[region] = m0*1.05/7**3
        f.m_min[region] = m0*0.0
        # region = (np.abs(y) < 1.5) & (x > 18) & (x < 22)
        # f.m_max[region] = m0*1.2/7**4
        # f.m_min[region] = m0*0.0

    def create_tools(self):
        fluid = self.particles[0]
        solid = self.particles[3]
        if self.vacondio:
            t = AdaptiveResolutionVacondio(
                self.particles[0], boundary=[],
                dim=2, do_mass_exchange=False, hybrid=self.hybrid
            )
        else:
            t = AdaptiveResolution(
                self.particles[0], boundary=[],
                dim=2, do_mass_exchange=False
            )

        for i in range(8):
            print("t.pre_step", i, end='\r')
            self.pre_step(None)
            t.pre_step()

        # Remove overlap particles.
        G.remove_overlap_particles(fluid, solid, self.dx_min, self.dim)

        # Remove ghost_inlet as a source for smoothing.
        sources = self.particles[:-1]

        smoothing_sph_eval = t.setup_smoothing(
            'fluid', sources, self.nnps, dim=self.dim, rho0=rho
        )

        h_copy = fluid.h.copy()
        # Use the (m/rho)^0.5 as h for the smoothing to work quickly.
        # THen reset it back.
        self.particles[0].h[:] = (self.particles[0].m / rho)**(1/2.0)

        for i in range(100):
            print("Smoothing", i, end='\r')
            smoothing_sph_eval.update()
            smoothing_sph_eval.evaluate(dt=self.dt)

        # Resetting h.
        fluid.h[:] = h_copy
        solid.h[:] = fluid.h.min()

        inlet = self.particles[1]
        if self.options.potential:
            u, v, p = potential_flow(inlet.x, inlet.y, umax, self.cxy, self.dc)
            inlet.u[:] = u
            inlet.v[:] = v
            inlet.p[:] = p
            u, v, p = potential_flow(fluid.x, fluid.y, umax, self.cxy, self.dc)
            fluid.u[:] = u
            fluid.v[:] = v
            fluid.p[:] = p
            fluid.uhat[:] = u
            fluid.vhat[:] = v
            fluid.vmag[:] = np.sqrt(u**2 + v**2)
        return [t]

    def configure_scheme(self):
        scheme = self.scheme
        self.iom = self._create_inlet_outlet_manager()
        scheme.inlet_outlet_manager = self.iom
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        self.iom.update_dx(self.dx)
        scheme.configure(h=self.h, nu=self.nu, cfl=self.cfl,
                         hybrid=self.hybrid)

        scheme.configure_solver(
            kernel=kernel, tf=self.tf, dt=self.dt, pfreq=pfreq,
            n_damp=self.options.n_damp,
            output_at_times=list(range(1, 7))
        )

    def _get_io_info(self):
        from pysph.sph.bc.hybrid.inlet import Inlet
        from pysph.sph.bc.hybrid.outlet import Outlet
        from hybrid_simple_inlet_outlet import SimpleInletOutlet

        inleteqns = [
            ResetInletVelocity('ghost_inlet', None, U=-umax, V=0.0, W=0.0),
            ResetInletVelocity('inlet', None, U=umax, V=0.0, W=0.0),
        ]

        i_has_ghost = True
        o_has_ghost = False
        i_update_cls = Inlet
        o_update_cls = Outlet
        manager = SimpleInletOutlet

        props_to_copy = [
            'x0', 'y0', 'z0', 'uhat', 'vhat', 'what', 'x', 'y', 'z',
            'u', 'v', 'w', 'm', 'h', 'rho', 'p', 'ioid'
        ]
        props_to_copy += ['uta', 'pta', 'u0', 'v0', 'w0', 'p0']

        inlet_info = InletInfo(
            pa_name='inlet', normal=[-1.0, 0.0, 0.0],
            refpoint=[0.0, 0.0, 0.0], equations=inleteqns,
            has_ghost=i_has_ghost, update_cls=i_update_cls, umax=umax
        )

        outlet_info = OutletInfo(
            pa_name='outlet', normal=[1.0, 0.0, 0.0],
            refpoint=[self.Lt, 0.0, 0.0], equations=None,
            has_ghost=o_has_ghost, update_cls=o_update_cls,
            props_to_copy=props_to_copy
        )

        return inlet_info, outlet_info, manager

    def _create_inlet_outlet_manager(self):
        inlet_info, outlet_info, manager = self._get_io_info()
        iom = manager(
            fluid_arrays=['fluid'], inletinfo=[inlet_info],
            outletinfo=[outlet_info]
        )
        return iom

    def create_inlet_outlet(self, particle_arrays):
        iom = self.iom
        io = iom.get_inlet_outlet(particle_arrays)
        return io

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')
        t, cd, cl, cd_sf, cl_sf = self._compute_force_vs_t()
        self._plot_coeffs(t, cd, cl, cd_sf, cl_sf)
        return t, cd, cl, cd_sf, cl_sf

    def _compute_force_vs_t(self):
        from pysph.sph.equation import Group
        from edac import (
            MomentumEquationPressureGradient, SummationDensityGather,
            SetWallVelocity, EvaluateNumberDensity
        )
        from vacondio import ComputeBeta

        data = load(self.output_files[0])
        solid = data['arrays']['solid']
        fluid = data['arrays']['fluid']

        prop = ['awhat', 'auhat', 'avhat', 'wg', 'vg', 'ug', 'V', 'uf', 'vf',
                'wf', 'wij', 'vmag', 'pavg', 'nnbr', 'auf', 'avf', 'awf']
        for p in prop:
            solid.add_property(p)
            fluid.add_property(p)

        # We find the force of the solid on the fluid and the opposite of that
        # is the force on the solid. Note that the assumption is that the solid
        # is far from the inlet and outlet so those are ignored.
        equations = [
            Group(equations=[
                SummationDensityGather(
                    dest='solid', sources=['fluid', 'solid']
                ),
                ComputeBeta(dest='solid', sources=['fluid', 'solid'], dim=2),
                EvaluateNumberDensity(dest='solid', sources=['fluid']),
                SetWallVelocity(dest='solid', sources=['fluid']),
            ], real=False),
            Group(equations=[
                # Pressure gradient terms
                MomentumEquationPressureGradient(
                    dest='solid', sources=['fluid']
                ),
                SolidWallNoSlipBCReverse(
                    dest='solid', sources=['fluid'], nu=self.nu
                ),
            ], real=True),
        ]
        sph_eval = SPHEvaluator(
            arrays=[solid, fluid], equations=equations, dim=2,
            kernel=QuinticSpline(dim=2)
        )
        t, cd, cl, cl_sf, cd_sf = [], [], [], [], []
        import gc
        msg = f"diameter: {self.dc}, dx_min: {self.dx_min}, "
        msg += f"dx_max: {self.dx}, nu: {self.nu:.4f}"
        print(msg)

        # Don't use the zeroth file as au is zero which results in nan's.
        at_file = 1
        if os.path.exists(self.res):
            results = np.load(self.res)
            t, cl, cd = map(list, [results['t'], results['cl'], results['cd']])
            cl_sf, cd_sf = map(list, [results['cl_sf'], results['cd_sf']])
            latest_time = t[-1]
            latest_file = len(t)
            guess_file = self.output_files[latest_file]
            guess_time = load(guess_file)['solver_data']['t']
            tdiff = guess_time - latest_time
            if tdiff > 0:
                step = -1
            else:
                step = 1
            for i, file in enumerate(self.output_files[latest_file::step]):
                data = load(file)
                sd  = data['solver_data']
                total_files = len(self.output_files[latest_file::])
                print(f"Searching for latest file, {i}/{total_files}", end='\r')
                if abs(t[-1] - sd['t']) < 1e-6:
                    at_file = self.output_files.index(file)
                    break

        files_total = len(self.output_files[at_file:])
        if files_total == 0:
            data = np.load(self.res)
            return data['t'], data['cd'], data['cl'], data['cd_sf'], data['cl_sf']

        i = 0
        for sd, arrays in iter_output(self.output_files[at_file:]):
            fluid = arrays['fluid']
            solid = arrays['solid']
            cx, cy = self.cxy
            radius = (self.dc/2 + np.max(solid.h)*10)
            cond = ((fluid.x - cx)**2 + (fluid.y - cy)**2 < radius**2)
            indices = np.where(~cond)[0]
            fluid.remove_particles(indices)
            for p in prop:
                solid.add_property(p)
                fluid.add_property(p)
            solid.add_property('beta')
            solid.beta[:] = 1.0
            t.append(sd['t'])
            fluid.rho[:] = 1000
            solid.rho[:] = 1000
            sph_eval.update_particle_arrays([solid, fluid])
            sph_eval.evaluate()
            fx = solid.m*solid.au
            fy = solid.m*solid.av
            auf = solid.m*solid.auf
            avf = solid.m*solid.avf
            cd.append(np.sum(fx)/(0.5 * rho * umax**2 * self.dc))
            cl.append(np.sum(fy)/(0.5 * rho * umax**2 * self.dc))
            cd_sf.append(np.sum(auf)/(0.5 * rho * umax**2 * self.dc))
            cl_sf.append(np.sum(avf)/(0.5 * rho * umax**2 * self.dc))
            msg = f"iters: {i}/{files_total}, "
            msg += f"t: {t[-1]:.4f}, Cd: {cd[-1]:.4f}, Cl: {cl[-1]:.4f}"
            msg += f" Clf: {cl_sf[-1]:.4f}, Cdf: {cd_sf[-1]:.4f}"
            print(msg)
            gc.collect()
            i += 1
        t, cd, cl, cd_sf, cl_sf = list(map(np.asarray, (t, cd, cl, cd_sf, cl_sf)))
        np.savez(self.res, t=t, cd=cd, cl=cl, cl_sf=cl_sf, cd_sf=cd_sf)
        return t, cd, cl, cd_sf, cl_sf

    def _plot_coeffs(self, t, cd, cl, cd_sf, cl_sf):
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, cd, label=r'$C_d$')
        plt.plot(t, cl, label=r'$C_l$')
        plt.plot(t, cd_sf, label=r'$C_df$')
        plt.plot(t, cl_sf, label=r'$C_lf$')
        plt.xlabel(r'$t$')
        plt.ylabel('cd/cl')
        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()

    def customize_output(self):
        self._mayavi_config('''
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')

    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()


if __name__ == '__main__':
    app = WindTunnel()
    app.run()
    app.post_process(app.info_filename)
