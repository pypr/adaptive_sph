"""
EDAC SPH formulation
#####################

Equations for the Entropically Damped Artificial Compressibility SPH scheme.

Please note that this scheme is still under development and this module may
change at some point in the future.

References
----------

    .. [PRKP2017] Prabhu Ramachandran and Kunal Puri, Entropically damped
       artificial compressibility for SPH, under review, 2017.
       http://arxiv.org/pdf/1311.2167v2.pdf

"""

from math import sin, sqrt
from math import pi as M_PI
from compyle.api import declare
import numpy as np

from pysph.base.utils import get_particle_array
from pysph.base.utils import DEFAULT_PROPS
from pysph.sph.equation import Equation, Group
from pysph.sph.integrator_step import IntegratorStep
from pysph.sph.scheme import Scheme, add_bool_argument
from pysph.sph.wc.linalg import mat_vec_mult


EDAC_PROPS = (
    'ap', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'x0', 'y0', 'z0', 'u0',
    'v0', 'w0', 'p0', 'V', 'vmag'
)


def get_particle_array_edac(constants=None, **props):
    "Get the fluid array for the transport velocity formulation"

    pa = get_particle_array(
        constants=constants, additional_props=EDAC_PROPS, **props
    )
    pa.set_output_arrays(['x', 'y', 'z', 'u', 'v', 'w', 'rho', 'p',
                          'au', 'av', 'aw', 'ap', 'm', 'h', 'vmag'])

    return pa


EDAC_SOLID_PROPS = ('ap', 'p0', 'wij', 'uf', 'vf', 'wf', 'ug', 'vg', 'wg',
                    'ax', 'ay', 'az', 'V')


def get_particle_array_edac_solid(constants=None, **props):
    "Get the fluid array for the transport velocity formulation"

    pa = get_particle_array(
        constants=constants, additional_props=EDAC_SOLID_PROPS, **props
    )
    pa.set_output_arrays(['x', 'y', 'z', 'u', 'v', 'w', 'rho', 'p',
                          'h'])

    return pa


class SummationDensity(Equation):
    def initialize(self, d_idx, d_rho, d_n_nbrs):
        d_rho[d_idx] = 0.0
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, s_idx, d_rho, s_m, d_n_nbrs, WIJ):
        d_rho[d_idx] += s_m[s_idx]*WIJ
        d_n_nbrs[d_idx] += 1


class SummationDensityScatter(Equation):
    def initialize(self, d_idx, d_rho, d_n_nbrs):
        d_rho[d_idx] = 0.0
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, s_idx, d_rho, s_m, d_n_nbrs, WJ):
        d_rho[d_idx] += s_m[s_idx]*WJ
        d_n_nbrs[d_idx] += 1


class SummationDensityGatherCorrected(Equation):
    def initialize(self, d_idx, d_rho):
        d_rho[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_m, WI, d_Linv, DWI):

        Linv = declare('matrix(4)')
        i = declare('int')
        for i in range(4):
            Linv[i] = d_Linv[d_idx*16 + i]
        WI = Linv[0]*WI + Linv[1]*DWI[0] + Linv[2]*DWI[1] + Linv[3]*DWI[2]

        d_rho[d_idx] += s_m[s_idx]*WI


class SummationDensityGather(Equation):
    def initialize(self, d_idx, d_rho, d_n_nbrs):
        d_rho[d_idx] = 0.0
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, s_idx, d_rho, s_m, d_n_nbrs, WI):
        d_rho[d_idx] += s_m[s_idx]*WI
        d_n_nbrs[d_idx] += 1


class ComputeAveragePressure(Equation):
    """Simple function to compute the average pressure at each particle.

    This is used for the Basa, Quinlan and Lastiwka correction from their 2009
    paper.  This equation should be in a separate group and computed before the
    Momentum equation.
    """
    def initialize(self, d_idx, d_pavg, d_nnbr):
        d_pavg[d_idx] = 0.0
        d_nnbr[d_idx] = 0.0

    def loop(self, d_idx, d_pavg, s_idx, s_p, d_nnbr):
        d_pavg[d_idx] += s_p[s_idx]
        d_nnbr[d_idx] += 1.0

    def post_loop(self, d_idx, d_pavg, d_nnbr):
        if d_nnbr[d_idx] > 0:
            d_pavg[d_idx] /= d_nnbr[d_idx]


class EDACStep(IntegratorStep):
    """Standard Predictor Corrector integrator for the WCSPH formulation

    Use this integrator for WCSPH formulations. In the predictor step,
    the particles are advanced to `t + dt/2`. The particles are then
    advanced with the new force computed at this position.

    This integrator can be used in PEC or EPEC mode.

    The same integrator can be used for other problems. Like for
    example solid mechanics (see SolidMechStep)

    """
    def initialize(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
                   d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_vmag):
        d_x0[d_idx] = d_x[d_idx]
        d_y0[d_idx] = d_y[d_idx]
        d_z0[d_idx] = d_z[d_idx]

        d_u0[d_idx] = d_u[d_idx]
        d_v0[d_idx] = d_v[d_idx]
        d_w0[d_idx] = d_w[d_idx]

        d_p0[d_idx] = d_p[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

    def stage1(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
               d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_au, d_av,
               d_aw, d_ax, d_ay, d_az, d_ap, dt, d_vmag):
        dtb2 = 0.5*dt
        d_u[d_idx] = d_u0[d_idx] + dtb2*d_au[d_idx]
        d_v[d_idx] = d_v0[d_idx] + dtb2*d_av[d_idx]
        d_w[d_idx] = d_w0[d_idx] + dtb2*d_aw[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

        d_x[d_idx] = d_x0[d_idx] + dtb2 * d_ax[d_idx]
        d_y[d_idx] = d_y0[d_idx] + dtb2 * d_ay[d_idx]
        d_z[d_idx] = d_z0[d_idx] + dtb2 * d_az[d_idx]

        d_p[d_idx] = d_p0[d_idx] + dtb2 * d_ap[d_idx]

    def stage2(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
               d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_au, d_av,
               d_aw, d_ax, d_ay, d_az, d_ap, d_vmag, dt):

        d_u[d_idx] = d_u0[d_idx] + dt*d_au[d_idx]
        d_v[d_idx] = d_v0[d_idx] + dt*d_av[d_idx]
        d_w[d_idx] = d_w0[d_idx] + dt*d_aw[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

        d_x[d_idx] = d_x0[d_idx] + dt * d_ax[d_idx]
        d_y[d_idx] = d_y0[d_idx] + dt * d_ay[d_idx]
        d_z[d_idx] = d_z0[d_idx] + dt * d_az[d_idx]

        d_p[d_idx] = d_p0[d_idx] + dt * d_ap[d_idx]


class SolidWallNoSlipBCReverse(Equation):
    def __init__(self, dest, sources, nu):
        self.nu = nu
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_auf, d_avf, d_awf):
        d_auf[d_idx] = 0.0
        d_avf[d_idx] = 0.0
        d_awf[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho,
             d_ug, d_vg, d_wg,
             d_auf, d_avf, d_awf,
             s_u, s_v, s_w,
             DWI, DWJ, R2IJ, EPS, XIJ):
        mj = s_m[s_idx]
        rhoij = (d_rho[d_idx] + s_rho[s_idx])
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWJ[0] + DWI[0])
        dwij[1] = 0.5 * (DWJ[1] + DWI[1])
        dwij[2] = 0.5 * (DWJ[2] + DWI[2])
        Fij = dwij[0]*XIJ[0] + dwij[1]*XIJ[1] + dwij[2]*XIJ[2]

        tmp = 4 * mj * self.nu * Fij / (rhoij * (R2IJ + EPS))

        d_auf[d_idx] += tmp * (d_ug[d_idx] - s_u[s_idx])
        d_avf[d_idx] += tmp * (d_vg[d_idx] - s_v[s_idx])
        d_awf[d_idx] += tmp * (d_wg[d_idx] - s_w[s_idx])


class SolidWallNoSlipBC(Equation):
    def __init__(self, dest, sources, nu):
        self.nu = nu
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho, d_u, d_v, d_w, d_au, d_av,
             d_aw, s_ug, s_vg, s_wg, DWI, DWJ, R2IJ, EPS, XIJ):
        mj = s_m[s_idx]
        rhoij = (d_rho[d_idx] + s_rho[s_idx])
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWJ[0] + DWI[0])
        dwij[1] = 0.5 * (DWJ[1] + DWI[1])
        dwij[2] = 0.5 * (DWJ[2] + DWI[2])
        Fij = dwij[0]*XIJ[0] + dwij[1]*XIJ[1] + dwij[2]*XIJ[2]

        tmp = 4 * mj * self.nu * Fij / (rhoij * (R2IJ + EPS))

        d_au[d_idx] += tmp * (d_u[d_idx] - s_ug[s_idx])
        d_av[d_idx] += tmp * (d_v[d_idx] - s_vg[s_idx])
        d_aw[d_idx] += tmp * (d_w[d_idx] - s_wg[s_idx])


class SolidWallPressureBC(Equation):
    r"""Solid wall pressure boundary condition from Adami and Hu (transport
    velocity formulation).

    """
    def __init__(self, dest, sources, gx=0.0, gy=0.0, gz=0.0):
        self.gx = gx
        self.gy = gy
        self.gz = gz

        super().__init__(dest, sources)

    def initialize(self, d_idx, d_p):
        d_p[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_p, s_p, s_rho,
             d_au, d_av, d_aw, WIJ, XIJ):

        # numerator of Eq. (27) ax, ay and az are the prescribed wall
        # accelerations which must be defined for the wall boundary
        # particle
        gdotxij = (self.gx - d_au[d_idx])*XIJ[0] + \
            (self.gy - d_av[d_idx])*XIJ[1] + \
            (self.gz - d_aw[d_idx])*XIJ[2]

        d_p[d_idx] += s_p[s_idx]*WIJ + s_rho[s_idx]*gdotxij*WIJ

    def post_loop(self, d_idx, d_wij, d_p):
        # extrapolated pressure at the ghost particle
        if d_wij[d_idx] > 1e-14:
            d_p[d_idx] /= d_wij[d_idx]


class ClampWallPressure(Equation):
    r"""Clamp the wall pressure to non-negative values.
    """
    def post_loop(self, d_idx, d_p):
        if d_p[d_idx] < 0.0:
            d_p[d_idx] = 0.0


class SourceNumberDensity(Equation):
    r"""Evaluates the number density due to the source particles"""
    def initialize(self, d_idx, d_wij):
        d_wij[d_idx] = 0.0

    def loop(self, d_idx, d_wij, WIJ):
        d_wij[d_idx] += WIJ


class SetWallVelocity(Equation):
    r"""Extrapolating the fluid velocity on to the wall Eq. (22) in REF1:

    .. math::

        \tilde{\boldsymbol{v}}_a = \frac{\sum_b\boldsymbol{v}_b W_{ab}}
        {\sum_b W_{ab}}

    Notes:

    This should be used only after (or in the same group) as the
    SolidWallPressureBC equation.

    The destination particle array for this equation should define the
    *filtered* velocity variables :math:`uf, vf, wf`.

    """
    def initialize(self, d_idx, d_uf, d_vf, d_wf):
        d_uf[d_idx] = 0.0
        d_vf[d_idx] = 0.0
        d_wf[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_uf, d_vf, d_wf,
             s_u, s_v, s_w, WIJ):
        # sum in Eq. (22)
        # this will be normalized in post loop
        d_uf[d_idx] += s_u[s_idx] * WIJ
        d_vf[d_idx] += s_v[s_idx] * WIJ
        d_wf[d_idx] += s_w[s_idx] * WIJ

    def post_loop(self, d_uf, d_vf, d_wf, d_wij, d_idx,
                  d_ug, d_vg, d_wg, d_u, d_v, d_w):

        # calculation is done only for the relevant boundary particles.
        # d_wij (and d_uf) is 0 for particles sufficiently away from the
        # solid-fluid interface

        # Note that d_wij is already computed for the pressure BC.
        if d_wij[d_idx] > 1e-12:
            d_uf[d_idx] /= d_wij[d_idx]
            d_vf[d_idx] /= d_wij[d_idx]
            d_wf[d_idx] /= d_wij[d_idx]

        # Dummy velocities at the ghost points using Eq. (23),
        # d_u, d_v, d_w are the prescribed wall velocities.
        d_ug[d_idx] = 2*d_u[d_idx] - d_uf[d_idx]
        d_vg[d_idx] = 2*d_v[d_idx] - d_vf[d_idx]
        d_wg[d_idx] = 2*d_w[d_idx] - d_wf[d_idx]


class EvaluateNumberDensity(Equation):
    def initialize(self, d_idx, d_wij, d_wij2):
        d_wij[d_idx] = 0.0
        d_wij2[d_idx] = 0.0

    def loop(self, d_idx, d_wij, d_wij2, XIJ, HIJ, RIJ, SPH_KERNEL):
        wij = SPH_KERNEL.kernel(XIJ, RIJ, HIJ)
        wij2 = SPH_KERNEL.kernel(XIJ, RIJ, 0.5*HIJ)
        d_wij[d_idx] += wij
        d_wij2[d_idx] += wij2


class SlipVelocityExtrapolation(Equation):
    '''Slip boundary condition on the wall

    The velocity of the fluid is extrapolated over to the wall using
    shepard extrapolation. The velocity normal to the wall is reflected back
    to impose no penetration.
    '''
    def initialize(self, d_idx, d_ug_star, d_vg_star, d_wg_star):
        d_ug_star[d_idx] = 0.0
        d_vg_star[d_idx] = 0.0
        d_wg_star[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_ug_star, d_vg_star, d_wg_star, s_u,
             s_v, s_w, d_wij2, XIJ, RIJ, HIJ, SPH_KERNEL):
        wij = SPH_KERNEL.kernel(XIJ, RIJ, HIJ)
        wij2 = SPH_KERNEL.kernel(XIJ, RIJ, 0.5*HIJ)

        if d_wij2[d_idx] > 1e-14:
            d_ug_star[d_idx] += s_u[s_idx]*wij2
            d_vg_star[d_idx] += s_v[s_idx]*wij2
            d_wg_star[d_idx] += s_w[s_idx]*wij2
        else:
            d_ug_star[d_idx] += s_u[s_idx]*wij
            d_vg_star[d_idx] += s_v[s_idx]*wij
            d_wg_star[d_idx] += s_w[s_idx]*wij

    def post_loop(self, d_idx, d_wij, d_wij2, d_ug_star, d_vg_star,
                  d_wg_star, d_normal, d_u, d_v, d_w):
        idx = declare('int')
        idx = 3*d_idx
        if d_wij2[d_idx] > 1e-14:
            d_ug_star[d_idx] /= d_wij2[d_idx]
            d_vg_star[d_idx] /= d_wij2[d_idx]
            d_wg_star[d_idx] /= d_wij2[d_idx]
        elif d_wij[d_idx] > 1e-14:
            d_ug_star[d_idx] /= d_wij[d_idx]
            d_vg_star[d_idx] /= d_wij[d_idx]
            d_wg_star[d_idx] /= d_wij[d_idx]

        # u_g \cdot n = 2*(u_wall \cdot n ) - (u_f \cdot n)
        # u_g \cdot t = (u_f \cdot t) = u_f - (u_f \cdot n)
        tmp1 = d_u[d_idx] - d_ug_star[d_idx]
        tmp2 = d_v[d_idx] - d_vg_star[d_idx]
        tmp3 = d_w[d_idx] - d_wg_star[d_idx]

        projection = (tmp1*d_normal[idx] +
                      tmp2*d_normal[idx+1] +
                      tmp3*d_normal[idx+2])

        d_ug_star[d_idx] += 2*projection * d_normal[idx]
        d_vg_star[d_idx] += 2*projection * d_normal[idx+1]
        d_wg_star[d_idx] += 2*projection * d_normal[idx+2]


class NoSlipVelocityExtrapolation(Equation):
    '''No Slip boundary condition on the wall

    The velocity of the fluid is extrapolated over to the wall using
    shepard extrapolation. The velocity normal to the wall is reflected back
    to impose no penetration.
    '''
    def initialize(self, d_idx, d_u, d_v, d_w):
        d_u[d_idx] = 0.0
        d_v[d_idx] = 0.0
        d_w[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_u, d_v, d_w, s_u, s_v, s_w, WIJ):
        d_u[d_idx] += s_u[s_idx]*WIJ
        d_v[d_idx] += s_v[s_idx]*WIJ
        d_w[d_idx] += s_w[s_idx]*WIJ

    def post_loop(self, d_idx, d_wij, d_u, d_v, d_w, d_xn, d_yn, d_zn):
        if d_wij[d_idx] > 1e-14:
            d_u[d_idx] /= d_wij[d_idx]
            d_v[d_idx] /= d_wij[d_idx]
            d_w[d_idx] /= d_wij[d_idx]

        projection = d_u[d_idx]*d_xn[d_idx] +\
            d_v[d_idx]*d_yn[d_idx] + d_w[d_idx]*d_zn[d_idx]

        d_u[d_idx] = d_u[d_idx] - 2 * projection * d_xn[d_idx]
        d_v[d_idx] = d_v[d_idx] - 2 * projection * d_yn[d_idx]
        d_w[d_idx] = d_w[d_idx] - 2 * projection * d_zn[d_idx]


class NoSlipAdvVelocityExtrapolation(Equation):
    '''No Slip boundary condition on the wall

    The advection velocity of the fluid is extrapolated over to the wall
    using shepard extrapolation. The advection velocity normal to the wall
    is reflected back to impose no penetration.
    '''
    def initialize(self, d_idx, d_uhat, d_vhat, d_what):
        d_uhat[d_idx] = 0.0
        d_vhat[d_idx] = 0.0
        d_what[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_uhat, d_vhat, d_what, s_uhat, s_vhat,
             s_what, WIJ):
        d_uhat[d_idx] += s_uhat[s_idx]*WIJ
        d_vhat[d_idx] += s_vhat[s_idx]*WIJ
        d_what[d_idx] += s_what[s_idx]*WIJ

    def post_loop(self, d_idx, d_wij, d_uhat, d_vhat, d_what, d_xn, d_yn,
                  d_zn):
        if d_wij[d_idx] > 1e-14:
            d_uhat[d_idx] /= d_wij[d_idx]
            d_vhat[d_idx] /= d_wij[d_idx]
            d_what[d_idx] /= d_wij[d_idx]

        projection = d_uhat[d_idx]*d_xn[d_idx] +\
            d_vhat[d_idx]*d_yn[d_idx] + d_what[d_idx]*d_zn[d_idx]

        d_uhat[d_idx] = d_uhat[d_idx] - 2 * projection * d_xn[d_idx]
        d_vhat[d_idx] = d_vhat[d_idx] - 2 * projection * d_yn[d_idx]
        d_what[d_idx] = d_what[d_idx] - 2 * projection * d_zn[d_idx]


class MomentumEquationPressureGradient(Equation):
    r"""Momentum equation (gradient of pressure) based on the number
    density formulation of Hu and Adams JCP 213 (2006), 844-861.

    """
    def __init__(self, dest, sources, gx=0.0, gy=0.0, gz=0.0, tdamp=0.0):
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.tdamp = tdamp

        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_m, d_rho, d_p, d_au, d_av, d_aw,
             s_m, s_rho, s_p, d_pavg, d_beta, s_beta, DWI, DWJ):

        # averaged pressure Eq. (7)
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]
        pavg = d_pavg[d_idx]
        pi = d_p[d_idx]
        pj = s_p[s_idx]
        betai = d_beta[d_idx]
        betaj = s_beta[s_idx]

        pi = rhoj * (pi - pavg) / (rhoj + rhoi)
        pj = rhoi * (pj - pavg) / (rhoj + rhoi)
        pi /= betai
        pj /= betaj

        # particle volumes
        Vi = d_m[d_idx]/d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        # inverse mass of destination particle
        mi1 = 1.0/d_m[d_idx]

        # accelerations 1st term in Eq. (8)
        tmp = -mi1 * (Vi2 + Vj2)

        d_au[d_idx] += tmp * (pi * DWI[0] + pj * DWJ[0])
        d_av[d_idx] += tmp * (pi * DWI[1] + pj * DWJ[1])
        d_aw[d_idx] += tmp * (pi * DWI[2] + pj * DWJ[2])

    def post_loop(self, d_idx, d_au, d_av, d_aw, t):
        damping_factor = 1.0
        if t < self.tdamp:
            damping_factor = 0.5 * (sin((-0.5 + t/self.tdamp)*M_PI) + 1.0)
        d_au[d_idx] += damping_factor*self.gx
        d_av[d_idx] += damping_factor*self.gy
        d_aw[d_idx] += damping_factor*self.gz


class EDACEquation(Equation):
    def __init__(self, dest, sources, cs, nu, rho0, edac_alpha):
        self.cs = cs
        self.nu = nu
        self.rho0 = rho0
        self.edac_alpha = edac_alpha

        super().__init__(dest, sources)

    def initialize(self, d_idx, d_ap, d_div, d_div_hat):
        d_ap[d_idx] = 0.0
        d_div[d_idx] = 0.0
        d_div_hat[d_idx] = 0.0

    def loop(self, d_idx, d_m, d_rho, d_ap, d_p, s_idx, s_m, s_rho, s_p,
             d_div, d_beta, d_u, d_v, d_w, d_uhat, d_vhat, d_h, s_h,
             d_what, d_pavg, DWJ, DWI, VIJ, XIJ, R2IJ, EPS):
        Vi = d_m[d_idx]/d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        etai = d_rho[d_idx] * self.edac_alpha * d_h[d_idx] * self.cs/8
        etaj = s_rho[s_idx] * self.edac_alpha * s_h[s_idx] * self.cs/8
        etaij = 2 * (etai * etaj)/(etai + etaj)

        # This is the same as continuity acceleration times cs^2
        rhoi = d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        betai = d_beta[d_idx]

        # Since we are using scatter formulation, DWJ is used instead of DWIJ.
        vijdotdwij = DWI[0]*VIJ[0] + DWI[1]*VIJ[1] + DWI[2]*VIJ[2]

        d_ap[d_idx] += self.rho0 * Vj *self.cs*self.cs*vijdotdwij/betai
        d_div[d_idx] += Vj * -vijdotdwij

        # Viscous damping of pressure.
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWI[0] + DWJ[0])
        dwij[1] = 0.5 * (DWI[1] + DWJ[1])
        dwij[2] = 0.5 * (DWI[2] + DWJ[2])

        xijdotdwij = dwij[0]*XIJ[0] + dwij[1]*XIJ[1] + dwij[2]*XIJ[2]

        tmp = 1.0/d_m[d_idx]*(Vi2 + Vj2)*etaij*xijdotdwij/(R2IJ + EPS)
        d_ap[d_idx] += tmp*(d_p[d_idx] - s_p[s_idx])

        # Apply corrections due to shifting.
        udiff = declare('matrix(3)')
        udiff[0] = d_uhat[d_idx] - d_u[d_idx]
        udiff[1] = d_vhat[d_idx] - d_v[d_idx]
        udiff[2] = d_what[d_idx] - d_w[d_idx]

        gradp = declare('matrix(3)')
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]
        pavg = d_pavg[d_idx]
        pi = d_p[d_idx]
        pj = s_p[s_idx]

        pi = rhoj * (pi - pavg) / (rhoj + rhoi)
        pj = rhoi * (pj - pavg) / (rhoj + rhoi)

        Vi = d_m[d_idx]/d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        mi1 = 1.0/d_m[d_idx]

        tmp = mi1 * (Vi2 + Vj2)

        gradp[0] = tmp * (pi * DWI[0] + pj * DWJ[0])
        gradp[1] = tmp * (pi * DWI[1] + pj * DWJ[1])
        gradp[2] = tmp * (pi * DWI[2] + pj * DWJ[2])

        d_ap[d_idx] += udiff[0]*gradp[0] + udiff[1]*gradp[1] + udiff[2]*gradp[2]


class EDACEquationSolids(Equation):
    def __init__(self, dest, sources, cs, nu, rho0, edac_alpha):
        self.cs = cs
        self.nu = nu
        self.rho0 = rho0
        self.edac_alpha = edac_alpha
        super().__init__(dest, sources)

    def loop(self, d_idx, d_m, d_rho, d_ap, d_p, s_idx, s_m, s_rho, s_p,
             d_div, d_beta, d_u, d_v, d_w, d_uhat, d_vhat, d_h, s_h,
             d_what, d_pavg, s_ug, s_vg, s_wg, DWJ, DWI, XIJ, R2IJ, EPS,):
        Vi = d_m[d_idx]/d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        etai = d_rho[d_idx] * self.edac_alpha * d_h[d_idx] * self.cs/8
        etaj = s_rho[s_idx] * self.edac_alpha * s_h[s_idx] * self.cs/8
        etaij = 2 * (etai * etaj)/(etai + etaj)

        # This is the same as continuity acceleration times cs^2
        rhoi = d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        betai = d_beta[d_idx]

        # Since we are using scatter formulation, DWJ is used instead of DWIJ.
        vij = declare('matrix(3)')
        vij[0] = d_u[d_idx] - s_ug[s_idx]
        vij[1] = d_v[d_idx] - s_vg[s_idx]
        vij[2] = d_w[d_idx] - s_wg[s_idx]
        vijdotdwij = DWI[0]*vij[0] + DWI[1]*vij[1] + DWI[2]*vij[2]

        d_ap[d_idx] += self.rho0 * Vj *self.cs*self.cs*vijdotdwij/betai
        d_div[d_idx] += Vj * -vijdotdwij

        # Viscous damping of pressure.
        xijdotdwij = DWI[0]*XIJ[0] + DWI[1]*XIJ[1] + DWI[2]*XIJ[2]

        tmp = 1.0/d_m[d_idx]*(Vi2 + Vj2)*etaij*xijdotdwij/(R2IJ + EPS)
        d_ap[d_idx] += tmp*(d_p[d_idx] - s_p[s_idx])

        # Apply corrections due to shifting.
        udiff = declare('matrix(3)')
        udiff[0] = d_uhat[d_idx] - d_u[d_idx]
        udiff[1] = d_vhat[d_idx] - d_v[d_idx]
        udiff[2] = d_what[d_idx] - d_w[d_idx]

        gradp = declare('matrix(3)')
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]
        pavg = d_pavg[d_idx]
        pi = d_p[d_idx]
        pj = s_p[s_idx]

        pi = rhoj * (pi - pavg) / (rhoj + rhoi)
        pj = rhoi * (pj - pavg) / (rhoj + rhoi)

        Vi = d_m[d_idx]/d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        mi1 = 1.0/d_m[d_idx]

        tmp = mi1 * (Vi2 + Vj2)

        gradp[0] = tmp * (pi * DWI[0] + pj * DWJ[0])
        gradp[1] = tmp * (pi * DWI[1] + pj * DWJ[1])
        gradp[2] = tmp * (pi * DWI[2] + pj * DWJ[2])

        d_ap[d_idx] += udiff[0]*gradp[0] + udiff[1]*gradp[1] + udiff[2]*gradp[2]


class MomentumEquationArtificialViscosity(Equation):
    r"""**Artificial viscosity for the momentum equation**

    Eq. (11) in [Adami2012]:

    .. math::

        \frac{d \boldsymbol{v}_a}{dt} = -\sum_b m_b \alpha h_{ab}
        c_{ab} \frac{\boldsymbol{v}_{ab}\cdot
        \boldsymbol{r}_{ab}}{\rho_{ab}\left(|r_{ab}|^2 + \epsilon
        \right)}\nabla_a W_{ab}

    where

    .. math::

        \rho_{ab} = \frac{\rho_a + \rho_b}{2}\\

        c_{ab} = \frac{c_a + c_b}{2}\\

        h_{ab} = \frac{h_a + h_b}{2}
    """
    def __init__(self, dest, sources, c0, alpha=0.1):
        r"""
        Parameters
        ----------
        alpha : float
            constant
        c0 : float
            speed of sound
        """

        self.alpha = alpha
        self.c0 = c0
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_au, d_av, d_aw,
             RHOIJ1, R2IJ, EPS, DWI, DWJ, VIJ, XIJ, HIJ):

        # v_{ab} \cdot r_{ab}
        vijdotrij = VIJ[0]*XIJ[0] + VIJ[1]*XIJ[1] + VIJ[2]*XIJ[2]

        # scalar part of the accelerations Eq. (11)
        piij = 0.0
        if vijdotrij < 0:
            muij = (HIJ * vijdotrij)/(R2IJ + EPS)

            piij = -self.alpha*self.c0*muij
            piij = s_m[s_idx] * piij*RHOIJ1

        d_au[d_idx] += -piij * 0.5 * (DWJ[0] + DWI[0])
        d_av[d_idx] += -piij * 0.5 * (DWJ[1] + DWI[1])
        d_aw[d_idx] += -piij * 0.5 * (DWJ[2] + DWI[2])


class MomentumEquationArtificialStress(Equation):
    def _get_helpers_(self):
        return [mat_vec_mult]

    def loop(self, d_idx, s_idx, d_rho, s_rho, d_u, d_v, d_w, d_uhat, d_vhat,
             d_what, s_u, s_v, s_w, s_uhat, s_vhat, s_what, d_au, d_av, d_aw,
             s_m, d_beta, s_beta, DWI, DWJ):
        mj = s_m[s_idx]
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]

        betai = d_beta[d_idx]
        betaj = s_beta[s_idx]

        i, j = declare('int')
        ui, uj, uidif, ujdif, resi, resj = declare('matrix(3)')
        Ai, Aj = declare('matrix(9)')

        for i in range(3):
            resi[i] = 0.0
            resj[i] = 0.0
            for j in range(3):
                Ai[3*i + j] = 0.0
                Aj[3*i + j] = 0.0

        ui[0] = d_u[d_idx]
        ui[1] = d_v[d_idx]
        ui[2] = d_w[d_idx]

        uj[0] = s_u[s_idx]
        uj[1] = s_v[s_idx]
        uj[2] = s_w[s_idx]

        uidif[0] = d_uhat[d_idx] - d_u[d_idx]
        uidif[1] = d_vhat[d_idx] - d_v[d_idx]
        uidif[2] = d_what[d_idx] - d_w[d_idx]

        ujdif[0] = s_uhat[s_idx] - s_u[s_idx]
        ujdif[1] = s_vhat[s_idx] - s_v[s_idx]
        ujdif[2] = s_what[s_idx] - s_w[s_idx]

        for i in range(3):
            for j in range(3):
                Ai[3*i + j] = ui[i]*uidif[j] / rhoi / betai
                Aj[3*i + j] = uj[i]*ujdif[j] / rhoj / betaj

        mat_vec_mult(Ai, DWI, 3, resi)
        mat_vec_mult(Aj, DWJ, 3, resj)

        d_au[d_idx] += mj * (resi[0] + resj[0])
        d_av[d_idx] += mj * (resi[1] + resj[1])
        d_aw[d_idx] += mj * (resi[2] + resj[2])

        # Additional correction terms due to shifting.
        uidif[0] = d_uhat[d_idx] - d_u[d_idx]
        uidif[1] = d_vhat[d_idx] - d_v[d_idx]
        uidif[2] = d_what[d_idx] - d_w[d_idx]

        ujdif[0] = s_uhat[s_idx] - s_u[s_idx]
        ujdif[1] = s_vhat[s_idx] - s_v[s_idx]
        ujdif[2] = s_what[s_idx] - s_w[s_idx]

        udiffij = declare('matrix(3)')
        udiffij[0] = uidif[0] - ujdif[0]
        udiffij[1] = uidif[1] - ujdif[1]
        udiffij[2] = uidif[2] - ujdif[2]

        # scalar part of the kernel gradient
        diff_div = -mj/rhoj/betai * (DWI[0]*udiffij[0] + DWI[1]*udiffij[1]
                                     + DWI[2]*udiffij[2])
        d_au[d_idx] -= d_u[d_idx] * diff_div
        d_av[d_idx] -= d_v[d_idx] * diff_div
        d_aw[d_idx] -= d_w[d_idx] * diff_div


class MomentumEquationViscosity(Equation):
    def __init__(self, dest, sources, nu):
        self.nu = nu
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_rho, d_m, s_m,
             d_au, d_av, d_aw,
             R2IJ, EPS, DWI, DWJ, VIJ, XIJ):

        # averaged shear viscosity Eq. (6)
        etai = self.nu * d_rho[d_idx]
        etaj = self.nu * s_rho[s_idx]

        etaij = 2 * (etai * etaj)/(etai + etaj)

        # scalar part of the kernel gradient
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWJ[0] + DWI[0])
        dwij[1] = 0.5 * (DWJ[1] + DWI[1])
        dwij[2] = 0.5 * (DWJ[2] + DWI[2])
        Fij = dwij[0]*XIJ[0] + dwij[1]*XIJ[1] + dwij[2]*XIJ[2]

        Vi = d_m[d_idx]/d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        # accelerations 3rd term in Eq. (8)
        tmp = 1./d_m[d_idx] * (Vi2 + Vj2) * etaij * Fij/(R2IJ + EPS)

        d_au[d_idx] += tmp * VIJ[0]
        d_av[d_idx] += tmp * VIJ[1]
        d_aw[d_idx] += tmp * VIJ[2]


class EDACTVFStep(IntegratorStep):

    def initialize(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
                   d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_vmag,
                   d_rho, d_rho0):
        d_x0[d_idx] = d_x[d_idx]
        d_y0[d_idx] = d_y[d_idx]
        d_z0[d_idx] = d_z[d_idx]

        d_u0[d_idx] = d_u[d_idx]
        d_v0[d_idx] = d_v[d_idx]
        d_w0[d_idx] = d_w[d_idx]

        d_p0[d_idx] = d_p[d_idx]
        d_rho0[d_idx] = d_rho[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

    def stage1(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
               d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_au,
               d_av, d_auhat, d_avhat, d_awhat, d_uhat, d_vhat, d_what,
               d_aw, d_ap, dt, d_vmag):
        dtb2 = 0.5*dt
        d_u[d_idx] = d_u0[d_idx] + dtb2*d_au[d_idx]
        d_v[d_idx] = d_v0[d_idx] + dtb2*d_av[d_idx]
        d_w[d_idx] = d_w0[d_idx] + dtb2*d_aw[d_idx]

        d_uhat[d_idx] = d_u[d_idx] + dtb2*d_auhat[d_idx]
        d_vhat[d_idx] = d_v[d_idx] + dtb2*d_avhat[d_idx]
        d_what[d_idx] = d_w[d_idx] + dtb2*d_awhat[d_idx]

        d_x[d_idx] = d_x0[d_idx] + dtb2 * d_uhat[d_idx]
        d_y[d_idx] = d_y0[d_idx] + dtb2 * d_vhat[d_idx]
        d_z[d_idx] = d_z0[d_idx] + dtb2 * d_what[d_idx]

        d_p[d_idx] = d_p0[d_idx] + dtb2 * d_ap[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

    def stage2(self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
               d_u0, d_v0, d_w0, d_u, d_v, d_w, d_p0, d_p, d_au, d_av,
               d_aw, d_auhat, d_avhat, d_awhat, d_uhat, d_vhat, d_what,
               d_ap, d_vmag, dt):
        d_u[d_idx] = d_u0[d_idx] + dt*d_au[d_idx]
        d_v[d_idx] = d_v0[d_idx] + dt*d_av[d_idx]
        d_w[d_idx] = d_w0[d_idx] + dt*d_aw[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx] * d_u[d_idx] + d_v[d_idx] *
                             d_v[d_idx] + d_w[d_idx] * d_w[d_idx])

        d_uhat[d_idx] = d_u[d_idx] + dt*d_auhat[d_idx]
        d_vhat[d_idx] = d_v[d_idx] + dt*d_avhat[d_idx]
        d_what[d_idx] = d_w[d_idx] + dt*d_awhat[d_idx]

        d_x[d_idx] = d_x0[d_idx] + dt * d_uhat[d_idx]
        d_y[d_idx] = d_y0[d_idx] + dt * d_vhat[d_idx]
        d_z[d_idx] = d_z0[d_idx] + dt * d_what[d_idx]

        d_p[d_idx] = d_p0[d_idx] + dt * d_ap[d_idx]


def update_rho(dests, sources, method):
    equations = []
    if method == 'scatter':
        for dest in dests:
            equations.append(Group(
                equations=[SummationDensityScatter(dest, sources)]
            ))
    elif method == 'gather':
        for dest in dests:
            equations.append(Group(
                equations=[SummationDensityGatherCorrected(dest, sources)]
            ))
    elif method == 'standard':
        for dest in dests:
            equations.append(Group(
                equations=[SummationDensity(dest, sources)]
            ))
    return equations


class InitializeUhat(Equation):
    def initialize(self, d_idx, d_dummy_uhat, d_dummy_vhat, d_dummy_what,
                   d_dummy_x, d_dummy_y, d_dummy_z):
        d_dummy_uhat[d_idx] = 0.0
        d_dummy_vhat[d_idx] = 0.0
        d_dummy_what[d_idx] = 0.0

        d_dummy_x[d_idx] = 0.0
        d_dummy_y[d_idx] = 0.0
        d_dummy_z[d_idx] = 0.0


class ShiftUpdatePosition(Equation):
    def __init__(self, dest, sources, dt_fac):
        self.dt_fac = dt_fac
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_y, d_z, d_auhat, d_avhat, d_awhat,
                  d_dummy_uhat, d_dummy_vhat, d_dummy_what, dt,
                  d_dummy_x, d_dummy_y, d_dummy_z):
        dt = dt/self.dt_fac
        dt2b2 = 0.5*dt*dt

        d_x[d_idx] += dt*d_dummy_uhat[d_idx] + dt2b2*d_auhat[d_idx]
        d_y[d_idx] += dt*d_dummy_vhat[d_idx] + dt2b2*d_avhat[d_idx]
        d_z[d_idx] += dt*d_dummy_what[d_idx] + dt2b2*d_awhat[d_idx]

        d_dummy_x[d_idx] += dt*d_dummy_uhat[d_idx] + dt2b2*d_auhat[d_idx]
        d_dummy_y[d_idx] += dt*d_dummy_vhat[d_idx] + dt2b2*d_avhat[d_idx]
        d_dummy_z[d_idx] += dt*d_dummy_what[d_idx] + dt2b2*d_awhat[d_idx]

        d_dummy_uhat[d_idx] += dt*d_auhat[d_idx]
        d_dummy_vhat[d_idx] += dt*d_avhat[d_idx]
        d_dummy_what[d_idx] += dt*d_awhat[d_idx]


class ShiftPositionLind(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_auhat, d_avhat, d_awhat):
        d_auhat[d_idx] = 0.0
        d_avhat[d_idx] = 0.0
        d_awhat[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_auhat, d_avhat, d_awhat, s_rho, d_h,
             HIJ, DWIJ, WIJ, SPH_KERNEL):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        # Don't update this rho i.e., don't correct this rho by using summation
        # density inside the shifting loop.
        Vj = mj/rhoj
        hi = d_h[d_idx]

        dp = SPH_KERNEL.get_deltap() * HIJ
        WDX = SPH_KERNEL.kernel([0.0, 0.0, 0.0], dp, HIJ)
        fac = 0.5 * hi * hi
        tmp = fac * Vj * (1 + 0.24*(WIJ/WDX)**4)
        d_auhat[d_idx] -= tmp * DWIJ[0]
        d_avhat[d_idx] -= tmp * DWIJ[1]
        d_awhat[d_idx] -= tmp * DWIJ[2]

    def post_loop(self, d_idx, d_auhat, d_avhat, d_awhat, d_x, d_y, d_z):
        d_x[d_idx] += d_auhat[d_idx]
        d_y[d_idx] += d_avhat[d_idx]
        d_z[d_idx] += d_awhat[d_idx]


class ShiftForceLind(Equation):
    def __init__(self, dest, sources, cs, cfl, rho0):
        self.cs = cs
        self.cfl = cfl
        self.rho0 = rho0
        self.fac = 0.5 * (self.cs/self.cfl)**2
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_auhat, d_avhat, d_awhat):
        d_auhat[d_idx] = 0.0
        d_avhat[d_idx] = 0.0
        d_awhat[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_auhat, d_avhat, d_awhat,
             DWIJ, WIJ, HIJ, SPH_KERNEL):
        mj = s_m[s_idx]
        # Don't update this rho i.e., don't correct this rho by using summation
        # density inside the shifting loop.
        Vj = mj/self.rho0

        dp = SPH_KERNEL.get_deltap() * HIJ
        WDX = SPH_KERNEL.kernel([0.0, 0.0, 0.0], dp, HIJ)
        tmp = self.fac * Vj * (1 + 0.24*(WIJ/WDX)**4)
        d_auhat[d_idx] -= tmp * DWIJ[0]
        d_avhat[d_idx] -= tmp * DWIJ[1]
        d_awhat[d_idx] -= tmp * DWIJ[2]

    def post_loop(self, d_idx, d_auhat, d_avhat, d_awhat, dt, d_h):
        shift_disp = dt * dt * sqrt(d_auhat[d_idx] * d_auhat[d_idx] +
                                    d_avhat[d_idx] * d_avhat[d_idx] +
                                    d_awhat[d_idx] * d_awhat[d_idx])
        max_disp = 0.025 * d_h[d_idx]
        disp_allowed = 0.25 * max_disp

        if shift_disp > disp_allowed:
            fac = disp_allowed / shift_disp
        else:
            fac = 1.0

        d_auhat[d_idx] *= fac
        d_avhat[d_idx] *= fac
        d_awhat[d_idx] *= fac


def shift_force(fluids, sources, cs, cfl, rho0, iters=1):
    eqns = []
    for fluid in fluids:
        eqns.append(Group(
            equations=[InitializeUhat(dest=fluid, sources=None)]
        ))
    shift = []
    for fluid in fluids:
        shift.append(
            ShiftForceLind(dest=fluid, sources=sources, cs=cs, cfl=cfl,
                            rho0=rho0)
        )
        shift.append(
            ShiftUpdatePosition(dest=fluid, sources=None, dt_fac=float(iters))
        )
    eqns.append(Group(shift, iterate=True, min_iterations=iters, max_iterations=iters))
    return eqns


def shift_positions(fluids, sources, dim, iters=0):
    shift = []

    iterate = False
    if iters > 0:
        iterate = True

    for fluid in fluids:
        shift.append(Group(equations=[
            ShiftPositionLind(
                dest=fluid, sources=sources, dim=dim
            ),
        ], iterate=iterate, min_iterations=iters, max_iterations=iters))
    return shift


class ContinuityEquation(Equation):
    def initialize(self, d_idx, d_arho):
        d_arho[d_idx] = 0.0

    def loop(self, d_idx, d_arho, s_idx, s_m, d_u, d_v, d_w, d_rho, s_rho,
             d_uhat, d_vhat, d_what, d_m, DWI, DWJ, VIJ):
        vijdotdwij = DWI[0]*VIJ[0] + DWI[1]*VIJ[1] + DWI[2]*VIJ[2]
        d_arho[d_idx] += s_m[s_idx]*vijdotdwij

        # Apply corrections due to shifting.
        udiff = declare('matrix(3)')
        udiff[0] = d_uhat[d_idx] - d_u[d_idx]
        udiff[1] = d_vhat[d_idx] - d_v[d_idx]
        udiff[2] = d_what[d_idx] - d_w[d_idx]

        gradrho = declare('matrix(3)')
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]

        Vi = d_m[d_idx]/d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        mi1 = 1.0/d_m[d_idx]

        tmp = mi1 * (Vi2 + Vj2)

        gradrho[0] = tmp * (rhoi * DWI[0] + rhoj * DWJ[0])
        gradrho[1] = tmp * (rhoi * DWI[1] + rhoj * DWJ[1])
        gradrho[2] = tmp * (rhoi * DWI[2] + rhoj * DWJ[2])

        d_arho[d_idx] += (udiff[0]*gradrho[0] + udiff[1]*gradrho[1] +
                        udiff[2]*gradrho[2])


class ContinuityEquationSolids(Equation):
    def initialize(self, d_idx, d_arho):
        d_arho[d_idx] = 0.0

    def loop(self, d_idx, d_arho, s_idx, s_m, s_ug, s_vg, s_wg, d_u, d_v, d_w,
             d_uhat, d_vhat, d_what, d_rho, s_rho, d_m, DWI, DWJ):
        # Since we are using scatter formulation, DWJ is used instead of DWIJ.
        vij = declare('matrix(3)')
        vij[0] = d_u[d_idx] - s_ug[s_idx]
        vij[1] = d_v[d_idx] - s_vg[s_idx]
        vij[2] = d_w[d_idx] - s_wg[s_idx]
        vijdotdwij = DWI[0]*vij[0] + DWI[1]*vij[1] + DWI[2]*vij[2]

        d_arho[d_idx] += s_m[s_idx]*vijdotdwij

        # Apply corrections due to shifting.
        udiff = declare('matrix(3)')
        udiff[0] = d_uhat[d_idx] - d_u[d_idx]
        udiff[1] = d_vhat[d_idx] - d_v[d_idx]
        udiff[2] = d_what[d_idx] - d_w[d_idx]

        gradrho = declare('matrix(3)')
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]

        Vi = d_m[d_idx]/d_rho[d_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        mi1 = 1.0/d_m[d_idx]

        tmp = mi1 * (Vi2 + Vj2)

        gradrho[0] = tmp * (rhoi * DWI[0] + rhoj * DWJ[0])
        gradrho[1] = tmp * (rhoi * DWI[1] + rhoj * DWJ[1])
        gradrho[2] = tmp * (rhoi * DWI[2] + rhoj * DWJ[2])

        d_arho[d_idx] += (udiff[0]*gradrho[0] + udiff[1]*gradrho[1] +
                          udiff[2]*gradrho[2])


#######################################################################
# Equations using m/rho formulation
#######################################################################
class MomentumEquationPressureGradient2(Equation):
    def __init__(self, dest, sources, gx=0.0, gy=0.0, gz=0.0, tdamp=0.0):
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.tdamp = tdamp
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, d_p, d_au, d_av, d_aw,
             s_m, s_rho, s_p, d_pavg, d_beta, s_beta, DWI, DWJ):
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]
        pavg = d_pavg[d_idx]
        pi = d_p[d_idx]
        pj = s_p[s_idx]
        betai = d_beta[d_idx]
        betaj = s_beta[s_idx]

        pi = (pi - pavg) / (rhoi * rhoi)
        pj = (pj - pavg) / (rhoj * rhoj)
        pi /= betai
        pj /= betaj

        mj = s_m[s_idx]

        d_au[d_idx] += -mj * (pi * DWI[0] + pj * DWJ[0])
        d_av[d_idx] += -mj * (pi * DWI[1] + pj * DWJ[1])
        d_aw[d_idx] += -mj * (pi * DWI[2] + pj * DWJ[2])

    def post_loop(self, d_idx, d_au, d_av, d_aw, t):
        damping_factor = 1.0
        if t < self.tdamp:
            damping_factor = 0.5 * (sin((-0.5 + t/self.tdamp)*M_PI) + 1.0)
        d_au[d_idx] += damping_factor*self.gx
        d_av[d_idx] += damping_factor*self.gy
        d_aw[d_idx] += damping_factor*self.gz


class MomentumEquationViscosity2(Equation):
    def __init__(self, dest, sources, nu):
        self.nu = nu
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_rho, s_m, d_au, d_av, d_aw,
             R2IJ, EPS, DWI, DWJ, VIJ, XIJ):
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWJ[0] + DWI[0])
        dwij[1] = 0.5 * (DWJ[1] + DWI[1])
        dwij[2] = 0.5 * (DWJ[2] + DWI[2])
        Fij = dwij[0]*XIJ[0] + dwij[1]*XIJ[1] + dwij[2]*XIJ[2]

        Vj = s_m[s_idx] / (d_rho[d_idx] + s_rho[s_idx])
        tmp = Vj * 4 * self.nu * Fij/(R2IJ + EPS)

        d_au[d_idx] += tmp * VIJ[0]
        d_av[d_idx] += tmp * VIJ[1]
        d_aw[d_idx] += tmp * VIJ[2]


class EDACEquation2(Equation):
    def __init__(self, dest, sources, cs, nu, rho0, edac_alpha):
        self.cs = cs
        self.nu = nu
        self.rho0 = rho0
        self.edac_alpha = edac_alpha

        super().__init__(dest, sources)

    def initialize(self, d_idx, d_ap, d_div, d_div_hat):
        d_ap[d_idx] = 0.0
        d_div[d_idx] = 0.0
        d_div_hat[d_idx] = 0.0

    def loop(self, d_idx, d_rho, d_ap, d_p, s_idx, s_m, s_rho, s_p, d_div,
             d_beta, s_beta, d_u, d_v, d_w, d_uhat, d_vhat, d_h, s_h, d_what,
             d_pavg, DWJ, DWI, VIJ, XIJ, R2IJ, EPS):
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]
        betai = d_beta[d_idx]
        betaj = s_beta[s_idx]
        cs2 = self.cs * self.cs
        mj = s_m[s_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]

        ki = self.edac_alpha * d_h[d_idx] * self.cs/8
        kj = self.edac_alpha * s_h[s_idx] * self.cs/8
        kij = 4 * (ki * kj)/(ki + kj)

        # Since we are using gather formulation, DWI is used instead of DWIJ.
        # divergence of velocity term.
        vijdotdwij = DWI[0]*VIJ[0] + DWI[1]*VIJ[1] + DWI[2]*VIJ[2]

        d_ap[d_idx] += self.rho0 * Vj * cs2 * vijdotdwij/betai
        d_div[d_idx] += Vj * -vijdotdwij

        # Viscous damping of pressure.
        dwij = declare('matrix(3)')
        dwij[0] = 0.5 * (DWI[0] + DWJ[0])
        dwij[1] = 0.5 * (DWI[1] + DWJ[1])
        dwij[2] = 0.5 * (DWI[2] + DWJ[2])

        xijdotdwij = dwij[0]*XIJ[0] + dwij[1]*XIJ[1] + dwij[2]*XIJ[2]

        tmp = Vj * kij * xijdotdwij/(R2IJ + EPS) / betai
        # Diffusion of pressure term.
        d_ap[d_idx] += tmp*(d_p[d_idx] - s_p[s_idx])

        # Apply corrections due to shifting.
        udiff = declare('matrix(3)')
        udiff[0] = d_uhat[d_idx] - d_u[d_idx]
        udiff[1] = d_vhat[d_idx] - d_v[d_idx]
        udiff[2] = d_what[d_idx] - d_w[d_idx]

        gradp = declare('matrix(3)')
        pavg = d_pavg[d_idx]
        pi = d_p[d_idx]
        pj = s_p[s_idx]

        pi = (pi - pavg) / (rhoi * rhoi) / betai
        pj = (pj - pavg) / (rhoj * rhoj) / betaj

        gradp[0] = mj * (pi * DWI[0] + pj * DWJ[0])
        gradp[1] = mj * (pi * DWI[1] + pj * DWJ[1])
        gradp[2] = mj * (pi * DWI[2] + pj * DWJ[2])

        d_ap[d_idx] += udiff[0]*gradp[0] + udiff[1]*gradp[1] + udiff[2]*gradp[2]


class EDACEquationSolids2(Equation):
    def __init__(self, dest, sources, cs, nu, rho0, edac_alpha):
        self.cs = cs
        self.nu = nu
        self.rho0 = rho0
        self.edac_alpha = edac_alpha
        super().__init__(dest, sources)

    def loop(self, d_idx, d_m, d_rho, d_ap, d_p, s_idx, s_m, s_rho, s_p,
             d_div, d_beta, d_u, d_v, d_w, d_uhat, d_vhat, d_h, s_h,
             d_what, d_pavg, s_ug, s_vg, s_wg, DWJ, DWI, XIJ, R2IJ, EPS,):
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]
        Vj = s_m[s_idx]/s_rho[s_idx]
        betai = d_beta[d_idx]
        cs2 = self.cs * self.cs

        ki = self.edac_alpha * d_h[d_idx] * self.cs/8
        kj = self.edac_alpha * s_h[s_idx] * self.cs/8
        kij = 4 * (ki * kj)/(ki + kj)

        # This is the same as continuity acceleration times cs^2
        # Since we are using gather formulation, DWI is used instead of DWIJ.
        vij = declare('matrix(3)')
        vij[0] = d_u[d_idx] - s_ug[s_idx]
        vij[1] = d_v[d_idx] - s_vg[s_idx]
        vij[2] = d_w[d_idx] - s_wg[s_idx]
        vijdotdwij = DWI[0]*vij[0] + DWI[1]*vij[1] + DWI[2]*vij[2]

        d_ap[d_idx] += self.rho0 * Vj * cs2 * vijdotdwij/betai
        d_div[d_idx] += Vj * -vijdotdwij

        # Viscous damping of pressure.
        xijdotdwij = DWI[0]*XIJ[0] + DWI[1]*XIJ[1] + DWI[2]*XIJ[2]

        tmp = Vj * kij * xijdotdwij/(R2IJ + EPS)
        d_ap[d_idx] += tmp*(d_p[d_idx] - s_p[s_idx])

        # Apply corrections due to shifting.
        udiff = declare('matrix(3)')
        udiff[0] = d_uhat[d_idx] - d_u[d_idx]
        udiff[1] = d_vhat[d_idx] - d_v[d_idx]
        udiff[2] = d_what[d_idx] - d_w[d_idx]

        gradp = declare('matrix(3)')
        pavg = d_pavg[d_idx]
        pi = d_p[d_idx]
        pj = s_p[s_idx]

        pi = (pi - pavg) / (rhoi * rhoi)
        pj = (pj - pavg) / (rhoj * rhoj)

        mj = s_m[s_idx]

        gradp[0] = mj * (pi * DWI[0] + pj * DWJ[0])
        gradp[1] = mj * (pi * DWI[1] + pj * DWJ[1])
        gradp[2] = mj * (pi * DWI[2] + pj * DWJ[2])

        d_ap[d_idx] += udiff[0]*gradp[0] + udiff[1]*gradp[1] + udiff[2]*gradp[2]


#######################################################################
#######################################################################


class UpdateGhostProps(Equation):
    def initialize(self, d_idx, d_tag, d_gid, d_rho, d_p, d_beta, d_pavg):
        idx = declare('int')
        if d_tag[d_idx] == 2:
            idx = d_gid[d_idx]
            d_rho[d_idx] = d_rho[idx]
            d_p[d_idx] = d_p[idx]
            d_beta[d_idx] = d_beta[idx]
            d_pavg[d_idx] = d_pavg[idx]


class ComputeBeta(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_beta):
        d_beta[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_beta, d_h, SPH_KERNEL, RIJ):
        mj = s_m[s_idx]
        dim_1 = 1.0/self.dim
        fac = mj * RIJ * dim_1
        dwdq = SPH_KERNEL.dwdq(RIJ, d_h[d_idx])
        dwdr = 1.0/d_h[d_idx] * dwdq
        d_beta[d_idx] += -fac * dwdr

    def post_loop(self, d_idx, d_rho, d_beta):
        # Division by rho in post_loop helps in putting this together with
        # summation density in the groups.
        if d_rho[d_idx] > 1e-16:
            d_beta[d_idx] /= d_rho[d_idx]
        if abs(d_beta[d_idx]) < 1e-16:
            d_beta[d_idx] = 1.0


class OptimizeH(Equation):
    def __init__(self, dest, sources, dim, rho0, hdx):
        self.dim = dim
        self.rho0 = rho0
        self.hdx = hdx
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_m_nbr_max, d_n_nbrs):
        d_m_nbr_max[d_idx] = 0.0
        d_n_nbrs[d_idx] = 0

    def loop(self, d_idx, s_m, s_idx, d_m_nbr_max, d_n_nbrs):
        d_m_nbr_max[d_idx] += s_m[s_idx]
        d_n_nbrs[d_idx] += 1

    def post_loop(self, d_idx, d_h, d_m_nbr_max, d_n_nbrs):
        mass = d_m_nbr_max[d_idx]/d_n_nbrs[d_idx]
        d_h[d_idx] = self.hdx * pow(mass/self.rho0, 1.0/self.dim)


def update_smoothing_length(dests, sources, dim, rho0, method, hdx):
    from adapt import UpdateSmoothingLength

    equations = []
    if method == 'optimize':
        eq = []
        for dest in dests:
            eq.append(OptimizeH(dest, sources, dim, rho0, hdx))
        equations.append(Group(equations=eq, update_nnps=False))
    elif method == 'yangkong':
        eqns = []
        for dest in dests:
            eqns.append(UpdateSmoothingLength(dest, sources, Nr=28))
        equations.append(Group(equations=eqns, iterate=True, max_iterations=5))
    elif method == 'donothing':
        equations = []
    return equations


class Vorticity(Equation):
    # Since we are only considering 2D flows,
    # vorticity only has the z-component.
    def initialize(self, d_idx, d_vor):
        d_vor[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_vor, s_m, s_rho, d_u, d_v,
             s_u, s_v, DWI, DWJ):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        dwij0 = (DWI[0] + DWJ[0]) * 0.5
        dwij1 = (DWI[1] + DWJ[1]) * 0.5
        d_vor[d_idx] += mj/rhoj * (
            (s_v[s_idx] - d_v[d_idx]) * dwij0 -
            (s_u[s_idx] - d_u[d_idx]) * dwij1
        )


class AdaptiveEDACScheme(Scheme):
    def __init__(self, fluids, solids, dim, c0, nu, rho0, cfl,
                 gx=0.0, gy=0.0, gz=0.0, tdamp=0.0, eps=0.0, h=0.0,
                 edac_alpha=1.5, alpha=0.0, bql=True, clamp_p=False,
                 inlet_outlet_manager=None, inviscid_solids=None,
                 integrator='PEC', adapt_h=True,
                 update_h='optimize', hdx=1.0, domain=None,
                 continuity=False, has_ghosts=False,
                 hybrid=True, volume_based=False):
        """The EDAC scheme.

        Parameters
        ----------

        fluids : list(str)
            List of names of fluid particle arrays.
        solids : list(str)
            List of names of solid particle arrays.
        dim: int
            Dimensionality of the problem.
        c0 : float
            Speed of sound.
        nu : float
            Kinematic viscosity.
        rho0 : float
            Density of fluid.
        gx, gy, gz : float
            Componenents of body acceleration (gravity, external forcing etc.)
        tdamp: float
            Time for which the acceleration should be damped.
        eps : float
            XSPH smoothing factor, defaults to zero.
        h : float
            Parameter h used for the particles -- used to calculate viscosity.
        edac_alpha : float
            Factor to use for viscosity.
        alpha : float
            Factor to use for artificial viscosity.
        bql : bool
            Use the Basa Quinlan Lastiwka correction.
        clamp_p : bool
            Clamp the boundary pressure to positive values.  This is only used
            for external flows.
        inlet_outlet_manager : InletOutletManager Instance
            Pass the manager if inlet outlet boundaries are present
        inviscid_solids : list
            list of inviscid solid array names
        """
        self.c0 = c0
        self.cfl = cfl
        self.nu = nu
        self.rho0 = rho0
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.tdamp = tdamp
        self.dim = dim
        self.eps = eps
        self.fluids = fluids
        self.solids = solids
        self.solver = None
        self.bql = bql
        self.clamp_p = clamp_p
        self.edac_alpha = edac_alpha
        self.alpha = alpha
        self.h = h
        self.inlet_outlet_manager = inlet_outlet_manager
        self.inviscid_solids = [] if inviscid_solids is None else\
            inviscid_solids
        self.attributes_changed()
        self.integrator = integrator
        self.adapt_h = adapt_h
        self.update_h = update_h
        self.hdx = hdx
        self.domain = domain
        self.continuity = continuity
        self.has_ghosts = has_ghosts
        self.hybrid = hybrid
        self.volume_based = volume_based

    # Public protocol ###################################################
    def add_user_options(self, group):
        group.add_argument(
            "--alpha", action="store", type=float, dest="alpha",
            default=None,
            help="Alpha for the artificial viscosity."
        )
        group.add_argument(
            "--edac-alpha", action="store", type=float, dest="edac_alpha",
            default=None,
            help="Alpha for the EDAC scheme viscosity."
        )
        add_bool_argument(
            group, 'clamp-pressure', dest='clamp_p',
            help="Clamp pressure on boundaries to be non-negative.",
            default=None
        )
        add_bool_argument(
            group, 'use-bql', dest='bql',
            help="Use the Basa-Quinlan-Lastiwka correction.",
            default=None
        )
        group.add_argument(
            "--tdamp", action="store", type=float, dest="tdamp",
            default=None,
            help="Time for which the accelerations are damped."
        )
        group.add_argument(
            "--integrator", action="store", type=str, dest='integrator',
            default='PEC', choices=['PEC', 'EPEC', 'RK3'],
            help="Choice of integrator to use for the simulation."
        )
        group.add_argument(
            "--update-h", action="store", type=str, dest='update_h',
            default='optimize',
            choices=['optimize', 'yangkong', 'donothing'],
            help="Choice of method to compute h."
        )
        add_bool_argument(
            group, 'adapt-h', dest='adapt_h', default=False,
            help="Turn on adaptive-h corrections."
        )
        add_bool_argument(
            group, 'continuity', dest='continuity', default=False,
            help="Use continuity equation to compute density."
        )
        add_bool_argument(
            group, 'volume-based', dest='volume_based', default=False,
            help="Used Hu & Adams formulation for equations."
        )

    def consume_user_options(self, options):
        var = ['alpha', 'edac_alpha', 'clamp_p', 'bql', 'tdamp', 'integrator',
               'adapt_h', 'update_h', 'continuity',
               'volume_based']
        data = dict((v, self._smart_getattr(options, v))
                    for v in var)
        self.configure(**data)

    def attributes_changed(self):
        self.use_tvf = True
        if self.h is not None and self.c0 is not None:
            self.art_nu = self.edac_alpha*self.h*self.c0/8

    def configure_solver(self, kernel=None, integrator_cls=None,
                         extra_steppers=None, **kw):
        """Configure the solver to be generated.

        This is to be called before `get_solver` is called.

        Parameters
        ----------

        dim : int
            Number of dimensions.
        kernel : Kernel instance.
            Kernel to use, if none is passed a default one is used.
        integrator_cls : pysph.sph.integrator.Integrator
            Integrator class to use, use sensible default if none is
            passed.
        extra_steppers : dict
            Additional integration stepper instances as a dict.
        **kw : extra arguments
            Any additional keyword args are passed to the solver instance.
        """
        from pysph.base.kernels import QuinticSpline
        from pysph.sph.integrator import PECIntegrator, EPECIntegrator

        if kernel is None:
            kernel = QuinticSpline(dim=self.dim)
        steppers = {}
        if extra_steppers is not None:
            steppers.update(extra_steppers)

        step_cls = EDACTVFStep
        if self.integrator == 'PEC':
            default_int_cls = PECIntegrator
        elif self.integrator == 'EPEC':
            step_cls = EDACTVFStep
            default_int_cls = EPECIntegrator

        cls = integrator_cls if integrator_cls is not None else default_int_cls

        for fluid in self.fluids:
            if fluid not in steppers:
                steppers[fluid] = step_cls()

        iom = self.inlet_outlet_manager
        if iom is not None:
            iom_stepper = iom.get_stepper(self, cls, self.use_tvf)
            for name in iom_stepper:
                steppers[name] = iom_stepper[name]

        integrator = cls(**steppers)

        from pysph.solver.solver import Solver
        self.solver = Solver(
            dim=self.dim, integrator=integrator, kernel=kernel, **kw
        )

        if iom is not None:
            iom.setup_iom(dim=self.dim, kernel=kernel)

    def get_equations(self):
        return self._get_internal_flow_equations()

    def get_solver(self):
        return self.solver

    def setup_properties(self, particles, clean=True):
        """Setup the particle arrays so they have the right set of properties
        for this scheme.

        Parameters
        ----------

        particles : list
            List of particle arrays.

        clean : bool
            If True, removes any unnecessary properties.
        """
        particle_arrays = dict([(p.name, p) for p in particles])
        TVF_FLUID_PROPS = set([
            'uhat', 'vhat', 'what', 'ap',
            'auhat', 'avhat', 'awhat', 'V',
            'p0', 'u0', 'v0', 'w0', 'x0', 'y0', 'z0',
            'pavg', 'nnbr'
        ])
        extra_props = TVF_FLUID_PROPS if self.use_tvf else EDAC_PROPS

        all_fluid_props = DEFAULT_PROPS.union(extra_props)
        iom = self.inlet_outlet_manager
        fluids_with_io = self.fluids
        if iom is not None:
            io_particles = iom.get_io_names(ghost=True)
            fluids_with_io = self.fluids + io_particles
        for fluid in fluids_with_io:
            pa = particle_arrays[fluid]
            self._ensure_properties(pa, all_fluid_props, clean)
            pa.set_output_arrays(['x', 'y', 'z', 'u', 'v', 'w', 'rho', 'p',
                                  'm', 'h', 'V'])
            if 'pavg' in pa.properties:
                pa.add_output_arrays(['pavg'])
            if iom is not None:
                iom.add_io_properties(pa, self)

        TVF_SOLID_PROPS = ['V', 'wij', 'ax', 'ay', 'az', 'uf', 'vf', 'wf',
                           'ug', 'vg', 'wg', 'ug_star', 'vg_star', 'wg_star']
        if self.inviscid_solids:
            TVF_SOLID_PROPS += ['xn', 'yn', 'zn', 'uhat', 'vhat', 'what']
        extra_props = TVF_SOLID_PROPS if self.use_tvf else EDAC_SOLID_PROPS
        all_solid_props = DEFAULT_PROPS.union(extra_props)
        for solid in self.solids + self.inviscid_solids:
            pa = particle_arrays[solid]
            self._ensure_properties(pa, all_solid_props, clean)
            pa.set_output_arrays(['x', 'y', 'z', 'u', 'v', 'w', 'rho', 'p',
                                  'm', 'h', 'V'])

        for fluid in fluids_with_io:
            pa = particle_arrays[fluid]
            pa.add_property('closest_idx', type='int')
            pa.add_property('split', type='int')
            pa.add_property('n_nbrs', type='int')

        props = [
            'htmp', 'uhat', 'vhat', 'what', 'vmag', 'm_min', 'm_max',
            'beta', 'div', 'div_hat', 'wij', 'm_nbr_max', 'arho',
            'rho0', 'vor', 'shift_x', 'shift_y', 'shift_z'
        ]
        for fluid in fluids_with_io:
            pa = particle_arrays[fluid]
            for prop in props:
                pa.add_property(prop)
            pa.add_output_arrays([
                'm_min', 'm_max', 'closest_idx', 'split', 'n_nbrs', 'h', 'vmag',
                'beta', 'wij', 'vor', 'div', 'shift_x', 'shift_y', 'auhat',
                'avhat',
            ])
            if 'vmax' not in pa.constants:
                pa.add_constant('vmax', [0.0])
            if 'iters' not in pa.constants:
                pa.add_constant('iters', [0.0])
            pa.add_property('gradv', stride=9)
            pa.add_property('gradp', stride=3)

        for solid in self.solids + self.inviscid_solids:
            pa = particle_arrays[solid]
            pa.add_property('beta')
            pa.add_property('wij2')
            pa.add_property('htmp')
            pa.add_property('n_nbrs', type='int')
            pa.beta[:] = 1.0
            self._get_normals(pa)

    # Private protocol ###################################################
    def _get_edac_nu(self):
        if self.art_nu > 0:
            nu = self.art_nu
            print("Using artificial viscosity for EDAC with nu = %s" % nu)
        else:
            nu = self.nu
            print("Using real viscosity for EDAC with nu = %s" % self.nu)
        return nu

    def _get_normals(self, pa):
        from pysph.tools.sph_evaluator import SPHEvaluator
        from wall_normal import ComputeNormals, SmoothNormals

        pa.add_property('normal', stride=3)
        pa.add_property('normal_tmp', stride=3)

        name = pa.name

        props = ['m', 'rho', 'h']
        for p in props:
            x = pa.get(p)
            if np.all(x < 1e-12):
                msg = f'WARNING: cannot compute normals "{p}" is zero'
                print(msg)

        seval = SPHEvaluator(
            arrays=[pa], equations=[
                Group(equations=[
                    ComputeNormals(dest=name, sources=[name])
                ]),
                Group(equations=[
                    SmoothNormals(dest=name, sources=[name])
                ]),
            ],
            dim=self.dim, domain_manager=self.domain
        )
        seval.evaluate()

    def _get_internal_flow_equations(self):
        edac_nu = self._get_edac_nu()

        iom = self.inlet_outlet_manager
        fluids_with_io = self.fluids
        all_solids = self.solids + self.inviscid_solids
        if iom is not None:
            fluids_with_io = self.fluids + iom.get_io_names()
        all = fluids_with_io + all_solids

        equations = []
        # inlet-outlet
        if iom is not None:
            io_eqns = iom.get_equations(self, self.use_tvf)
            for grp in io_eqns:
                equations.append(grp)

        group1 = update_smoothing_length(
            fluids_with_io, all, self.dim, self.rho0, self.update_h,
            hdx=self.hdx
        )
        equations.extend(group1)

        group1 = []
        avg_p_group = []
        has_solids = len(all_solids) > 0
        for fluid in fluids_with_io:
            if not self.continuity:
                group1.append(
                    SummationDensityGather(dest=fluid, sources=all),
                )
            group1.append(
                ComputeBeta(dest=fluid, sources=all, dim=self.dim)
            )
            if self.bql:
                eq = ComputeAveragePressure(dest=fluid, sources=all)
                if has_solids:
                    avg_p_group.append(eq)
                else:
                    group1.append(eq)
        if self.solids:
            for solid in all_solids:
                group1.extend([
                    SummationDensityGather(dest=solid, sources=all),
                    EvaluateNumberDensity(dest=solid, sources=fluids_with_io),
                ])
            for solid in self.solids:
                group1.extend([
                    SetWallVelocity(dest=solid, sources=fluids_with_io),
                    # FIXME: Make this an option. This is useful for kepleian problem.
                    # SlipVelocityExtrapolation(dest=solid, sources=fluids_with_io),
                ])
            for solid in self.inviscid_solids:
                group1.extend([
                    NoSlipVelocityExtrapolation(
                        dest=solid, sources=fluids_with_io),
                    NoSlipAdvVelocityExtrapolation(
                        dest=solid, sources=fluids_with_io)
                ])
        equations.append(Group(equations=group1))

        if self.solids:
            group_bc = []
            for solid in all_solids:
                group_bc.append(
                    SolidWallPressureBC(
                        dest=solid, sources=fluids_with_io,
                        gx=self.gx, gy=self.gy, gz=self.gz
                    )
                )
            equations.append(Group(equations=group_bc))

        if self.has_ghosts:
            eq = []
            for fluid in self.fluids:
                eq.append(UpdateGhostProps(dest=fluid, sources=None))
            equations.append(Group(equations=eq, real=False))

        # Compute average pressure *after* the wall pressure is setup.
        if self.bql and has_solids:
            equations.append(Group(equations=avg_p_group, real=True))

        group2 = []
        for fluid in self.fluids:
            if self.continuity:
                group2.append(
                    ContinuityEquation(dest=fluid, sources=fluids_with_io)
                )
                if len(self.solids) > 0:
                    group2.append(
                        ContinuityEquationSolids(dest=fluid, sources=all_solids)
                    )
            if self.volume_based:
                group2.append(
                    MomentumEquationPressureGradient(
                        dest=fluid, sources=all, gx=self.gx, gy=self.gy,
                        gz=self.gz, tdamp=self.tdamp
                    )
                )
            else:
                group2.append(
                    MomentumEquationPressureGradient2(
                        dest=fluid, sources=all, gx=self.gx, gy=self.gy,
                        gz=self.gz, tdamp=self.tdamp
                    )
                )
            if self.alpha > 0.0:
                sources = fluids_with_io + self.solids
                group2.append(
                    MomentumEquationArtificialViscosity(
                        dest=fluid, sources=sources, alpha=self.alpha,
                        c0=self.c0
                    )
                )
            if self.nu > 0.0:
                if self.volume_based:
                    group2.append(
                        MomentumEquationViscosity(
                            dest=fluid, sources=fluids_with_io, nu=self.nu
                        )
                    )
                else:
                    group2.append(
                        MomentumEquationViscosity2(
                            dest=fluid, sources=fluids_with_io, nu=self.nu
                        )
                    )
            if len(self.solids) > 0 and self.nu > 0.0:
                group2.append(
                    SolidWallNoSlipBC(
                        dest=fluid, sources=self.solids, nu=self.nu
                    )
                )
            # group2.append(
            #     MomentumEquationArtificialStress(
            #         dest=fluid, sources=fluids_with_io
            #     )
            # )
            if self.volume_based:
                group2.append(
                    EDACEquation(
                        dest=fluid, sources=fluids_with_io, nu=edac_nu,
                        cs=self.c0, rho0=self.rho0, edac_alpha=self.edac_alpha
                ))
            else:
                group2.append(
                    EDACEquation2(
                        dest=fluid, sources=fluids_with_io, nu=edac_nu,
                        cs=self.c0, rho0=self.rho0, edac_alpha=self.edac_alpha
                ))
            group2.append(
                ShiftForceLind(
                    dest=fluid, sources=all, cs=self.c0, cfl=self.cfl,
                    rho0=self.rho0
                ))
            if len(self.solids) > 0 and self.nu > 0.0:
                if self.volume_based:
                    group2.append(
                        EDACEquationSolids(
                            dest=fluid, sources=all_solids, nu=edac_nu,
                            cs=self.c0, rho0=self.rho0,
                            edac_alpha=self.edac_alpha
                    ))
                else:
                    group2.append(
                        EDACEquationSolids2(
                            dest=fluid, sources=all_solids, nu=edac_nu,
                            cs=self.c0, rho0=self.rho0,
                            edac_alpha=self.edac_alpha
                    ))
            group2.append(
                Vorticity(
                    dest=fluid, sources=fluids_with_io
                )
            )
        equations.append(Group(equations=group2))

        # inlet-outlet
        if iom is not None:
            io_eqns = iom.get_equations_post_compute_acceleration()
            for grp in io_eqns:
                equations.append(grp)

        return equations
