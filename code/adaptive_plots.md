---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Plots for adaptive background particles

```python
%matplotlib notebook
from matplotlib import pyplot as plt
import numpy as np
```

```python
from compyle.api import get_config
get_config().use_openmp = True
```

```python
import os
figdir = os.path.join('..', 'manuscript', 'figures', 'background')
if not os.path.exists(figdir):
    os.makedirs(figdir)
```

```python
import check_bg_mesh as CBG
```

```python
f, s, bg, t = CBG.main(dx_min=0.1, cr=1.2)
```

```python
t.set_initial_bg()
```

```python
for i in range(10): t.update_bg()
```

```python
ds_min = bg.ds_min[0]
cr = bg.cr[0]
plt.figure()
plt.scatter(bg.x, bg.y, c=np.log(bg.ds/ds_min)/np.log(cr), s=1, cmap='viridis_r');
plt.colorbar();
plt.scatter(s.x, s.y, s=1, c='k');
plt.xlabel('x'); plt.ylabel('y');
plt.savefig(os.path.join(figdir, 'bg_levels.pdf'))
```

```python
t.initialize_fluid()
```

```python
plt.figure()
plt.axis('equal')
plt.scatter(f.x, f.y, c=f.m, s=1, cmap='viridis_r')
plt.colorbar();
plt.scatter(s.x, s.y, s=1, c='k');
plt.xlabel('x'); plt.ylabel('y');
plt.savefig(os.path.join(figdir, 'fluid_mass.pdf'))
```

## Moving bodies

```python
f = CBG.create_fluid(0.4)
s = CBG.create_solid(0.1)
s.x[:] = s.x - 2
#s.y[:] = s.y - 1.0

s1 = CBG.create_solid(0.1)
s1.set_name('solid1')
s1.x[:] = s1.x + 2
#s1.y[:] = s1.y + 1.0

t = CBG.UpdateBackground(
    f, dim=2, boundary=[s, s1], ds_min=0.1, ds_max=0.4, cr=1.2,
    rho_ref=1.0
)
bg = t.bg_pa
t.set_initial_bg()
```

```python
ds_min = bg.ds_min[0]
cr = bg.cr[0]
plt.figure()
plt.scatter(bg.x, bg.y, c=np.log(bg.ds/ds_min)/np.log(cr), s=1, cmap='viridis_r');
plt.colorbar();
plt.scatter(s.x, s.y, s=1, c='k');
plt.scatter(s1.x, s1.y, s=1, c='k');
plt.xlabel('x'); plt.ylabel('y');
plt.savefig(os.path.join(figdir, 'two_body_bg.pdf'))
```

```python
for i in range(60):
    s.x[:] = s.x - 0.025
    s1.x[:] = s1.x + 0.025
    t.update_bg()
```

```python
plt.figure()
plt.scatter(bg.x, bg.y, c=np.log(bg.ds/ds_min)/np.log(cr), s=1, cmap='viridis_r');
plt.colorbar();
plt.scatter(s.x, s.y, s=1, c='k');
plt.scatter(s1.x, s1.y, s=1, c='k');
plt.xlabel('x'); plt.ylabel('y');
plt.savefig(os.path.join(figdir, 'two_body_moved_bg.pdf'))
```

```python

```
