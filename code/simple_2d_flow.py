from pysph.examples.trivial_inlet_outlet import InletOutletApp
from adapt import AdaptiveResolution
from adaptv import AdaptiveResolutionVacondio
from pysph.sph.scheme import add_bool_argument


class AdaptiveFlow(InletOutletApp):
    def add_user_options(self, group):
        super().add_user_options(group)
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting"
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.vacondio = self.options.vacondio

    def create_nnps(self):
        from pysph.base.nnps import OctreeNNPS as NNPS
        return NNPS(dim=2.0, particles=self.particles, radius_scale=3.0,
                    cache=True)

    def create_particles(self):
        particles = super(AdaptiveFlow, self).create_particles()

        for pa in particles:
            for prop in ('uhat', 'vhat', 'what', 'm_min',
                         'm_max', 'vmag'):
                pa.add_property(prop)
            pa.add_property('closest_idx', type='int')
            pa.add_property('split', type='int')
            pa.add_output_arrays(['m_min', 'm_max', 'closest_idx', 'split'])
        return particles

    def pre_step(self, solver):
        f = self.particles[1]
        dx = 0.1
        m0 = dx*dx
        x = f.x
        y = f.y
        f.m_max[:] = m0*1.05
        f.m_min[:] = m0*0.7
        region = (x > 0.25) & (x < 0.75) & (y > 0.25) & (y < 0.75)
        m0 *= 0.5
        f.m_max[region] = m0*1.05
        f.m_min[region] = m0*0.7
        region = (x > 0.375) & (x < 0.625) & (y > 0.375) & (y < 0.625)
        m0 *= 0.5
        f.m_max[region] = m0*1.05
        f.m_min[region] = m0*0.7

    def create_tools(self):
        if self.vacondio:
            t = AdaptiveResolutionVacondio(self.particles[1], dim=2, rho0=1.0)
        else:
            t = AdaptiveResolution(self.particles[1], dim=2, rho0=1.0)
        return [t]


if __name__ == '__main__':
    app = AdaptiveFlow()
    app.run()
