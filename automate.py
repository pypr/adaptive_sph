#!/usr/bin/env python
import os
import json
import glob
from cycler import cycler
import numpy as np
import matplotlib as mpl
from matplotlib import rc, patches, colors
from matplotlib.collections import PatchCollection

from automan.api import PySPHProblem as Problem
from automan.api import (Automator, Simulation, filter_cases, filter_by_name)
from automan.api import CondaClusterManager
from automan.jobs import free_cores

from pysph.base.kernels import QuinticSpline
from pysph.tools.interpolator import Interpolator
from pysph.solver.utils import load, get_files


rc('font', **{'size': 14})
rc('legend', fontsize='medium')
rc('axes', grid=True, linewidth=1.2)
rc('axes.grid', which='both', axis='both')
# rc('axes.formatter', limits=(1, 2), use_mathtext=True, min_exponent=1)
rc('grid', linewidth=0.5, linestyle='--')
rc('xtick', direction='in', top=True)
rc('ytick', direction='in', right=True)
rc('savefig', format='pdf', bbox='tight', pad_inches=0.05,
   transparent=False, dpi=200)
rc('lines', linewidth=1.5)
rc('axes', prop_cycle=(
    cycler('color', ['tab:blue', 'tab:green', 'tab:red',
                     'tab:orange', 'm', 'tab:purple',
                     'tab:pink', 'tab:gray']) +
    cycler('linestyle', ['-.', '--', '-', ':',
                         (0, (3, 1, 1, 1)), (0, (3, 1, 1, 1, 1, 1)),
                         (0, (3, 2, 1, 1)), (0, (3, 2, 2, 1, 1, 1)),
                         ])
))


n_core = -1
n_thread = -2
backend = ' --openmp'


def _get_cpu_time(case):
    info = glob.glob(case.input_path('*.info'))[0]
    with open(info) as fp:
        data = json.load(fp)
    return round(data['cpu_time'], 2)


def _save_perf(out_dir, timings):
    path = os.path.join(out_dir, 'performance.json')
    data = {}
    if os.path.exists(path):
        with open(path, 'r+') as f:
            data = json.load(f)
    data.update(timings)
    with open(path, 'w') as f:
        json.dump(data, f, indent=4)


def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    # https://stackoverflow.com/a/18926541
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap


def create_streamlines(f_array, n, xmin, xmax, ymin, ymax):
    _x = np.linspace(xmin, xmax, n)
    _y = np.linspace(ymin, ymax, n)
    xx, yy = np.meshgrid(_x, _y)
    cond = ((f_array.x > xmin) & (f_array.x < xmax) &
            (f_array.y > ymin) & (f_array.y < ymax))
    x_start, y_start = f_array.x[cond], f_array.y[cond]
    f_array.h[:] = np.mean(f_array.h[cond])
    kernel = QuinticSpline(dim=2)
    interp = Interpolator([f_array], x=xx, y=yy,
    kernel=kernel)
    ui = np.zeros_like(xx)
    vi = np.zeros_like(xx)
    interp.update_particle_arrays([f_array])
    _u = interp.interpolate('u')
    _v = interp.interpolate('v')
    _u.shape = n, n
    _v.shape = n, n
    ui += _u
    vi += _v
    return xx, yy, ui, vi, x_start, y_start


def get_files_at_given_times(files, times):
    result = []
    count = 0
    for f in files:
        data = load(f)
        t = data['solver_data']['t']
        if count >= len(times):
            break
        if abs(t - times[count]) < t*1e-8:
            result.append(f)
            count += 1
        elif t > times[count]:
            count += 1
    return result


def get_files_at_given_times_from_log(files, times, logfile):
    import re
    result = []
    time_pattern = r"output at time\ (\d+(?:\.\d+)?)"
    d = {}
    for f in files:
        fname = os.path.basename(f)
        noext = os.path.splitext(fname)[0]
        iters = noext.split('_')[-1]
        d.update({iters: f})
    with open(logfile, 'r') as f:
        for line in f:
            t = re.findall(time_pattern, line)
            if t and float(t[0]) in times:
                iters = re.findall(r"iteration\ (\d+)", line)
                result.append(iters[0].zfill(5))
    out_files = []
    for i in result:
        out_files.append(d[i])
    return out_files


def merge_records(case_info):
    for key in case_info:
        info = case_info[key]
        if isinstance(info, tuple):
            if len(info) == 2:
                info, extra = case_info[key]
                info.update(extra)
            else:
                info = info[0]
            case_info[key] = info


def scheme_opts(params):
    if isinstance(params, tuple):
        return params[0]
    return params


class TaylorGreen(Problem):
    '''
    1. Adaptive - optimize h.
    2. No adaptive - finest resolution.
    4. Adaptive - Vacondio.
    Plots.
    1. Decay plot (1, 3)
    2. No of neighbours (1, 4, 5)
    3. L1_error vmag (1, 2, 3)
    4. L1_error vmag (1, 2, 3)
    5. Time it took to run. (1, 4, 5)
    6. h-plots (1, 4, 5)
    '''
    def get_name(self):
        return 'taylor_green'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/taylor_green.py' + backend

        self.res = [100, 1000]
        self.nxs = [100, 150]
        self.case_info = {
            f'tg_re_100_nx_{nx}_patch_7': dict(
                re=100, tf=2.5, pf=100, pert=0.1, nx=nx, adaptive=None,
                patches=7
            )
            for nx in self.nxs
        }
        self.case_info.update({
            f'tg_re_1000_nx_{nx}_patch_7': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=nx, adaptive=None,
                patches=7
            )
            for nx in self.nxs
        })
        self.case_info.update({
            f'tg_re_100_nx_{nx}_patch_4': dict(
                re=100, tf=2.5, pf=100, pert=0.1, nx=nx, adaptive=None,
                patches=4
            )
            for nx in self.nxs
        })
        self.case_info.update({
            f'tg_re_1000_nx_{nx}_patch_4': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=nx, adaptive=None,
                patches=4
            )
            for nx in self.nxs
        })
        self.case_info.update({
            f'tg_re_100_no_adapt_nx_{nx}': dict(
                re=100, tf=2.5, pf=100, pert=0.1, nx=nx, no_adaptive=None
            )
            for nx in self.nxs
        })
        self.case_info.update({
            f'tg_re_1000_no_adapt_nx_{nx}': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=nx, no_adaptive=None
            )
            for nx in self.nxs
        })

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._plot_re()
        self._plot_particles(dict(nx=150, re=1000, adaptive=None),
                             label='nx_150_re_1000')

    def _plot_re(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:green', 'tab:blue', 'tab:red',
                             'tab:olive', 'tab:cyan', 'tab:orange']) +
            cycler('linestyle', ['-', '-', '-', '--', '--', '--'])
        ))

        import matplotlib.pyplot as plt

        for re in self.res:
            fig, ax = plt.subplots(1, 2, figsize=(14, 4.5))
            for case in filter_cases(self.cases, re=re):
                nx = case.params['nx']
                label = r'$L/\Delta x_{\max} = $' + f'{nx}'
                if 'no_adaptive' in case.params:
                    label += ', no adaptive'
                data = np.load(case.input_path('results.npz'))
                t = data['t']
                decay_ex = data['decay_ex']
                ax[0].semilogy(data['t'], data['decay'], label=label)
                ax[1].plot(data['t'], data['l1'], label=label)
            ax[0].semilogy(t, decay_ex, 'k-', label='exact')
            for i in range(2):
                ax[i].legend(loc='best', fontsize='small')
                ax[i].set_xlabel('time')
            ax[0].set_ylabel('max velocity')
            ax[1].set_ylabel(r'$L_1$ error')
            fig.savefig(self.output_path(f'tg_re_{re}'))
            plt.clf()
            plt.close()

    def _plot_particles(self, conditions, label=None):
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots(1, 1, figsize=(6, 4), sharey=True)
        plt.setp(ax, aspect=1.0, adjustable='box')
        for case in filter_cases(self.cases, **conditions):
            files = get_files(case.input_path(), 'taylor_green')
            data = load(files[-1])
            f = data['arrays']['fluid']
            vmag = np.sqrt(f.u*f.u + f.v*f.v)
            tmp = ax.scatter(
                f.x, f.y, c=vmag, s=3, cmap=plt.cm.get_cmap('jet', 20),
                edgecolors='none', rasterized=True
            )
            ax.set_xlabel(r'$x$')
            ax.set_ylabel(r'$y$')
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
            if 'patches' in conditions:
                if conditions['patches'] == 1:
                    rect = [
                        patches.Rectangle(
                            [0.1, 0.1], width=0.4, height=0.4,
                            edgecolor='k', fill=False, linestyle='--'
                        )
                    ]
                    ax.annotate(
                        r'$m$', xy=(0.12, 0.8), xycoords='data',
                        xytext=(-0.2, 0.7), textcoords='axes fraction',
                        arrowprops=dict(arrowstyle='->')
                    )
                    ax.annotate(
                        r'$\frac{m}{4}$', xy=(0.12, 0.4), xycoords='data',
                        xytext=(-0.2, 0.2), textcoords='axes fraction',
                        arrowprops=dict(arrowstyle='->')
                    )
                else:
                    xys = [(0.15, 0.15), (0.15, 0.65), (0.65, 0.65),
                           (0.65, 0.15)]
                    if conditions['patches'] == 7:
                        xys.extend(
                            [(0.15, 0.4), (0.4, 0.65), (0.65, 0.4)]
                        )
                    rect = [
                        patches.Rectangle(
                            xy, width=0.2, height=0.2, edgecolor='k',
                            fill=False, linestyle='--'
                        )
                        for xy in xys
                    ]
                pc = PatchCollection(rect, match_original=True)
                ax.add_collection(pc)
        ax.grid()
        fig.colorbar(tmp, ax=ax, shrink=0.9, label=r'$|\mathbf{u}|$', pad=0.01)
        plt.savefig(self.output_path(f'tg_pplot_{label}'))
        plt.close()


class TaylorGreenAdaptiveErrors(TaylorGreen):
    def get_name(self):
        return 'taylor_green_adapt_err'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/taylor_green.py' + backend

        self.res = [1000]
        self.nxs = [50, 100]
        self.case_info = {
            f'tg_re_1000_nx_50_patch_{patch}': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=50, adaptive=None,
                patch=patch
            )
            for patch in range(1, 9)
        }
        self.case_info.update({
            f'tg_re_1000_nx_100_patch_{patch}': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=100, adaptive=None,
                patch=patch
            )
            for patch in range(1, 9)
        })

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._plot_re()

    def _plot_re(self):
        import matplotlib.pyplot as plt

        for nx in self.nxs:
            fig, ax = plt.subplots(1, 2, figsize=(15, 6))
            for case in filter_cases(self.cases, nx=nx):
                patch = case.params['patch']
                label = f'patch {patch}'
                if 'no_adaptive' in case.params:
                    label += ', no adaptive'
                data = np.load(case.input_path('results.npz'))
                t = data['t']
                decay_ex = data['decay_ex']
                ax[0].semilogy(data['t'], data['decay'], label=label)
                ax[1].plot(data['t'], data['l1'], label=label)
            ax[0].semilogy(t, decay_ex, 'k-', label='exact')
            for i in range(2):
                ax[i].legend(loc='best', fontsize='small')
                ax[i].set_xlabel('time')
            ax[0].set_ylabel('max velocity')
            ax[1].set_ylabel(r'$L_1$ error')
            fig.savefig(self.output_path(f'tg_re_1000_nx_{nx}'))
            plt.clf()
            plt.close()


class TaylorGreenPatch(TaylorGreen):
    def get_name(self):
        return 'taylor_green_patch'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/taylor_green.py' + backend

        self.res = [200, 1000]
        self.nxs = [100]
        self.case_info = {
            f'tg_re_1000_nx_100_patch_{patch}': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=100, adaptive=None,
                patches=patch
            )
            for patch in [1, 4, 7]
        }
        self.case_info.update({
            f'tg_re_200_nx_100_patch_{patch}': dict(
                re=200, tf=2.5, pf=100, pert=0.1, nx=100, adaptive=None,
                patches=patch
            )
            for patch in [1, 4, 7]
        })

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._plot_re()
        for re in [200, 1000]:
            self._plot_ke(re=re)
        for p in [1, 4, 7]:
            self._plot_particles(dict(nx=100, re=1000, patches=p),
                                 label=f'nx_100_patch_{p}')

    def _plot_re(self):
        import matplotlib.pyplot as plt

        for nx in self.nxs:
            fig, ax = plt.subplots(1, 2, figsize=(15, 6))
            for case in filter_cases(self.cases, nx=nx):
                patch = case.params['patches']
                label = f'patch {patch}'
                if 'no_adaptive' in case.params:
                    label += ', no adaptive'
                data = np.load(case.input_path('results.npz'))
                t = data['t']
                decay_ex = data['decay_ex']
                ax[0].semilogy(data['t'], data['decay'], label=label)
                ax[1].plot(data['t'], data['l1'], label=label)
            ax[0].semilogy(t, decay_ex, 'k-', label='exact')
            for i in range(2):
                ax[i].legend(loc='best', fontsize='small')
                ax[i].set_xlabel('time')
            ax[0].set_ylabel('max velocity')
            ax[1].set_ylabel(r'$L_1$ error')
            fig.savefig(self.output_path(f'tg_re_1000_nx_{nx}'))
            plt.clf()
            plt.close()


class TaylorGreenChiron(TaylorGreen):
    def get_name(self):
        return 'taylor_green_chiron'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/taylor_green.py' + backend

        self.res = [200, 1000]
        self.nxs = [50, 100, 150]
        self.case_info = {
            f'tg_re_200_nx_{nx}_patch_1': dict(
                re=200, tf=2.5, pf=100, pert=0.1, nx=nx, adaptive=None,
                patches=1
            )
            for nx in self.nxs
        }

        self.case_info.update({
            f'tg_re_1000_nx_{nx}_patch_1': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=nx, adaptive=None,
                patches=1
            )
            for nx in self.nxs
        })

        self.case_info.update({
            f'tg_re_200_nx_{nx}_no_adapt': dict(
                re=200, tf=2.5, pf=100, pert=0.1, nx=nx, no_adaptive=None,
            )
            for nx in self.nxs
        })
        self.case_info.update({
            f'tg_re_1000_nx_{nx}_no_adapt': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=nx, no_adaptive=None,
            )
            for nx in self.nxs
        })

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        rc('axes', prop_cycle=(
            cycler('color', ['tab:green', 'tab:blue', 'tab:red',
                             'tab:olive', 'tab:cyan', 'tab:orange']) +
            cycler('linestyle', ['-', '-', '-', '--', '--', '--'])
        ))
        self._plot_re()
        for re in [200, 1000]:
            self._plot_ke(re=re)
            self._plot_volume(re)
        self._plot_particles(dict(nx=100, re=1000, patches=1),
                             label='nx_100_re_1000')
        self._plot_particles(dict(nx=100, re=200, patches=1),
                             label='nx_100_re_200')
        self._plot_particle_property(dict(nx=100, re=1000, patches=1),
                                     label='nx_100', plot_key='h')
        self._plot_particle_property(dict(nx=100, re=1000, patches=1),
                                     label='nx_100', plot_key='n_nbrs')
        self._plot_particle_property(dict(nx=100, re=1000, patches=1),
                                     label='nx_100', plot_key='p')
        self._plot_particle_property(dict(nx=100, re=200, patches=1),
                                     label='nx_100', plot_key='p')

    def _plot_ke(self, re):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:green', 'tab:olive', 'tab:blue',
                             'tab:cyan', 'tab:red', 'tab:orange']) +
            cycler('linestyle', ['-', '--', '-', '--', '-', '--'])
        ))

        import pandas as pd
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(6, 5))
        fige, axe = plt.subplots(1, 1, figsize=(6, 5))
        for nx in self.nxs:
            for case in filter_cases(self.cases, nx=nx, re=re):
                data = case.data
                nx = case.params['nx']
                label = f'nx = {nx}'
                if 'no_adaptive' in case.params:
                    label += ', no adaptive'
                t = data['t']
                ke = data['ke']
                ke_ex = data['ke_ex']
                ax.semilogy(t, ke, label=label)
                axe.semilogy(t, np.abs(ke - ke_ex), label=label)
        ax.semilogy(t, ke_ex, 'k-', label='exact')
        ax.set_xlabel('time')
        ax.set_ylabel('Kinetic energy')
        axe.set_xlabel('time')
        axe.set_ylabel('Error in kinetic energy')

        if re == 200:
            val = pd.read_csv('code/data/chiron_ke_decay_apr.csv')
            # Divide by 1000 as density for Chiron's is 1000.
            # ax.plot(val['t'], val['ke']/1000, 'c+', markersize=3,
            ax.semilogy(val['t'], val['ke']/1000, 'm-+', markersize=3,
                        label='APR Chiron et al.')

        ax.legend(loc='best', fontsize='small')
        axe.legend(loc='best', fontsize='small')
        fig.savefig(self.output_path(f'tg_ke_re_{re}'))
        fige.savefig(self.output_path(f'tg_ke_error_re_{re}'))
        plt.clf()
        plt.close()

    def _plot_volume(self, re):
        import pandas as pd
        rc('axes', prop_cycle=(
            cycler('color', ['tab:green', 'tab:olive', 'tab:blue',
                             'tab:cyan', 'tab:red', 'tab:orange']) +
            cycler('linestyle', ['-', '--', '-', '--', '-', '--'])
        ))
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(6, 5))
        for nx in self.nxs:
            for case in filter_cases(self.cases, nx=nx, re=re):
                data = case.data
                nx = case.params['nx']
                label = f'nx = {nx}'
                if 'no_adaptive' in case.params:
                    label += ', no adaptive'
                t = data['t']
                vol_err = data['vol_err']
                ax.semilogy(t, vol_err, label=label)

        if re == 1000:
            val = pd.read_csv('code/data/sun_dpsph_volume.csv')
            ax.semilogy(val['t'][::3], val['vol'][::3], 'm+', markersize=5,
                        label=r' $\delta^{+}$ SPH Sun et al.')
        ax.set_xlabel('time')
        ax.set_ylim([1e-5, 1])
        ax.set_xlim([0, 2.5])
        ax.set_ylabel(r'$\epsilon_V$(%) error in volume')
        ax.legend(loc='best', fontsize='x-small')
        fig.savefig(self.output_path(f'tg_vol_err_re_{re}'))
        plt.clf()
        plt.close()

    def _plot_particle_property(self, conditions, label=None, plot_key='h'):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(6, 4), sharey=True)
        plt.setp(ax, aspect=1.0, adjustable='box')
        for case in filter_cases(self.cases, **conditions):
            re = case.params['re']
            files = get_files(case.input_path(), 'taylor_green')
            data = load(files[-1])
            f = data['arrays']['fluid']
            c = f.get(plot_key)
            if plot_key == 'p':
                c = c - np.mean(c)
            tmp = ax.scatter(
                f.x, f.y, c=c, s=2,
                cmap=plt.cm.get_cmap('jet', 16),
                edgecolors='none', rasterized=True,
                vmin=np.min(c),
                vmax=np.max(c)
            )
            ax.set_xlabel(r'$x$')
            ax.set_ylabel(r'$y$')
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
        ax.grid()
        if plot_key == 'n_nbrs':
            label="Neighbors"
        elif plot_key == 'p':
            label=r"$p - p_{avg}$"
        else:
            label=plot_key
        cbar = fig.colorbar(tmp, ax=ax, shrink=0.9, label=label, pad=0.01)
        plt.savefig(self.output_path(f'tg_pplot_{plot_key}_re_{re}'))
        plt.close()


class TaylorGreenFormulation(TaylorGreen):
    def get_name(self):
        return 'taylor_green_formulation'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/taylor_green.py' + backend

        self.res = [100, 1000]
        self.nxs = [100]
        self.edac_alpha = [1.0, 2.0]
        self.case_info = {
            f'tg_re_100_nx_100_alpha_{alpha}_volume': dict(
                re=100, tf=2.5, pf=100, pert=0.1, nx=100, adaptive=None,
                edac_alpha=alpha, volume_based=None
            )
            for alpha in self.edac_alpha
        }
        self.case_info.update({
            f'tg_re_1000_nx_100_alpha_{alpha}_volume': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=100, adaptive=None,
                edac_alpha=alpha, volume_based=None
            )
            for alpha in self.edac_alpha
        })

        self.case_info.update({
            f'tg_re_100_nx_100_alpha_{alpha}_density': dict(
                re=100, tf=2.5, pf=100, pert=0.1, nx=100, adaptive=None,
                edac_alpha=alpha, no_volume_based=None
            )
            for alpha in self.edac_alpha
        })
        self.case_info.update({
            f'tg_re_1000_nx_100_alpha_{alpha}_density': dict(
                re=1000, tf=2.5, pf=100, pert=0.1, nx=100, adaptive=None,
                edac_alpha=alpha, no_volume_based=None
            )
            for alpha in self.edac_alpha
        })

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._plot_alpha()

    def _plot_alpha(self):
        import matplotlib.pyplot as plt

        for re in self.res:
            fig, ax = plt.subplots(1, 2, figsize=(15, 6))
            for case in filter_cases(self.cases, re=re):
                alpha = case.params['edac_alpha']
                label = r'$\alpha =\ $' + f'{alpha}'
                if 'volume_based' in case.params:
                    label += ', V'
                else:
                    label += r', m/$\rho$.'
                data = np.load(case.input_path('results.npz'))
                t = data['t']
                decay_ex = data['decay_ex']
                ax[0].semilogy(data['t'], data['decay'], label=label)
                ax[1].plot(data['t'], data['l1'], label=label)
            ax[0].semilogy(t, decay_ex, 'k-', label='exact')
            for i in range(2):
                ax[i].legend(loc='best')
                ax[i].set_xlabel('time')
            ax[0].set_ylabel('max velocity')
            ax[1].set_ylabel(r'$L_1$ error')
            fig.savefig(self.output_path(f're_{re}'))
            plt.clf()
            plt.close()


class Cavity(Problem):
    '''
    1. Adaptive - optimize h.
    2. No adaptive - coarse resolution.
    3. No adaptive - finest resolution.
    Plots.
    1. Decay plot (1, 2, 3)
    2. No of neighbours (1, 4, 5)
    3. L1_error vmag (1, 2, 3)
    4. L1_error vmag (1, 2, 3)
    5. Time it took to run. (1, 4, 5)
    6. h-plots (1, 4, 5)
    '''
    def get_name(self):
        return 'cavity'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/cavity.py' + backend
        self.re = [100, 1000]

        self.case_info = {
            'cavity_re_100_nx_50': dict(re=100, tf=15, pf=400, nx=50),
            'cavity_re_100_nx_100': dict(re=100, tf=15, pf=400, nx=100),
            'cavity_re_100_nx_150': dict(re=100, tf=15, pf=400, nx=150),

            'cavity_re_1000_nx_50': dict(re=1000, tf=80, pf=800, nx=50),
            'cavity_re_1000_nx_100': dict(re=1000, tf=80, pf=800, nx=100),
            'cavity_re_1000_nx_150': dict(re=1000, tf=80, pf=800, nx=150),
        }

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._plot_uv_profile()
        self._plot_particles(dict(nx=150, re=1000), annotate=True)
        import matplotlib.pyplot as plt
        self._plot_particles(dict(nx=150, re=1000), plot_prop='p',
                             cmap=plt.cm.get_cmap('jet', 21))

    def _plot_uv_profile(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:blue', 'tab:green', 'tab:red']) +
            cycler('linestyle', ['-.', '--', '-'])
        ))
        import matplotlib.pyplot as plt
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        # Re 100
        # u vs y
        y, exp_u = get_u_vs_y()
        x, exp_v = get_v_vs_x()

        for re in self.re:
            fig, ax = plt.subplots(1, 2, figsize=(10, 4.5))
            ax[0].plot(exp_u[re], y, 'ko', fillstyle='none',
                        label=f'Ghia et al. (Re={re})')
            ax[0].set_xlabel('$u$')
            ax[0].set_ylabel('$y$')
            ax[1].plot(x, exp_v[re], 'ko', fillstyle='none',
                        label=f'Ghia et al. (Re={re})')
            ax[1].set_xlabel('$x$')
            ax[1].set_ylabel('$v$')
            for case in filter_cases(self.cases, re=re):
                data = case.data
                nx = case.params['nx']
                label = r'L/$\Delta x_{\min}$ = ' + f'{nx}'
                ax[0].plot(data['u_c'], data['x'], label=label)
                ax[1].plot(data['x'], data['v_c'], label=label)
            ax[0].legend(loc='best')
            ax[1].legend(loc='best')
            fig.savefig(self.output_path(f'uv_re_{re}'))
            plt.close()

    def _plot_particles(self, conditions, plot_prop='vmag', cmap='viridis',
                        annotate=False):
        import matplotlib.pyplot as plt

        for case in filter_cases(self.cases, **conditions):
            fig1, ax1 = plt.subplots(1, 1, figsize=plt.figaspect(0.8))
            files = get_files(case.input_path(), 'cavity')
            re = case.params['re']
            data = load(files[-1])
            f = data['arrays']['fluid']
            if plot_prop == 'vmag':
                c = np.sqrt(f.u**2 + f.v**2)
            elif plot_prop == 'p':
                c = f.p - np.mean(f.p)
            else:
                c = f.get(plot_prop)
            tmp = ax1.scatter(
                f.x, f.y, s=8, c=c, edgecolors='none',
                alpha=1.0, rasterized=True, vmin=np.min(c), vmax=np.max(c),
                cmap=cmap
            )
            if annotate:
                rect = [
                    patches.Rectangle(
                        [0.3, 0.3], width=0.4, height=0.55,
                        edgecolor='k', fill=False, linestyle='--'
                    ),
                    patches.Rectangle(
                        [0.4, 0.4], width=0.2, height=0.2,
                        edgecolor='k', fill=False, linestyle='-'
                    )
                ]
                pc = PatchCollection(rect, match_original=True)
                ax1.add_collection(pc)
                ax1.annotate(
                    r'$2m$', xy=(0.35, 0.35), xycoords='data',
                    xytext=(-0.2, 0.22), textcoords='axes fraction',
                    arrowprops=dict(arrowstyle='->')
                )
                ax1.annotate(
                    r'$4m$', xy=(0.45, 0.45), xycoords='data',
                    xytext=(-0.2, 0.42), textcoords='axes fraction',
                    arrowprops=dict(arrowstyle='->')
                )
                ax1.annotate(
                    r'$m$', xy=(0.1, 0.7), xycoords='data',
                    xytext=(-0.2, 0.65), textcoords='axes fraction',
                    arrowprops=dict(arrowstyle='->')
                )
            if plot_prop == 'p':
                label = r'$p - p_{avg}$'
            else:
                label = plot_prop
            fig1.colorbar(tmp, label=label)
            ax1.set_xlim([0, 1])
            ax1.set_ylim([0, 1])
            ax1.set_xlabel(r'$x$')
            ax1.set_ylabel(r'$y$')
            ax1.grid()
            fig1.tight_layout(pad=0)
            fig1.savefig(self.output_path(f'pplot_re_{re}_{plot_prop}'),
                         dpi=150)
            plt.close()


class CavityFormulation(Cavity):
    def get_name(self):
        return 'cavity_formulation'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/cavity.py' + backend

        self.re = [1000]
        self.edac_alpha = [1.0, 2.0]
        self.case_info = {
            f'cavity_re_1000_nx_100_alpha_{alpha}_density': dict(
                re=1000, tf=60, pf=400, adaptive=None, edac_alpha=alpha,
                no_volume_based=None, nx=100
            )
            for alpha in self.edac_alpha
        }
        self.case_info.update({
            f'cavity_re_1000_nx_100_alpha_{alpha}_volume': dict(
                re=1000, tf=60, pf=400, adaptive=None, edac_alpha=alpha,
                volume_based=None, nx=100
            )
            for alpha in self.edac_alpha
        })

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def _plot_uv_profile(self):
        import matplotlib.pyplot as plt
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        # Re 100
        # u vs y
        y, exp_u = get_u_vs_y()
        x, exp_v = get_v_vs_x()

        for re in self.re:
            fig, ax = plt.subplots(1, 2, figsize=(12, 7))
            fig.subplots_adjust(hspace=0)
            ax[0].plot(exp_u[re], y, 'ko', fillstyle='none',
                        label=f'Ghia et al. (Re={re})')
            ax[0].set_xlabel(r'$u$')
            ax[0].set_ylabel(r'$y$')
            ax[1].plot(x, exp_v[re], 'ko', fillstyle='none',
                        label=f'Ghia et al. (Re={re})')
            ax[1].set_xlabel(r'$x$')
            ax[1].set_ylabel(r'$v$')
            for case in filter_cases(self.cases, re=re):
                data = case.data
                alpha = case.params['edac_alpha']
                if 'volume_based' in case.params:
                    msg = ', V'
                else:
                    msg = r', m/$\rho$.'
                ax[0].plot(data['u_c'], data['x'],
                           label=r'$\alpha = $' + f'{alpha}' + msg)
                ax[1].plot(data['x'], data['v_c'],
                           label=r'$\alpha = $' + f'{alpha}' + msg)
            ax[0].legend(loc='best')
            ax[1].legend(loc='best')
            ax[0].set_aspect('equal', 'box')
            ax[1].set_aspect('equal', 'box')
            fig.savefig(self.output_path(f'uv_re_{re}'))
            plt.close()

class GreshoChan(Problem):
    '''
    Cases:
    1. Adaptive - optimize h.
    2. No adaptive - [50, 100, 200]
    '''
    def get_name(self):
        return 'gresho_chan'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/gresho_chan.py' + backend

        nxs = [50, 100]
        tf = 3.0
        self.case_info = {
            f'gresho_chan_{nx}_no_adapt': dict(
                tf=tf, nx=nx, no_adaptive=None, pf=100
            )
            for nx in nxs
        }

        self.case_info.update({
            'gresho_chan_adaptive_nx_50': dict(
               tf=tf, nx=50, pf=100, adaptive=None,
            ),
            'gresho_chan_adaptive_nx_100': dict(
                tf=tf, nx=100, pf=100, adaptive=None
            )
        })

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:cyan', 'tab:orange', 'tab:blue', 'tab:red']) +
            cycler('linestyle', ['--', '--', '-', '-'])
        ))
        self.make_output_dir()
        self.plot_conservation()
        self.plot_particles()
        self.plot_decay()

    def plot_decay(self):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(7, 6))
        for case in self.cases:
            results = case.data
            t = results['t_total']
            decay = results['decay']
            nx = case.params['nx']
            if 'adaptive' in case.params:
                label = r'Adaptive, $L/\Delta x_{\max}$' + f' = {nx}'
            elif 'no_adaptive' in case.params:
                label = r'No adaptive, $L/\Delta x$' + f' = {nx}'
            ax.plot(t, decay, label=label)
        ax.set_xlabel("time")
        ax.set_ylabel("maximum velocity")
        ax.legend()
        fig.tight_layout()
        fig.savefig(self.output_path('decay'))

    def plot_convergence(self):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(7, 6))
        nx, l1_err = [], []
        for case in filter_cases(self.cases, no_adaptive=None):
            results = case.data
            nx.append(case.params['nx'])
            l1norm = results['l1norm']
            l1_err.append(l1norm)

        nx_adapt, l1_err_adapt = [], []
        for case in filter_cases(self.cases, adaptive=None):
            results = case.data
            l1norm = results['l1norm']
            nx_adapt.append(case.params['nx'])
            l1_err_adapt.append(l1norm)
        ax.semilogy(nx, l1_err, 'ko-')
        ax.semilogy(nx_adapt, l1_err_adapt, 'rx')
        ax.set_xlabel(r"No. of particles in one direction, $N_{1D}$.")
        ax.set_ylabel(r"$L_1(\mathbf{u})$ error in velocity magnitude.")
        fig.tight_layout()
        fig.savefig(self.output_path('l1_norm'))

    def plot_conservation(self):
        import matplotlib.pyplot as plt

        fig1, ax1 = plt.subplots(1, 1, figsize=(7, 6))
        fig2, ax2 = plt.subplots(1, 1, figsize=(7, 6))
        fig3, ax3 = plt.subplots(1, 1, figsize=(7, 6))
        fig4, ax4 = plt.subplots(1, 1, figsize=(7, 6))
        figs = [fig1, fig2, fig3, fig4]
        axs = [ax1, ax2, ax3, ax4]
        for case in self.cases:
            results = case.data
            nx = case.params['nx']
            t_total = results['t_total']
            angular_mom = results['angular_mom']
            mom_x = results['mom_x']
            mom_y = results['mom_y']
            tot_mass = results['tot_mass']
            if 'adaptive' in case.params:
                label = r'Adaptive, $L/\Delta x_{\max}$' + f' = {nx}'
            elif 'no_adaptive' in case.params:
                label = r'No adaptive, $L/\Delta x$' + f' = {nx}'
            axs[0].semilogy(t_total, angular_mom, label=label)
            axs[1].semilogy(t_total, mom_x, label=label)
            axs[2].semilogy(t_total, mom_y, label=label)
            axs[3].plot(t_total, tot_mass, label=label)

        axs[0].set_ylabel(r"Angular momentum in $z$-direction")
        axs[1].set_ylabel(r"Linear momentum in $x$-direction")
        axs[2].set_ylabel(r"Linear momentum in $y$-direction")
        axs[3].set_ylabel(r"Total mass")
        for ax in axs:
            ax.legend()
            ax.set_xlabel("time")
        figs[0].savefig(self.output_path('ang_mom'))
        figs[1].savefig(self.output_path('lin_mom_x'))
        figs[2].savefig(self.output_path('lin_mom_y'))
        figs[3].savefig(self.output_path('mass'))

    def plot_particles(self):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots()
        fig, ax = plt.subplots(
            1, 3, figsize=(12, 4), sharey=True,
            constrained_layout=True
        )
        fig2, ax2 = plt.subplots(
            1, 3, figsize=(12, 4), sharey=True, sharex=True,
            constrained_layout=True
        )
        plt.setp(ax2.flat, aspect=1.0, adjustable='box')
        ax = ax.ravel()
        ax2 = ax2.ravel()

        names = []
        for case in self.cases:
            if '50_no_adapt' not in case.name:
                names.append(case.name)

        for i, case in enumerate(filter_by_name(self.cases, names)):
            nx = case.params['nx']
            files = get_files(case.input_path(), 'gresho_chan')
            data = load(files[-1])
            f = data['arrays']['fluid']
            t = data['solver_data']['t']
            r1 = np.sqrt(f.x**2 + f.y**2)
            vmag = np.sqrt(f.u**2 + f.v**2)
            results = case.data
            r = results['r']
            umage = results['umage']
            ax[i].plot(r, umage, 'r-', linewidth=1)
            ax[i].scatter(r1, vmag, s=1, color='k', rasterized=True)
            l1norm = results['l1norm'][0]
            if 'adaptive' in case.params:
                title = msg = r'Adaptive, $L/\Delta x_{\max}$' + f' = {nx}'
            elif 'no_adaptive' in case.params:
                title = msg = r'No adaptive, $L/\Delta x$' + f' = {nx}'
            msg += "\n" + r"$L_1$" + f" = {l1norm:.2e}"
            ax[i].set_title(msg)
            ax[i].set_xlabel(r"$r$")
            ax[i].set_xlim(0, 1.0)
            ax[i].set_ylim(-0.05, np.max(vmag)*1.1)
            # Particle plots.
            tmp = ax2[i].scatter(f.x, f.y, s=2, c=vmag, vmin=0, vmax=1,
                                 cmap='jet', rasterized=True)
            ax2[i].set_xlim(-0.5, 0.5)
            ax2[i].set_ylim(-0.5, 0.5)
            ax2[i].set_title(title)
            ax2[i].set_xlabel(r"$x$")
        ax2[0].set_ylabel(r"$y$")
        fig2.colorbar(tmp, shrink=0.6)

        ax[0].set_ylabel(r"$|\mathbf{u}|$, velocity magnitude")
        fig.savefig(self.output_path('vmag'))
        fig2.savefig(self.output_path('pplots'), dpi=100)


class FPC(Problem):
    '''
    Cases:
    1. Re 40
    2. Re 550
    3. Re 1000
    4. Re 3000
    5. Re 9500
    '''
    def get_name(self):
        return 'fpc'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_auto.py' + backend
        self.output_times = list(range(1, 7))

        self.res = [40, 550, 1000, 3000, 9500]
        self.tf = tf = 6.0
        dx_min = [0.02, 0.01, 0.004]
        self.case_info = {
            f'fpc_re_40_dx_min_{dx}': dict(
                re=40, tf=tf, pf=200, dx_min=dx, cr=1.08
            )
            for dx in dx_min
        }

        dx_min = [0.02, 0.01, 0.004]
        self.case_info.update({
            f'fpc_re_550_dx_min_{dx}': dict(
                re=550, tf=tf, pf=100, dx_min=dx, cr=1.08
            )
            for dx in dx_min
        })

        dx_min = [0.02, 0.01, 0.004]
        self.case_info.update({
            f'fpc_re_1000_dx_min_{dx}': dict(
                re=1000, tf=tf, pf=100, dx_min=dx, cr=1.08
            )
            for dx in dx_min
        })

        dx_min = [0.02, 0.01, 0.004]
        self.case_info.update({
            f'fpc_re_3000_dx_min_{dx}': dict(
                re=3000, tf=tf, pf=100, dx_min=dx, cr=1.08
            )
            for dx in dx_min
        })

        dx_min = [0.004, 0.0025]
        self.case_info.update({
            f'fpc_re_9500_dx_min_{dx}': dict(
                re=9500, tf=tf, pf=100, dx_min=dx, cr=1.08
            )
            for dx in dx_min
        })
        self.case_info.update({
            'fpc_re_9500_dx_min_0.002_cr_115': dict(
                re=9500, tf=tf, pf=100, dx_min=0.002, cr=1.15
            )
            for dx in dx_min
        })

        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._skin_friction_drag_plots(plot_dx_min=0.004)
        self._drag_plots()
        times = [3, 6]
        plot_dx_min = [0.002]
        plot_re = [9500]
        # self._vorticity_plots(times, plot_dx_min=plot_dx_min, plot_re=plot_re)
        import matplotlib.pyplot as plt
        cmap = plt.cm.get_cmap('jet_r', 20)
        self._plot_prop(dict(re=9500, dx_min=0.002), prop='h',
                        xmin=18, xmax=22, cmap=cmap, dpi=100)
        self._plot_prop_hist(dict(re=9500, dx_min=0.002), prop='n_nbrs')
        self._centerline_velocity(dx_min=0.004, re=3000, times=[1, 2, 3, 4, 5])
        self._centerline_velocity(dx_min=0.002, re=9500, times=[1, 2, 3])
        # self.plot_prop(dict(re=9500, dx_min=0.002), prop='vor', size=10,
        #                vmin=-6, vmax=6, figsize=(8, 5),
        #                ymin=-2, ymax=2, cmap='seismic', xmin=3.8,
        #                times=[1.25818,])

    def _plot_prop_hist(self, conditions, label=None, prop='h'):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(8, 6))
        for case in filter_cases(self.cases, **conditions):
            files = get_files(case.input_path(), 'fpc_auto')
            if prop == 'n_nbrs':
                label='Neighbors'
            else:
                label = prop
            data = load(files[-1])
            f = data['arrays']['fluid']
            val = f.get(prop)
            ax.hist(val, bins=20)
            ax.grid()
        fig.savefig(self.output_path(f'hist_{prop}'))
        plt.clf()

    def plot_prop(self, conditions=None, prop='ds', cmap='rainbow',
                  xmin=3, xmax=14.2, ymin=-4, ymax=4, figsize=(10, 4),
                  size=20, dpi=150, vmin=-6, vmax=6, times=None):
        import matplotlib.pyplot as plt
        if conditions is None:
            conditions = {}
        if times is None:
            times = [1, 2, 3]
        for case in filter_cases(self.cases, **conditions):
            filename_w_ext = os.path.basename(case.base_command.split(' ')[1])
            filename = os.path.splitext(filename_w_ext)[0]
            files = get_files(case.input_path(), filename)
            logfile = case.input_path(f'{filename}.log')
            files = get_files_at_given_times_from_log(files, times, logfile)
            for file, t in zip(files, times):
                fig, ax = plt.subplots(1, 1, figsize=figsize)
                data = load(file)
                t = data['solver_data']['t']
                f = data['arrays']['fluid']
                s1 = data['arrays']['solid']
                label = rf"${prop}$"
                val = f.get(prop)
                cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

                tmp = ax.scatter(
                    f.x[cond], f.y[cond], c=val[cond],
                    s=size*f.m[cond]/np.max(f.m[cond]),
                    rasterized=True, cmap=cmap,
                    edgecolor='none', vmin=vmin, vmax=vmax
                )
                msg = r"$t = $" + f'{t:.1f}'
                ax.annotate(
                    msg, (xmin*1.2, ymax*0.8), fontsize='small',
                    bbox=dict(boxstyle="square,pad=0.3", fc='white')
                )
                ax.scatter(s1.x, s1.y, c=s1.m, cmap='viridis', s=size,
                           rasterized=True)
                ax.grid()
                ax.set_aspect('equal')
                ax.set_xlim(xmin, xmax)
                ax.set_ylim(ymin, ymax)
                fig.savefig(self.output_path(f'{prop}_t_{t:.1f}.png'),
                            dpi=dpi)
                plt.clf()
                plt.close()
            # Generate standalone colorbar.
            figc, axc = plt.subplots(figsize=(0.2, figsize[1]))
            figc.subplots_adjust(bottom=0.5)

            norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
            figc.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), cax=axc,
                          orientation='vertical', label=prop)
            figc.savefig(self.output_path(f'cbar.png'), dpi=dpi)

    def _plot_prop(self, conditions, label=None, prop='h', size=10,
                   cmap='jet_r', xmin=18, xmax=22, ymax=2, dpi=120):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 2, figsize=(12, 8))
        for case in filter_cases(self.cases, **conditions):
            files = get_files(case.input_path(), 'fpc_auto')
            data = load(files[-1])
            f = data['arrays']['fluid']
            val = f.get(prop)
            tmp = ax[0].scatter(
                f.x, f.y, c=val, rasterized=True,
                cmap=cmap, s=0.2, #edgecolor='none'
            )
            if prop == 'n_nbrs':
                label='Neighbors'
            else:
                label = prop
            fig.colorbar(tmp, ax=ax[0], shrink=0.4, label=rf'${label}$')
            cond = (np.abs(f.y) < ymax) & (f.x < xmax) & (f.x > xmin)
            tmp = ax[1].scatter(
                f.x[cond], f.y[cond], c=val[cond],
                rasterized=True, cmap=cmap,
                s=2*size*f.m[cond]/np.max(f.m[cond]),
                #edgecolor='none'
            )
            fig.colorbar(tmp, ax=ax[1], shrink=0.4, label=rf'${label}$')
        for i in range(2):
            ax[i].grid()
            ax[i].set_aspect('equal', 'box')
            circle1 = plt.Circle((20, 0), 1, color='gray', alpha=1.0)
            ax[i].add_patch(circle1)
        fig.savefig(self.output_path(f'dist_{label}'), dpi=dpi)
        plt.clf()

    def _stream_plots(self, times, plot_dx_min, plot_re):
        import matplotlib.pyplot as plt

        for case in self.cases:
            re = case.params['re']
            dx_min = case.params['dx_min']
            if (re not in plot_re or dx_min not in plot_dx_min):
                continue
            files = get_files(case.input_path(), 'fpc_auto')
            logfile = case.input_path('fpc_auto.log')
            files = get_files_at_given_times_from_log(files, times, logfile)
            fig, ax = plt.subplots(1, len(times), figsize=(16, 6))
            plt.setp(ax.flat, aspect=1.0, adjustable='box')
            ax = ax.ravel()
            for i, fname in enumerate(files):
                n = 301
                xmin, xmax = 18.75, 23
                ymin, ymax = -1.5, 1.5
                data = load(fname)
                t = data['solver_data']['t']
                print(f'Stream plots for Re={re} at t: {t}s', end='\r')
                f = data['arrays']['fluid']
                xx, yy, ui, vi, xs, ys = create_streamlines(
                    f, n, xmin, xmax, ymin, ymax
                )
                ax[i].streamplot(
                    xx, yy, ui, vi, linewidth=1.2, density=3, color='k',
                    arrowsize=0.5, arrowstyle='->',
                    integration_direction='both',
                    minlength=0.1, start_points=np.c_[xs, ys]
                )
                circle1 = plt.Circle((20, 0), 1, color='gray', alpha=1.0)
                msg = r"$t = $" + f'{t:.1f}s'
                ax[i].annotate(msg, (20, 0), fontsize='small',
                               bbox=dict(boxstyle="square,pad=0.3",
                                         fc='white'))
                ax[i].add_patch(circle1)
                ax[i].set(xlim=(xmin, xmax), ylim=(ymin, ymax))
                ax[i].set_xticks([])
                ax[i].set_yticks([])
                ax[i].set_yticklabels([])
                ax[i].set_xticklabels([])
                fig.subplots_adjust(left=None, bottom=None, right=None,
                                    top=None, wspace=0.0, hspace=0.0)

            fig.savefig(self.output_path(f'stream_{re}'))
            plt.clf()

    def _vorticity_plots(self, times, plot_dx_min, plot_re):
        import matplotlib.pyplot as plt

        for case in self.cases:
            re = case.params['re']
            dx_min = case.params['dx_min']
            if (re not in plot_re or dx_min not in plot_dx_min):
                continue
            files = get_files(case.input_path(), 'fpc_auto')
            logfile = case.input_path('fpc_auto.log')
            files = get_files_at_given_times_from_log(files, times, logfile)
            fig, ax = plt.subplots(1, len(times), figsize=(18, 6))
            plt.setp(ax.flat, aspect=1.0, adjustable='box')
            ax = ax.ravel()
            for i, file in enumerate(files):
                data = load(file)
                t = data['solver_data']['t']
                print(f'Vorticity plots for Re={re} at t: {t}s', end='\r')
                f = data['arrays']['fluid']
                xmin, xmax = 18.75, 23
                ymin, ymax = -1.5, 1.5

                cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)
                xx, yy, vor = f.x[cond], f.y[cond], f.vor[cond]
                im = ax[i].scatter(xx, yy, c=vor, cmap='seismic_r',
                                   s=40*f.m[cond]/np.max(f.m[cond]),
                                   vmin=-10, vmax=10, rasterized=True)
                circle1 = plt.Circle((20, 0), 1, color='gray', alpha=1.0)
                msg = r"$t = $" + f'{t:.1f}s'
                ax[i].annotate(msg, (19.8, -0.05), fontsize='small',
                               bbox=dict(boxstyle="square,pad=0.3",
                                         fc='white'))
                ax[i].add_patch(circle1)
                ax[i].set(xlim=(xmin, xmax), ylim=(ymin, ymax))
                ax[i].set_xticks([])
                ax[i].set_yticks([])
                ax[i].set_yticklabels([])
                ax[i].set_xticklabels([])
            cbar_ax = fig.add_axes([0.81, 0.3, 0.01, 0.4])
            fig.colorbar(im, cax=cbar_ax)
            fig.subplots_adjust(left=None, bottom=None, right=0.8,
                                top=None, wspace=0.0, hspace=0.0)
            fig.savefig(self.output_path(f'vorticity_{re}'))
            plt.clf()

    def _lift_plots(self, res):
        import matplotlib.pyplot as plt
        fig1, ax1 = plt.subplots(len(res), 1, figsize=(8, 10),
                                 sharex=True)
        for i, re in enumerate(res):
            for case in filter_cases(self.cases, re=re):
                data = case.data
                re = case.params['re']
                dx_min = case.params['dx_min']
                label = r'D/$\Delta x_{\mathrm{min}}$ =' + f' {2/dx_min:.0f}'
                ax1[i].plot(data['t'], data['cl'], label= label)
                ax1[i].set_ylabel(r'$c_{l}$')
            ax1[i].legend()
            ax1[i].set_title(rf'$Re$ = {re}')
        ax1[-1].set_xlabel(r'$T = U_{\infty}t/R$')
        fig1.savefig(self.output_path('cl_pr'))

    def _drag_plots(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:blue', 'tab:green', 'tab:red']) +
            cycler('linestyle', ['-.', '--', '-'])
        ))
        import pandas as pd
        import matplotlib.pyplot as plt

        for re in self.res:
            fig, ax = plt.subplots(1, 1, figsize=(7, 6))
            for case in filter_cases(self.cases, re=re):
                data = case.data
                re = case.params['re']
                dx_min = case.params['dx_min']
                label = r'D/$\Delta x_{\mathrm{min}}$ =' + f' {2/dx_min:.0f}'
                cr = case.params['cr']
                if cr > 1.08:
                    label += r', $Cr$ = ' + f'{cr}'
                ax.plot(data['t'], data['cd'], label=label)
                ax.set_ylabel(r'$c_{d}$, pressure drag')

            # RVM
            val = pd.read_csv(f'code/data/re_{re}.csv')
            ax.plot(val['pr_pdrag_t'][::4], val['pr_pdrag'][::4], 'b+',
                    markersize=4, label='Ramachandran (2004)', zorder=0,
                    mfc='none')
            ax.plot(val['kl_pdrag_t'], val['kl_pdrag'], 'ko', zorder=1,
                    markersize=4, label='Koumoutsakos et al. (1995)',
                    mfc='none')
            ax.set_xlim(0, self.tf)
            ax.set_xlabel(r'$T = U_{\infty}t/R$')
            ax.legend()
            fig.savefig(self.output_path(f'cd_pr_re_{re}'))
        plt.clf()

    def _skin_friction_drag_plots(self, plot_dx_min):
        import pandas as pd
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(7, 6))
        for re in self.res:
            for case in filter_cases(self.cases, re=re, dx_min=plot_dx_min):
                data = case.data
                ax.semilogy(data['t'], data['cd_sf'], '-', zorder=1)
        labels = []
        for re in self.res:
            label = r'$Re = $' + f' {re}'
            labels.append(label)

        for re in self.res:
            # RVM
            val = pd.read_csv(f'code/data/re_{re}.csv')
            ax.semilogy(val['pr_fdrag_t'][::4], val['pr_fdrag'][::4], 'b+',
                        markersize=4, zorder=0, mfc='none')
            ax.semilogy(val['kl_fdrag_t'], val['kl_fdrag'], 'ko',
                        markersize=4, zorder=1, mfc='none')

        labels.extend(['Ramachandran (2004)',
                       'Koumoutsakos et al. (1995)'])
        ax.legend(labels)
        ax.set_xlim(0, self.tf)
        ax.set_xlabel(r'$T = U_{\infty}t/R$')
        ax.set_ylabel(r'$c_{d}$, skin friction')
        fig.savefig(self.output_path('cd_sf_all_re'))
        plt.clf()

    def _centerline_velocity(self, dx_min, times, re):
        import pandas as pd
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(7, 6))
        val = pd.read_csv(f'code/data/re_{re}_rear.csv')
        for case in filter_cases(self.cases, re=re, dx_min=dx_min):
            data = np.load(case.input_path('u_rear.npz'))
            re = case.params['re']
            dx_min = case.params['dx_min']
            label = r'D/$\Delta x_{\mathrm{min}}$ =' + f' {2/dx_min:.0f}'
            cr = case.params['cr']
            if cr > 1.08:
                label += r', $Cr$ = ' + f'{cr}'
            for t in times:
                if t in data['t']:
                    idx = list(data['t']).index(t)
                    ax.plot(data['xpos'][idx], data['urear'][idx], 'r-')
                    ax.plot(
                        val[f'x_{t}'], val[f'u_{t}'], 'b+',
                        markersize=4, zorder=0, mfc='none'
                    )
                    ax.plot(
                        val[f'x_{t}_s'], val[f'u_{t}_s'], 'ko', zorder=1,
                        markersize=4, mfc='none'
                    )
                    ax.set_xlim(np.min(val[f'x_{t}_s'])*0.9,
                                np.max(val[f'x_{t}_s'])*1.1)
                    if re == 3000:
                        xy = (1.6, 0.8)
                        xytext = (2.5, -0.5)
                    elif re == 9500:
                        xy = (1.25, 0.6)
                        xytext = (2, 0.2)
                    ax.annotate(f"T = {list(data['t'])}",
                                xy=xy, xytext=xytext,
                                arrowprops=dict(arrowstyle="<-"))
        ax.set_xlabel(r'$x/R$, distance from the center')
        ax.set_ylabel(r'$u_{r}$, radial velocity')
        ax.legend([label, 'Ramachandran (2004)', 'Shankar, S. (1996)'])
        fig.savefig(self.output_path(f'centerline_re_{re}'))
        plt.clf()
        plt.close()

    def plot_comparison(self):
        import matplotlib.pyplot as plt
        data = load('fpc_auto_23480.npz')
        f = data['arrays']['fluid']
        cond = (f.x > 18.89) & (f.x < 23) & (f.y < 1.5) & (f.y >= 0)
        jet_trunc = truncate_colormap(plt.cm.get_cmap('jet_r', 41), 0.1, 0.9)
        plt.scatter(f.x[cond], f.y[cond], s=1*f.m[cond]/np.max(f.m[cond]),
                    c=2*f.vor[cond], vmin=-10, vmax=10, cmap=jet_trunc)
        plt.axis('equal')
        plt.grid()
        plt.ylim(-1.5, 1.5)
        plt.savefig('asph_6_4', dpi=200)

        a = np.array([[-10, 10]])
        plt.figure(figsize=(0.5, 10))
        img = plt.imshow(a, cmap=jet_trunc)
        plt.gca().set_visible(False)
        cax = plt.axes([0.1, 0.2, 0.8, 0.6])
        cbar = plt.colorbar(orientation="vertical", cax=cax,
                            label=r'$\omega D/U_{\infty}$')
        cbar.ax.tick_params(labelsize=20)
        cbar.ax.set_ylabel(r'$\omega D/U_{\infty}$', fontsize=25)
        plt.savefig('dr_cbar', dpi=100)


class ComparisonFPC(FPC):
    '''
    Cases:
    1. Compare with Yang and Kong for different Reynolds numbers.
    '''
    def get_name(self):
        return 'fpc_comparison'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_auto.py' + backend
        self.output_times = list(range(1, 7))

        Res = [1000, 3000]
        self.tf = tf = 6.0
        self.case_info = {
            f'yk_re_{re}': dict(
                re=re, tf=tf, pf=50, dx_min=0.0125, cr=1.08,
                no_vacondio=None, update_h='yangkong'
            )
            for re in Res
        }
        self.case_info = {
            f'optimized_re_{re}': dict(
                re=re, tf=tf, pf=50, dx_min=0.0125, cr=1.08
            )
            for re in Res
        }

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()


class FPCNoAdapt(FPC):
    '''
    Cases:
    1. Comparison with no adaptive.
    '''
    def get_name(self):
        return 'fpc_no_adapt'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_auto.py' + backend
        self.output_times = list(range(1, 7))

        self.res = [1000, 3000]
        self.tf = tf = 6.0
        self.case_info = {
            'fpc_re_1000_no_adapt': dict(
                re=1000, tf=tf, pf=100, dx_min=0.04, dx_max=0.04
            ),
            'fpc_re_1000_adapt_cr_1.08_sol_adapt_dx_min_0.04': dict(
                re=1000, tf=tf, pf=100, dx_min=0.04, dx_max=0.5, cr=1.08,
                solution_adapt=0.05
            ),
            'fpc_re_1000_adapt_cr_1.08_sol_adapt': dict(
                re=1000, tf=tf, pf=100, dx_min=0.02, dx_max=0.5, cr=1.08,
                solution_adapt=0.05
            ),
            'fpc_re_3000_no_adapt': dict(
                re=3000, tf=tf, pf=100, dx_min=0.04, dx_max=0.04
            ),
            'fpc_re_3000_adapt_cr_1.08_sol_adapt_dx_min_0.04': dict(
                re=3000, tf=tf, pf=100, dx_min=0.04, dx_max=0.5, cr=1.08,
                solution_adapt=0.05
            ),
            'fpc_re_3000_adapt_cr_1.08_sol_adapt_dx_min_0.01': dict(
                re=3000, tf=tf, pf=100, dx_min=0.01, dx_max=0.5, cr=1.08,
                solution_adapt=0.05
            ),
            'fpc_re_3000_adapt_cr_1.08_sol_adapt': dict(
                re=3000, tf=tf, pf=100, dx_min=0.02, dx_max=0.5, cr=1.08,
                solution_adapt=0.05
            ),
        }

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._drag_plots()

    def _drag_plots(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:blue', 'tab:orange', 'tab:red', 'tab:pink']) +
            cycler('linestyle', ['--', '-', '-', '-'])
        ))
        import pandas as pd
        import matplotlib.pyplot as plt

        for re in self.res:
            fig, ax = plt.subplots(1, 1, figsize=(7, 6))
            for case in filter_cases(self.cases, re=re):
                data = case.data
                re = case.params['re']
                dx_min = case.params['dx_min']
                dx_max = case.params['dx_max']
                if abs(dx_min - dx_max) < 1e-9:
                    label = r'No adaptive, D/$\Delta x$ =' + f' {2/dx_min:.0f}'
                else:
                    cr = case.params['cr']
                    label = r'Adaptive, D/$\Delta x_{\min}$ ='
                    label += f' {2/dx_min:.0f},' + r' $Cr$ = ' + f'{cr}'
                ax.plot(data['t'], data['cd'], label=label)
                ax.set_ylabel(r'$c_{d}$, pressure drag')

            # RVM
            val = pd.read_csv(f'code/data/re_{re}.csv')
            ax.plot(val['pr_pdrag_t'][::2], val['pr_pdrag'][::2], 'b+',
                     markersize=4, label='Ramachandran (2004)', zorder=0,
                     mfc='none')
            ax.plot(val['kl_pdrag_t'], val['kl_pdrag'], 'ko', zorder=1,
                    markersize=4, label='Koumoutsakos et al. (1995)',
                    mfc='none')

            ax.set_xlim(0, self.tf)
            ax.set_xlabel(r'$T = U_{\infty}t/R$')
            ax.legend()
            fig.savefig(self.output_path(f'cd_pr_re_{re}'))
        plt.clf()


class FPCSolutionAdapt(FPC):
    def get_name(self):
        return 'fpc_solution_adapt'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_auto.py' + backend
        self.output_times = list(range(1, 7))

        self.res = [3000]
        self.tf = tf = 6.0

        dx_min = [0.02, 0.01, 0.004]
        self.case_info = {
            f'fpc_re_3000_dx_min_{dx}_sol': dict(
                re=3000, tf=tf, pf=100, dx_min=dx, cr=1.08, solution=0.04
            )
            for dx in dx_min
        }

        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._drag_plots()

    def _plot_prop(self, conditions, label=None, prop='h',
                   cmap='jet_r', xmin=18, xmax=22, ymax=2):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(16, 6))
        for case in filter_cases(self.cases, **conditions):
            files = get_files(case.input_path(), 'fpc_auto')
            data = load(files[-1])
            f = data['arrays']['fluid']
            val = f.get(prop)
            if prop == 'n_nbrs':
                label='Neighbors'
            else:
                label = prop
            cond = (np.abs(f.y) < ymax) & (f.x < xmax) & (f.x > xmin)
            tmp = ax.scatter(
                f.x[cond], f.y[cond], c=val[cond],
                s=2, rasterized=True, cmap=cmap, edgecolor='none'
            )
            fig.colorbar(tmp, ax=ax, shrink=0.6, label=rf'${label}$')
            ax.grid()
            circle1 = plt.Circle((20, 0), 1, color='gray', alpha=1.0)
            ax.add_patch(circle1)
            ax.set_aspect('equal', 'box')
        fig.savefig(self.output_path(f'dist_{label}'))
        plt.clf()

    def _bg_sol_adapt_plots(self, times, plot_dx_min, plot_re, xmin=18.75,
                            xmax=23, ymin=-1.5, ymax=1.5):
        import matplotlib.pyplot as plt
        for case in self.cases:
            re = case.params['re']
            dx_min = case.params['dx_min']
            if (re not in plot_re or dx_min not in plot_dx_min):
                continue
            files = get_files(case.input_path(), 'fpc_auto')
            logfile = case.input_path('fpc_auto.log')
            files = get_files_at_given_times_from_log(files, times, logfile)
            fig, ax = plt.subplots(1, len(times), figsize=(16, 6))
            if len(times) > 1:
                ax = ax.ravel()
                plt.setp(ax, aspect=1.0, adjustable='box')
            else:
                plt.setp(ax, aspect=1.0, adjustable='box')
                ax = [ax]
            for i, file in enumerate(files):
                data = load(file)
                t = data['solver_data']['t']
                print(f'Solution adaptivity plots for Re={re} at t: {t}s',
                      end='\r')
                bg = data['arrays']['bg']

                cond = ((bg.x < xmax) & (bg.x > xmin) &
                        (bg.y < ymax) & (bg.y > ymin))
                x, y, fixed = bg.x[cond], bg.y[cond], bg.fixed[cond]
                ax[i].scatter(x, y, c=fixed, cmap='viridis', s=0.5)
                circle1 = plt.Circle((20, 0), 1, color='gray', alpha=1.0)
                ax[i].add_patch(circle1)
                ax[i].set(xlim=(xmin, xmax), ylim=(ymin, ymax))
                ax[i].set_xticks([])
                ax[i].set_yticks([])
                ax[i].set_yticklabels([])
                ax[i].set_xticklabels([])
            fig.subplots_adjust(left=None, bottom=None, right=0.8,
                                top=None, wspace=0.0, hspace=0.0)
            fig.savefig(self.output_path(f'bg_fixed_{re}'))
            plt.clf()


class FPCLongRun(FPCSolutionAdapt):
    def get_name(self):
        return 'fpc_long_run'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_auto.py' + backend
        self.output_times = list(range(1, 7))

        self.res = [3000]
        self.tf = tf = 30

        self.case_info = {
            'fpc_re_1000_dx_min_0.00625_sol': dict(
                re=1000, tf=tf, pf=400, dx_min=0.00625, cr=1.2, solution=0.1,
            ),
            'fpc_re_3000_dx_min_0.008_sol': dict(
                re=3000, tf=tf, pf=400, dx_min=0.008, cr=1.12246,
                solution=0.05,
            ),
        }

        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._drag_plots()

        import matplotlib.pyplot as plt
        cmap = plt.cm.get_cmap('jet_r', 20)
        self._plot_prop(dict(re=3000, dx_min=0.008), prop='h',
                        xmin=18, xmax=35, cmap=cmap, ymax=5)
        self._plot_vor(dict(re=3000, dx_min=0.008),
                        xmin=18, xmax=35, cmap='seismic')
        times = [30]
        plot_dx_min = [0.008]
        plot_re = [3000]

    def _drag_plots(self):
        import matplotlib.pyplot as plt

        for re in self.res:
            fig, ax = plt.subplots(1, 1)
            for case in filter_cases(self.cases, re=re):
                data = case.data
                re = case.params['re']
                ax.plot(data['t'], data['cd'], 'm-', label=r'$c_d$')
                ax.plot(data['t'], data['cl'], 'k-', label=r'$c_l$')
            ax.set_xlim(0, self.tf)
            ax.set_xlabel(r'$T = U_{\infty}t/R$')
            ax.legend()
            fig.savefig(self.output_path(f'cd_pr_re_{re}'))
        plt.clf()

    def _plot_vor(self, conditions,
                   cmap='jet_r', xmin=18, xmax=22):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(16, 6))
        for case in filter_cases(self.cases, **conditions):
            files = get_files(case.input_path(), 'fpc_auto')
            data = load(files[-1])
            f = data['arrays']['fluid']
            val = f.get('vor')
            cond = (np.abs(f.y) < 5) & (f.x < xmax) & (f.x > xmin)
            tmp = ax.scatter(
                f.x[cond], f.y[cond], c=val[cond],
                s=0.5, rasterized=True, cmap=cmap,
                vmin=-10, vmax=10, edgecolor='none'
            )
            fig.colorbar(tmp, ax=ax, shrink=0.6, label='vorticity')
            circle1 = plt.Circle((20, 0), 1, color='gray', alpha=1.0)
            ax.add_patch(circle1)
            ax.grid()
            ax.set_aspect('equal', 'box')
        fig.savefig(self.output_path('dist_vor'))
        plt.clf()


class CShape(FPCLongRun):
    def get_name(self):
        return 'cshape'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/c_shape.py' + backend
        self.output_times = list(range(1, 7))

        self.tf = 30
        self.case_info = {
            'cshape_sol_adapt': dict(
                pf=200, tf=20, solution_adapt=0.05, dx_min=0.008
            ),
            'cshape': dict(pf=200, tf=self.tf),
        }
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._drag_plots()

        import matplotlib.pyplot as plt
        # cmap = plt.cm.get_cmap('RdYlBu_r', 21)
        cmap = plt.cm.get_cmap('jet', 21)
        self._plot_prop(
            dict(solution_adapt=0.05), prop='h', xmin=3.45, xmax=10,
            cmap=cmap, label='h_sa', ymin=-2.5, ymax=2, figsize=(8, 6),
            dpi=150
        )
        self._plot_prop(
            dict(solution_adapt=0.05), prop='vor', xmin=3.45, xmax=16.2,
            cmap='seismic', ymin=-1.5, ymax=1, label='vor_sa', dpi=150
        )
        self._plot_prop(
            dict(tf=30), prop='vor', ymin=-1.5, ymax=1, xmin=3.45, xmax=16.2,
            cmap='seismic', label='vor_no_sa', dpi=150
        )
        self._plot_prop(
            dict(solution_adapt=0.05), prop='vor', xmin=3.45, xmax=6,
            cmap='seismic', label='zoom', figsize=(8, 6), ymin=-1.1, ymax=0.6,
            size=1, dpi=120
        )

    def _plot_prop(self, conditions, label=None, prop='h', cmap='jet_r',
                   xmin=18, xmax=22, ymin=-2, ymax=2, figsize=(16, 6),
                   size=2, dpi=200):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=figsize)
        for case in filter_cases(self.cases, **conditions):
            files = get_files(case.input_path(), 'c_shape')
            data = load(files[-1])
            t = data['solver_data']['t']
            f = data['arrays']['fluid']
            s = data['arrays']['solid']
            val = f.get(prop)
            cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

            if prop=='vor':
                vmin, vmax = -10, 10
            else:
                vmin, vmax = np.min(val), np.max(val)
            tmp = ax.scatter(
                f.x[cond], f.y[cond], c=val[cond],
                s=size, rasterized=True, cmap=cmap,
                edgecolor='none', vmin=vmin, vmax=vmax
            )
            msg = r"$T = $" + f'{t:.1f}'
            ax.annotate(
                msg, (xmin*1.2, ymax*0.8), fontsize='small',
                bbox=dict(boxstyle="square,pad=0.3", fc='white')
            )
            ax.scatter(s.x, s.y, c=s.m, cmap='viridis', s=size, rasterized=True)
            fig.colorbar(tmp, ax=ax, shrink=0.4, label=rf'${prop}$', pad=0.01)
            ax.grid()
            ax.set_aspect('equal', 'box')
        fig.savefig(self.output_path(f'dist_{label}.png'), dpi=dpi)
        plt.clf()

    def _drag_plots(self):
        import pandas as pd
        import matplotlib.pyplot as plt

        for case in self.cases:
            fig, ax = plt.subplots(1, 1, figsize=(5.5, 5))
            fig1, ax1 = plt.subplots(1, 1, figsize=(5.5, 5))
            data = case.data
            label = 'Adaptive-SPH'
            if 'solution_adapt' in case.params:
                label += ', SA'
            ax.plot(data['t'], data['cd'] + data['cd_sf'], 'r-', label=label)
            ax.set_ylabel(r'$c_{d}$, total drag')

            val = pd.read_csv('code/data/c_shape.csv')
            ax.plot(val['dvh_cd_t'], val['dvh_cd'], 'ko', markersize=4,
                    label='DVH', zorder=0, mfc='none')
            ax.plot(val['sun_cd_t'], val['sun_cd'], 'b+', markersize=4,
                    label='Sun et al (2018)', zorder=0, mfc='none')
            ax.set_xlim(0, case.params['tf'])
            ax.set_ylim(0, 1.55)
            ax.set_xlabel(r'$T = U_{\infty}t/R$')
            ax.legend()
            fig.savefig(self.output_path(f'cd_{case.name}'))

            ax1.plot(data['t'], data['cl'] + data['cl_sf'], 'r-', label=label)
            ax1.set_ylabel(r'$c_{l}$, total lift')

            val = pd.read_csv('code/data/c_shape.csv')
            ax1.plot(val['dvh_cl_t'], val['dvh_cl'], 'ko', markersize=4,
                    label='DVH', zorder=1, mfc='none')
            ax1.plot(val['sun_cl_t'], val['sun_cl'], 'b+', markersize=4,
                    label='Sun et al (2018)', zorder=0, mfc='none')
            ax1.set_xlim(0, case.params['tf'])
            ax1.set_ylim(-1.5, 3)
            ax1.set_xlabel(r'$T = U_{\infty}t/R$')
            ax1.legend()
            fig1.savefig(self.output_path(f'cl_{case.name}'))
            plt.clf()


if __name__ == '__main__':
    PROBLEMS = [
        FPC,
        FPCSolutionAdapt,
        FPCNoAdapt,
        Cavity,
        GreshoChan,
        TaylorGreenChiron,
        CShape,
    ]
    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=PROBLEMS,
        cluster_manager_factory=CondaClusterManager
    )
    automator.run()
