\section{Adaptive refinement}
\label{sec:adapt}

The algorithm is based on the work of Yang and Kong but there are several
differences. Their algorithm is discussed only in the context of
two-dimensional flow. We suggest some changes that allow the algorithm to work
in three-dimensions as well.

The basic algorithm is as follows
\begin{itemize}
\item Find all split and merge candidates.
\item Merge all particles that need to be merged.
\item Count the number of particles to be added or removed. Add/remove enough
  particles.
\item Assign the indices of the particles to split into.
\item Actually split the particles.
\end{itemize}

When splitting particles, Yang and Kong find the closest particle and split
along the direction of the perpendicular bisector. This is complex to do in
three dimensions and also slightly ambiguous. We propose a simpler approach.

\begin{itemize}
\item Find the closest particle to the one we wish to split. Let its position
  be $\vec{r}_c$, let $\vec{r}_s$ be the position vector of the particle we
  are splitting.
\item Compute $\vec{r}_s - \vec{r}_c$ and split the particle along the
  coordinate axis whose magnitude is least.
\end{itemize}

This approach is easy to implement and works in two and three dimensions.

During the initial steps, it is highly likely that users start with a coarse
particle distribution that is automatically refined. This can go wrong near
boundaries.  Since the splitting boundaries are typically in bands, this is
not likely to happen after the particles are initialized correctly and the
simulation proceeds.

In order to solve the problem we simply identify all the particles which have
wall neighbors that are closer than the splitting radius $ds/2$ and do not
split them.

Specification of the $m_{ref}$ is a bit tricky for the fixed refinement region
problems.

\begin{itemize}
\item Store $\Delta s_{min}$ as a constant as well as $\Delta s_{max}$.
\item Add a tag for marking that this particle is next to a boundary.
\item For all particles next to the boundary set $\Delta s = \Delta s_{min}$.
\item For all other particles, find their neighbors, find the minimum $\Delta
  s$, then set $\Delta s = C_r \Delta s_{min}$, where $C_r$ is the ratio of
  increase in spacing.

\end{itemize}

What we do is first create a fixed Eulerian mesh of background points that are
spaced at $\Delta s_{max}$. These background points are fixed in space and we
first find the appropriate $\Delta s$ as above on these and then refine them
adaptively using the resulting $m_{ref}$.

Then as the fluid particles move through space, we use the background fixed
mesh to set their reference mass. The background particles spacing is updated
every 20 time steps in the case of ISPH or 200 steps in the case of EDAC.
