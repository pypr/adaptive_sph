\section{The SPH method}%
\label{sec:sph}

In this paper we deal specifically with weakly-compressible flows.  We use the
entropically damped artificial compressibility (EDAC)
method~\cite{edac-sph:cf:2019} to simulate the weakly-compressible flows. The
position update, pressure evolution, and momentum equations in the EDAC
formulation are,
%
\begin{equation}
  \label{eq:pos-ode}
  \frac{\mathrm{d} \ten{r}}{\mathrm{d} t} = \ten{u},
\end{equation}
%
\begin{equation}
  \label{eq:edac}
  \frac{\mathrm{d} p}{\mathrm{d} t} = -\rho c_s^2 \text{div}(\ten{u})
  + \nu_{\text{e}} \nabla^2 p,
\end{equation}
%
\begin{equation}
  \label{eq:mom}
  \frac{\text{d} \ten{u}}{\text{d}t} =
  -\frac{1}{\rho} \nabla p
  + \nu \nabla^2 \ten{u} + \ten{f},
\end{equation}
where $\ten{r}$, $\ten{u}$, $p$, and $t$ denotes the position, velocity,
pressure, and time respectively. $\rho$ is the density, $\nu$ is the kinematic
viscosity of the fluid, $c_s$ is the artificial speed of sound, $\ten{f}$ is
the external body force, and $\nu_{\text{e}}$ is the EDAC viscosity parameter.

In order to further enhance the uniformity of the particles we use the transport
velocity formulation~\cite{Adami2013}, with the corrections
incorporated~\cite{adepu2021}. Then the above equations are re-formulated as,
%
\begin{equation}
  \label{eq:pos-ode:tvf}
  \frac{\mathrm{d} \ten{r}}{\mathrm{d} t} = \tilde{\ten{u}}
\end{equation}
%
\begin{equation}
  \label{eq:edac-corr}
  \frac{\tilde{\mathrm{d}}p}{\mathrm{d}t} =
  -\rho c_s^2 \text{div}(\ten{u})
  + \nu_{\text{e}} \nabla^2 p
  + (\tilde{\ten{u}}
  - \ten{u}) \cdot \nabla p
\end{equation}
%
\begin{equation}
  \label{eq:mom-corr}
  \frac{\tilde{\text{d}} \ten{u}}{\text{d}t} =
  -\frac{1}{\rho} \nabla p
  + \nu \nabla^2 \ten{u} + \ten{f}
  + \frac{1}{\rho} \nabla \cdot \rho (\ten{u} \otimes (\tilde{\ten{u}} - \ten{u}))
  + \ten{u}\,\mathrm{div}(\tilde{\ten{u}})
\end{equation}
%
where $\tilde{\ten{u}}$ refers to the transport velocity, and
$\frac{\tilde{\mathrm{d}} (.)}{\mathrm{d} t} = \frac{\partial (.)}{\partial t} +
\tilde{\ten{u}} \cdot \text{grad} (.)$ is the material time derivative of a
particle advecting with the transport velocity $\tilde{\ten{u}}$. The
computation of the transport velocity is shown in \cref{sec:shift}.
%
\begin{remark}
  In our numerical experiments with the Taylor-Green problem we found that the
  addition of the divergence correction terms in the pressure evolution
  equation is crucial for accuracy. However, we find that the use of the last
  two terms in the momentum equation~\eqref{eq:mom-corr} introduces noise
  where the particles are merged or split. Consequently, we do not use them in
  this work.  We note that \citet{sun_consistent_2019} observes that the
  effect of these terms in the momentum equation is minor.
\end{remark}

We discretize the governing equations using variable-$h$ SPH.~The domain is
discretized into points whose spatial location is denoted by $\ten{r}_i$, where
the subscript $i$ denotes the index of an arbitrary particle. The mass of the
particle, which vary as a function of space, is denoted by $m_i$, and its
smoothing length by $h_i$. In the variable-$h$ SPH the density is approximated
by the summation density equation using a~\emph{gather
  formulation}~\cite{hernquist1989, vacondio_accurate_2012} written as,
\begin{equation}
  \label{eq:sd-gather}
  \rho(\ten{r}_i) = \sum_j m_j W(|\ten{r}_i - \ten{r}_j|, h_i),
\end{equation}
%
where, $W(|\ten{r}_i - \ten{r}_j|, h_i)$ is the kernel function. We use the
quintic spline kernel in all our simulations, the quintic spline kernel is given
by,
\begin{equation}
  \label{eq:kernel}
  W(q) =
  \begin{cases}
    \sigma_2 [{(3 - q)}^5 - 6{(2 - q)}^5 + 15{(1 - q)}^5] \quad%
    &\text{if}\ 0 \le{} q < 1, \\
     \sigma_2 [{(3 - q)}^5 - 6{(2 - q)}^5] \quad
    &\text{if}\ 1 \le{} q < 2, \\
     \sigma_2 {(3 - q)}^5 \quad &\text{if}\ 2 \le{} q < 3, \\
     0 \quad &\text{if}\ q \ge{} 3, \\
  \end{cases}
\end{equation}
where $\sigma_2 = 7/(478 \pi {h(\ten{r})}^2)$, and $q = |\ten{r}|/h$.

The EDAC pressure evolution equation in variable-$h$ SPH
(see~\cite{monaghan-review:2005,vacondio_accurate_2012}, for a derivation of the
terms in the R.H.S) is given by,
\begin{equation}
  \label{eq:edac:sph}
\begin{split}
  \frac{\tilde{\mathrm{d}}p}{\mathrm{d} t}(\ten{r}_i) =
  \quad&
     \frac{\rho_0 c_s^2}{\beta_i}\sum_j
     \frac{m_j}{\rho_j} \ten{u}_{ij} \cdot \nabla W(r_{ij}, h_i) \\
  +&
     \frac{1}{\beta_i}\sum_j \frac{m_j}{\rho_j} \nu_{\text{e}, ij}  (p_i - p_j)
     (
       \ten{r}_{ij} \cdot \nabla W(r_{ij}, h_{ij})
     ) \\
  +&
     \sum_j m_j
     [(\tilde{\ten{u}}_{i} - \ten{u}_{i})
     \cdot (P_i \nabla W(r_{ij}, h_i) + P_j \nabla W(r_{ij}, h_j))],
   \end{split}
 \end{equation}
 %
 where $\rho_0$ is the reference density, $p_i$ is the pressure of particle $i$,
 $\rho_j$ is the density of the $j^{\text{th}}$ particle computed using
 summation density~\cref{eq:sd-gather},
 $\ten{u}_{ij} = (\ten{u}_i - \ten{u}_j)$,
 $r_{ij} =|\ten{r}_{ij}| = |\ten{r}_i - \ten{r}_j|$, $\beta_i$ is the
 variable-$h$ correction term~\cite{vacondio_accurate_2012}, which in $d$
 dimensions is given by,
\begin{equation}
  \label{eq:beta}
  \beta_i = - \frac{1}{\rho_i d} \sum_j m_j r_{ij}
  \frac{\mathrm{d} W(r_{ij}, h_i)}{\mathrm{d}r_{ij}},
\end{equation}
%
$P_i$ and $P_j$ are given by,
\begin{equation}
  \label{eq:mom:pre}
  P_i = \frac{(p_i - p_{\text{avg}, i})}{\rho_i^2 \beta_i}, \quad
  P_j = \frac{(p_j - p_{\text{avg}, j})}{\rho_j^2 \beta_j},
\end{equation}
here we employ the pressure reduction technique proposed
by~\citet{sph:basa-etal-2009}, where, the average pressure is computed as,
\begin{equation}
  \label{eq:pavg}
  p_{\text{avg}, i} = \frac{\sum_{j = 1}^{N_i} p_j}{N_i},
\end{equation}
where $N_i$ is the number of neighbours for a particle with index $i$, and
\begin{equation}
  \label{eq:dw-avg}
  \nabla W(r_{ij}, h_{ij}) =
  \left(\frac{\nabla W(r_{ij}, h_i) + \nabla W(r_{ij}, h_j)}{2}\right).
\end{equation}

The EDAC viscosity of the pressure diffusion term in the EDAC equation with the
SPH discretization is given by,
\begin{equation}
  \label{eq:edac-alpha}
  \nu_{\text{e}, i} = \frac{\alpha_{\text{e}} c_s h_i}{8},
\end{equation}
where $\alpha_{\text{e}} = 1.5$ is used in all our simulations. Since this is a
function of the smoothing length, which is varying in space, we use the approach
of~\citet{cleary1999} to model the pressure diffusion term where,
%
 \begin{equation}
   \label{eq:nu-cleary}
   \nu_{\text{e}, ij} = 4\frac{\nu_{\text{e}, i} \nu_{\text{e}, j}}
   {(\nu_{\text{e}, i} + \nu_{\text{e}, j})}.
 \end{equation}

The momentum equation in the variable-$h$ SPH discretization is given by,
%
\begin{equation}
  \label{eq:mom-sph}
  \begin{split}
  \frac{\tilde{\mathrm{d}}\ten{u}}{\mathrm{d} t}(\ten{r}_i, t) =
  -&\sum_j m_j
    \left((P_i + A_i) \nabla W(r_{ij}, h_i)
    + (P_j + A_j) \nabla W(r_{ij}, h_j)\right) \\
    +& \frac{1}{\beta_i}\sum_j m_j \frac{4 \nu}{(\rho_i + \rho_j)}
    \frac{\ten{r}_{ij} \cdot \nabla W(r_{ij}, h_{ij})}
    {(|\ten{r}_{ij}|^{2} + \eta)} \ten{u}_{ij} \\
    -&
    \frac{1}{\beta_i}\sum_{j} \frac{m_j}{\rho_j}
  [(\tilde{\ten{u}}_{ij} - \ten{u}_{ij}) \cdot \nabla W(r_{ij}, h_i)] \ten{u}_i
  \end{split}
\end{equation}
%
where,
%
\begin{equation}
  \label{eq:astress}
{A}_{i} = \frac{1}{\rho_{i} \beta_i} \ten{u}_{i} \otimes(\tilde{\ten{u}}_{i} -
\ten{u}_{i}),
\quad
{A}_{j} = \frac{1}{\rho_{j} \beta_j} \ten{u}_{j} \otimes(\tilde{\ten{u}}_{j} - \ten{u}_{j}),
\end{equation}
and $\eta = 0.001 h_i^2$ is a small number added to ensure a non-zero
denominator in case when $i = j$.

\begin{remark}
  We do not employ any artificial viscosity in our benchmark cases. We note that
  the proposed scheme is not conservative due to shifting, the adaptive-h
  correction terms, and the non-standard form of the pressure gradient.
\end{remark}

\subsection{Particle shifting}%
\label{sec:shift}

We use a limited form of the particle shifting technique
of~\citet{diff_smoothing_sph:lind:jcp:2009} which is based on evaluating the
gradient of the kernel function. A particle with an index $i$ at a current
position $\ten{r}_i$ is shifted to a new position $\ten{r}'_i$ as,
%
\begin{equation}%
  \label{eq:shift}
  \ten{r}'_{i} = \ten{r}_{i} + \theta \delta \ten{r}_{i},
\end{equation}
%
where,
%
\begin{equation}%
  \label{eq:shift-deltar}
  \delta \ten{r}_{i} = - \frac{h^2_i}{2} \sum_{j}
  \frac{m_j}{\rho_0} \left (
    1 + 0.24 {\left(
        \frac{W(r_{ij}, h_{ij})}{W(\Delta x, \xi h_{ij})}
      \right)}^{4}
  \right)
  \nabla W_{ij},
\end{equation}
where $\xi$ is the point of inflection of the kernel~\cite{crespo2008}, and
$h_{ij} = (h_i + h_j)/2$. For quintic spline the point of inflection is
$ \xi = 0.759298480738450$. We found that using $\rho_j$ in the volume
approximation makes the shifting less effective and hence have used $\rho_0$.  We
limit the shifting by restricting the movement of particle which is shifted by
more than 25\% of its smoothing length:
\begin{equation}
  \label{eq:shift-limit}
  \theta =
  \begin{cases}
     \frac{0.25 h_i}{|\delta \ten{r}_{i}|}\quad &\text{if}\ |\delta \ten{r}_{i}|
     > 0.25 h_i, \\
     1 \quad &\text{otherwise}.
  \end{cases}
\end{equation}

We employ shifting while solving the fluid equations and also after our adaptive
refinement procedure. Since we use the transport velocity scheme which already
accounts for the shifting no additional correction is necessary. However, after
the adaptive refinement procedure and subsequent shifting we correct the fluid
properties by using a Taylor series approximation. Consider a fluid property
$\varphi_i$ the corrected value $\varphi'_i$ is obtained by,
\begin{equation}%
  \label{eq:shift-correct}
  \varphi'_{i} = \varphi_i + {(\nabla \varphi)}_i\cdot \delta \ten{r}_{i}.
\end{equation}
The transport velocity is computed using the shifting as,
\begin{equation}
  \label{eq:shift-tvf}
  \tilde{\ten{u}}_i = \ten{u}_i + \frac{\delta \ten{r}_i}{\Delta t}.
\end{equation}
\subsection{Boundary conditions}%
\label{sec:bc}

We employ periodic, no-slip, free-slip, no-penetration and the inlet-outlet
boundary conditions in our test cases. We enforce periodic boundary conditions
by the use of ghost particles onto which the properties are directly copied from
the particles exiting the domain through a periodic boundary.

For the no-slip, free-slip and no-penetration boundary conditions we use the
dummy particle technique of~\citet{Adami2012}. Dummy particles placed in uniform
layers are used to discretize the wall. The no-penetration is implicitly
enforced by using the wall velocity in the EDAC
equation~\cite{Adami2012}. For the no-slip or free-slip we extrapolate the
values of velocity of the fluid onto the dummy wall particles by,
%
\begin{equation}
  \label{eq:bc-wall}
  u_w = 2\ten{u}_i - \hat{\ten{u}}_i,
\end{equation}
%
where the subscript $w$ denotes the dummy wall particles, $\ten{u}_i$ is the
prescribed wall velocity, and
\begin{equation}
  \label{eq:shepard}
  \hat{\ten{u}}_i = \frac{\sum_j \ten{u}_j W(r_{ij}, h_{ij})}
  {\sum_j W(r_{ij}, h_{ij})}
\end{equation}
is the Shepard extrapolated velocity of the fluid particles indexed by $j$ onto
the dummy wall particles $i$. The pressure on the wall is calculated from the
fluid, to accurately impose the pressure gradient, by,
%
\begin{equation}
  \label{eq:bc-pre}
  p_w = \frac{\sum_f p_f W(r_{wf}, h_{wf}) +
    (\ten{g} - \ten{a}_w)\cdot
    \sum_f \rho_f \ten{r}_{wf} W(r_{wf}, h_{wf})}
  {\sum_f W(r_{wf}, h_{wf})},
\end{equation}
%
where the subscript $f$ denotes the fluid particles, $\ten{a}_w$ is the
acceleration of the wall, $r_{wf} = |\ten{r}_w - \ten{r}_f|$, and
$h_{wf} = (h_w + h_f)/2$.

For the inlet and outlet we use the non-reflecting boundary condition of
\citet{lastiwka2009}. First we compute the characteristic properties, referred
to as $J_1, J_2,$ and $J_3$ in aforementioned article, of the fluid. Then, we
extrapolate the characteristic variables of the fluid onto the inlet and outlet
particles using Shepard interpolation. Finally we determine the fluid dynamical
properties from the characteristic variables.

\subsection{Force computation}%
\label{sec:force-comp}

We compute the forces on the circular cylinder in the flow past a circular
cylinder simulation and evaluate the coefficients of lift and
drag. Specifically, we compute the forces due to the pressure and the
skin-friction on the cylinder by evaluating,
\begin{equation}
  \label{eq:force}
  \ten{f}^{\text{solid}} = m^{\text{solid}}\left(-\frac{1}{\rho}\nabla p
  + \nu \nabla \cdot \nabla \ten{u}\right),
\end{equation}
which in the variable-$h$ SPH discretization is written as,
\begin{equation}
  \label{eq:force-sph}
  \begin{split}
  \ten{f}^{\text{solid}}_{i} =
  \ &\underbrace{-m^{\text{solid}}_{i} \sum_j m_j
  \left(P_i \nabla W(r_{ij}, h_i)
    + P_j \nabla W(r_{ij}, h_j)\right)}_{\ten{f}_{i, \text{p}}} \\
  &+ \underbrace{m^{\text{solid}}_{i} \frac{1}{\beta_i}
  \sum_j m_j \frac{4 \nu}{(\rho_i + \rho_j)}
  \frac{\ten{r}_{ij} \cdot \nabla W(r_{ij}, h_{ij})}
  {(|\ten{r}_{ij}|^{2} + \eta)} \ten{u}_{ij}}_{\ten{f}_{i, \text{visc}}}
  \end{split}
\end{equation}
where the summation index $j$ is over all the fluid particles in the
neighborhood of a solid particle indexed by $i$. We compute the coefficient of
pressure drag $c_{d, \text{pressure}}$ and skin-friction drag
$c_{d, \text{skin-friction}}$, and coefficient of lift $c_l$ due to pressure by,
\begin{equation}
  \label{eq:cdcl}
  c_{d, \text{pressure}} = \frac{\ten{f}_{\text{p}} \cdot \ten{e}_x}
  {\frac{1}{2} \rho_0 U^2_{\infty} L}, \quad
  c_{d, \text{skin-friction}} = \frac{\ten{f}_{\text{visc}} \cdot \ten{e}_x}
  {\frac{1}{2} \rho_0 U^2_{\infty} L}, \quad
  c_{l} = \frac{\ten{f}_{\text{p}} \cdot \ten{e}_y}
  {\frac{1}{2} \rho_0 U^2_{\infty} L},
\end{equation}
where $L$ is the characteristic length of the simulation, $U_{\infty}$ is the
free stream velocity, $\ten{f}_{\text{visc}} = \sum_j \ten{f}_{j, \text{visc}}$
and $\ten{f}_{\text{p}} = \sum_j \ten{f}_{j, \text{p}}$ is the sum over all the
dummy wall particles, and $\ten{e}_x$ and $\ten{e}_y$ are the unit vectors in
the $x$ and $y$ directions respectively.

\subsection{Time integration}%
\label{sec:integration}

We use Predict-Evaluate-Correct (PEC) integrator to integrate the position
$\ten{r}_i$, velocity $\ten{u}_i$, and pressure $p_i$. The integrator is as
follows: We first predict the properties at an intermediate time value
$n + \frac{1}{2}$,
%
\begin{align}
  \label{eq:int-pre}
  \ten{u}^{n + \frac{1}{2}}_i &= \ten{u}^{n}_i + \frac{\Delta t}{2}
  \frac{\tilde{\mathrm{d}} \ten{u}_i^{n}}{\mathrm{d} t}, \\
  \tilde{\ten{u}}^{n + \frac{1}{2}}_i &= \ten{u}^{n+\frac{1}{2}}_i
                                        + \frac{\delta \ten{r}^n_i}{\Delta t}, \\
  \ten{r}^{n + \frac{1}{2}}_i &= \ten{r}^{n}_i + \frac{\Delta t}{2}
  \tilde{\ten{u}}_i^{n+\frac{1}{2}}, \\
  p^{n + \frac{1}{2}}_i &= p^{n}_i + \frac{\Delta t}{2}
  \frac{\tilde{\mathrm{d}}\ten{p}_i^{n}}{\mathrm{d} t},
\end{align}
next we estimate the new accelerations at $n + \frac{1}{2}$. We then correct the
properties to get the corresponding values at the new time $n + 1$,
%
\begin{align}
  \label{eq:int-cor}
  \ten{u}^{n + 1}_i &= \ten{u}^{n}_i + \Delta t
  \frac{\tilde{\mathrm{d}} \ten{u}_i^{n + \frac{1}{2}}}{\mathrm{d} t}, \\
  \tilde{\ten{u}}^{n + 1}_i &= \ten{u}^{n+1}_i
                              + \frac{\delta \ten{r}^{n+\frac{1}{2}}_i}{\Delta t}, \\
  \ten{r}^{n + 1}_i &= \ten{r}^{n}_i + \Delta t
  \tilde{\ten{u}}_i^{n+1}, \\
  p^{n + 1}_i &= p^{n}_i + \Delta t
                \frac{\tilde{\mathrm{d}}\ten{p}_i^{n+\frac{1}{2}}}{\mathrm{d} t}.
\end{align}
%
The time-step is determined by the highest resolution used in the domain, and
the minimum of the CFL criterion and the viscous condition is taken:
%
\begin{align}
  \label{eq:timestep}
  \Delta t = \min\left(0.25 \left(\frac{h_{\min}}{U + c_s}\right),
  0.125 \left(\frac{h^2_{\min}}{\nu}\right)\right).
\end{align}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
