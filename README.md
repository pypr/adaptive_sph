# Adaptive Smoothed Particle Hydrodynamics for incompressible flow

This repository contains the code and manuscript for research done on
the Adaptive SPH scheme for incompressible flow.


## Installation

This requires pysph to be setup along with automan. See the
`requirements.txt`. To setup perform the following:

0. Setup a suitable Python distribution, using
   [edm](https://docs.enthought.com/edm/) or [conda](https://conda.io) or a
   [virtualenv](https://virtualenv.pypa.io/).

1. Clone this repository:
```
    $ git clone https://gitlab.com/pypr/adaptive_sph.git
```

2. Run the following from your Python environment:
```
    $ cd XXX
    $ pip install -r requirements.txt
```


## Generating the results

The paper and the results are all automated using the
[automan](https://automan.readthedocs.io) package which should already be
installed as part of the above installation. This will perform all the
required simulations (this can take a while) and also generate all the plots
for the manuscript.

To use the automation code, do the following::

    $ python automate.py
    # or
    $ ./automate.py

By default the simulation outputs are in the ``outputs`` directory and the
final plots for the paper are in ``manuscript/figures``.


## Building the paper

The manuscript is written with LaTeX and if you have that installed you may do
the following:

```
$ cd manuscript
$ pdflatex paper.tex
$ bibtex paper
$ pdflatex paper.tex
$ pdflatex paper.tex
```

## Supplementary files

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/pypr%2Fadaptive_sph/HEAD?labpath=binder%2Flaplacian_approx.ipynb)

We have made a Google Colab jupyter notebook to show the convergence of the
modified Monaghan-Gingold Laplacian operator. The notebook can be viewed here on
Google Colab:
http://colab.research.google.com/drive/1caEJoFU99fM3gq7vqb7_5McCHfBAzAnJ. You are
free to edit this yourself and change anything you wish to and re-run the
computations. The same notebook is also in the current repository in the
``binder/`` directory which can be explored, using the binder interface,
from the link:
https://mybinder.org/v2/gl/pypr%2Fadaptive_sph/HEAD?labpath=binder%2Flaplacian_approx.ipynb.
